angular.module( 'dashboard.components')
.directive('labelComponent', ['queryService','conditionService', function(queryService, conditionService){

        return {
            restrict:'E',
            replace:true,
            templateUrl: '/templates/view/components/label.html',
            controller: function($scope){

                function evalCondition(){
                    if(!$scope.component.conditions) return;

                    angular.forEach($scope.component.conditions, function(condition){

                        if(!condition) return;
                        if(!condition._then_value) return;

                        var conditionString = conditionService.parseCondition(condition);
                        var templateString = _.template(conditionString);
                        var evalScript = templateString({
                            "_if":'$scope.component.display_value',
                            "_then":"$scope.component.css_style+=';background:"+condition._then_value+"'"
                        });
                        var fun = new Function('$scope', evalScript);
                        //eval(evalScript); /**kkk*/
                        fun($scope);
                    })
                }
            	$scope.$emit('component-register', $scope.component);
                
                $scope.component.display_value = "";
                if($scope.component.data == null){
                	setTimeout(function(){
                        $scope.$emit('component-ready', $scope.component);
                    },10)
                    return;
                }

                $scope.update = function(data){
                    $scope.component.display_value = data.data.join();
                    evalCondition();
                    setTimeout(function () {
                        $scope.$emit('component-ready', $scope.component);
                    }, 0)
                }


                if($scope.component.data.type=='data'){
                    if($scope.component.data.expr!=null && $scope.component.data.datasource!=null){
                        queryService.xhr_fetch_expr_both($scope.component.data.expr, $scope.component.data.datasource, $scope.component.data.interval, $scope.update);
                    }
                    else{
                    	setTimeout(function(){
                            $scope.$emit('component-ready', $scope.component);
                        },10)
                    }
                }
                else{
                    $scope.component.display_value = $scope.component.data.expr;
                    evalCondition();
                    setTimeout(function(){
                        $scope.$emit('component-ready', $scope.component);
                    },0)
                }
            },
            link: function(scope, element, attrs){
            }
        };
}])
