angular.module( 'dashboard.components')
    .directive('gaugeComponent', ['$rootScope','queryService','$timeout','configuration',function($rootScope,queryService, $timeout,configuration){
        var gauge_chart;
        var verify_data = function(component){
            return true;
        }



        var gaugeOptions = {

            chart: {
                type: 'solidgauge',
                backgroundColor: "rgba(255, 255, 255, 0)",
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false
            },

            pane: {
                "center": [
                    "50%",
                    "85%"
                ],
                "size": "140%",
                "startAngle": "-90",
                "endAngle": "90",
                "background": {
                    "backgroundColor": "#EEE",
                    "innerRadius": "60%",
                    "outerRadius": "100%",
                    "shape": "arc"
                }
            },
            "tooltip": {
                "enabled": true
            },
            credits: {
                enabled: false
            },
            // the value axis
            "yAxis": {
                "stops": [
                    [
                        0.1,
                        "#55BF3B"
                    ],
                    [
                        0.5,
                        "#DDDF0D"
                    ],
                    [
                        0.9,
                        "#DF5353"
                    ]
                ],
                tickAmount:2,
                tickPositioner: function() {
                    return [this.min, this.max];
                },
                "min": 0,
                "max": 100,
                "lineWidth": 0,
                "minorTickInterval": null,
                "tickPixelInterval": 400,
                "tickWidth": 0,
                "title": {
                    "y": -70
                },
                "labels": {
                    "y": 16
                }
            },
            series: [{
                name: 'Speed',
                data: [50]
            }]

        }


        var draw_chart = function(container, chart_data){
            gaugeOptions.title={text:chart_data.key};
            if(chart_data.background){
                gaugeOptions.chart.backgroundColor=chart_data.background
            }
            gaugeOptions.series[0].name = chart_data.key;
            if(isNaN(chart_data.value)){
                chart_data.value = 0;
            }
            gaugeOptions.series[0].data = [parseFloat(chart_data.value)];
            gaugeOptions.yAxis.min = isNaN(chart_data.min)?0:parseFloat(chart_data.min);
            gaugeOptions.yAxis.max = isNaN(chart_data.max)?100:parseFloat(chart_data.max);
            gaugeOptions.yAxis.title = {'text': chart_data.key};
            $timeout(function(){
                gauge_chart=container.find(".chart-container").highcharts(gaugeOptions).highcharts();
                gauge_chart.reflow()
            },10)
            return ;
        }


        return {
            restrict:'AE',
            replace:true,
            templateUrl:'/templates/view/components/gauge.html',
            controller: function($scope,$element){
                $scope.$emit('component-register', $scope.component);
                if(!verify_data($scope.component)){
                    draw_chart($element, null,$scope.component.options);
                    setTimeout(function(){
                        $scope.$emit('component-ready', $scope.component);
                    },10)
                    return;
                }


                var chart_data = {
                    key:$scope.component.options.title,
                    min:$scope.component.options.min,
                    max:$scope.component.options.max,
                    value:$scope.component.data.expr,
                    background: $scope.component.options.background
                };
                if($scope.component.data.type=='data'){
                    if($scope.component.data.expr!=null && $scope.component.data.datasource!=null){

                        queryService.xhr_fetch_expr_both($scope.component.data.expr, $scope.component.data.datasource, $scope.component.data.interval, function(data){
                            chart_data.value = data.data.join();
                            draw_chart($element, chart_data);
                            $scope.$emit('component-ready', $scope.component);
                        })
                    }

                }
                else{
                    chart_data.value = $scope.component.data.expr;
                    draw_chart($element, chart_data);
                    $scope.$emit('component-ready', $scope.component);
                }


                $scope.$on('component-reflow', function(){
                    gauge_chart.reflow();
                })
            },
            link: function(scope, element, attrs){
            }
        };
    }])
