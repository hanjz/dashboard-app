var gulp = require('gulp');
var gulp = require("gulp");
var concat = require("gulp-concat");
var uglify = require('gulp-uglify');
var wrap = require("gulp-wrap");
var less = require("gulp-less");
var rename = require('gulp-rename');
var cssmin = require('gulp-minify-css');

var js_root_path = 'src/main/resources/static/js/';
var css_root_path = 'src/main/resources/static/css/';
gulp.task('edit_scripts', function(){
	gulp.src(js_root_path+'app/edit/**/*.js')
	.pipe(concat('app.js'))
	.pipe(gulp.dest(js_root_path+'/dist/edit'))
	.pipe(rename('app.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest(js_root_path+'/dist/edit'))
});

gulp.task('view_scripts', function(){
	gulp.src(js_root_path+'app/view/**/*.js')
	.pipe(concat('app.js'))
	.pipe(gulp.dest(js_root_path+'/dist/view'))
	.pipe(rename('app.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest(js_root_path+'/dist/view'))
})

gulp.task('edit_css', function(){
	gulp.src([css_root_path+'common/*.less',css_root_path+'app/edit/edit.less'])
	.pipe(less())
	.pipe(concat('edit.css'))
	.pipe(cssmin())
	.pipe(rename('edit.min.css'))
	.pipe(gulp.dest(css_root_path+'/dist/edit'));
});

gulp.task('view_css', function(){
	gulp.src([css_root_path+'common/*.less', css_root_path+'app/view/view.less'])
	.pipe(less())
	.pipe(concat('view.css'))
	.pipe(cssmin())
	.pipe(rename('view.min.css'))
	.pipe(gulp.dest(css_root_path+'/dist/view'));
})

gulp.task('edit', ['edit_scripts', 'edit_css'], function(){
	gulp.watch(js_root_path+'app/edit/**/*.js', function(){
		gulp.run('edit_scripts');
	})
	gulp.watch([css_root_path+'common/*.less',css_root_path+'app/edit/edit.less'], function() {
		gulp.run('edit_css');
	})
});

gulp.task('view', ['view_scripts', 'view_css'], function(){
	gulp.watch(js_root_path+'app/view/**/*.js', function(){
		gulp.run('view_scripts');
	})
	gulp.watch([css_root_path+'common/*.less',css_root_path+'app/view/view.less'], function(){
		gulp.run('view_css');
	})
	
})

gulp.task('all', ['view', 'edit'], function(){
	
})
