#!/bin/sh
curl -X POST localhost:9999/shutdown
mvn clean package
nohup java -jar target/dashboard-app-0.0.1-SNAPSHOT.jar > /dev/null 2>&1 &
