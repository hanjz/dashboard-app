angular.module('dashboard')
    .controller('singleController',
        [
            '$rootScope',
            '$scope',
            'queryService',
            function(
                $rootScope,
                $scope,
                queryService
            ){
                $scope.id = angular.element("#widget_id").val();
                $rootScope.widget_id = $scope.id;
                var is_snapshot = angular.element('#is_snapshot').val();

                if(is_snapshot) {
                    $rootScope.is_snapshot = true;
                }
                else{$rootScope.is_snapshot = false}

                queryService.xhr_widget($scope.id).success(function(data){
                    $scope.component = data;
                    if(data.components!=null){
                        $scope.component.components = JSON.parse(data.components);
                    }
                })
            }])
