angular.module("app.services.scope.fullscreen",[],angular.noop)
.factory("fullscreenOperationService",['$interval',function($interval){
    return {
       init_operation: function($scope){
            $scope.full_screen_mode = function(){
                if($scope.full_screen == false){
                    angular.element('body').addClass('fullscreen')
                }
                else{
                    angular.element('body').removeClass('fullscreen')
                }
                $scope.full_screen = !$scope.full_screen;
                $scope.layout_panel_visible = false;
                if($scope.carouseling!=null){
                    $interval.cancel($scope.carouseling)
                    $scope.carouseling = null;
                }
                 angular.element(window).resize();
            }

            $scope.toggle_carousel = function(){
                if($scope.full_screen){
                    if($scope.carouseling == null){
                        $scope.carouseling = $interval(function(){
                            $scope.tab_active_index  = ($scope.tab_active_index+1)%$scope.dashboards.length
                        }, 3000);
                    }
                    else{
                        $interval.cancel($scope.carouseling);
                        $scope.carouseling = null;
                    }
                }
            }
       }    
    };
}]);