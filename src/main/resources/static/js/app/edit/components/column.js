angular.module('widget.components')
.directive('columnComponent', ["$rootScope",'queryService',function($rootScope,queryService){
        return {
            restrict:'AE',
            replace:true,
            templateUrl: '/templates/edit/components/column.html',
            controller: function($scope){
                $scope.toggle_select = function($event){
                    $event.stopPropagation() 
                    if($rootScope.selected_component != null && $rootScope.selected_component.ctrl_id == $scope.component.ctrl_id){
                        $rootScope.selected_component = null;
                    }
                    else{
                        $rootScope.selected_component = $scope.component;
                    }
                }


                $scope.component.extra.update = function(){
                    $scope.component.display_value = "";
                    if($scope.component.data == null){
                        return;
                    }
                    if($scope.component.data.type=='data'){
                        if($scope.component.data.expr!=null && $scope.component.data.datasource!=null){
                            queryService.xhr_fetch_expr($scope.component.data.expr, $scope.component.data.datasource).success(function(data){
                                $scope.component.display_value = data.data;
                            })
                        }
                    }
                    else{
                        $scope.component.display_value = $scope.component.data.expr;
                    }
                }

                $scope.component.extra.update();
            },
            link: function(scope, element, attrs){
                //scope.table = {}
                scope.data = [];
                scope.component.extra.change_data = function(data){
                    
                    var row_option = scope.component.extra.table.row_option;
                    var row_num = scope.component.extra.table.row_num;


                    if(angular.isArray(data)){
                        if(row_option == 2){
                            scope.data = data;
                        }
                        else{
                            if(row_option == 0){
                                //show at most;
                                if(data.length>row_num){
                                    scope.data = data.slice(0, row_num);
                                }
                                else{
                                    scope.data = data;
                                } 
                            }
                            else{
                                if(data.length>row_num){
                                    scope.data = data.slice(0, row_num);
                                }
                                else{
                                    scope.data = [];
                                    for(var i = 0 ;i < row_num ; i++) {
                                        if(i<data.length){
                                            scope.data[i] = data[i]
                                        }
                                        else{
                                            scope.data[i] = "";
                                        }
                                        
                                    }
                                }
                            }
                        }
                    }
                    else{
                        scope.data = [data];
                        if(row_option == 1) {
                            for(var i = 1 ;i<row_num ;i ++){
                                scope.data[i] = ""
                            }
                        }
                    }
                }

            }
        };
}])
.directive('columnProperties', ['queryService',function(queryService){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/column.html',
            replace:true,
            controller: function($scope){
                
            },
            link: function(scope, element, attrs){

            },
        };
}])
.filter('chop', function(){
    return function(data, row_option, row_num){
        if(data==null){
            return data;
        }
        
       if(angular.isArray(data)){
            if(row_option == 2){
                return data;
            }
            else{
                if(row_option == 0){
                    //show at most;
                    if(data.length>row_num){
                        return data.slice(0, row_num);
                    }
                    else{
                        return data
                    } 
                }
                else{
                    if(data.length>row_num){
                        return data.slice(0, row_num);
                    }
                    else{
                        for(var i = 0 ;i < row_num ; i++) {
                            if(i>=data.length){
                                data[i] = "";
                            }
                        }
                        return data;
                    }
                }
            }
        }
        else{
            data = [data];
            if(row_option == 1) {
                for(var i = 1 ;i<row_num ;i ++){
                    data[i] = ""
                }
            }
            return data;
        }
    }
})