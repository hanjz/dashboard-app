angular.module( 'widget.components')
.directive('labelComponent', ["$rootScope","queryService","conditionService", function($rootScope,queryService,conditionService){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/components/label.html',
            replace:true,
            controller: function($scope){


                $scope.component.extra.update = function(_data){
                    if(_data){
                        $scope.component.display_value = _data
                        return;
                    }
                    $scope.component.display_value = "";
                    if($scope.component.data == null){
                        return;
                    }
                    if($scope.component.data.type=='data'){
                        if($scope.component.data.expr!=null && $scope.component.data.datasource!=null){
                            queryService.xhr_fetch_expr($scope.component.data.expr, $scope.component.data.datasource).success(function(data){
                                $scope.component.display_value = data.data.join();

                                
                            })
                        }
                    }
                    else{
                        $scope.component.display_value = $scope.component.data.expr;
                    }
                }

                $scope.component.extra.update()
            },
            link: function(scope, element, attrs){

                //scope.component.display_value =
            }
        };
}])
.directive('labelProperties', [function(){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/label.html',
            replace:true,
            controller: function($scope){
               $scope.change_wrap = function(){

                    if($scope.css['white-space']=='normal') {
                        $scope.css['white-space'] = 'nowrap';
                        $scope.css['word-break'] = 'normal';
                    }
                    else{
                        $scope.css['white-space'] = 'normal';
                        $scope.css['word-break'] = 'break-all';
                    }
                    //render_style();
               }
            },
            link: function(scope, element, attrs){
                scope.css = {}
                if(scope.style!=null){
                    var style_array = scope.style.split(";");
                    for(var i = 0 ;i<style_array.length;i++){
                        var css_clip = style_array[i];
                        if(css_clip!=null && css_clip!=""){
                            if(css_clip.split(":").length==2){
                                scope.css[css_clip.split(":")[0]] = css_clip.split(":")[1];
                            }
                        }
                    }
                }
            },
        };
}])
