package com.iota.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by guoxu.wu on 16/8/5.
 */
@Getter
@Setter
public class DataObject {
    protected String type;
    protected Metadata meta;
    private List data;
}
