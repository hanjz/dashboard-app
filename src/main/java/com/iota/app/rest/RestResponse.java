package com.iota.app.rest;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by cqiu on 16/08/02.
 */

@Setter
@Getter
public class RestResponse<T> {
	private int rescode = RestResponseCode.OK;
	private String message = "";
	private T result;
}
