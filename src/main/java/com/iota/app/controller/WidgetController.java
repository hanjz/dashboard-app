package com.iota.app.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.h2.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.iota.app.data.AccountDao;
import com.iota.app.model.Account;
import com.iota.app.model.Widget;
import com.iota.app.service.ImageCaptureService;
import com.iota.app.service.ValueService;
import com.iota.app.service.WidgetService;


@Controller
@RequestMapping(value="/widget")
public class WidgetController {
	@Resource
	private WidgetService widgetService;
	@Resource
	private ImageCaptureService imageCaptureService;
	@Resource
	private ValueService valueService;
    @Autowired
    private AccountDao accountRepository;
	
    /**
       * 作者：张明育  
       * 日期：2017年1月22日 
       * 功能：跳转到home.html页面
     */
    @RequestMapping(value="/home",method=RequestMethod.GET)
	public String home(ModelMap map){
		map.put("imgServ", valueService.getImageServerUrl());  //在页面上显示图片
		return "widget/home";                                  //登入成功后跳转到widget/home页面
	}
    
    
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public String index(){
		return "widget/index";
	}
	
	/**
	   * 作者：张明育  
	   * 日期：2017年1月22日 
	   * 功能：跳转到edit页面(新建widget)
	 */
	@RequestMapping(value="/new", method=RequestMethod.GET)
	public String _new(ModelMap map, @RequestParam("is_canvas") boolean isCanvas){
		Widget widget = new Widget();
		widget.setIsCanvas(isCanvas);
		widgetService.save(widget);
		map.put("id", widget.getId());
		return "widget/edit";
	}
   
	/**
	   * 作者：张明育  
	   * 日期：2017年1月22日 
	   * 功能：根据iD全屏查看widget详情
	 */
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public String widgetDetail(ModelMap map, @PathVariable String id){
		Widget widget = widgetService.findById(id);   //根据id获取到widget
		map.put("id", id);
		map.put("widget", widget);
		return "widget/detail";
	}

	/**
	 * 作者：张明育  
	 * 日期：2017年1月22日 
	 * 功能：根据id设置widget页面的宽高和名称
	 */
	@RequestMapping(value="/html/{id}", method= RequestMethod.GET)
	public String widgetHtml(ModelMap map, @PathVariable String id){
		map.put("id", id);
		map.put("isSnapshot", true);
		return "widget/widget";
	}
	

	@RequestMapping(value="/{id}/image", method=RequestMethod.GET)
	public void widgetImage(HttpServletRequest request, HttpServletResponse response, @PathVariable String id) throws IOException {
		response.setContentType("image/png");
		OutputStream os = response.getOutputStream();
		imageCaptureService.getImage(id, os);
		os.flush();
		os.close();
	}

	@RequestMapping(value="/{id}/image2",method=RequestMethod.GET)
	public void widgetImage2(HttpServletRequest request, HttpServletResponse response, @PathVariable String id) throws IOException{
		response.setContentType("image/png");
		byte[] imgData=null;
		try {
			imgData = imageCaptureService.genImage(request.getServerName(), request.getServerPort(), id);
		} catch (Exception e) {
			System.out.println("imageCaptureService.genImageERROR....................");
			e.printStackTrace();
		}
		ByteArrayInputStream bais= new ByteArrayInputStream(imgData);
		OutputStream os = response.getOutputStream();
		IOUtils.copy(bais, os);
		os.flush();
		os.close();
	}

	
	/**
	 * 作者：张明育  
	 * 日期：2017年1月22日 
	 * 功能:点击修改按钮，跳转到edit.html页面(根据widget_id修改)
	 */
	@RequestMapping(value="/{widget_id}/edit", method=RequestMethod.GET)
	public String edit(ModelMap map, @PathVariable(value="widget_id") String widgetId){
		map.put("id", widgetId);
		return "widget/edit";
	}

	/**
	  * 作者：张明育  
	  *  日期：2017年1月22日 
	  *  功能:轮播widget
	 */
	@RequestMapping(value="/scroll", method=RequestMethod.GET)
	public String scroll(){
		return "widget/scroll";
	}
	
	/**
	 * 作者：张明育  
	  *  日期：2017年1月22日 
	  *  功能:根据widget的id进行删除home页面中的单个widget
	 */
	@RequestMapping(value="/{widget_id}/delete", method=RequestMethod.GET)
	public String delete(ModelMap map , @PathVariable(value="widget_id")String widgetId){
		widgetService.delete(widgetId);
		return "redirect:/widget/home";
	}
	
	/**
	 * 作者：张明育  
	  *  日期：2017年1月22日 
	  *  功能:跳转到sample.html页面
	 */
	@RequestMapping(value="/sample/{type}")
	public String imageSample(ModelMap map, @PathVariable(value="type") String type){
		map.put("type", type);
		return "widget/sample";
	}
	
	/**
	  *  作者：张明育  
	  *  日期：2017年1月22日 
	  *  功能:跳转到baidu_map.html页面
	 */
	@RequestMapping(value="/baidu_map")
	public String baidu_map(){
		return "widget/baidu_map";
	}
	
	/**
	 * 作者：张明育  
	  *  日期：2017年1月22日 
	  *  功能:跳转到baidu_map_all.html页面
	 */
	@RequestMapping(value="/baidu_map_all")
	public String baidu_map_all(){
		return "widget/baidu_map_all";
	}
	@RequestMapping(value="/addData", method=RequestMethod.GET)
	public String addData(){
			Widget widget = new Widget();
			widget.setName("w1");
			widget.setBackground("");
			widget.setIndex(0);
			widget.setIsCanvas(false);
			widget.setIsValid(false);
			widget.setComponents("[]");
			widgetService.save(widget);

			Widget widget2 = new Widget();
			widget2.setName("w2");
			widget2.setBackground("");
			widget2.setIndex(1);
			widget2.setIsCanvas(true);
			widget2.setIsValid(false);
			widget2.setComponents("[]");
			widgetService.save(widget2);
		return "widget/test";
	}


}
