package com.iota.app.dto.builder;

import com.iota.app.dto.DataObject;
import com.iota.app.dto.Metadata;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by guoxu.wu on 16/8/6.
 */
public abstract class DataObjectBuilder extends DataObject {
	/**
	 * 添加csv和db的数据
	 */
	private static Map<String, Class> dataType = new HashMap<String , Class>();
    static {
        dataType.put("CSV", CSVDataObjectBuilder.class);
        dataType.put("DB", DatabaseDataObjectBuilder.class);
        /*other data types register*/
    }

    public abstract DataObject buildDataObject(Map map) throws Exception;

    
    /**
     * 创建元数据
     * @param metadata
     * @param infoMap
     */
    public void buildMetadata(Metadata metadata, Map infoMap){
        metadata.setDataSourceName((String)infoMap.get("name"));
        metadata.setDataSourceDescription((String)infoMap.get("description"));
        metadata.setDataSourceUid(UUID.randomUUID().toString());
        metadata.setCreateTime(new Date());
        metadata.setUpdateTime(new Date());
    }
    public static DataObject build( Map infoMap) throws Exception{
        String type = (String)infoMap.get("type");
        DataObjectBuilder  dataObjectBuilder = (DataObjectBuilder)dataType.get(type).newInstance();
        DataObject object =  dataObjectBuilder.buildDataObject(infoMap);
        object.setType(type);
        return object;
    }
}
