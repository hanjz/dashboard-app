package com.iota.app.vo;


import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataSourceVo implements Serializable {
	private Long id;
	private String uid;
	private String type;
	private String description;
	private Date createdTime; 
	private String name;
	private String datamodel;
}
