angular.module("widget.services",[
    'ngSanitize'
])
angular.module('widget.components',[
    'colorpicker.module',
    'ui.bootstrap',
    'widget.services',
    'ngFileUpload'
])

/*angular.module("services", 
    [
        'app.services.query',
        'app.services.render',
        'app.services.scope.init',
        'app.services.scope.controls',
        'app.services.uuid',
        'app.services.util',
        'ngSanitize',
    ], 
angular.noop);

angular.module("components", 
    [ 
        'colorpicker.module',
        'app.components.utilities',
        'app.components.properties.common',
        'app.components.tab',
        'app.components.resizer',
        'app.components.editing_component',
        'app.components.tree',
        'app.components.widget.label',
        'app.components.widget.grid',
        'app.components.widget.valuepair',
        'app.components.widget.separator',
        'app.components.widget.html',
        'app.components.widget.frame',
        'app.components.widget.image',
        'app.components.ctrl_draggable',
        'app.components.ctrl_droppable'
    ], angular.noop);
*/
var post_wrapper = function($httpProvider){
    // Use x-www-form-urlencoded Content-Type
      $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

      /**
       * The workhorse; converts an object to x-www-form-urlencoded serialization.
       * @param {Object} obj
       * @return {String}
       */ 
      var param = function(obj) {
        var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

        for(name in obj) {
          value = obj[name];

          if(value instanceof Array) {
            for(i=0; i<value.length; ++i) {
              subValue = value[i];
              fullSubName = name + '[' + i + ']';
              innerObj = {};
              innerObj[fullSubName] = subValue;
              query += param(innerObj) + '&';
            }
          }
          else if(value instanceof Object) {
            for(subName in value) {
              subValue = value[subName];
              fullSubName = name + '[' + subName + ']';
              innerObj = {};
              innerObj[fullSubName] = subValue;
              query += param(innerObj) + '&';
            }
          }
          else if(value !== undefined && value !== null)
            query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }

        return query.length ? query.substr(0, query.length - 1) : query;
      };

      // Override $http service's default transformRequest
      $httpProvider.defaults.transformRequest = [function(data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
      }];   
};
angular.module('widget.components')
.directive('chartlabelComponent', ['queryService',function(queryService){
        return {
            restrict:'E',
            controller: function($scope){
                /*$scope.component.extra.update = function(){
                     $scope.component.extra.chart.extra.update()
                } */
            },
            link: function(scope, element, attrs){
                scope.component.extra.update = function(){
                     //scope.component.extra.chart.extra.update()
                     scope.component.extra.chart.extra.update();
                } 
            }
        };
}])
.directive('chartlabelProperties', ['queryService',function(queryService){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/chart-label.html',
            replace:true,
            
            controller: function($scope){
                 
            },
            link: function(scope, element, attrs){

            },
        };
}])
angular.module('widget.components')
.directive('chartvalueComponent', ['queryService',function(queryService){
        return {
            restrict:'E',
            controller: function($scope){
            },
            link: function(scope, element, attrs){
               scope.component.extra.update = function(){
                     //scope.component.extra.chart.extra.update()
                     scope.component.extra.chart.extra.update();
                }  
            }
        };
}])
.directive('chartvalueProperties', ['queryService',function(queryService){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/chart-value.html',
            replace:true,
            controller: function($scope){
                
            },
            link: function(scope, element, attrs){

            },
        };
}])
angular.module('widget.components')
.directive('chartseriesComponent', [function(){
        return {
            restrict:'E',
            templateUrl: '/templates/edit/components/chart.html',
            controller: function($scope){

            },
            link: function(scope, element, attrs){
                var chart_data = scope.component;
                var axes_data =  chart_data.axes;
                var series_data = chart_data.series;

                var type_map = {
                    'bar':'column',
                    'line':'line',
                    'area':'area'
                }
                var x_axis_data = axes_data.filter(function(a){return a.axis_type==0})
                var y_axis_data = axes_data.filter(function(a){return a.axis_type==1})
                if(x_axis_data.length == 0 || y_axis_data.length==0) {
                    return ;
                }

                var x_axis_labels = null;
                try{
                    x_axis_labels = eval(x_axis_data[0].data);
                }   
                catch(error){
                    console.error('x axis data format error : '+ x_axis_data[0])
                    return;
                }   

                var chart_elem = element.find('.chart-container');
                var option = {
                    chart: {
                        renderTo: chart_elem[0],
                         events: {
                            load: function() {
                                var c = this;
                                console.log('load')
                                setTimeout(function() {
                                    c.reflow();
                                }, 123)
                            }
                        }
                    },
                    title: {
                        text: chart_data.name
                    },
                    legend: {
                        enabled: chart_data.show_legend||false
                    }, 
                    xAxis: {
                        categories: eval(x_axis_data[0].data)
                    },
                    yAxis: {
                        title: {
                            text: y_axis_data[0].title
                        },
                        lineWidth: 1,
                        lineColor: "#d4d4d4"
                    },
                    credits: {
                        "enabled": false
                    },
                    colors: [
                            "#518a86",
                            "#3f6973",
                            "#2e8780",
                            "#44989f",
                            
                        ],
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: series_data.map(function(obj){
                        var y_data = null;
                        try{
                            y_data = eval(obj.data)
                        }
                        catch(error){
                            console.error('y data error: '+ obj.data)
                        }
                        return {
                            name: obj.series_label,
                            data: y_data,
                            type: type_map[obj.chart_type] == null? obj.chart_type: type_map[obj.chart_type]
                        }
                    })
                    
                }

                var chart = new Highcharts.Chart(option)
                if(window.charts ==null){
                    window.charts = []
                }
                window.charts.push(chart)
                angular.element(window).resize(function(){
                    chart.reflow()
                })
                scope.$watch('component.widget.sizeX', function(x){
                    console.log('sizex')
                    setTimeout(function() {
                        chart.reflow();
                        }, 200)
                })

                scope.$watch('component.dashboard.opts.columns', function(x){
                    console.log('watch')


                    setTimeout(function() {
                        chart.reflow();
                        }, 200)

                }) 

            }
        };
}])
angular.module('widget.components')
.directive('columnComponent', ["$rootScope",'queryService',function($rootScope,queryService){
        return {
            restrict:'AE',
            replace:true,
            templateUrl: '/templates/edit/components/column.html',
            controller: function($scope){
                $scope.toggle_select = function($event){
                    $event.stopPropagation() 
                    if($rootScope.selected_component != null && $rootScope.selected_component.ctrl_id == $scope.component.ctrl_id){
                        $rootScope.selected_component = null;
                    }
                    else{
                        $rootScope.selected_component = $scope.component;
                    }
                }


                $scope.component.extra.update = function(){
                    $scope.component.display_value = "";
                    if($scope.component.data == null){
                        return;
                    }
                    if($scope.component.data.type=='data'){
                        if($scope.component.data.expr!=null && $scope.component.data.datasource!=null){
                            queryService.xhr_fetch_expr($scope.component.data.expr, $scope.component.data.datasource).success(function(data){
                                $scope.component.display_value = data.data;
                            })
                        }
                    }
                    else{
                        $scope.component.display_value = $scope.component.data.expr;
                    }
                }

                $scope.component.extra.update();
            },
            link: function(scope, element, attrs){
                //scope.table = {}
                scope.data = [];
                scope.component.extra.change_data = function(data){
                    
                    var row_option = scope.component.extra.table.row_option;
                    var row_num = scope.component.extra.table.row_num;


                    if(angular.isArray(data)){
                        if(row_option == 2){
                            scope.data = data;
                        }
                        else{
                            if(row_option == 0){
                                //show at most;
                                if(data.length>row_num){
                                    scope.data = data.slice(0, row_num);
                                }
                                else{
                                    scope.data = data;
                                } 
                            }
                            else{
                                if(data.length>row_num){
                                    scope.data = data.slice(0, row_num);
                                }
                                else{
                                    scope.data = [];
                                    for(var i = 0 ;i < row_num ; i++) {
                                        if(i<data.length){
                                            scope.data[i] = data[i]
                                        }
                                        else{
                                            scope.data[i] = "";
                                        }
                                        
                                    }
                                }
                            }
                        }
                    }
                    else{
                        scope.data = [data];
                        if(row_option == 1) {
                            for(var i = 1 ;i<row_num ;i ++){
                                scope.data[i] = ""
                            }
                        }
                    }
                }

            }
        };
}])
.directive('columnProperties', ['queryService',function(queryService){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/column.html',
            replace:true,
            controller: function($scope){
                
            },
            link: function(scope, element, attrs){

            },
        };
}])
.filter('chop', function(){
    return function(data, row_option, row_num){
        if(data==null){
            return data;
        }
        
       if(angular.isArray(data)){
            if(row_option == 2){
                return data;
            }
            else{
                if(row_option == 0){
                    //show at most;
                    if(data.length>row_num){
                        return data.slice(0, row_num);
                    }
                    else{
                        return data
                    } 
                }
                else{
                    if(data.length>row_num){
                        return data.slice(0, row_num);
                    }
                    else{
                        for(var i = 0 ;i < row_num ; i++) {
                            if(i>=data.length){
                                data[i] = "";
                            }
                        }
                        return data;
                    }
                }
            }
        }
        else{
            data = [data];
            if(row_option == 1) {
                for(var i = 1 ;i<row_num ;i ++){
                    data[i] = ""
                }
            }
            return data;
        }
    }
})
angular.module('widget.components')
.directive('legendProperties',[function(){
  return {
    restrict:'E',
      replace:true,
      templateUrl:'/templates/edit/properties/legend.html',
      scope:{
        component:"=",
      },
      controller: function($scope){

      },
      link: function(scope,element,attributes){
          scope.change_align = function(align_str){
              scope.component.options.legend_position = align_str;
              scope.component.extra.update();
          }
      }
  }
}])
.directive('alignProperties',[function(){
    return {
      restrict:'E',
      replace:true,
      templateUrl:'/templates/edit/properties/align.html',
      scope:{
        component:"=",
        title:"@"
      },
      controller: function($scope){

      },
      link: function(scope,element,attributes){
          scope.align_manager = new css_manager(scope.component.css_style);
          scope.change_align = function(obj){
              scope.align_manager = new css_manager(scope.component.css_style);
              scope.align_manager.apply_css(obj)
              scope.component.css_style = scope.align_manager.tostring();
          }
      }
    }
}])
    .directive('autoWidthProperties', [function(){
        return {
            restrict:'E',
            replace:true,
            templateUrl:'/templates/edit/properties/autowidth.html',
            scope:{
                component:'=',
                title: "@"
            },
            link: function(scope, element, attributes){
                scope.width_manager = new css_manager(scope.component.css_style);
                if(!scope.width_manager.style['display']){
                    scope.width_manager.style['display']  = 'block';
                }
                scope.change_width_auto = function(){
                    scope.width_manager = new css_manager(scope.component.css_style);
                    if(scope.width_manager.style['display']==='block') {
                        scope.width_manager.apply_css({'display':'inline-block'});
                    }
                    else{
                        scope.width_manager.apply_css({'display':'block'})
                    }
                    scope.component.css_style = scope.width_manager.tostring();
                }
            }
        }
    }])
    .directive('radiusProperties', [function(){
        return {
            restrict:'E',
            replace:true,
            templateUrl:'/templates/edit/properties/radius.html',
            scope:{
                component:'=',
                title: "@"
            },
            link: function(scope, element, attributes){
                scope.radius_manager = new css_manager(scope.component.css_style);
                if(!scope.radius_manager.style['border-radius']){
                   scope.radius_manager.style['border-radius'] = 'initial'
                }
                scope.change_radius = function(obj){
                    scope.radius_manager = new css_manager(scope.component.css_style);
                    scope.radius_manager.apply_css(obj);
                    scope.component.css_style = scope.radius_manager.tostring();
                }
            }
        }
    }])
.directive('fontStyleProperties',[function(){
    return {
      restrict:'E',
      replace:true,
      templateUrl:'/templates/edit/properties/fontstyle.html',
      scope:{
        component:"=",
        title:"@"
      },
      controller: function($scope){

      },
      link: function(scope,element,attributes){
          scope.font_manager = new css_manager(scope.component.css_style);
          scope.change_font_style = function(obj){
              scope.font_manager = new css_manager(scope.component.css_style);
              scope.font_manager.apply_css(obj)
              scope.component.css_style = scope.font_manager.tostring();
          }
      }
    }
}])
.directive('fontSizeProperties',[function(){
   return {
      restrict:'E',
      replace:true,
      templateUrl:'/templates/edit/properties/fontsize.html',
      scope:{
        component:"=",
        title:"@"
      },
      controller: function($scope){
      },
      link: function(scope,element,attributes){
          scope.font_manager = new css_manager(scope.component.css_style);
          scope.change_font_size = function(obj){
              scope.font_manager = new css_manager(scope.component.css_style);
              scope.font_manager.apply_css(obj)
              scope.component.css_style = scope.font_manager.tostring();
          }
      }
   }
}])
.directive('colorProperties',[function(){
   return {
      restrict:'E',
      replace:true,
      templateUrl:'/templates/edit/properties/color.html',
      scope:{
          option:"@",  //0,  前景， 1 背景,  2 both
          component:"=",
          title:"@",
          cssData:"@",
          style:"@",
          pureCss:"@",
          type:"@"
      },
      controller: function($scope){
      },
      link: function(scope,element,attributes){

          if(!scope.style){
              scope.style = "hex"
          }

          if(scope.cssData==null) {
              scope.color_manager = new css_manager(scope.component.css_style);
          }
          else{
              scope.color_manager =  new css_manager(scope.component[scope.cssData]||"");
          }

          scope.fcolor=scope.color_manager.style['color']||'#000'
          scope.bcolor=scope.color_manager.style['background']||'#fff'

          scope.$watch('fcolor', function(nv,ov){
                if(nv!=null && nv!=ov){
                  if(scope.cssData==null) {
                    scope.color_manager = new css_manager(scope.component.css_style);
                }
                else{
                    scope.color_manager =  new css_manager(scope.component[scope.cssData]||"");
                }
                scope.color_manager.apply_css({color:nv})
                if(scope.cssData==null) {
                  scope.component.css_style = scope.color_manager.tostring();
                }
                else{
                  scope.component[scope.cssData] = scope.color_manager.tostring();
                }
              }
          })
          scope.$watch('bcolor', function(nv,ov){
              if(nv!=null && nv!=ov){
                if(scope.cssData==null) {
                    scope.color_manager = new css_manager(scope.component.css_style);
                }
                else{
                    scope.color_manager =  new css_manager(scope.component[scope.cssData]||"");
                }
                scope.color_manager.apply_css({background:nv})
                if(scope.cssData==null) {
                  scope.component.css_style = scope.color_manager.tostring();
                }
                else{
                  scope.component[scope.cssData] = scope.color_manager.tostring();
                }
              }
          })
      }
   }
}])
    .directive('streamData', [function(){
        return {
            restrict:'E',
            template:'<div class="stream-data-control"><div class="stream-indicator"></div><div class="stream-data">{{}}</div></div>',
            scope:{
                data:'@'
            },
            controller: function($scope){
                    
            }
        }
    }])
.directive('borderProperties', [function() {
  return {
    restrict: 'E',
    templateUrl:'/templates/edit/properties/border.html',
    controller: function($scope){
      var refresh_style = function(key, value){
          if($scope.component.css_style == null){
            $scope.component.css_style = key+":"+value+";";
            return;
          }
          var style_array = $scope.component.css_style.split(";");
          var css = {}
          for(var i = 0 ;i<style_array.length;i++){
              var css_clip = style_array[i];
              if(css_clip!=null && css_clip!=""){
                  if(css_clip.split(":").length==2){
                      var _key = css_clip.split(":")[0];
                      var _value = css_clip.split(":")[1];
                      if(_key == key){
                        css[key] = value;
                      }
                      else{
                        css[_key] = _value;
                      }
                  }
              }
          }
          if(css[key] == null){
            css[key] = value;
          }
          var component_css = ""
          for(var k in css){
              var css_clip = ""+k+":"+css[k]+";";
              component_css+=css_clip
          }
         $scope.component.css_style = component_css;

      }
      $scope.$watch('css["border-width"]', function(newv, oldv){
          if(newv!=null && newv!=oldv){
            if(!isNaN(newv.replace("px",''))){

              if(newv>10){
                newv = 10;
              }
              refresh_style('border-width', newv)
            }
          }
      })

      $scope.$watch('css["border-style"]', function(newv, oldv){
        if(newv!=null&&newv!=oldv){
          refresh_style('border-style', newv)
        }
      })

      $scope.$watch('css["border-color"]', function(newv, oldv){
        if(newv!=null && newv!=oldv){
          refresh_style('border-color', newv);
        }
      })
    },
    link: function(scope, element, attributes) {
        scope.css = {}
            if(scope.component.css_style!=null){
                var style_array = scope.component.css_style.split(";");
                for(var i = 0 ;i<style_array.length;i++){
                    var css_clip = style_array[i];
                    if(css_clip!=null && css_clip!=""){
                        if(css_clip.split(":").length==2){
                            scope.css[css_clip.split(":")[0]] = css_clip.split(":")[1];
                        }
                    }
                }
        }
    },
    scope: {
        component:"="
    }
  }
}]);

angular.module('widget.components')
    .directive('conditionalFormatting',[function(){
        return {
            restrict:'E',
            replace:true,
            templateUrl:'/templates/edit/properties/conditional-formatting.html',
            scope:{
                component:"=",
            },
            controller: function($scope){
                $scope.selected_option = 0;
                $scope.action = 1;

                $scope.if_options = [
                    {
                        id:0,
                        name:'Equals To'
                    },
                    {
                        id:1,
                        name:'Greater Than'
                    },
                    {
                        id:2,
                        name:'Greater Or Equal To'
                    },
                    {
                        id:3,
                        name:'Less Than'
                    },
                    {
                        id:4,
                        name:'Less Or Equal To'
                    }


                ]
                $scope.actions = [
                    {
                        id:0,
                        name:'Background'
                    } ,
                    {
                        id:1,
                        name:'Blink'

                    },
                    {
                        id:2,
                        name:'Send Email'
                    }

                ]
                $scope.action = $scope.actions[0];
                $scope.change_event = function(){
                }

                if($scope.component.condition == null) $scope.component.condition= [];

                $scope.add_condition = function(){
                    $scope.component.conditions =  $scope.component.conditions || [];
                    $scope.component.conditions.push({
                        _ifs : [
                            {
                                _if:0,
                                _value:0
                            }
                        ],
                        _then: 0,
                        _then_value:'#fff'
                    })
                }
                $scope.delete_condition = function(index){
                    $scope.component.conditions.splice(index, 1);
                }
                $scope.add_if = function(condition){
                   condition._ifs.push({
                       _if:0,
                       _value:0
                   })
                }
                $scope.remove_if = function(condition, index){
                    if(condition._ifs && condition._ifs.length>0){
                        condition._ifs.splice(index, 1);
                    }
                }
            },
            link: function(scope,element,attributes){
                console.log(scope.component.conditions[0])
            }
        }
    }])
function css_manager(css_string){
    function parse_css_string(css_string){
        
        var style_obj = {};
        if(css_string==null){
            return style_obj;
        }
        var style_array = css_string.split(";")
        for(var i = 0 ;i<style_array.length;i++){
            var css_clip = style_array[i];
            if(css_clip!=null && css_clip!=""){
                var css_kv = css_clip.split(":");
                if(css_kv.length==2){
                    var _key = _.trim(css_kv[0]);
                    var _value = _.trim(css_kv[1]);
                    style_obj[_key] = _value;
                }
            }
        }

        return style_obj;
    }

    this.append_css = function(css_obj){
        if(typeof css_obj == 'string'){
            var append_obj = parse_css_string(css_obj)
            this.append_css(append_obj);
        }
        else if(typeof css_obj == 'object'){
            for(var key in css_obj){
                this.style[key] = css_obj[key];
            }
        }
    }
    this.apply_css = function(css_obj){
        if(typeof css_obj == 'string'){
            var append_obj = parse_css_string(css_obj)
            this.apply_css(append_obj);
        }
        else if(typeof css_obj == 'object'){
            for(var key in css_obj){
                //this.style[key] = css_obj[key];
                if(this.style[key]!=null){
                    if(this.style[key] == css_obj[key]){
                        delete this.style[key];
                    }
                    else{
                        this.style[key] = css_obj[key]
                    }
                }
                else{
                    this.style[key] = css_obj[key]
                }
            }
        }
    }
    this.remove_css = function(css_key){
        delete style_obj[css_key]
    }

    this.tostring = function(){
        var ret = "";
        for(var key in this.style){
            ret = ret+key+":"+this.style[key]+";"
        }

        return ret;
    }

    this.style = parse_css_string(css_string)
}
angular.module('widget.components')
.directive('ctrlDraggable', [function(){
    return {
       scope:{
            data:"="
       },
       link:function(scope, element, attrs){
            if(attrs.ctrlDraggable == 'false'){
                return;
            }
            element.draggable({
                    helper:'clone',
                    appendTo:'.middle',
                    handle : attrs.handle=='no'?false:'.handle',
                    start:function(event, ui){
                        event.stopPropagation();
                        $(this).data("current-dragging-data", scope.data)
                        console.log(scope.data)
                    },
                    drag: function(event, ui) {
                        event.stopPropagation();
                        var $droppable = $(this).data("current-droppable");
                        if ($droppable){
                            var dragging_top = ui.offset.top;
                            var dragging_left = ui.offset.left;
                            var is_break = false;
                            var children_length = 0 ;
                            
                            $droppable.find(">ul").children("li").each(function(index, obj){
                                children_length +=1;
                                if(!is_break){
                                    var ctrls_top = $(obj).offset().top;
                                    var ctrls_left = $(obj).offset().left;
                                    if(dragging_top < ctrls_top){
                                        is_break = true;
                                        if($droppable.find(".placeholder").length==0 || $droppable.find(".placeholder").index()!=index){
                                            $($droppable).find('.placeholder').remove()
                                            $(obj).before($("<li class='placeholder'></li>"))
                                        }
                                    }
                                    
                                }
                            })

                            if(is_break == false ){
                                if($droppable.find(".placeholder").length ==0 || $droppable.find(".placeholder").index() != $droppable.children().length-1){
                                    $droppable.find(".placeholder").remove();
                                    $droppable.find(">ul").append("<li class='placeholder'></li>");
                                }
                            }
                        }
                    }
                })
            element.disableSelection();
       }
    }
}])
.directive('canvasDraggable',[function(){
     return {
       scope:{
            data:"="
       },
       link:function(scope, element, attrs){
            if(attrs.ctrlDraggable == 'false'){
                return;
            }
            element.draggable({
                    helper:attrs.clone?'clone':'original',
                    appendTo:'.middle',
                    handle : attrs.handle=='no'?false:'.handle',
                    start:function(event, ui){
                       
                        event.stopPropagation();
                        $(this).data("current-dragging-data", scope.data)
                        $(this).data('is-new-data', attrs.clone)
                    },
                    drag: function(event, ui) {
                        event.stopPropagation();
                    },
                    stop: function(event, ui){
                        var parent_width = $(this).parent().width();
                        var parent_height = $(this).parent().height();

                        var current_top = $(this).position().top;
                        var current_left = $(this).position().left;

                        var current_top_percentage = current_top*100/parent_height + "%";
                        var current_left_percentage = current_left*100/parent_width + "%";
                        $(this).css('left',current_left_percentage);
                        $(this).css('top',current_top_percentage);
                        scope.data.left = current_left_percentage;
                        scope.data.top = current_top_percentage;
                    }
            })
            element.disableSelection();
       }
    }
}])
angular.module('widget.components')
.directive('ctrlDroppable', ['util', 'renderService',function(util,renderService){
    return {
        restrict:'A',
       link:function(scope, element, attrs){
            element.disableSelection();
            element.droppable({
                greedy:true,
                hoverClass:'drop-hover',
                drop: function(event, ui){
                    $('.replaceholder').removeClass("replaceholder");
                    var data = ui.draggable.data('current-dragging-data');
                    var cloned_data = angular.copy(data);
                    cloned_data.ctrl_id = util.uuid()
                    if(scope.widget.type == 'cell'){
                        scope.widget.components = [cloned_data]                 
                    }
                    else{
                        var placeholder = $(this).find(".placeholder");
                        var index = placeholder.index();
                         $(".placeholder").remove();
                        //data.ctrl_id = uuid.gen();
                        scope.widget.components.splice(index, 0, cloned_data); 

                    }
                   

                    //deep iteration for finding the dragging data 
                    renderService.remove(data.ctrl_id);

                    scope.$apply();
                },
                over: function(event, ui){
                    $(".replaceholder").removeClass('replaceholder')
                    if($(this).attr("root") == 'true'){
                        ui.draggable.data("current-droppable", $(this));
                    }
                    else{
                        $(this).addClass('replaceholder');
                    }
                },
                out: function(event, ui){
                    $('.replaceholder').removeClass("replaceholder")
                    $(".placeholder").remove();
                    ui.draggable.removeData("current-droppable");  
                }
            }) 
            
       }
    }
}])
.directive('canvasDroppable', ['util','renderService',function(util, renderService){
    return {
        restrict:'A',
        link:function(scope, element, attrs){
            element.droppable({
                greedy:true,
                drop: function(event, ui){
                    var data = ui.draggable.data('current-dragging-data');
                    var new_data = ui.draggable.data('is-new-data');
                    if(!new_data){
                        return;
                    }
                    var offset_left = ui.position.left;
                    var offset_top = ui.position.top;
                    var scroll_left = element.parent().scrollLeft();
                    var scroll_top = element.parent().scrollTop();
                   
                    var absolute_top = offset_top+scroll_top;
                    var absolute_left = offset_left + scroll_left;

                    var absolute_top_percentage = absolute_top*100/element.height()+"%";
                    var absolute_left_percentage = absolute_left*100/element.width()+"%";

                    var cloned_data = angular.copy(data);
                    cloned_data.ctrl_id = util.uuid()
                    cloned_data.top = absolute_top_percentage;
                    cloned_data.left = absolute_left_percentage

                    scope.widget.components.push(cloned_data);
                    scope.$apply();
                }
            });
        }
    }
}])
angular
		.module('widget.components')
		.directive(
				'ctrlList',
				[
						'queryService',
						function(queryService) {
							var ctrls_list = function(widget_type) { /*
																		 * widget
																		 * or
																		 * canvas
																		 */
								return [
										{
											name : 'Layout',
											icon : 'fa-th',
											canvas_exclude : true,
											data : {
												"type" : "grid",
												"name" : "new_grid",
												"rows" : 2,
												"cols" : 3,
												"cells" : [
														{
															"id" : 5,
															"parent" : 24,
															"type" : "cell",
															"name" : "new_cell",
															"last_modified" : "2015-12-23T09:44:52.027423Z",
															"width" : 1,
															"height" : 1,
															"coord_x" : 0,
															"coord_y" : 0,
															"padding" : 0,
															"components" : []
														},
														{
															"type" : "cell",
															"name" : "new_cell",
															"width" : 1,
															"height" : 1,
															"coord_x" : 0,
															"coord_y" : 1,
															"padding" : 0,
															"components" : []
														},
														{
															"type" : "cell",
															"name" : "new_cell",
															"width" : 1,
															"height" : 1,
															"coord_x" : 0,
															"coord_y" : 2,
															"padding" : 0,
															"components" : []
														},
														{
															"type" : "cell",
															"name" : "new_cell",
															"width" : 1,
															"height" : 1,
															"coord_x" : 1,
															"coord_y" : 0,
															"padding" : 0,
															"components" : []
														},
														{
															"type" : "cell",
															"name" : "new_cell",
															"width" : 1,
															"height" : 1,
															"coord_x" : 1,
															"coord_y" : 1,
															"padding" : 0,
															"components" : []
														},
														{
															"type" : "cell",
															"name" : "new_cell",
															"width" : 1,
															"height" : 1,
															"coord_x" : 1,
															"coord_y" : 2,
															"padding" : 0,
															"components" : []
														}, ]
											}
										},

										{
											name : 'Label',
											icon : 'fa-font',
											data : {
												css_style : "line-height:1.42857143;font-size:1.4em;",
												ctrl_id : "4d062f49-47d4-4556-af42-d362afcca92f",
												auto_width: false,
												name : "label",
												format : "text",
												data : {
													type : 'value',
													expr : 'Label'
												},
												display_value : "Label",
												type : "label",
												wrap : true
											}
										},
										{
											name : 'Value Pair',
											icon : 'fa-cc',
											data : {
												ctrl_id : "166f3432-edec-4f9b-86e5-32f970cf2d73",
												name : "valuepair",
												components:[
													{
														css_style : "line-height:1.42857143;font-size:1.4em;font-style:italic",
														ctrl_id : "4d062f49-47d4-4556-af423232-d362afcca92f",
														name : "key",
														format : "text",
														data : {
															type : 'value',
															expr : 'Key'
														},
														display_value : "Label",
														type : "label",
														wrap : true
													},
													{
														css_style : "line-height:1.42857143;font-size:1.8em;font-weight:bold",
														ctrl_id : "4d062f49-47d4-4556-af4223232-d362afcca92f",
														name : "value",
														format : "text",
														data : {
															type : 'value',
															expr : 'Value'
														},
														display_value : "Label",
														type : "label",
														wrap : true
													}
												],
												type : "value_pair"
											}
										},
										{
											name : 'Separator',
											icon : 'fa-scissors',
											canvas_exclude : true,
											data : {
												ctrl_id : "166f3432-edec-4f9b-86e5-32f97dcf2d73",
												name : "separator",
												line_color : "#aaa",
												line_weight : 1,
												orientation : 0,
												type : "separator"
											}
										},
										{
											name : 'HTMLTemplate',
											icon : 'fa-code',
											data : {
												ctrl_id : '166f34ad-edec-4f9b-86e5-32f97dcf2d73',
												html_string : '<h4>HTML Template</h4><p>Please edit the html code in the properties panel</p>',
												html_script : ' ',
												name : 'html_template',
												height : widget_type == 'canvas' ? '200px'
														: 'Auto',
												custom_height : 200,
												overflow : 0,
												type : 'html_template'
											}
										},
										{
											name : 'Inline Frame',
											icon : 'fa-at',
											data : {
												type : 'inline_frame',
												ctrl_id : '166f34ad-edec-4f9b-86e5-32f97dcf2dbp',
												name : 'inline_frame',
												url : '/widget/baidu_map',
												tmp_url : '/widget/sample/frame',
												height : widget_type == 'canvas' ? '200px'
														: 'Medium',
												custom_height : 200,
												overflow : 0,
												refresh : -1
											}
										},
										{
											name : 'Image',
											icon : 'fa-picture-o',
											data : {
												type : "image",
												name : "image_1",
												ctrl_id : "166f34ad-32ec-4f9b-86e5-32f97dcf2dbp",
												url : '',
												height : widget_type == 'canvas' ? '200px'
														: 'Auto',
												custom_height : 200,
												keep_ratio : false,
												resize : 0,
												custom_resize_width : 100,
												custom_resize_height : 100,
												alignment_horizontal : 0
											}
										},
										{
											name : 'Table',
											icon : 'fa-table',
											data : {
												type : "table",
												name : "table_1",
												ctrl_id : "166f34ad-32ec-4bcd-86e5-32f97dcf2dbp",
												show_header : true,
												first_row_header : false,
												total_cols : 4,
												row_option : 0,// 0 at most, 1
																// exactly, 2
																// all rows
												row_num : 8,
												components : [
														{
															title : 'untitled',
															name : 'column',
															type : 'column',
															ctrl_id : "166f34ad-32ec-4bcd-86e5-32f97dcf2db1",
														},
														{
															title : 'untitled',
															name : 'column',
															type : 'column',
															ctrl_id : "166f34ad-32ec-4bcd-86e5-32f97dcf2db2",
														},
														{
															title : 'untitled',
															name : 'column',
															type : 'column',
															ctrl_id : "166f34ad-32ec-4bcd-86e5-32f97dcf2db3",
														},
														{
															title : 'untitled',
															name : 'column',
															type : 'column',
															ctrl_id : "166f34ad-32ec-4bcd-86e5-32f97dcf2db4",
														} ]
											}
										},
										{
											name : 'Pie',
											icon : 'fa-pie-chart',
											data : {
												type : 'pie',
												name : "pie",
												ctrl_id : "166f34ad-32ec-4bcd-86e5-32c3d2df2dbp",
												height : widget_type == 'canvas' ? '200px'
														: 'Medium',
												custom_height : 200,
												options : {
													max_slice : 6,
													label : 2,
													show_legend : true,
													show_labels : true,
													show_percentage : false,
													legend_position : "t,r",
													legend_fcolor : '#000',
													legend_bcolor : '#fff',
													background:'#fff'
												},
												chart_value : {
													name : 'values',
													type : 'chart_value',
													ctrl_id : "166f34ad-32ec-4e322-86e5-32f97dcf2db4",
													data : {
														type : 'data',
														expr : ''
													}
												},
												chart_label : {
													name : 'labels',
													type : 'chart_label',
													ctrl_id : "165564ad-32ec-4bc9-86e5-32f97dcf2db4",
													data : {
														type : 'data',
														expr : ''
													}
												},
											}
										},
										{
											name : 'Gauge',
											icon : 'fa-tachometer',
											data : {
												type : 'gauge',
												name : "Gauge",
												ctrl_id : "166f34ad-32ec-4bcd-86e5-32c3d2df2djp",
												height : widget_type == 'canvas' ? '200px'
														: 'Medium',
												custom_height : 200,
												options : {
													title:'value',
													min:0,
													max:100
												},
												data : {
													type : 'value',
													expr : '50',
													datasource : ''
												}
											}
										},
										{
											name : 'Line/Bar',
											icon : 'fa-bar-chart',
											data : {
												type : 'series_chart',
												name : "line/bar",
												ctrl_id : "166f34ad-32qc-4bcd-86e5-32a392df2dbp",
												height : widget_type == 'canvas' ? '200px'
														: 'Medium',
												custom_height : 200,
												options : {
													show_legend : true,
													stack_bar : false,
													stack_line : false,
													stack_area : false,
													inverted : false
												},

												components : [
														{
															// x-axis
															type : 'x_axis',
															name : 'X Axis',
															ctrl_id : "166334ad-32qc-4bcd-86e5-32a392df2dbp",
															data : {
																type : 'data',
																expr : '',
																datasource : ''
															},
															time: {
																frequency:'S',
																duration:0
															},
															options : {
																show_axis : false,
																axis_title : '',
																show_label : true,
																rotation : 0,
																show_tick : true,
																sort : 0, // -1,desc
																			// 0,no
																			// sort
																			// 1
																			// ase
																show_grid : false,
																axis_color:'#000'
															}
														},
														
														{
															// series
															type : 'series_collection',
															ctrl_id : "166f34aa-32qc-4bcd-86e5-32a392df2dbp",
															name : "Series Set",
															components : [
																	{
																		type : 'series',
																		style:'line',
																		axis : 'untitled',
																		ctrl_id : "166134ad-32qc-4bcd-86e5-32a392df2dbp",
																		name : 'series 1',
																		options : {
																			show_value : false,
																			use_custom_color : false,
																			custom_color : '#f00',
																			line : {},
																			bar : {},
																		},
																		data : {
																			type : 'data',
																			expr : '',
																			datasource : ''
																		}
																	},

															]
														}, ]

											}
										}

								]

							}

							return {
								scope : {
									widget : '='
								},
								restrict : 'E',
								template : "<div class='ctrls-list'>"
										+ "<ul>"
										+ "<li ng-repeat='ctrl in ctrls'>"
										+ "<div ng-if='widget.isCanvas==false' class='ctrl-item' ctrl-draggable data='ctrl.data' handle='no' clone='true'>"
										+ "<i class='fa' ng-class='ctrl.icon'></i>"
										+ "</div>"
										+ "<div ng-if='widget.isCanvas==true' class='ctrl-item' canvas-draggable data='ctrl.data' handle='no' clone='true'>"
										+ "<i class='fa' ng-class='ctrl.icon'></i>"
										+ "</div>" + "</li>"
										+ "</ul>",
								controller : function($scope) {
									$scope.ctrls = [];
									$scope
											.$watch(
													'widget',
													function(nv, ov) {
														if (nv != null) {
															if (!$scope.widget.isCanvas) {
																$scope.ctrls = ctrls_list('widget');
															} else {
																angular
																		.forEach(
																				ctrls_list('canvas'),
																				function(
																						ctrl) {
																					if (!ctrl.canvas_exclude) {
																						$scope.ctrls
																								.push(ctrl)
																					}
																				})
															}
														}
													})

								},
								link : function(scope, element, attrs) {

								}
							};
						} ])
angular.module('widget.components')
.directive('ctrlSortable', [function(){
    return {
       link:function(scope, element, attrs){
            element.sortable({
               revert:true
            })

            element.disableSelection();
       }
    }
}])

angular.module('widget.components')
.directive('dataComponent', ['queryService','util','$rootScope','$uibModal',function(queryService, util, $rootScope,$uibModal){
        return {

            restrict:'AE',
            templateUrl: '/templates/edit/properties/data.html',
            replace:true,
            controller: function($scope,$element){
            },
            scope:{
                component:"=",
                defaultOption:"="
            },
            link: function(scope, element, attrs){
                scope.datasources = util.cache.get('datasources');
                var datasources_map = {};
                angular.forEach(scope.datasources, function(datasource){
                    datasources_map[datasource.uid] = datasource;
                })

                scope.toggle_datasource = function(uid){
                    scope.current_datasource_uid = uid;
                    if(datasources_map[scope.current_datasource_uid].data==null){
                        queryService.xhr_fetch_data(scope.current_datasource_uid).success(function(data){
                            datasources_map[scope.current_datasource_uid].data = data;

                            if(data.dataSource.type==='NIFI' && (!!data.dataSource.intervaled)===false ){
                                if(!scope.component.data){
                                   alert('component does not support Nifi!') ;
                                    return;
                                }
                                data.dataSource.intervaled = true;
                                scope.component.data.interval = 5000;
                                setInterval(function(){
                                    queryService.xhr_fetch_data(scope.current_datasource_uid).success(function(data){
                                        datasources_map[scope.current_datasource_uid].data = data;
                                    })
                                }, 5000);
                            }
                        })
                    }
                }

                scope.add_datasource = function(type){
                     scope.modal_instance = $uibModal.open({
                        animation: true,
                        template: "<div class='add-datasource-popup'>"+
                        "<h4>Select A Datasource : </h4>"+
                        "<div class='add-datasource-list'><table>"+
                        	"<thead><tr><th>Name</th><th>Type</th><th>Create Time</th><th>Description</th></tr></thead>"+
                        	"<tbody><tr ng-repeat='datasource in datasources' ng-click='choose(datasource)'>"+
                        	"<td>{{datasource.name}}</td>"+
                        	"<td>{{datasource.type}}</td>"+
                        	"<td>{{datasource.createdTime}}</td>"+
                        	"<td>{{datasource.description}}</td>"+
                        	"</tr></tbody>"+
                        "</table></div></div>",
                        size: 'lg',
                        controller:function($scope){
                        	queryService.xhr_datasources().success(function(datasources){
                                if(type){
                                    $scope.datasources = datasources.filter(function(obj){
                                        return obj.type==type
                                    })
                                }
                                else {
                                    $scope.datasources = datasources;
                                }
                        	});
                            $scope.choose= function(datasource){
                            	scope.choose(datasource);
                            	scope.modal_instance.close();
                            }
                        }
                    });
                }
                scope.choose = function(datasource) {
                	var datasources = util.cache.get('datasources');
                	if(datasources && datasources.length>0){
                		if(datasources.filter(function(obj){
                			return obj.uid==datasource.uid
                		}).length>0){return;}
                	}
                    //
                    //var datasources = util.cache.get('datasources')
                    /*angular.forEach(datasources, function(datasource){
                        if(datasource.uid == uid){
                            return;
                        }
                    })*/
                    //var widget_uid = $rootScope.widget.uid;
                    //scope.datasources.push(datasource);
                    //datasources_map[datasource.uid]= datasource;
                	/*
                	 *
                	 *  var widget_uid = $rootScope.widget.uid;
                    queryService.xhr_attach_datasource(widget_uid, uid).success(function(datasource){
                        scope.datasources.push(datasource);
                        datasources_map[datasource.uid]= datasource;
                        scope.toggle_datasource(datasource.uid)
                    })
                	 *
                	 * */
                	queryService.xhr_attach_datasource($rootScope.widget.id, datasource.uid).success(function(){
                		scope.datasources.push(datasource);
                        datasources_map[datasource.uid]= datasource;
                        scope.toggle_datasource(datasource.uid)
                	})
                };

                if(scope.datasources.length>0){
                    var datasource_uid;
                    if(scope.component.data!=null){
                        if(scope.component.data.datasource==null){
                            datasource_uid = scope.datasources[0].uid;
                        }
                        else{
                            if(datasources_map[scope.component.data.datasource]!=null){
                                datasource_uid = scope.component.data.datasource;
                            }
                            else{
                                datasource_uid = scope.datasources[0].uid;
                            }
                        }
                        scope.toggle_datasource(datasource_uid)
                    }
                    else{
                        if(scope.defaultOption=='data'){
                            datasource_uid = scope.datasources[0].uid;
                            scope.toggle_datasource(datasource_uid)
                        }
                    }
                }


            }
        };
}])
.directive('dataElement', ['queryService','util','$rootScope',function(queryService, util, $rootScope){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/components/data-element.html',
            replace:true,
            require:"^dataProperties",
            scope:{
                datasource:"=",
                component:"="
            },
            controller: function($scope,$element){

            	$scope.select_column = function(index){
            		var expr = {
            			column_start_index: index,
            			column_end_index: index
            		}

            		var current_component = $scope.component;
                    current_component.data = {
                        type:'data',
                        expr:expr,
                        datasource : $scope.datasource.uid
                    }
                    if(current_component.extra){
                    	current_component.extra.update();
                    }
                    $scope.select_start = {column:index, row:0}
                    $scope.select_end = {column:index, row: $scope.datasource.data.data.length}
            	}

            	$scope.selecting = false;
            	$scope.select_item_start = function(row, column){
            		$scope.selecting = true;
            		$scope.select_end = null;
            		$scope.select_start = {
            				column:column,
            				row:row
            		}
            	}

            	$scope.select_item_move = function(row, column){
            		if($scope.selecting){
            			$scope.select_end = {
            					column: column,
            					row:row
            			};
            		}
            	}
            	$scope.select_item_end = function(row,column){
            		if($scope.selecting){

            			$scope.select_end = {
            					column: column,
            					row:row
            			};
            			var expr = {
            				column_start_index: $scope.select_start.column,
                    		column_end_index: $scope.select_end.column,
                    		row_start_index : $scope.select_start.row,
                    		row_end_index: $scope.select_end.row
            			}
            			var current_component = $scope.component;
            			current_component.data = {
                                type:'data',
                                expr:expr,
                                datasource : $scope.datasource.uid
                        }
            			if(current_component.extra){
                            if(current_component.extra.interval){
                                clearInterval(current_component.extra.interval);
                            }
                            current_component.extra.update();
                        }
            		}
            		$scope.selecting =false;
            	}
                $scope.select_stream = function(){
                    var current_component = $scope.component;
                    current_component.data = {
                        type:'data',
                        expr:'',
                        datasource: $scope.datasource.uid,
                        interval: 5000
                    }
                    if(current_component.extra)    {
                        current_component.extra.update();
                        if(current_component.extra.interval){
                            clearInterval(current_component.extra.interval);
                         }
                        current_component.extra.interval = setInterval(function(){current_component.extra.update()},5000);

                    }
                }
                if($scope.datasource.type==='NIFI') {
                    $scope.$watch('datasource.data', function (nv, ov) {
                        var si = $element.find('.stream-indicator').html('')[0];


                        if (nv && nv != ov) {
                            var circle = new ProgressBar.Circle(si, {
                                strokeWidth: 10,
                                duration: 5000,
                                easing: 'easeInOut',
                                trailColor: '#aaa',
                                color: '#FCB03C'
                            });
                            circle.animate(1);
                        }
                    })
                }

            },
            link: function(scope, element, attrs, dataPropertiesCtrl){

                var datasource = scope.datasource;


                if(datasource.data!=null){
                    if(scope.component.data!=null) {
                        if (scope.component.data.type == 'data' && scope.component.data.datasource == datasource.uid) {
                            var data_expr = scope.component.data.expr;
                            scope.select_start = {};
                            scope.select_end = {};
                            scope.select_start.column = data_expr.column_start_index;
                            scope.select_end.column =data_expr.column_end_index;
                            scope.select_start.row =data_expr.row_start_index||0;
                            scope.select_end.row =data_expr.row_end_index!=null?data_expr.row_end_index:datasource.data.data.length;
                        }
                    }
                }
            }

        };
}])

angular.module('widget.components')
.directive('dataProperties', ['queryService','util','$uibModal',function(queryService, util, $uibModal){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/data-properties.html',
            replace:true,
            scope:{
                component:"=",
                defaultOption:"@"
            },
            controller: function($scope,$element){
                var visualizers_map = {};
               this.disable_input = function(){
                    $scope.value_expr = ""
               } 

               
               this.set_pointer = function(uid,expr){
                    for(var key in visualizers_map){
                        if(key==uid){
                            visualizers_map[key].setPointer(expr)
                        }
                    }
                }

               this.add_visualizer = function(uid,visualizer){
                    visualizers_map[uid] = visualizer;

                    visualizer.setSelectionCallback(function(obj){
                        var data = obj.data;
                        var current_component = $scope.component;
                        current_component.data = {
                            type:'data',
                            expr:data,
                            datasource : uid
                        }
                        current_component.extra.update();

                        for(var key in visualizers_map){
                            if(key!=uid){
                                visualizers_map[key].setPointer(false);
                            }
                        }

                        $scope.value_expr = "";
                    })
               }

                $scope.apply_value= function(){
                    $scope.component.data = $scope.component.data||{}
                    $scope.component.data.type = 'value' ;
                    $scope.component.data.expr = $scope.value_expr;
                    $scope.component.extra.update();
                }
            },
            link: function(scope, element, attrs){
                scope.datasources = util.cache.get('datasources');
                if(scope.component.data == null){
                    scope.option = scope.defaultOption;
                }
                else{
                    if(scope.component.data.type == 'data'){
                        scope.option = 'data';
                    }
                    else{
                        scope.option = 'value';
                        scope.value_expr = scope.component.display_value;
                    }
                }
                scope.toggle_option = function(){
                    if(scope.option == 'data'){
                        scope.option = 'value';
                    }
                    else{
                        scope.option = 'data';
                    }
                }
            }
        };
}])
angular.module('widget.components')
.directive('widget', ['$rootScope',function($rootScope){
    return {
        restrict:'E',
        replace:true,
        template: "<div class='editing-component' ctrl-droppable class='droppable' ng-class=\"{true:'root-content', false:''}[root=='true']\">"+
                        "<ul>"+
                            "<li ng-repeat='component in widget.components' "+
                            "ctrl-draggable='{{!component.atom}}' data='component' "+
                            "ng-click='toggle_select(component,$event)' "+
                            "ng-class=\"{'ctrl-selected':component.ctrl_id == $root.selected_component.ctrl_id, "+
                            "'height-small':component.height=='Small','height-medium':component.height=='Medium', 'height-large':component.height=='Large'}\""+
                            "ng-style=\"component.height=='Custom'?{height:component.custom_height+'px'}:{}\">"+
                                "<x-inline-component component='component'></x-inline-component>"+
                            "</li>"+
                        "</ul>"+
                  "</div>",

                  /*
                        ng-class="{'height-small':component.options.height=='Small', 'height-medium':component.options.height=='Medium','height-large':component.options.height=='Large'}" ng-style="component.options.height=='Custom'?{height:component.options.custom_height+'px'}:{}"
                  */
        controller:function($scope){
            $scope.active =function(event){
            }

            $scope.toggle_select = function(component, $event){
                    $event.stopPropagation()
                    if($rootScope.selected_component != null && $rootScope.selected_component.ctrl_id == component.ctrl_id){
                        $rootScope.selected_component = null;
                    }
                    else{
                        $rootScope.selected_component = component;
                    }
            }
        },
        link: function(scope, element, attrs){
            element.attr('root', scope.root)
        },
        scope: {
            widget:"=",
            root:"@"
        }
    }
}])
.directive('canvas', ['$document','Upload','$rootScope',function($document, Upload,$rootScope){
    return {
        restrict:'E',
        replace:true,
        template: "<div class='editing-canvas-content' free='true' class='droppable' ng-class=\"{true:'root-content', false:''}[root=='true']\">"+
                        "<div class='editing-canvas-container'>"+
                            "<div class='editing-canvas-inner' canvas-droppable ng-style='container_style'>"+ // big canvas width:1000px height:1000px
                                "<ul class='editing-canvas-target'>"+
                                    "<li ng-repeat='component in widget.components' canvas-draggable='{{!component.atom}}' resizable data='component' ng-click='toggle_select(component,$event)' ng-class=\"{true:'ctrl-selected', false:''}[component.ctrl_id == $root.selected_component.ctrl_id]\" style='top:{{component.top}};left:{{component.left}};width:{{component.width}};height:{{component.height}}' >"+
                                        "<x-inline-component component='component'></x-inline-component>"+
                                    "</li>"+
                                "</ul>"+
                                "<img class='widget-bg' draggable=\"false\"  ng-src='{{background}}' width='100%'/>"+
                            "</div>"+
                        "</div>"+
                        "<ul class='pan-navigator'>"+
                            "<li><a href='#' ng-click='zoomin()'><i class='fa fa-plus'></i></a></li>"+
                            "<li><a href='#' ng-click='zoomout()'><i class='fa fa-minus'></i></a></li>"+
                            "<li><a href='#' class='fa fa-picture-o upload-image'>"+
                                "<input type='file' ng-model='upload_image' name='upload_image' ngf-max-size=\"20MB\"  ngf-select='upload_file($file)'/>"+
                            "</a></li>"+
                        "</ul>"+
                  "</div>",
        controller:function($scope, $element){
            var zoom_scale = 1.2
            var container = $element.find('.editing-canvas-container');
            var init_width = container.width();
            if($scope.widget.width < init_width){
                $scope.widget.width = init_width;
                $scope.widget.height= null;
            }
            var inner = $element.find('.editing-canvas-inner');
            if($scope.widget.background==""|| $scope.widget.background==null){
                $scope.background = "/img/transparent.png";
            }
            else{
                $scope.background = $scope.widget.background;
            }
             $element.find('.widget-bg').load(function(){
                 if(!$scope.widget.height) {
                     inner.height($(this).height());
                     $scope.widget.height = $(this).height();
                 }
                 if(!$scope.widget.width){
                     inner.width($(this).width());
                     $scope.widget.width = $(this).width();
                 }
             })
             $scope.$broadcast('component-reflow');
             $scope.toggle_select = function(component, $event){
                    $event.stopPropagation()
                    if($rootScope.selected_component != null && $rootScope.selected_component.ctrl_id == component.ctrl_id){
                        $rootScope.selected_component = null;
                    }
                    else{
                        $rootScope.selected_component = component;
                    }
            }
            $scope.refresh_size = function(){
                $scope.container_style = {
                    width: 'auto',
                    height: 'auto'
                }

                if($scope.widget.width && $scope.widget.height) {
                    $scope.container_style =  {
                        width: $scope.widget.width+"px",
                        height: $scope.widget.height+"px"
                    }
                }
            }

            $scope.refresh_size();

            $scope.zoomin = function(){
                //var width = inner.width()+zoom_scale;
                //var height = inner.height+zoom_scale;

                var widget_width =  Math.round(inner.width()*zoom_scale);
                var widget_height = Math.round(inner.height()*zoom_scale) ;
                inner.width(widget_width);
                inner.height(widget_height);
                container.scrollTop(container.scrollTop()*zoom_scale);
                container.scrollLeft(container.scrollLeft()*zoom_scale);
                $scope.widget.width = widget_width;
                $scope.widget.height = widget_height;
                $scope.refresh_size();
            }

            $scope.zoomout = function(){
                var widget_width = Math.round(inner.width()/zoom_scale);
                if(widget_width<init_width){return;};
                var widget_height = Math.round(inner.height()/zoom_scale);
                inner.width(widget_width);
                inner.height(widget_height);
                $scope.widget.width = widget_width;
                $scope.widget.height = widget_height;
                $scope.refresh_size();
            }

            $scope.upload_file = function(file){
                if(file){
                    Upload.upload({
                        url:'/api/widgets/upload/',
                        data:{
                            widget:$rootScope.widget.id,
                            file:file
                        }
                    }).then(function(ret){
                        $scope.widget_bg = ret.data.url;
                        $scope.background= ret.data.remote+ret.data.url;
                        $rootScope.widget.background = ret.data.url;
                    })
                }
            }
        },
        link: function(scope, element, attrs){
            element.attr('root', scope.root)

            var container = element.find('.editing-canvas-container')
            var startx, starty;
            var start_scroll_x = container.scrollLeft();
            var start_scroll_y = container.scrollTop();
            var mousemove = function(event){
                var moving_x = event.pageX;
                var moving_y = event.pageY;
                var movex = moving_x - startx;
                var movey = moving_y - starty;

                container.scrollTop(start_scroll_y-movey);
                container.scrollLeft(start_scroll_x-movex);
            }

            var mouseup = function(){
                $document.unbind('mousemove', mousemove);
                $document.unbind('mouseup', mouseup);
                container.unbind('mousemove', mousemove);
            }


            container.on('mousedown', function(event){
                if(!$(event.target).hasClass('editing-canvas-target')){
                    return;
                }
                event.stopPropagation();
                startx = event.pageX;
                starty = event.pageY;

                start_scroll_y = container.scrollTop();
                start_scroll_x = container.scrollLeft();
                container.on('mousemove', mousemove)
                $document.on('mouseup', mouseup);
            })

        },
        scope: {
            widget:"=",
            root:"@"
        }
    }
}])
.directive('widgetProperties', [function(){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/widget.html',
            replace:true,
            controller: function($scope){
            	$scope.change_width_percentage = function(){
            		$scope.component.widthPercentage = !$scope.component.widthPercentage;
            		if($scope.component.widthPercentage === true){
            			$scope.component.width = 100;
            		}
            		else{
            			$scope.component.width = 800;
            		}
            	}
            	$scope.change_height_percentage = function(){
            		$scope.component.heightPercentage = !$scope.component.heightPercentage;
            		if($scope.component.heightPercentage === true){
            			$scope.component.height=100;
            		}
            		else{
            			$scope.component.height = 600;
            		}
            	}
            },
            link: function(scope, element, attrs){

            },
        };
}])
.directive('resizable',[function(){
    return {
            restrict:'AE',
            controller: function($scope){
            },
            link: function(scope, element, attrs){
                   element.resizable({
                        stop: function(event, ui){
                            scope.component.width=ui.size.width+"px";
                            scope.component.height = ui.size.height+"px";
                            scope.$broadcast('component-reflow')
                        }
                   });
            },
        };
}])

angular.module( 'widget.components')
.directive('inlineframeComponent', ["$rootScope","$sce",function($rootScope, $sce){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/components/frame.html',
            replace:true,
            controller: function($scope){
                
            },
            link: function(scope, element, attrs){
                //console.log(scope.component)
                //element.find('.html-container').html(scope.component.html_string);
                scope.component.tmp_url = $sce.trustAsResourceUrl(scope.component.url)
            }
        };
}])
.directive('inlineframeProperties', ['util', '$sce', function(util, $sce){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/frame.html',
            replace:true,
            controller: function($scope, $element){
                $scope.change_url = function(){
                   var current_url = $scope.component.url;
                   $scope.component.tmp_url = $sce.trustAsResourceUrl(current_url);
                }
                $scope.change_overflow = function(){
                    if($scope.component.overflow == 0){
                        $scope.component.overflow = 1;
                    }
                    else{
                        $scope.component.overflow = 0;
                    }
                    var url = $scope.component.url;
                    //$scope.component.url = url;
                    //console.log($element.html())
                    //$element.find('iframe').attr('src', url)

                    if(url!=null){
                        if(typeof url == 'object'){
                            url = url.toString();
                        }

                        var seed_index = url.indexOf('&refresh_seed');
                        if(seed_index>0){
                            url = url.substring(0,seed_index);
                        }
                        seed_index = url.indexOf('?refresh_seed')
                        if(seed_index>0){
                            url = url.substring(0,seed_index);
                        }
                        if(url.indexOf("?")>0){
                            $scope.component.tmp_url = $sce.trustAsResourceUrl(url+"&refresh_seed="+util.uuid());
                        }
                        else{
                            $scope.component.tmp_url = $sce.trustAsResourceUrl(url+"?refresh_seed="+util.uuid());
                        }
                    }
                }

                $scope.refresh_intervals = [
                    {
                        id:-1,
                        name:'Never'
                    },
                    {
                        id:15,
                        name:'15 min.'
                    },
                    {
                        id:30,
                        name:'30 min.'
                    },
                    {
                        id:60,
                        name:'60 min.'
                    }
                ]
            },
            link: function(scope, element, attrs){
                
            },
        };
}])

angular.module( 'widget.components')
.directive('gaugeComponent', ['$rootScope','util','queryService','$timeout','$q',function($rootScope,util,queryService, $timeout, $q){
	
	var verify_data = function(component){
       
        return true;
    }

	
        return {
            restrict:'AE',
            templateUrl:'/templates/edit/components/gauge.html',
            replace:true,
            controller: function($scope,$element){
                $scope.gauge_chart = null; 
                var gaugeOptions = {

        chart: {
            type: 'solidgauge',
            backgroundColor: "rgba(255, 255, 255, 0)",
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false
        },

        pane: {
    		"center": [
    			"50%",
    			"85%"
    		],
    		"size": "140%",
    		"startAngle": "-90",
    		"endAngle": "90",
    		"background": {
    			"backgroundColor": "#EEE",
    			"innerRadius": "60%",
    			"outerRadius": "100%",
    			"shape": "arc"
    		}
    	},
    	"tooltip": {
    		"enabled": true
    	},
    	credits: {
            enabled: false
        },
        // the value axis
    	"yAxis": {
    		"stops": [
    			[
    				0.1,
    				"#73d423"
    			],
                [
                    0.25,
                    "#b5dc31"
                ],
    			[
    				0.5,
    				"#EFCE2B"
    			],
                [
                    0.75,
                    "#d4823b"
                ],
    			[
    				0.9,
    				"#A00808"
    			]
    		],
            tickAmount:2,
            tickPositioner: function() {
                return [this.min, this.max];
            },
    		"min": 0,
    		"max": 100,
    		"lineWidth": 0,
    		"minorTickInterval": null,
    		"tickPixelInterval": 400,
    		"tickWidth": 0,
    		"title": {
    			"y": -70
    		},
    		"labels": {
    			"y": 16
    		}
    	},
        series: [{
            name: 'Speed',
            data: [50]
        }]

    }
                var draw_chart = function(container, chart_data){
                    gaugeOptions.title={text:chart_data.key};
                    if(chart_data.background){
                        gaugeOptions.chart.backgroundColor=chart_data.background
                    }
                	gaugeOptions.series[0].name = chart_data.key;
                	if(isNaN(chart_data.value)){
                		chart_data.value = 0;
                	}
                	gaugeOptions.series[0].data = [parseFloat(chart_data.value)];
                	gaugeOptions.yAxis.min = isNaN(chart_data.min)?0:parseFloat(chart_data.min);
                	gaugeOptions.yAxis.max = isNaN(chart_data.max)?100:parseFloat(chart_data.max);
                    $timeout(function(){
                         $scope.gauge_chart=container.find(".chart-container").highcharts(gaugeOptions).highcharts();
                         $scope.gauge_chart.reflow()
                    },10)
                    return ;

                }



                $scope.component.extra.update = function(){
                    var chart_data = {
                        key:$scope.component.options.title,
                        min:$scope.component.options.min,
                        max:$scope.component.options.max,
                        value:$scope.component.data.expr,
                        background: $scope.component.options.background
                    };
                	if(!verify_data($scope.component)){
                		draw_chart($element, chart_data);
                        return;
                    }	
                	if($scope.component.data.type=='data'){
                        if($scope.component.data.expr!=null && $scope.component.data.datasource!=null){
                            queryService.xhr_fetch_expr($scope.component.data.expr, $scope.component.data.datasource).success(function(data){
                                chart_data.value = data.data.join();
                                draw_chart($element, chart_data);
                            })
                        }
                    }
                    else{
                    	chart_data.value = $scope.component.data.expr;
                    	draw_chart($element, chart_data);
                    }
                	
                	
                } 
                $scope.component.extra.update();

                $scope.$on('component-reflow',function(){
                    $scope.gauge_chart.reflow();
                })

            },
            link: function(scope, element, attrs){
            
            }
        };
}])
.directive('gaugeProperties', [function(){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/gauge.html',
            replace:true,
            controller: function($scope){
                $scope.toggle_pie_style = function(_options){
                    $scope.component.options = $scope.component.options||{};
                    if(_options == 0){
                        $scope.component.options.inner_size="0%"
                    }
                    else{
                        $scope.component.options.inner_size="60%"
                    }
                    $scope.component.extra.update()

                } 

                $scope.label_options = [
                    {
                        id:0,
                        name:'Show Labels Around Chart'
                    },
                    {
                        id:1,
                        name:"Don't Show Labels"
                    },
                    {
                        id:2,
                        name:'Show Labels in Legend'
                    }
                ]

                $scope.apply_value = function(){
                    $scope.component.extra.update();
                }

                $scope.change_label = function(){
                    $scope.component.options = $scope.component.options||{};
                    switch($scope.component.options.label){
                        case 1:
                            $scope.component.options.show_legend = false
                            $scope.component.options.data_labels=false
                            break;
                        case 0:
                            $scope.component.options.data_labels=true
                            $scope.component.options.show_legend = false 
                            break;
                        case 2:
                            $scope.component.options.show_legend = true; 
                            $scope.component.options.data_labels=false
                            break;
                        default:
                            break;
                    }
                        
                    $scope.component.extra.update();
                }


                $scope.$on('colorpicker-selected', function (event, colorObj) {
                    $scope.component.options.background = colorObj.value;
                    $scope.component.extra.update();
                })


                $scope.change_show_labels = function(){
                    $scope.component.options = $scope.component.options||{}
                    $scope.component.options.show_labels = !$scope.component.options.show_labels;
                    $scope.component.extra.update();
                }

                $scope.change_show_percentage = function(){
                    $scope.component.options = $scope.component.options||{}
                    $scope.component.options.show_percentage = !$scope.component.options.show_percentage;
                    $scope.component.extra.update();
                }

            },
            link: function(scope, element, attrs){
               scope.component.display_value=scope.component.data.expr;
            },
        };
}])
angular.module( 'widget.components')
.directive('gridComponent', ['renderService', '$rootScope','util',function(renderService, $rootScope, util){
        function render_grid(scope){
            var grid_component = scope.component;
            scope.rows = grid_component.rows;
            scope.cols = grid_component.cols;
            scope.layout_cells = new Array(scope.rows);

            for(var i = 0;i<scope.rows;i++){
                scope.layout_cells[i] = new Array(scope.cols);
            }
            var grid_cells = grid_component.cells;
            angular.forEach(grid_cells, function(grid_cell){
                grid_cell.ctrl_id = util.uuid();
                grid_cell.rows = grid_component.rows;
                grid_cell.cols = grid_component.cols;
                grid_cell.visible = true;
                if(grid_cell.coord_x<scope.rows&&grid_cell.coord_y<scope.cols){
                    scope.layout_cells[grid_cell.coord_x][grid_cell.coord_y] = grid_cell;
                }
            })
            for(var i = 0 ;i<scope.rows;i++){
                for(var j = 0;j<scope.cols;j++){
                   /* if(scope.layout_cells[i][j]!=null){
                        //to find the first not null right cell.
                       var k = j;
                       while(k<scope.cols&&scope.layout_cells[i][++k]==null);
                       scope.layout_cells[i][j].right_cell = scope.layout_cells[i][k];

                       //to find the first not null down cell.
                       var x = i;
                       while(x<scope.rows&&scope.layout_cells[++x][j]==null);
                       scope.layout_cells[i][j].down_cell = scope.layout_cells[x][j];
                    }*/

                    var current_cell = scope.layout_cells[i][j];
                    if(current_cell==null){
                        throw new Error('null cell in grid.');
                    }
                    if(j<scope.cols-1){
                        scope.layout_cells[i][j].right_cell = scope.layout_cells[i][j+1];
                    }
                    if(j>0) {
                        scope.layout_cells[i][j].left_cell = scope.layout_cells[i][j-1]
                    }
                    if(i<scope.rows-1){
                        scope.layout_cells[i][j].down_cell = scope.layout_cells[i+1][j];
                    }
                    if(i>0){
                        scope.layout_cells[i][j].up_cell = scope.layout_cells[i-1][j];
                    }

                    for(var k = 1 ;k<scope.layout_cells[i][j].width;k++){
                        scope.layout_cells[i][j+k].visible = false;
                    }

                    for(var x = 1;x<scope.layout_cells[i][j].height;x++){
                        scope.layout_cells[i+x][j].visible = false;
                    }
                }
            }
        }
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/components/grid.html',
            replace:true,
            controller: function($scope){
                $scope.getTimes = function(n){
                    return new Array(n)
                }

                $scope.toggle_cell_select = function(cell, $event){
                    $event.stopPropagation();
                    if($rootScope.selected_component != null && $rootScope.selected_component.ctrl_id == cell.ctrl_id){
                        $rootScope.selected_component = null;
                    }
                    else{
                        $rootScope.selected_component = cell;
                    }
                }

                $scope.$watch('component.cols', function(newv, oldv){
                    if(newv!=null && newv!=oldv){
                        render_grid($scope);
                    }
                })

                $scope.$watch('component.rows', function(newv, oldv){
                    if(newv!=null && newv!=oldv){
                        render_grid($scope);
                    }
                })
            },
            link: function(scope, element, attrs){
                /*
                    to implement the colspan/rowspan,  need to hide the overflow td.
                    the hidden td can only be right or down position of the *span td.
                    so each cell must hold the ptr of right and down position cell so as to 
                    make them hidden when be set as *span.

                */
                render_grid(scope);
            }
        };
}])
.directive('gridProperties', [function(){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/grid.html',
            replace:true,
            controller: function($scope){
                var grid =  $scope.component;
                $scope.increase_cols = function(){
                    for(var i = 0 ; i<grid.rows; i++){
                        var cell = {
                            "type": "cell",
                            "name": "new_cell",
                            "width": 1,
                            "height": 1,
                            "coord_x": i,
                            "coord_y": grid.cols,
                            "padding": 0,
                            "components": []
                        }
                        grid.cells.push(cell);
                    }
                    grid.cols +=1;
                }

                $scope.decrease_cols = function(){
                    if(grid.cols==1){
                        return;
                    }
                    for(var i = 0 ; i<grid.cells.length;i++){
                        var cell = grid.cells[i];
                        if(cell.coord_y == grid.cols-1){
                            // determine whether to remove, one fail, all fail. 
                            if(cell.visible == false){
                                return;
                            }
                            else{
                                if(cell.components!=null&&cell.components.length>0){
                                    return;
                                }
                            }
                        }
                    }

                    // all pass, remove(splice)
                    for(var i = 0;i<grid.cells.length;i++){
                        var cell = grid.cells[i];
                        if(cell.coord_y == grid.cols-1){
                            grid.cells.splice(i,1);
                        }
                    }
                    grid.cols-=1; 
                }


                $scope.increase_rows = function(){
                    for(var i = 0 ; i<grid.cols; i++){
                        var cell = {
                            "type": "cell",
                            "name": "new_cell",
                            "width": 1,
                            "height": 1,
                            "coord_x": grid.rows,
                            "coord_y": i,
                            "padding": 0,
                            "components": []
                        }
                        grid.cells.push(cell);
                    }

                    grid.rows+=1; 
                }

                $scope.decrease_rows = function(){
                    if(grid.rows==1){
                        return;
                    }
                    for(var i = 0;i<grid.cells.length;i++){
                        var cell = grid.cells[i];
                        if(cell.coord_x==grid.rows-1){
                            if(cell.visible == false){
                                return;
                            }
                            else{
                                if(cell.components!=null && cell.components.length>0){
                                    return;
                                }
                            }
                        }
                    }

                    for(var i = 0;i<grid.cells.length;i++){
                         var cell = grid.cells[i];
                        if(cell.coord_x==grid.rows-1){
                            grid.cells.splice(i, 1);
                        }
                    }

                    grid.rows-=1; 
                }
            },
            link: function(scope, element, attrs){
                
            },
        };
}])
.directive('cellProperties', [function(){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/cell.html',
            replace:true,
            controller: function($scope){

                $scope.increase_colspan = function(){
                    var cell = $scope.component;

                    if(cell.height>1){
                        return;
                    }
                    if(cell.right_cell == null || cell.width == cell.cols){
                        return;
                    }

                   
                    var tmp_cell = cell.right_cell;
                    while(tmp_cell != null && tmp_cell.visible!=true){
                        tmp_cell = tmp_cell.right_cell;
                    }

                    if(tmp_cell == null){
                        return;
                    }
                    else{
                        //tmp_cell.visible == true;
                        //tmp_cell.visible = false;
                        if(tmp_cell.width >1){
                            return;
                        }
                        else{
                            tmp_cell.visible = false
                            cell.width +=1;
                        }
                    }
                    
                }

                $scope.decrease_colspan = function(){
                    var cell = $scope.component;
                    if(cell.height>1){
                        return;
                    }
                    if(cell.right_cell == null || cell.width == 1){
                        return;
                    }

                    var tmp_cell = cell;
                    while(tmp_cell.right_cell != null && tmp_cell.right_cell.visible!=true){
                        tmp_cell = tmp_cell.right_cell;
                    }
                    tmp_cell.visible = true;
                    cell.width-=1;
                }


                $scope.increase_rowspan = function(){
                    var cell = $scope.component;

                    if(cell.width>1){
                        return;
                    }
                     if(cell.down_cell == null || cell.height == cell.rows){
                        return;
                    }

                   
                    var tmp_cell = cell.down_cell;
                    while(tmp_cell != null && tmp_cell.visible!=true){
                        tmp_cell = tmp_cell.down_cell;
                    }

                    if(tmp_cell == null){
                        return;
                    }
                    else{
                        //tmp_cell.visible == true;
                        //tmp_cell.visible = false;
                        if(tmp_cell.height >1){
                            return;
                        }
                        else{
                            tmp_cell.visible = false
                            cell.height +=1;
                        }
                    }
                }

                $scope.decrease_rowspan = function(){
                    var cell = $scope.component;
                    if(cell.width>1){
                        return;
                    }
                    if(cell.down_cell == null || cell.height == 1){
                        return;
                    }

                    var tmp_cell = cell;
                    while(tmp_cell.down_cell != null && tmp_cell.down_cell.visible!=true){
                        tmp_cell = tmp_cell.down_cell;
                    }
                    tmp_cell.visible = true;
                    cell.height-=1;
                }
            },
            link: function(scope, element, attrs){
                
            },
        };
}])
angular.module( 'widget.components' )
.directive('htmltemplateComponent', ["$rootScope",function($rootScope){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/components/html.html',
            replace:true,
            controller: function($scope){
                $scope.toggle_select = function($event){
                    $event.stopPropagation() 
                    if($rootScope.selected_component != null && $rootScope.selected_component.ctrl_id == $scope.component.ctrl_id){
                        $rootScope.selected_component = null;
                    }
                    else{
                        $rootScope.selected_component = $scope.component;
                    }
                }

                
            },
            link: function(scope, element, attrs){
                //console.log(scope.component)
                //element.find('.html-container').html(scope.component.html_string);
            }
        };
}])
.filter('to_trusted', ['$sce', function($sce){
    return function(text){
        return $sce.trustAsHtml(text);
    }
}])
.directive('htmltemplateProperties', [function(){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/html.html',
            replace:true,
            controller: function($scope){
                $scope.change_overflow = function(){
                    if($scope.component.overflow == 0){
                        $scope.component.overflow = 1;
                    }
                    else{
                        $scope.component.overflow = 0;
                    }
                }
            },
            link: function(scope, element, attrs){
                
            },
        };
}])

angular.module( 'widget.components' )
.directive('imageComponent', ["$rootScope","$sce",function($rootScope, $sce){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/components/image.html',
            replace:true,
            controller: function($scope, $element){
                
                $scope.imgload = function($event){
                    var origin_width = $element.find('img')[0].naturalWidth;
                    var origin_height = $element.find('img')[0].naturalHeight
                    $scope.component.aspect_ratio = origin_width/origin_height;
                }                
            },
            link: function(scope, element, attrs){
                scope.component.tmp_url = $sce.trustAsResourceUrl(scope.component.url)
                
            }
        };
}])
.directive('imgLoad', ['$parse', function ($parse) {
return {
  restrict: 'A',
  link: function (scope, elem, attrs) {
    var fn = $parse(attrs.imgLoad);
    elem.on('load', function (event) {
      scope.$apply(function() {
        fn(scope, { $event: event });
      });
    });
  }
};
}])
.directive('imageProperties', ['$sce', function($sce){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/image.html',
            replace:true,
            controller: function($scope, $element){
                $scope.change_url = function(){
                   var current_url = $scope.component.url;
                   $scope.component.tmp_url = $sce.trustAsResourceUrl(current_url);
                }
                $scope.change_overflow = function(){
                    
                }
                $scope.change_ratio = function(){
                    $scope.component.keep_ratio = !$scope.component.keep_ratio
                    if($scope.component.keep_ratio == true){
                        var custom_width = $scope.component.custom_resize_width;
                        if(custom_width!=null && $scope.component.aspect_ratio!=null){
                            $scope.component.custom_resize_height = Math.round(custom_width/$scope.component.aspect_ratio);
                        }
                    }
                }

                $scope.change_width = function(){
                        if($scope.component.keep_ratio == true){
                           $scope.component.custom_resize_height = Math.round($scope.component.custom_resize_width/$scope.component.aspect_ratio);
                        }
                }
                $scope.change_height = function(){
                        if($scope.component.keep_ratio == true){
                            $scope.component.custom_resize_width = Math.round($scope.component.custom_resize_height*$scope.component.aspect_ratio)
                        }
                }

                $scope.resize_options = [
                   {
                        id:0,
                        name:'Do not Resize'
                   } ,
                   {
                        id:1,
                        name:'Full'
                   },
                   {
                        id:2,
                        name:'Custom'
                   }

                ]

                $scope.height_options = [
                    {
                        id:'Auto',
                        name:'Auto'
                    },
                    {
                        id:'Small',
                        name:'Small'
                    },
                    {
                        id:'Medium',
                        name:'Medium'
                    },
                    {
                        id:'Large',
                        name:'Large'
                    },
                    {
                        id:'Custom',
                        name:'Custom'
                    }

                ]
            },
            link: function(scope, element, attrs){
               
            },
        };
}])

angular.module( 'widget.components')
.directive('labelComponent', ["$rootScope","queryService","conditionService", function($rootScope,queryService,conditionService){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/components/label.html',
            replace:true,
            controller: function($scope){


                $scope.component.extra.update = function(_data){
                    if(_data){
                        $scope.component.display_value = _data
                        return;
                    }
                    $scope.component.display_value = "";
                    if($scope.component.data == null){
                        return;
                    }
                    if($scope.component.data.type=='data'){
                        if($scope.component.data.expr!=null && $scope.component.data.datasource!=null){
                            queryService.xhr_fetch_expr($scope.component.data.expr, $scope.component.data.datasource).success(function(data){
                                $scope.component.display_value = data.data.join();

                                
                            })
                        }
                    }
                    else{
                        $scope.component.display_value = $scope.component.data.expr;
                    }
                }

                $scope.component.extra.update()
            },
            link: function(scope, element, attrs){

                //scope.component.display_value =
            }
        };
}])
.directive('labelProperties', [function(){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/label.html',
            replace:true,
            controller: function($scope){
               $scope.change_wrap = function(){

                    if($scope.css['white-space']=='normal') {
                        $scope.css['white-space'] = 'nowrap';
                        $scope.css['word-break'] = 'normal';
                    }
                    else{
                        $scope.css['white-space'] = 'normal';
                        $scope.css['word-break'] = 'break-all';
                    }
                    //render_style();
               }
            },
            link: function(scope, element, attrs){
                scope.css = {}
                if(scope.style!=null){
                    var style_array = scope.style.split(";");
                    for(var i = 0 ;i<style_array.length;i++){
                        var css_clip = style_array[i];
                        if(css_clip!=null && css_clip!=""){
                            if(css_clip.split(":").length==2){
                                scope.css[css_clip.split(":")[0]] = css_clip.split(":")[1];
                            }
                        }
                    }
                }
            },
        };
}])

angular.module( 'widget.components')
.directive('pieComponent', ['$rootScope','util','queryService','$timeout',function($rootScope,util,queryService, $timeout){
        
        var verify_data = function(component){
            var chart_value = component.chart_value;
            var chart_label = component.chart_label;
            if(chart_value==null|| chart_label == null){
                return false;
            }
            if(chart_value.data==null || chart_label.data == null){
                return false;
            }
           
            if(chart_label.data.expr == null || chart_label.data.expr==''){
                return false;
            }

            if(chart_label.data.datasource == null || chart_label.data.datasource==''){
                return false;
            }

            return true;
        }

        
        return {
            restrict:'AE',
            templateUrl:'/templates/edit/components/pie.html',
            replace:true,
            controller: function($scope,$element){


            var draw_chart = function(container, chart_data,_options){
                var options = util.charts_options();
                options.chart.type='pie';
                
                //options.chart.height=container.height();

                options.series = [{
                    data: ((chart_data!=null&&chart_data.length>0)?chart_data:[["No Data",1]])
                }]
                options.legend.labelFormat = (function(opt){
                    var formatter = "{name}";
                    if(opt==null){
                        return formatter
                    }
                    if(opt.show_labels){
                        formatter +="({y:.2f})"
                    }
                    if(opt.show_percentage){
                        formatter+="[{percentage:.2f}%]"
                    }
                    return formatter
                })(_options)
                if(_options.legend_fcolor){
                	options.legend.itemStyle={
                			color:_options.legend_fcolor
                	}
                }
                if(_options.legend_bcolor){
                	options.legend.backgroundColor=_options.legend_bcolor
                }
                if(_options.background){
                    options.chart.backgroundColor=_options.background;
                }
                switch(_options.legend_position){
                    case 'left':
                        options.legend.align='left'
                        options.legend.verticalAlign='middle'
                        options.legend.layout= "vertical"
                        break;
                    case 'right':
                        options.legend.align='right'
                        options.legend.verticalAlign='middle'
                        options.legend.layout= "vertical"
                        break;
                    case 'bottom':
                        options.legend.align='right'
                        options.legend.verticalAlign='bottom'
                        options.legend.layout= "horizontal"
                        break;
                    case 'top':
                        options.legend.align='right'
                        options.legend.verticalAlign='top'
                        options.legend.layout= "horizontal"
                        break;
                    default:
                        break;
                }

                options.plotOptions.pie = {
                    "allowPointSelect": true,
                    "cursor": true,
                    "showInLegend": _options?_options.show_legend:true,
                    "innerSize": _options?_options.inner_size:"0%",
                    borderWidth: 0,
                    "dataLabels": {
                        enabled:_options?_options.data_labels:false,
                        format: (function(opt){
                            var formatter = "{point.name}";
                            if(opt==null){
                                return formatter
                            }
                            if(opt.show_labels){
                                formatter +="({point.y:.2f})"
                            }
                            if(opt.show_percentage){
                                formatter+="[{point.percentage:.2f}%]"
                            }
                            return formatter
                        })(_options),
                        style:{
                            fontWeight:"normal"
                        }
                    },
                    "tooltip": {
                        "headerFormat":"<strong>{point.key}</strong> : ",
                        "pointFormat": "<span style='text-decoration:italic'>{point.y}</span>"
                    }
                }
                $timeout(function(){
                     $scope.pie_chart=container.find(".chart-container").highcharts(options).highcharts();
                     $scope.pie_chart.reflow()
                },10)
                return 

            }


                $scope.component.extra.update = function(_data){
                    if(!verify_data($scope.component)){
                        draw_chart($element, null,$scope.component.options);
                        return;
                    }
                    var chart_data = []
                    queryService.xhr_fetch_expr($scope.component.chart_label.data.expr, $scope.component.chart_label.data.datasource)
                    .success(function(_label_data){
                        var label_data = _label_data.data
                        if(label_data!=null && label_data.length>0){
                            if($scope.component.chart_value.data.datasource==null || $scope.component.chart_value.data.expr==null){
                                var value_data = []
                                var iteration_num = $scope.component.options.max_slice<label_data.length?$scope.component.options.max_slice:label_data.length;
                                for(var i = 0 ; i<iteration_num;i++){
                                    var push_data = [];
                                    push_data[0] = label_data[i];
                                    if(value_data[i]!= null && value_data[i] >=0 && _(value_data[i]).toNumber(2)!=NaN){
                                        push_data[1] = _(value_data[i]).toNumber(2);
                                    }
                                    else{
                                        push_data[1] = 1;
                                    }
                                    chart_data.push(push_data);
                                }
                                draw_chart($element, chart_data,$scope.component.options);
                                return;
                            }
                             queryService.xhr_fetch_expr($scope.component.chart_value.data.expr, $scope.component.chart_value.data.datasource)
                             .success(function(_value_data){
                                 var value_data = _value_data.data;
                                var iteration_num = $scope.component.options.max_slice<label_data.length?$scope.component.options.max_slice:label_data.length;
                                for(var i = 0 ; i<iteration_num;i++){
                                    var push_data = [];
                                    push_data[0] = label_data[i];
                                    if(value_data[i]!= null && value_data[i] >=0 && _(value_data[i]).toNumber(2)!=NaN){
                                        push_data[1] = _(value_data[i]).toNumber(2);
                                    }
                                    else{
                                        push_data[1] = 0;
                                    }
                                    chart_data.push(push_data);
                                }

                                if(label_data.length>$scope.component.options.max_slice){
                                    var push_data = [];
                                    push_data[0] = 'Others'
                                    var tmp_sum = 0;
                                    for(var i = $scope.component.options.max_slice;i<label_data.length;i++){
                                        if(value_data[i]!=null && value_data[i]>0 && _(value_data[i]).toNumber(2)!=NaN){
                                            tmp_sum+=_(value_data[i]).toNumber(2);
                                        }
                                    }
                                    push_data[1] = Math.round(tmp_sum*100)/100
                                    chart_data.push(push_data);
                                }
                                draw_chart($element, chart_data,$scope.component.options);
                             })
                        }
                    })

                    
                    
                }
                
                $scope.component.extra.update();

                $scope.$on('component-reflow',function(){
                    $scope.pie_chart.reflow();
                })

            },
            link: function(scope, element, attrs){
                scope.component.chart_label.extra = scope.component.chart_label.extra||{}
                scope.component.chart_value.extra = scope.component.chart_value.extra||{}
                scope.component.chart_label.extra.chart = scope.component;
                scope.component.chart_value.extra.chart = scope.component;
                scope.component.components = [scope.component.chart_label, scope.component.chart_value] 
            }
        };
}])
.directive('pieProperties', [function(){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/pie.html',
            replace:true,
            controller: function($scope){
                $scope.backgroundCss = {background:$scope.component.options.background};
                $scope.legendCss = {color:$scope.component.options.legend_fcolor, background :$scope.component.options.legend_bcolor};

                $scope.toggle_pie_style = function(_options){
                    $scope.component.options = $scope.component.options||{};
                    if(_options == 0){
                        $scope.component.options.inner_size="0%"
                    }
                    else{
                        $scope.component.options.inner_size="60%"
                    }
                    $scope.component.extra.update()

                } 

                $scope.label_options = [
                    {
                        id:0,
                        name:'Show Labels Around Chart'
                    },
                    {
                        id:1,
                        name:"Don't Show Labels"
                    },
                    {
                        id:2,
                        name:'Show Labels in Legend'
                    }
                ]
                if(!$scope.bindColorEvent) {
                    $scope.bindColorEvent = true;
                    var offCall = $scope.$on('colorpicker-selected', function (event, colorObj) {
                            var type = event.targetScope.type;
                            if (type === 'legend') {
                                if (colorObj.name == 'fcolor') {
                                    $scope.component.options.legend_fcolor = colorObj.value;
                                }
                                else if (colorObj.name == 'bcolor') {
                                    $scope.component.options.legend_bcolor = colorObj.value;
                                }
                            }
                            else if (type === 'background') {
                                $scope.component.options.background = colorObj.value;
                            }
                            $scope.component.extra.update();
                    })
                    $scope.$on("$destroy", function(){
                        offCall();
                    })
                }


                $scope.change_label = function(){
                    $scope.component.options = $scope.component.options||{};
                    switch($scope.component.options.label){
                        case 1:
                            $scope.component.options.show_legend = false
                            $scope.component.options.data_labels=false
                            break;
                        case 0:
                            $scope.component.options.data_labels=true
                            $scope.component.options.show_legend = false 
                            break;
                        case 2:
                            $scope.component.options.show_legend = true; 
                            $scope.component.options.data_labels=false
                            break;
                        default:
                            break;
                    }
                        
$scope.component.extra.update();
                }

                $scope.change_show_labels = function(){
                    $scope.component.options = $scope.component.options||{}
                    $scope.component.options.show_labels = !$scope.component.options.show_labels;
                    $scope.component.extra.update();
                }

                $scope.change_show_percentage = function(){
                    $scope.component.options = $scope.component.options||{}
                    $scope.component.options.show_percentage = !$scope.component.options.show_percentage;
                    $scope.component.extra.update();
                }

            },
            link: function(scope, element, attrs){
               
            },
        };
}])

angular.module('widget.services')
.directive('resizer', ['$document',function($document){
    return {
        restrict:'E',
        replace:true,
        scope:{
            callbackstart:"&",
            callbackmoving:"&",
            callbackover:"&"
        },
        template: '<div class="resize-bar"></div>',
        controller: function(){

        },
        link: function(scope, element, attrs){

            var position = attrs['position'];
            var h_or_v = attrs['direction'];
            var max_string = attrs['maxoffset'];
            var resize_class = attrs['resizeclass'];
            var is_double = attrs['double']

            var resize_node_selector = attrs["resizenode"];
            var resize_node = element.parent();
            if(resize_node_selector!=null){
                resize_node = angular.element(resize_node_selector)
            }
            var max_offset = -1;
            var init_width = 0;
            var init_height = 0;
            scope.offset_x = 0;
            scope.offset_y = 0;
            if(max_string){
                if(!isNaN(max_string)){
                    max_offset = parseFloat(max_string);
                }
            }
            element.addClass(_.startsWith(h_or_v,'h')?'horizontal':'vertical')

            element.addClass( {'left': 'pl', "right":'pr',"top":"pt", "bottom":"pb"}[position]);
            element.addClass(resize_class)
            if(resize_node.css("position")!=('relative'||'absolute')){
                resize_node.css('position','relative')
            }
            var window_width = angular.element(window).width();

            var start_x, start_y ;
            
            element.on('mousedown', function(event) {
                init_width = resize_node.width();
                init_height = resize_node.height();
                event.preventDefault();
                start_x = event.pageX;
                start_y = event.pageY;
                $document.on('mousemove', mousemove);
                $document.on('mouseup', mouseup);
                scope.callbackstart()
            });

            function mousemove(event) {
                $document.find('body').addClass(_.startsWith(h_or_v,'h')?'e-resize':'s-resize');
                if (_.startsWith(h_or_v,'h')) {
                    // Handle h resizer
                    scope.offset_x = event.pageX - start_x;
                    if(_.startsWith(position,'r')){
                        if(max_offset<0 || scope.offset_x<max_offset){
                            if(is_double){
                                scope.offset_x*=2;
                            }
                            resize_node.css({
                                width:(init_width+scope.offset_x)+'px'
                            })
                        }
                    }
                    else{
                        if(max_offset<0||-scope.offset_x<max_offset){
                            if(is_double){
                                scope.offset_x*=2;
                            }
                            resize_node.css({
                                width: (init_width-scope.offset_x) +'px'
                            })
                        }
                    }

                }
                else{
                    scope.offset_y = event.pageY - start_y;
                    if(_.startsWith(position,"t")){
                        if(max_offset<0 || -scope.offset_y<max_offset){
                            if(is_double){
                                scope.offset_y*=2;
                            }
                            resize_node.css({
                                height: (init_height - scope.offset_y)+'px'
                            })
                        }
                    }
                }

                scope.callbackmoving({
                    offset_x : scope.offset_x,
                    offset_y : scope.offset_y
                });

            }

            function mouseup(event) {
                $document.find('body').removeClass('e-resize');
                $document.find('body').removeClass('s-resize');
                $document.unbind('mousemove', mousemove);
                $document.unbind('mouseup', mouseup);
                scope.callbackover({
                    offset_x : scope.offset_x,
                    offset_y : scope.offset_y
                });
            }

        }
    }
}])
angular.module('widget.components')
.directive('separatorComponent', ["$rootScope",function($rootScope){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/components/separator.html',
            replace:true,
            controller: function($scope){
               
            },
            link: function(scope, element, attrs){
            }
        };
}])
.directive('separatorProperties', [function(){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/separator.html',
            replace:true,
            controller: function($scope){

            },
            link: function(scope, element, attrs){
            },
        };
}])
angular.module( 'widget.components')
.directive('serieschartComponent', ['$rootScope','util','queryService','$timeout',"$q",function($rootScope,util,queryService, $timeout, $q){
        
        var verify_data = function(component){
            var x_axis = component.extra.x_axis;
            var series_collection = component.extra.series_collection;
            if(!x_axis.data){
                return false
            }
            if(x_axis.data.type=='data' && x_axis.data.datasource ==""){
                return false;
            }
            return true;
        }
        
        return {
            restrict:'AE',
            templateUrl:'/templates/edit/components/series-chart.html',
            controller: function($scope,$element){
                
                var draw_chart = function(container, chart_data, _options){
                    if(chart_data && chart_data.stream){
                        Highcharts.setOptions({
                            global : {
                                useUTC : false
                            }
                        });
                    }
                    var options = util.charts_options();
                    options.legend = {
                        enabled: _options?_options.show_legend:true,
                        align: "right",
                        verticalAlign: "top",
                        layout: "horizontal",
                        itemStyle: {
                            fontWeight: "normal"
                        }
                    },
                    options.chart.type='line';

                    options.chart.inverted = _options?_options.inverted:false;
                    if(chart_data && chart_data.stream!=true) {

                        options.xAxis = {
                            categories: chart_data ? chart_data.xAxis.data : [1],
                            title: {
                                enabled: chart_data ? chart_data.xAxis.options.show_axis : false,
                                text: chart_data ? chart_data.xAxis.options.axis_title : null
                            },
                            labels: {
                                enabled: chart_data ? chart_data.xAxis.options.show_label : true,
                                style: {
                                    color: chart_data ? chart_data.xAxis.options.axis_color : "#000"
                                }
                            },
                            gridLineWidth: chart_data && chart_data.xAxis.options.show_grid ? 1 : 0
                        };

                        if (chart_data && chart_data.xAxis.options.show_tick == false) {
                            options.xAxis.tickLength = 0;
                        }
                        if (chart_data && chart_data.xAxis.options.rotation != 0) {
                            options.xAxis.labels.rotation = chart_data.xAxis.options.rotation;
                        }
                        if (chart_data && chart_data.xAxis.options.color) {
                            options.xAxis.labels.color = chart_data.xAxis.options.color;
                        }
                    }


                    if(chart_data && chart_data.stream){
                        options.rangeSelector = {
                            buttons: [{
                                count: 1,
                                type: 'minute',
                                text: '1M'
                            }, {
                                count: 5,
                                type: 'minute',
                                text: '5M'
                            }, {
                                type: 'all',
                                text: 'All'
                            }],
                            inputEnabled: false,
                            selected: 0
                        }
                    }
                    options.series = chart_data?chart_data.series:[
                        {
                            name:'empty',
                            data:[0]
                        }
                    ]

                    options.legend.labelFormat = (function(opt){
                        var formatter = "{name}";
                        if(opt==null){
                            return formatter
                        }
                        if(opt.show_labels){
                            formatter +="({y:.2f})"
                        }
                        if(opt.show_percentage){
                            formatter+="[{percentage:.2f}%]"
                        }
                        return formatter
                    })(_options)
                    
                    if(_options.legend_fcolor){
                    	options.legend.itemStyle={
                    			color:_options.legend_fcolor
                    	}
                    }
                    
                    if(_options.legend_bcolor){
                    	options.legend.backgroundColor=_options.legend_bcolor
                    }

                    if(_options.background){
                        options.chart.backgroundColor = _options.background;
                    }

                    if(chart_data && chart_data.loadCallback){
                        options.chart.events = {
                            load: function(){
                                var chart_obj = this
                                chart_data.loadCallback(chart_obj);
                            }
                        }
                    }
                    if($scope.intervals){
                        $scope.intervals.forEach(function(interval){
                            clearInterval(interval);
                        })
                    }
                    $timeout(function(){

                        if(chart_data && chart_data.stream){
                            if($scope.series_chart){
                                container.find(".chart-container").highcharts().destroy();
                            }
                            $scope.series_chart =null;
                            $scope.series_chart = container.find(".chart-container").highcharts('StockChart', options);
                        }
                        else {
                            $scope.series_chart = container.find(".chart-container").highcharts(options).highcharts();
                            $scope.series_chart.reflow();
                        }
                    },10)
                }


                $scope.$on('component-reflow',function(){
                    $scope.series_chart.reflow();
                })

                $scope.query_update = function(){
                    var chart_data = {}
                    queryService.xhr_fetch_expr($scope.component.extra.x_axis.data.expr,$scope.component.extra.x_axis.data.datasource)
                        .success(function(_x_data){
                            var x_data = _x_data.data;
                            if(x_data.length>0){

                                chart_data.series = [];
                                var x_axis_data = x_data;
                                chart_data.xAxis = {
                                    data:x_axis_data,
                                    options: $scope.component.extra.x_axis.options
                                };
                                var promises = [];
                                angular.forEach($scope.component.extra.series_collection, function(series){
                                    if(series.data.expr!="" && series.data.datasource!=""){

                                        promises.push(queryService.xhr_fetch_expr(series.data.expr,series.data.datasource).success(function(_series_data){
                                            if($scope.component.extra.x_axis.data.type==='time'){
                                                if(_series_data.data.dataSource.type!=='NIFI'){
                                                    alert('time series chart should use stream data !');
                                                    return;
                                                }
                                            }
                                            var series_data = _series_data.data;
                                            var result_data = [];
                                            if(series_data.length!=x_data.length){
                                                for(var i = 0 ; i<x_data.length;i++){
                                                    if(series_data[i]!=null && _(series_data[i]).toNumber(2)!=NaN){
                                                        result_data[i] = _(series_data[i]).toNumber(2) ;
                                                    }
                                                    else{
                                                        result_data[i] = 0;
                                                    }
                                                }
                                            }
                                            else{
                                                result_data = series_data.map(function(data){
                                                    if(_(data).toNumber(2)!=NaN){
                                                        return _(data).toNumber(2);
                                                    }
                                                    else{
                                                        return 0;
                                                    }
                                                });
                                            }

                                            if($scope.component.extra.x_axis.options.sort!=0){
                                                var order= $scope.component.extra.x_axis.options.sort;
                                                var sort_obj = []
                                                for(var i = 0 ;i<x_data.length;i++){
                                                    sort_obj.push({key:x_data[i],value:result_data[i]})
                                                }
                                                sort_obj.sort(function(a,b){
                                                    return a.key>b.key?order:-order
                                                })

                                                result_data = []
                                                x_axis_data = []
                                                for(var i =0 ;i<x_data.length;i++){
                                                    x_axis_data.push(sort_obj[i].key)
                                                    result_data.push(sort_obj[i].value);
                                                }
                                            }



                                            var series_obj = {
                                                type:series.style,
                                                name:series.name,
                                                data: result_data,
                                                dataLabels: {
                                                    enabled:series.options.show_value,
                                                    style: {
                                                        color: "#717171",
                                                        fontSize: "8px",
                                                        fontWeight:"normal"
                                                    },
                                                }
                                            }

                                            if(series.options.use_custom_color){
                                                series_obj.color = series.options.custom_color;
                                            }
                                            chart_data.series.push(series_obj);
                                        }))
                                    }})

                                $q.all(promises).then(function(){
                                    draw_chart($element, chart_data, $scope.component.options);
                                })
                            }
                            else{
                                draw_chart($element, null, $scope.component.options);
                                return;
                            }

                        })


                }

                $scope.stream_update = function(){
                    var chart_data = {};
                    chart_data.stream = true;
                    chart_data.series = [];
                    angular.forEach($scope.component.extra.series_collection, function(series){


                        var series_obj= {
                            name:series.name,
                            style:series.style,
                            datasource:series.datasource,
                            dataLabels: {
                                enabled:series.options.show_value,
                                style: {
                                    color: "#717171",
                                    fontSize: "8px",
                                    fontWeight:"normal"
                                }
                            },
                            data: (function () {
                                // generate an array of random data
                                var data = [], time = (new Date()).getTime(), i;

                                for (i = -999; i <= 0; i += 1) {
                                    data.push([
                                        time + i * 1000,
                                        0
                                    ]);
                                }
                                return data;
                            }())
                        }

                        chart_data.series.push(series_obj);
                    });

                    chart_data.loadCallback = function(chart_obj){
                        chart_data.series.forEach(function(series_obj, index){
                            if(!$scope.interval){
                                $scope.intervals = [];
                            }
                            var current_series_component = $scope.component.extra.series_collection[index];
                            var interval = setInterval(function () {
                                var x = (new Date()).getTime(), // current time
                                    y = 0;
                                if(current_series_component.data.datasource) {
                                    queryService.xhr_fetch_data(current_series_component.data.datasource).success(function (data) {
                                        if (chart_obj.options) {
                                            y = data.data[0]
                                            if(angular.isArray(y)){
                                                y = y[0]

                                            }
                                            if(!isNaN(y)){
                                                y = parseInt(y);
                                            }
                                            chart_obj.series[index].addPoint([x, y], true, true);
                                        }
                                    })
                                }
                                else{
                                    if(chart_obj.options) {
                                        chart_obj.series[index].addPoint([x, y], true, true);
                                    }
                                }

                            }, 3000);

                            $scope.intervals.push(interval);
                        });

                    }
                    draw_chart($element, chart_data, $scope.component.options);
                    
                }
                $scope.component.extra.update = function(){
                    if(!verify_data($scope.component)){
                        draw_chart($element, null, $scope.component.options);
                        return;
                    }

                    if($scope.component.extra.x_axis.data.type==='time'){
                        if($scope.init_stream_chart){
                            
                        }
                        else {
                            $scope.init_stream_chart=  true;
                            $scope.stream_update();
                        }
                    }
                    else{
                        $scope.query_update();
                    }
                }


                angular.forEach($scope.component.components, function(component){
                    component.extra = component.extra||{}
                    component.extra.parent = $scope.component;
                    if(component.type=='series_collection'){
                        $scope.component.extra.series_collection = component.components;
                    }
                    else if(component.type == 'y_axis_collection'){
                        $scope.component.extra.y_axis_collection = component.components;
                    }
                    else{
                        $scope.component.extra.x_axis = component;
                        component.extra.chart_component = $scope.component;
                    }
                })


                $scope.component.extra.update();
            },
            link: function(scope, element, attrs){

            }
        };
}])
.directive('serieschartProperties', ['$rootScope','util','$timeout',function($rootScope, util, $timeout){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/series-chart.html',
            replace:true,
            controller: function($scope){
               $scope.change_show_legend = function(){
                    $scope.component.options.show_legend = !$scope.component.options.show_legend;
                    $scope.component.extra.update() 
               }

               $scope.change_inverted = function(){
                    $scope.component.options.inverted = !$scope.component.options.inverted;
                    $scope.component.extra.update() 
               }
               
               $scope.$on('colorpicker-selected', function(event, colorObj){
                   var type = event.targetScope.type;
                   if(type==='legend') {
                       if (event.targetScope.title != 'Axis Colors') {
                           if (colorObj.name == 'fcolor') {
                               $scope.component.options.legend_fcolor = colorObj.value;
                           }
                           else if (colorObj.name == 'bcolor') {
                               $scope.component.options.legend_bcolor = colorObj.value;
                           }
                       }
                   }
                   else if(type==='background'){
                       $scope.component.options.background=colorObj.value;
                   }
                   $scope.component.extra.update();
               })
               
               $scope.add_series = function(){
                    var new_series_component = {
                        type:'series',
                        ctrl_id:util.uuid(),
                        style:'line',
                        name:'untitled',
                        axis:'untitled',
                        extra:{
                            chart_component:$scope.component
                        },
                        options:{
                            line:{},
                            bar:{},
                        },
                        data:{
                            type:'data',
                            datasource:'',
                            expr:'',
                        }
                    }
                    $scope.component.extra.series_collection.push(new_series_component)
                    $timeout(function(){
                        $rootScope.selected_component = new_series_component;
                    },0);
                    
                }

                $scope.add_y_axis = function(){

                }


            },
            link: function(scope, element, attrs){
               
            },
        };
}])
angular.module( 'widget.components')
.directive('seriesComponent',[function(){
    return {
        restrict:'AE',
        controller: function($scope,$element){

        },
        link: function(scope, element, attrs){

        }
    };
}])
.directive('seriesProperties',[function(){
    return {
        restrict:'AE',
        templateUrl:'/templates/edit/properties/series.html',
        controller: function($scope,$element){

        },
        link: function(scope, element, attrs){
            scope.component.extra.update = function(){
                scope.component.extra.chart_component.extra.update();
            }
            scope.toggle_line_chart = function(){
                scope.component.style='line';
                scope.component.extra.update()
            }

            scope.toggle_bar_chart = function(){
                scope.component.style='column';
                scope.component.extra.update()
            }

            scope.change_showvalue = function(){
                scope.component.options.show_value = !scope.component.options.show_value;
                scope.component.extra.update();
            }
            scope.update_label = function(){
                scope.component.extra.update();
            }
            scope.use_custom_color = function(){
                scope.component.options.use_custom_color = !scope.component.options.use_custom_color;
                scope.component.extra.update();
            }
            /*scope.$on("colorpicker-selected", function(evt, colorObj){
                console.log(colorObj)
                scope.component.options.custom_color=colorObj.value;
                scope.component.extra.update();
            })*/

            scope.change_color = function(){
                scope.component.extra.update();
            }

        }
    };
}])
.directive('seriescollectionComponent', [function(){
    return {
        restrict:'AE',
        template:'<div><ul><li ng-repeat="item in component.components"><x-inline-component component="item"></x-inline-component></li></ul></div>',
        controller: function($scope,$element){
        },
        link: function(scope, element, attrs){
            var chart_component = scope.component.extra.parent;
            angular.forEach(scope.component.components, function(component){
                component.extra=component.extra||{}
                component.extra.chart_component = chart_component;
            })
        }
    };
}])

angular.module('widget.components'). 
directive('tabs', function() { 
    return { 
      restrict: 'E', 
      transclude: true, 
      link:function(scope, element, attrs){
          scope.pane_style = {
              height: "280px"
          }
      },
      controller: [ "$scope", function($scope) { 
        var panes = $scope.panes = []; 
  
        $scope.select = function(pane) { 
          angular.forEach(panes, function(pane) { 
            pane.selected = false; 
          }); 
          pane.selected = true; 
        } 
  
        this.addPane = function(pane) { 
          if (panes.length == 0) $scope.select(pane); 
          panes.push(pane); 
          if(pane.active=='true'){
            $scope.select(pane)
          }
        } 

        $scope.resize_properties_panel_moving = function(offset_x, offset_y){

            var properties_panel = angular.element('.tab-content');
            var edit_panel = angular.element('.edit-panel')
            edit_panel.css('bottom', properties_panel.height())
            //$scope.properties_init_height = (parseInt($scope.properties_init_height)-offset_y)+"px";
            //console.log($scope.pro)
        }

        $scope.resize_properties_panel_over = function(offset_x, offset_y){
            $scope.properties_init_height = $scope.properties_init_height-offset_y;
        }

      }], 
      template: 
        '<div class="tabbable">' + 
          '<ul class="nav nav-tabs">' + 
            '<li ng-repeat="pane in panes" ng-class="{active:pane.selected}">'+ 
              '<a href="" ng-click="select(pane)">{{pane.title}}</a>' + 
            '</li>' + 
          '</ul>' + 
          '<div class="tab-content" ng-style="pane_style">'+
          '<x-resizer position="top" direction="vertical" maxoffset="200" resizeclass="transparent" callbackmoving="resize_properties_panel_moving(offset_x, offset_y)" callbackover="resize_properties_panel_over(offset_x, offset_y)"></x-resizer>'+
          '<div style="height:100%;" ng-transclude></div>'+
          '</div>' + 
        '</div>', 
      replace: true 
    }; 
  }). 
  directive('pane', function() { 
    return { 
      require: '^tabs', 
      restrict: 'E', 
      replace:true,
      transclude: true, 
      scope: { title: '@' , active:'@'}, 
      link: function(scope, element, attrs, tabsCtrl) { 
        tabsCtrl.addPane(scope); 
      }, 
      template: 
        '<div class="tab-pane" ng-class="{active: selected}" ng-transclude>' + 
        '</div>', 
      replace: true 
    }; 
  })
angular.module('widget.components')
.directive('tableComponent', ['util', function(util){
        return {
            restrict:'AE',
            replace:true,
            templateUrl: '/templates/edit/components/table.html',
            controller: function($scope){
                 
            },
            link: function(scope, element, attrs){
                //scope.table = {}
                angular.forEach(scope.component.components, function(column){
                    column.extra = column.extra||{}
                    column.extra.table = scope.component;
                })

            },

        };
}])
.directive('tableProperties', ['util',function(util){
        return {
            restrict:'AE',
            replace:true,
            templateUrl: '/templates/edit/properties/table.html',
            controller: function($scope){
                $scope.rows_options = [
                   {
                        id:0,
                        name:'Show At Most : '
                   } ,
                   {
                        id:1,
                        name:'Show Exactly : '
                   },
                   {
                        id:2,
                        name:'Show All '
                   }

                ]
            },
            link: function(scope, element, attrs){
                //scope.table = {}

                             

                scope.increase_cols = function(){
                    scope.component.total_cols+=1;
                    var column = {
                        title:'untitled',
                        name:'column',
                        type:'column',
                        ctrl_id:util.uuid(),
                        data:[],
                        extra:{},
                    }
                    column.extra.table = scope.component;
                    scope.component.components.push(column);
                }

                scope.decrease_cols = function(){
                    scope.component.total_cols-=1;
                    scope.component.components.pop();
                }

                scope.increase_row_num = function(){
                    scope.component.row_num +=1;
                }

                scope.decrease_row_num = function(){
                    if(scope.component.row_num>1){
                        scope.component.row_num-=1;
                    }
                    else{
                        scope.component.row_num = 1;
                    }
                }
            },

        };
}])

angular.module('widget.components')
.directive('componentsTree', ["$compile","$rootScope","renderService",function($compile, $rootScope, renderService){
    return {
        replace:true,
        templateUrl:'/templates/edit/components/tree.html',
        controller: function($scope){
            $scope.icon_selector = {
                'widget':'fa fa-object-group',
                'grid':'fa fa-th-large',
                'cell':'fa fa-hashtag',
                'label':'fa fa-font',
                'value_pair':'fa fa-cc',
                'pie':'fa fa-pie-chart',
                'separator':'fa fa-scissors',
                'html_template':'fa fa-code',
                'image':'fa fa-picture-o',
                'table':'fa fa-table',
                'chart_label':'fa fa-cog',
                'chart_value':'fa fa-cog',
                'series_chart':'fa fa-bar-chart',
                'series_collection':'fa fa-list',
                'y_axis_collection':'fa fa-list',
                'series':'fa fa-cog',
                'x_axis':'fa fa-cog',
                'y_axis':'fa fa-cog'
            }

            $scope.collapsed = false;
            $scope.highlight = false;
            $scope.toggle_select = function(component){
                 if($rootScope.selected_component != null && $rootScope.selected_component.ctrl_id == component.ctrl_id){
                        //$rootScope.selected_component = null;
                    }
                    else{
                        $rootScope.selected_component = component;
                    }
            }
            $scope.remove_component = function(widget){
                if(confirm('Are You Sure? ')){
                    renderService.remove(widget.ctrl_id);
                }   
            }
            $scope.over_close = function(){
            	$scope.highlight = true;
            }
            $scope.out_close= function(){
            	$scope.highlight = false;
            }
            
        },
        link: function(scope,element,attrs){
            //if(scope.root==='true'){
                //$rootScope.treescope = scope;
        },

        compile: function(tElement, tAttr) {
            var contents = tElement.contents().remove();
            var compiledContents;
            return function(scope, iElement, iAttr) {
                if(scope.root === 'true'){
                    $rootScope.treescope = scope;
                }
                if(!compiledContents) {
                    compiledContents = $compile(contents);
                }
                compiledContents(scope, function(clone, scope) {
                    iElement.append(clone); 
                });
            };
        },

        scope:{
            widget:'=',
            root:'@'
        }
    }
}])

angular.module('widget.components')
.directive('inlineComponent',['renderService', 'util','$compile', function(renderService,util, $compile){
    return {
        restrict:'E',
        replace:true,
        link: function(scope, element, attrs){
            scope.component.ctrl_id = util.uuid();
            scope.component.extra = scope.component.extra||{}
            var type = scope.component.type;
            if(type!==null){
                var type_name = type.replace(/[_]+/,'')+"_component"
                element.parent().append($compile(angular.element("<"+type_name+"></"+type_name+">"))(scope))
                element.remove();
            }
        },
        scope: {
            component:"="
        }
    }
}])
.directive('propertiesComponent',['queryService','$rootScope','$compile','util',function(queryService, $rootScope, $compile,util){
    return {
        restrict:"AE",
        replace:true,
        controller: function($scope,$element){
            $rootScope.$watch("selected_component", function(data){
                if(data!=null){
                    var type = data.type;
                    if(type){
                        var type_name = type.replace(/[_]+/,'')+"_properties"
                        $scope.component= data
                        var _html = $compile(angular.element("<"+type_name+"></"+type_name+">"))($scope)
                        $element.html(_html)
                    }
                }
            })


        },
        link: function(scope, element, attrs){
            scope.properties_init_height = 180;
        },
        scope:true
    }
}])
.directive('modelSuffix', [function() {
  return {
    restrict: 'AE',
    require: '^ngModel',
    link: function(scope, element, attributes, ngModelController) {
          var suffix = attributes.modelSuffix;
          // Pipeline of functions called to read value from DOM 
          ngModelController.$parsers.push(function(value) {
            return value==null?value:value + suffix;
          });

          // Pipeline of functions called to display on DOM
          ngModelController.$formatters.push(function(value) {
            return value==null?value:value.replace(suffix, '');
          });
        }
  }
}])
.directive('integer', function(){
    return {
        require: 'ngModel',
        link: function(scope, ele, attr, ctrl){
            ctrl.$parsers.unshift(function(viewValue){
                var clean = viewValue.replace( /[^0-9]+/g, '');
                if (viewValue !== clean) {
                  ctrl.$setViewValue(clean);
                  ctrl.$render();
                }
                else{
                    return parseInt(viewValue, 10);
                }
            });

        }
    };
});
angular.module('widget.components')
.directive('valuepairComponent', ["$rootScope",function($rootScope){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/components/valuepair.html',
            controller: function($scope){
                if($scope.component.components.length>1) {
                    $scope.key_component = $scope.component.components[0];
                    $scope.value_component = $scope.component.components[1];
                }
            },
            link: function(scope, element, attrs){

            }
        };
}])
.directive('valuepairProperties', [function(){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/valuepair.html',
            replace:true,
            controller: function($scope){

            },
            link: function(scope, element, attrs){
            }
        };
}])
angular.module( 'widget.components')
.directive('xaxisComponent',[function(){
    return {
        restrict:'AE',
        controller: function($scope,$element){

        },
        link: function(scope, element, attrs){
            scope.component.extra.update = function(){
                scope.component.extra.chart_component.extra.update();
            }
                        
        }
    };
}])
.directive('xaxisProperties', [function(){
    return {
        restrict:'AE',
        templateUrl:'/templates/edit/properties/x-axis.html',
        controller: function($scope,$element){
            $scope.sort_order = [
                {
                    id:0,
                    name:"Don't Sort"
                },
                {
                    id:1,
                    name:'Ascending'
                },
                {
                    id:-1,
                    name:'Descending'
                }
                

            ]

            $scope.use_time_series = function(){
                if($scope.component.data.type =='data'){
                    $scope.component.data.type = 'time';
                }
                else{
                    $scope.component.data.type = 'data'
                }
                $scope.component.extra.update();
            }
            $scope.change_show_label = function(){
                $scope.component.options.show_label = !$scope.component.options.show_label;
                $scope.component.extra.update();
            }

            $scope.change_show_axis = function(){
                $scope.component.options.show_axis = !$scope.component.options.show_axis;
                $scope.component.extra.update();
            }

            $scope.$on('colorpicker-selected', function(event, colorObj){
            	$scope.component.options.axis_color = colorObj.value;
            	$scope.component.extra.update();
            })
            $scope.change_rotate = function(){
                $scope.component.options.rotation = ($scope.component.options.rotation+45)%360;
                $scope.component.extra.update();
            }

            $scope.change_show_tick = function(){
                $scope.component.options.show_tick = !$scope.component.options.show_tick;
                $scope.component.extra.update();
            }
            $scope.change_show_grid = function(){
                $scope.component.options.show_grid = !$scope.component.options.show_grid;
                $scope.component.extra.update();
            }

        },
        link: function(scope, element, attrs){
            
        }
    };
}])
angular.module( 'widget.components')
.directive('yaxisComponent',[function(){
    return {
        restrict:'AE',
        controller: function($scope,$element){
            
        },
        link: function(scope, element, attrs){
            
        }
    };
}])
.directive('yaxiscollectionComponent', [function(){
        return {
            restrict:'AE',
            templateUrl:'<div><ul><li ng-repeat="item in component.components"><x-inline-component component="item"></x-inline-component></li></ul></div>',
            controller: function($scope,$element){
            },
            link: function(scope, element, attrs){
            }
        };
}])


angular.module('widget', ['widget.services', 'widget.components'], angular.noop)
.run(["$rootScope",function($rootScope){
    //$rootScope.context_url = angular.element("#context_url").val();
    //$rootScope.current_user_id = angular.element("#usr_id").val();
}])
.controller('controller', 
    [
    '$rootScope',
    '$scope',
    'queryService',
    'initService',
    'util',
    '$element',
    function(
        $rootScope,
        $scope, 
        queryService,
        initService,
        util, $element){
        $scope.widget_id = angular.element("#widget_id").val();
        initService.init_operation($scope);
        $scope.load = function(){
        }

        this.switch_selected_component = function(component){
            $scope.selected_component = component;
        }

        $scope.save= function(){
            //!important : must send heartbeat ping before wash.
        	$scope.save = function(){}
        	$scope.loading = true;
            var widget = $scope.widget;
            var _widget = angular.copy(widget);
            var new_height = Math.ceil(angular.element('.editing-widget').height()/200);
            util.wash(_widget);
            _widget.components = JSON.stringify(_widget.components);
            if($scope.mode === 'new'){
                queryService.xhr_new_widget(_widget).success(function(data){
                	 window.location.href="/widget/home/"
                })
            }
            else {
                queryService.xhr_update_widget(_widget).success(function (data) {
                    window.location.href="/widget/home/"
                }).error(function (data) {
                    alert('Save Error, see the console for detail.');
                })
            }
            
            
        }


        $scope.hide_scrollbar = function(a, b){
            angular.element('.nicescroll-cursors').hide();
        }

        $scope.show_scrollbar = function(a, b){
            angular.element('.nicescroll-cursors').show();
        }

        $scope.reflow = function(){
            $scope.$broadcast('component-reflow')
        }
}])

var post_wrapper = function($httpProvider){
	// Use x-www-form-urlencoded Content-Type
	  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

	  /**
	   * The workhorse; converts an object to x-www-form-urlencoded serialization.
	   * @param {Object} obj
	   * @return {String}
	   */ 
	  var param = function(obj) {
	    var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

	    for(name in obj) {
	      value = obj[name];

	      if(value instanceof Array) {
	        for(i=0; i<value.length; ++i) {
	          subValue = value[i];
	          fullSubName = name + '[' + i + ']';
	          innerObj = {};
	          innerObj[fullSubName] = subValue;
	          query += param(innerObj) + '&';
	        }
	      }
	      else if(value instanceof Object) {
	        for(subName in value) {
	          subValue = value[subName];
	          fullSubName = name + '[' + subName + ']';
	          innerObj = {};
	          innerObj[fullSubName] = subValue;
	          query += param(innerObj) + '&';
	        }
	      }
	      else if(value !== undefined && value !== null)
	        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
	    }

	    return query.length ? query.substr(0, query.length - 1) : query;
	  };

	  // Override $http service's default transformRequest
	  $httpProvider.defaults.transformRequest = [function(data) {
	    return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
	  }];	
};

angular.module('widget.services')
    .factory("conditionService",[function(){

        var ifConditionMap = {
            "GT":' > ',
            "GTE" : ' >= ',
            "LT":' < ',
            "LTE":' <= ',
            "EQ":' == ',
            "NEQ" : ' != '
        }

        var thenConditionMap = {
            "Background":''
        }

        return {

            parseCondition: function(condition){
                    var ifString = "if( (1==1) "
                    var ifs = condition._ifs;
                    if(!ifs) return;

                    angular.forEach(ifs, function(_if){
                        ifString+=" &&( ";

                        ifString+="<%=_if%>";
                        ifString+=ifConditionMap[_if._if];
                        ifString+=ifConditionMap[_if._value];
                        ifString+=" )";
                    })

                    ifString += " )";

                    var thenString = " { ";
                    thenString += " <%=_then%> ";
                    thenString += " } ";

                return ifString+thenString;
            }

        };
    }]);
angular.module("widget.services").config(['$httpProvider',function(a){
    a.defaults.headers.put['Content-Type'] = 'application/json'
    a.defaults.xsrfCookieName = 'csrftoken';
    a.defaults.xsrfHeaderName = 'X-CSRFToken';
    //post_wrapper(a);
}])
.factory("queryService",["$rootScope","$http", function($rootScope, $http){
   
    var widget = function(widget_id){
        return $http({
            url:'/api/widgets/'+widget_id+"/",
        })
    }
    var new_widget = function(widget){
        return $http({
            url:'/api/widgets/',
            method:'post',
            data: widget
        })
    }
    var update_widget = function(widget){
        /*widget.coord_x = widget.sizeX;
        widget.coord_y = widget.sizeY;
        widget.height = widget.row;
        widget.width = widget.col*/
    	widget.isValid = true
        return $http({
            url:'/api/widgets/'+widget.id+"/",
            method:'post',
            data: widget
        })
    }

   
    var fetch_expr = function(expr, uid){
        var request_param="";
    	if(expr.column_start_index!=null){
    		request_param+=("&column_start_index="+expr.column_start_index);
    	}
    	if(expr.column_end_index!=null){
    		request_param+=("&column_end_index="+expr.column_end_index);
    	}
    	if(expr.row_start_index!=null){
    		request_param+=('&row_start_index='+expr.row_start_index);
    	}
    	if(expr.row_end_index!=null){
    		request_param+=('&row_end_index='+expr.row_end_index);
    	}
        return $http({
            url:'/api/datasources/'+uid+'/data/?flat=true'+request_param
        })//column_start_index=1&column_end_index=10&row_start_index=M&row_end_index=N
    }

    var datasources = function(){
        return $http({
            url:'/api/datasources/'
        })
    }

    var widget_datasources = function(widget_id){
    	return $http({
    		url:'/api/widgets/'+widget_id+"/datasources"
    	})
    }
    
    var datasources_detail = function(datasource_id_list){
    	var requestParam = "";
    	datasource_id_list.forEach(function(datasource_id){
    		requestParam+=("uids="+datasource_id+"&");
    	})
    	return $http({
    		url:'/api/datasources/list/?'+requestParam
    	})
    }
    var sample_data = function(id){
    	return $http({
    		url:'/api/datasources/'+id+'/data'
    	})
    }
    var fetch_data = function(uid){
        //
        return $http({
            url:'/api/datasources/'+uid+'/'
        })
    }

    var attach_datasource = function(widget_id, datasource_uid){
    	///api/widgets/{widget_id}/datasources/{datasource_id}
        return $http({
            url:'/api/widgets/'+widget_id+"/datasources/"+datasource_uid+"/",
            method:'post',
        })
    }

    return {
        xhr_widget: function(id){
            return widget(id);
        },
        xhr_get_dashboard_by_widget: function(uid) {
            return get_dashboard_by_widget(uid);
        },
        xhr_new_widget: function(widget){
            return new_widget(widget);
        },
        xhr_update_widget: function(widget){
            return update_widget(widget);
        },
        xhr_fetch_expr : function(expr,uid){
            return fetch_expr(expr, uid);
        },
        xhr_datasources: function(uid){
            return datasources(uid);
        },
        xhr_fetch_data : function(uid){
            return sample_data(uid)
        },
        xhr_attach_datasource: function(widget_uid, datasource_uid){
            return attach_datasource(widget_uid, datasource_uid);
        },
        xhr_sample_data: function(id){
        	return sample_data(id)
        },
        xhr_widget_datasources: function(widget_id){
        	return widget_datasources(widget_id)
        },
        xhr_datasources_detail: function(datasources_id_list){
        	return datasources_detail(datasources_id_list);
        }
    };
}]) ;
angular.module('widget.services')
.factory("renderService",['$rootScope','$compile',function($rootScope, $compile){
    function remove_by_id(widget, ctrl_id){
        for(var i = 0 ;i<widget.components.length; i++){
            var component = widget.components[i];
            if(component.ctrl_id == ctrl_id){
                // remove
                widget.components.splice(i, 1);
            }

            if(component.cells!=null){
                for(var j = 0;j<component.cells.length; j++){
                    var cell = component.cells[j];
                    remove_by_id(cell,ctrl_id);
                }
            }
        }
       
    }
    return {
        render:function(scope){
            //return $dom
            
        },

        render_properties:function(component){
            //return $dom
            if(component == null){
                return;
            }
            var type = component.type;
            if(type!==null){
                var type_name = type.replace(/[_]+/,'')+"_properties"
                var new_scope = $rootScope.$new(true)
                new_scope.component = component
                return $compile(angular.element("<"+type_name+"></"+type_name+">"))(new_scope)
            }
        },

        remove: function(ctrl_id){
            if($rootScope.widget){
                remove_by_id($rootScope.widget, ctrl_id)
            }
        }

    };
}]);
angular.module("widget.services")
.factory("util",['$cacheFactory',function($cacheFactory){
    function wash_cell(cell){
        if(cell.type!='cell'){
            return;
        }
        delete cell.right_cell;
        delete cell.left_cell;
        delete cell.up_cell;
        delete cell.down_cell;
    }

    function wash_component(component){
        if(component.index ==null || isNaN(component.index)){
          component.index = 0
        }
        delete component.ctrl_id;
        delete component.last_modified;
        delete component.$$hashkey;
        delete component.version;
        delete component.extra;
        delete component.display_value
        if(component.type=='pie'){
            delete component.components;
        }
    }
    function wash_other(widget){
        return;
    }

    function _wash(_widget){
        if(_widget==null){
          return;
        }

        if(_widget.components!=null){
          for(var i =0 ;i<_widget.components.length;i++){
              _wash(_widget.components[i]);
          }
        }

        if(_widget.cells!=null){
          for(var  i =0 ;i<_widget.cells.length;i++){
              _wash(_widget.cells[i]);
          }
        }

        if(_widget.type =='value_pair'){
            _wash(_widget.primary);
            _wash(_widget.secondary);
        }

        wash_component(_widget);
        wash_cell(_widget);
        wash_other(_widget);
        return _widget;
    }

    function getRandom(max) {
        return Math.random() * max;
    }

    function __uuid(){
        var id = '', i;

        for(i = 0; i < 36; i++)
        {
          if (i === 14) {
            id += '4';
          }
          else if (i === 19) {
            id += '89ab'.charAt(getRandom(4));
          }
          else if(i === 8 || i === 13 || i === 18 || i === 23) {
            id += '-';
          }
          else
          {
            id += '0123456789abcdef'.charAt(getRandom(16));
          }
        }
        return id;
    }

    var cache = $cacheFactory('cacheId');
    return {
        wash: function(widget){
            return _wash(widget);
        },
        uuid: function(){
            return __uuid();
        },
        cache: {
            get: function(key){
                return cache.get(key)
            },
            put: function(key , value){
                cache.put(key, value)
            }
        },
        credits: {
            "enabled": false
        },
        charts_options : function(){
            return {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    backgroundColor: 'rgba(0,0,0,0)',
                    animation:false,
                    "borderWidth": 1,
                    "borderColor": "rgba(0,0,0,0)"
                },
                title: {
                    text: ''
                },
                "legend": {
                    "align": "right",
                    "verticalAlign": "middle",
                    "layout": "vertical",
                    "itemStyle": {
                        "fontWeight": "normal"
                    }
                },
                "colors": [
                    "#497bac",
                    "#a63600",
                    "#fb7737",
                    "#8a6060",
                    "#2584ac",
                    "#29b4b8",
                    "#344a60",
                    "#00437a",
                    "#96b60d",
                    "#b6cbdf"
                ],
                credits: {
                    enabled: false
                },
                plotOptions: {
                    dataLabels: {
                        style: {
                            color: "#717171",
                            fontSize: "10px",
                            fontWeight:"normal"
                        },
                        enabled:true
                    },

                    pie: {
                        dataLabels: {
                            enabled: false
                        },
                        shadow: false,
                        center: ['50%', '50%'],
                        borderWidth: 0 // < set this option
                    },
                    series: {
                        states: {
                            hover: {
                                enabled: false,
                                halo: {
                                    size: 0
                                }
                            }
                        }
                    },
                    "column": {
                        "borderWidth": 0
                    }

                },
                series: [{
                    colorByPoint: true,

                    data: []
                }]

            }
        }

    };
}]);
angular.module("widget.services")
.factory("ctrlService",[function(seedService){
    
    return {

       init_operation: function($scope){
            $scope.ctrls = ctrls            
       },
    };
}]);
angular.module("widget.services")
.factory("initService",["$rootScope","queryService",'util','$compile',function($rootScope,queryService, util, $compile){
    function init_widget($scope, data){
        var widget = {}
        if(data) {
            widget = data;
            widget.components = JSON.parse(data.components);
        }
        else{
            widget.type = 'widget';
            widget.isCanvas = false;
            widget.isValid = true;
            widget.name="new widget";
            widget.components = [];
        }
        $scope.widget= widget;
        widget.ctrl_id =  util.uuid();
        $rootScope.selected_component = $scope.widget = $rootScope.widget=widget;
        $scope.datasources = [];
        $scope.widget.datasources = [];
        $scope.loading = false;
    }
    return {
       init_operation: function($scope){

          $scope.widget_style={width:'120px;'}
          $scope.selected_component = null;
          $scope.loading = true;
          
          $rootScope.$watch('selected_component', function(component){
              if(component!=null){
               var ctrl_scope = angular.element("#"+component.ctrl_id).scope()
               if(ctrl_scope == null){
                  return;
               }
               var parent_scope = ctrl_scope.$parent;
               while(parent_scope!=null){
                  if(parent_scope.hasOwnProperty('collapsed')){
                      parent_scope.collapsed = true;
                  }

                  if(parent_scope.hasOwnProperty('root')&&parent_scope.root=='true'){
                      break;
                  }
                  parent_scope = parent_scope.$parent;
               }
             }
           })
           if($scope.widget_id===""){
               $scope.mode = 'new';
               init_widget($scope);
           }
           else {
               $scope.mode = 'edit';
               queryService.xhr_widget($scope.widget_id).success(function (data) {
                   init_widget($scope, data);
                   queryService.xhr_widget_datasources($scope.widget_id).success(function(datasources){
                	   if(datasources.length==0){
                		   $scope.datasources = [];
                		   util.cache.put('datasources', datasources);
                		   return;
                	   }
                	   var uids = datasources.map(function(obj){return obj.uid});
                	   queryService.xhr_datasources_detail(uids).success(function(datasources_details){
                		  $scope.datasources= datasources_details; 
                		  util.cache.put('datasources', datasources_details);
                	   })
                    //angular.element('body').append(visualizer)
              })
               })
           }
          
       }    
    };
}]);