angular.module('widget.components')
    .directive('conditionalFormatting',[function(){
        return {
            restrict:'E',
            replace:true,
            templateUrl:'/templates/edit/properties/conditional-formatting.html',
            scope:{
                component:"=",
            },
            controller: function($scope){
                $scope.selected_option = 0;
                $scope.action = 1;

                $scope.if_options = [
                    {
                        id:0,
                        name:'Equals To'
                    },
                    {
                        id:1,
                        name:'Greater Than'
                    },
                    {
                        id:2,
                        name:'Greater Or Equal To'
                    },
                    {
                        id:3,
                        name:'Less Than'
                    },
                    {
                        id:4,
                        name:'Less Or Equal To'
                    }


                ]
                $scope.actions = [
                    {
                        id:0,
                        name:'Background'
                    } ,
                    {
                        id:1,
                        name:'Blink'

                    },
                    {
                        id:2,
                        name:'Send Email'
                    }

                ]
                $scope.action = $scope.actions[0];
                $scope.change_event = function(){
                }

                if($scope.component.condition == null) $scope.component.condition= [];

                $scope.add_condition = function(){
                    $scope.component.conditions =  $scope.component.conditions || [];
                    $scope.component.conditions.push({
                        _ifs : [
                            {
                                _if:0,
                                _value:0
                            }
                        ],
                        _then: 0,
                        _then_value:'#fff'
                    })
                }
                $scope.delete_condition = function(index){
                    $scope.component.conditions.splice(index, 1);
                }
                $scope.add_if = function(condition){
                   condition._ifs.push({
                       _if:0,
                       _value:0
                   })
                }
                $scope.remove_if = function(condition, index){
                    if(condition._ifs && condition._ifs.length>0){
                        condition._ifs.splice(index, 1);
                    }
                }
            },
            link: function(scope,element,attributes){
                console.log(scope.component.conditions[0])
            }
        }
    }])