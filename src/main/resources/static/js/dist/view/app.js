angular.module("dashboard.services",[
    'ngSanitize'
]);
angular.module("dashboard.components",[
    'ngAnimate',
    'ui.bootstrap',
    'dashboard.services'
]);
angular.module('dashboard', [
  'dashboard.services',
  'dashboard.components'
]);
var post_wrapper = function($httpProvider){
	// Use x-www-form-urlencoded Content-Type
	  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

	  /**
	   * The workhorse; converts an object to x-www-form-urlencoded serialization.
	   * @param {Object} obj
	   * @return {String}
	   */ 
	  var param = function(obj) {
	    var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

	    for(name in obj) {
	      value = obj[name];

	      if(value instanceof Array) {
	        for(i=0; i<value.length; ++i) {
	          subValue = value[i];
	          fullSubName = name + '[' + i + ']';
	          innerObj = {};
	          innerObj[fullSubName] = subValue;
	          query += param(innerObj) + '&';
	        }
	      }
	      else if(value instanceof Object) {
	        for(subName in value) {
	          subValue = value[subName];
	          fullSubName = name + '[' + subName + ']';
	          innerObj = {};
	          innerObj[fullSubName] = subValue;
	          query += param(innerObj) + '&';
	        }
	      }
	      else if(value !== undefined && value !== null)
	        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
	    }

	    return query.length ? query.substr(0, query.length - 1) : query;
	  };

	  // Override $http service's default transformRequest
	  $httpProvider.defaults.transformRequest = [function(data) {
	    return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
	  }];	
};
angular.module( 'app.widget.components.chart',['services'], angular.noop )
.directive('chartseriesComponent', [function(){
        return {
            restrict:'E',
            templateUrl: '/templates/view/components/chart.html',
            controller: function($scope){

            },
            link: function(scope, element, attrs){
                var chart_data = scope.component;
                var axes_data =  chart_data.axes;
                var series_data = chart_data.series;

                var type_map = {
                    'bar':'column',
                    'line':'line',
                    'area':'area'
                }
                var x_axis_data = axes_data.filter(function(a){return a.axis_type==0})
                var y_axis_data = axes_data.filter(function(a){return a.axis_type==1})
                if(x_axis_data.length == 0 || y_axis_data.length==0) {
                    return ;
                }

                var x_axis_labels = null;
                try{
                    x_axis_labels = eval(x_axis_data[0].data);
                }   
                catch(error){
                    console.error('x axis data format error : '+ x_axis_data[0])
                    return;
                }   

                var chart_elem = element.find('.chart-container');
                var option = {
                    chart: {
                        renderTo: chart_elem[0],
                         events: {
                            load: function() {
                                var c = this;
                                console.log('load')
                                setTimeout(function() {
                                    c.reflow();
                                }, 123)
                            }
                        }
                    },
                    title: {
                        text: chart_data.name
                    },
                    legend: {
                        enabled: chart_data.show_legend||false
                    }, 
                    xAxis: {
                        categories: eval(x_axis_data[0].data)
                    },
                    yAxis: {
                        title: {
                            text: y_axis_data[0].title
                        },
                        lineWidth: 1,
                        lineColor: "#d4d4d4"
                    },
                    credits: {
                        "enabled": false
                    },
                    colors: [
                            "#518a86",
                            "#3f6973",
                            "#2e8780",
                            "#44989f",
                            
                        ],
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: series_data.map(function(obj){
                        var y_data = null;
                        try{
                            y_data = eval(obj.data)
                        }
                        catch(error){
                            console.error('y data error: '+ obj.data)
                        }
                        return {
                            name: obj.series_label,
                            data: y_data,
                            type: type_map[obj.chart_type] == null? obj.chart_type: type_map[obj.chart_type]
                        }
                    })
                    
                }

                var chart = new Highcharts.Chart(option)
                if(window.charts ==null){
                    window.charts = []
                }
                window.charts.push(chart)
                angular.element(window).resize(function(){
                    chart.reflow()
                })
                scope.$watch('component.widget.sizeX', function(x){
                    console.log('sizex')
                    setTimeout(function() {
                        chart.reflow();
                        }, 200)
                })

                scope.$watch('component.dashboard.opts.columns', function(x){
                    console.log('watch')


                    setTimeout(function() {
                        chart.reflow();
                        }, 200)

                }) 

            }
        };
}])
angular.module('dashboard.components')
.directive('columnComponent', ["$rootScope",'queryService',function($rootScope,queryService){
        return {
            restrict:'AE',
            replace:true,
            templateUrl: '/templates/view/components/column.html',
            controller: function($scope){
            	$scope.$emit('component-register', $scope.component);
                $scope.component.display_value = "";
                if($scope.component.data == null){
                	  setTimeout(function(){
                          $scope.$emit('component-ready', $scope.component);
                      },10)
                    return;
                }
                if($scope.component.data.type=='data'){
                    if($scope.component.data.expr!=null && $scope.component.data.datasource!=null){
                        queryService.xhr_fetch_expr($scope.component.data.expr, $scope.component.data.datasource).success(function(data){
                            $scope.component.display_value = data.data;
                            setTimeout(function(){
                                $scope.$emit('component-ready', $scope.component);
                            },10)
                        })
                    }
                    else{
                    	setTimeout(function(){
                            $scope.$emit('component-ready', $scope.component);
                        },10)
                    }
                }
                else{
                    $scope.component.display_value = $scope.component.data.expr;
                    setTimeout(function(){
                        $scope.$emit('component-ready', $scope.component);
                    },10)
                }
            },
            link: function(scope, element, attrs){
                //scope.table = {}
                scope.data = [];
                scope.component.extra.change_data = function(data){

                    var row_option = scope.component.extra.table.row_option;
                    var row_num = scope.component.extra.table.row_num;


                    if(angular.isArray(data)){
                        if(row_option == 2){
                            scope.data = data;
                        }
                        else{
                            if(row_option == 0){
                                //show at most;
                                if(data.length>row_num){
                                    scope.data = data.slice(0, row_num);
                                }
                                else{
                                    scope.data = data;
                                }
                            }
                            else{
                                if(data.length>row_num){
                                    scope.data = data.slice(0, row_num);
                                }
                                else{
                                    scope.data = [];
                                    for(var i = 0 ;i < row_num ; i++) {
                                        if(i<data.length){
                                            scope.data[i] = data[i]
                                        }
                                        else{
                                            scope.data[i] = "";
                                        }

                                    }
                                }
                            }
                        }
                    }
                    else{
                        scope.data = [data];
                        if(row_option == 1) {
                            for(var i = 1 ;i<row_num ;i ++){
                                scope.data[i] = ""
                            }
                        }
                    }
                }

            }
        };
}])

.filter('chop', function(){
    return function(data, row_option, row_num){
        if(data==null){
            return data;
        }

       if(angular.isArray(data)){
            if(row_option == 2){
                return data;
            }
            else{
                if(row_option == 0){
                    //show at most;
                    if(data.length>row_num){
                        return data.slice(0, row_num);
                    }
                    else{
                        return data
                    }
                }
                else{
                    if(data.length>row_num){
                        return data.slice(0, row_num);
                    }
                    else{
                        for(var i = 0 ;i < row_num ; i++) {
                            if(i>=data.length){
                                data[i] = "";
                            }
                        }
                        return data;
                    }
                }
            }
        }
        else{
            data = [data];
            if(row_option == 1) {
                for(var i = 1 ;i<row_num ;i ++){
                    data[i] = ""
                }
            }
            return data;
        }
    }
})

angular.module( 'dashboard.components')
.directive('inlineframeComponent', ["$sce",function($sce){
        return {
            restrict:'E',
            replace:true,
            templateUrl: '/templates/view/components/frame.html',
            controller: function($scope){
            	$scope.$emit('component-register', $scope.component);
            },
            link: function(scope, element, attrs){
                scope.tmp_url = $sce.trustAsResourceUrl(scope.component.url);
                setTimeout(function(){
            		scope.$emit('component-ready', scope.component);
            	},0)
            }
        };
}])
angular.module( 'dashboard.components')
    .directive('gaugeComponent', ['$rootScope','queryService','$timeout','configuration',function($rootScope,queryService, $timeout,configuration){
        var gauge_chart;
        var verify_data = function(component){
            return true;
        }



        var gaugeOptions = {

            chart: {
                type: 'solidgauge',
                backgroundColor: "rgba(255, 255, 255, 0)",
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false
            },

            pane: {
                "center": [
                    "50%",
                    "85%"
                ],
                "size": "140%",
                "startAngle": "-90",
                "endAngle": "90",
                "background": {
                    "backgroundColor": "#EEE",
                    "innerRadius": "60%",
                    "outerRadius": "100%",
                    "shape": "arc"
                }
            },
            "tooltip": {
                "enabled": true
            },
            credits: {
                enabled: false
            },
            // the value axis
            "yAxis": {
                "stops": [
                    [
                        0.1,
                        "#55BF3B"
                    ],
                    [
                        0.5,
                        "#DDDF0D"
                    ],
                    [
                        0.9,
                        "#DF5353"
                    ]
                ],
                tickAmount:2,
                tickPositioner: function() {
                    return [this.min, this.max];
                },
                "min": 0,
                "max": 100,
                "lineWidth": 0,
                "minorTickInterval": null,
                "tickPixelInterval": 400,
                "tickWidth": 0,
                "title": {
                    "y": -70
                },
                "labels": {
                    "y": 16
                }
            },
            series: [{
                name: 'Speed',
                data: [50]
            }]

        }


        var draw_chart = function(container, chart_data){
            gaugeOptions.title={text:chart_data.key};
            if(chart_data.background){
                gaugeOptions.chart.backgroundColor=chart_data.background
            }
            gaugeOptions.series[0].name = chart_data.key;
            if(isNaN(chart_data.value)){
                chart_data.value = 0;
            }
            gaugeOptions.series[0].data = [parseFloat(chart_data.value)];
            gaugeOptions.yAxis.min = isNaN(chart_data.min)?0:parseFloat(chart_data.min);
            gaugeOptions.yAxis.max = isNaN(chart_data.max)?100:parseFloat(chart_data.max);
            gaugeOptions.yAxis.title = {'text': chart_data.key};
            $timeout(function(){
                gauge_chart=container.find(".chart-container").highcharts(gaugeOptions).highcharts();
                gauge_chart.reflow()
            },10)
            return ;
        }


        return {
            restrict:'AE',
            replace:true,
            templateUrl:'/templates/view/components/gauge.html',
            controller: function($scope,$element){
                $scope.$emit('component-register', $scope.component);
                if(!verify_data($scope.component)){
                    draw_chart($element, null,$scope.component.options);
                    setTimeout(function(){
                        $scope.$emit('component-ready', $scope.component);
                    },10)
                    return;
                }


                var chart_data = {
                    key:$scope.component.options.title,
                    min:$scope.component.options.min,
                    max:$scope.component.options.max,
                    value:$scope.component.data.expr,
                    background: $scope.component.options.background
                };
                if($scope.component.data.type=='data'){
                    if($scope.component.data.expr!=null && $scope.component.data.datasource!=null){

                        queryService.xhr_fetch_expr_both($scope.component.data.expr, $scope.component.data.datasource, $scope.component.data.interval, function(data){
                            chart_data.value = data.data.join();
                            draw_chart($element, chart_data);
                            $scope.$emit('component-ready', $scope.component);
                        })
                    }

                }
                else{
                    chart_data.value = $scope.component.data.expr;
                    draw_chart($element, chart_data);
                    $scope.$emit('component-ready', $scope.component);
                }


                $scope.$on('component-reflow', function(){
                    gauge_chart.reflow();
                })
            },
            link: function(scope, element, attrs){
            }
        };
    }])

angular.module( 'dashboard.components')
.directive('gridComponent', [function(){
        return {
            restrict:'E',
            templateUrl: '/templates/view/components/grid.html',
            controller: function($scope){
                $scope.getTimes = function(n){
                    return new Array(n)
                }
            },
            link: function(scope, element, attrs){
                /*
                    to implement the colspan/rowspan,  need to hide the overflow td.
                    the hidden td can only be right or down position of the *span td.
                    so each cell must hold the ptr of right and down position cell so as to 
                    make them hidden when be set as *span.

                */
                var grid_component = scope.component;
                scope.rows = grid_component.rows;
                scope.cols = grid_component.cols;
                scope.widget = scope.component.widget;
                scope.dashboard = scope.component.dashboard;
                scope.layout_cells = new Array(scope.rows);

                for(var i = 0;i<scope.rows;i++){
                    scope.layout_cells[i] = new Array(scope.cols);
                }
                var grid_cells = grid_component.cells;
                angular.forEach(grid_cells, function(grid_cell){
                    grid_cell.rows = grid_component.rows;
                    grid_cell.cols = grid_component.cols;
                    grid_cell.visible = true;
                    if(grid_cell.coord_x<scope.rows&&grid_cell.coord_y<scope.cols){
                        scope.layout_cells[grid_cell.coord_x][grid_cell.coord_y] = grid_cell;
                    }
                })
                for(var i = 0 ;i<scope.rows;i++){
                    for(var j = 0;j<scope.cols;j++){
                       /* if(scope.layout_cells[i][j]!=null){
                            //to find the first not null right cell.
                           var k = j;
                           while(k<scope.cols&&scope.layout_cells[i][++k]==null);
                           scope.layout_cells[i][j].right_cell = scope.layout_cells[i][k];

                           //to find the first not null down cell.
                           var x = i;
                           while(x<scope.rows&&scope.layout_cells[++x][j]==null);
                           scope.layout_cells[i][j].down_cell = scope.layout_cells[x][j];
                        }*/

                        var current_cell = scope.layout_cells[i][j];
                        if(current_cell==null){
                            throw new Error('null cell in grid.');
                        }
                        if(j<scope.cols-1){
                            scope.layout_cells[i][j].right_cell = scope.layout_cells[i][j+1];
                        }
                        if(j>0) {
                            scope.layout_cells[i][j].left_cell = scope.layout_cells[i][j-1]
                        }
                        if(i<scope.rows-1){
                            scope.layout_cells[i][j].down_cell = scope.layout_cells[i+1][j];
                        }
                        if(i>0){
                            scope.layout_cells[i][j].up_cell = scope.layout_cells[i-1][j];
                        }

                        for(var k = 1 ;k<scope.layout_cells[i][j].width;k++){
                            scope.layout_cells[i][j+k].visible = false;
                        }

                        for(var x = 1;x<scope.layout_cells[i][j].height;x++){
                            scope.layout_cells[i+x][j].visible = false;
                        }
                    }
                }
                
            }
        };
}])
angular.module( 'dashboard.components')
.directive('htmltemplateComponent', [function(){
        return {
            restrict:'E',
            replace:true,
            templateUrl: '/templates/view/components/html.html',
            controller: function($scope){
            	$scope.$emit('component-register', $scope.component);
            },
            link: function(scope, element, attrs){
            	setTimeout(function(){
            		scope.$emit('component-ready', scope.component);
            	},0)
            }
        };
}])
.filter('to_trusted', ['$sce', function($sce){
    return function(text){
        return $sce.trustAsHtml(text);
    }
}])
angular.module( 'dashboard.components')
.directive('imageComponent', [function(){
        return {
            restrict:'E',
            replace:true,
            templateUrl: '/templates/view/components/image.html',
            controller: function($scope){
            	$scope.$emit('component-register', $scope.component);
            },
            link: function(scope, element, attrs){
            	setTimeout(function(){
            		scope.$emit('component-ready', scope.component);
            	},0)
            }
        };
}])
angular.module( 'dashboard.components')
.directive('labelComponent', ['queryService','conditionService', function(queryService, conditionService){

        return {
            restrict:'E',
            replace:true,
            templateUrl: '/templates/view/components/label.html',
            controller: function($scope){

                function evalCondition(){
                    if(!$scope.component.conditions) return;

                    angular.forEach($scope.component.conditions, function(condition){

                        if(!condition) return;
                        if(!condition._then_value) return;

                        var conditionString = conditionService.parseCondition(condition);
                        var templateString = _.template(conditionString);
                        var evalScript = templateString({
                            "_if":'$scope.component.display_value',
                            "_then":"$scope.component.css_style+=';background:"+condition._then_value+"'"
                        });
                        var fun = new Function('$scope', evalScript);
                        //eval(evalScript); /**kkk*/
                        fun($scope);
                    })
                }
            	$scope.$emit('component-register', $scope.component);
                
                $scope.component.display_value = "";
                if($scope.component.data == null){
                	setTimeout(function(){
                        $scope.$emit('component-ready', $scope.component);
                    },10)
                    return;
                }

                $scope.update = function(data){
                    $scope.component.display_value = data.data.join();
                    evalCondition();
                    setTimeout(function () {
                        $scope.$emit('component-ready', $scope.component);
                    }, 0)
                }


                if($scope.component.data.type=='data'){
                    if($scope.component.data.expr!=null && $scope.component.data.datasource!=null){
                        queryService.xhr_fetch_expr_both($scope.component.data.expr, $scope.component.data.datasource, $scope.component.data.interval, $scope.update);
                    }
                    else{
                    	setTimeout(function(){
                            $scope.$emit('component-ready', $scope.component);
                        },10)
                    }
                }
                else{
                    $scope.component.display_value = $scope.component.data.expr;
                    evalCondition();
                    setTimeout(function(){
                        $scope.$emit('component-ready', $scope.component);
                    },0)
                }
            },
            link: function(scope, element, attrs){
            }
        };
}])

angular.module('dashboard.components')
.directive('layoutSelector', ['queryService', 'layoutAlgorithms', 'dashboardBase', function(queryService, layoutAlgorithms, dashboardBase){

    var layout_templates = [];
    var layout_template_columns = 3;

    layout_templates.push(new layoutAlgorithms.LayoutTemplate(2, [2, 1, 1]));
    layout_templates.push(new layoutAlgorithms.LayoutTemplate(3, [1, 1, 1]));
    layout_templates.push(new layoutAlgorithms.LayoutTemplate(3, [1, 1, 1, 3]));

//for test.
    layout_templates.push(new layoutAlgorithms.LayoutTemplate(6, [1, 1, 2, 2, 3, 3, 2, 2, 2]));


        return {
            restrict:'E',
            replace:true,
            templateUrl: '/templates/view/components/layout_selector.html',
            controller: function($scope){

                 $scope.disable_layout_mode = function(){
                    $scope.use_custom = false;
                    $scope.use_template = false;
                    $scope.use_canvas = false;
                }

                $scope.toggle_custom_layout = function(){
                    $scope.disable_layout_mode()
                    $scope.use_custom = true;
                }

                $scope.get_default_layout_column_number = function(num){
                    return new Array(num);
                }

                $scope.decrease_layout_column = function(){
                    --$scope.current_dashboard.grid_cols;
                    $scope.current_dashboard._reflow();
                    $scope.current_dashboard._change();
                }

                $scope.increase_layout_column = function(){
                    $scope.current_dashboard.grid_cols+=1;
                    $scope.current_dashboard._change();
                }

                $scope.switch_layout = function(index){
                    var columns =  $scope.current_dashboard.grid_cols;

                    $scope.disable_layout_mode();
                    $scope.use_template =true;

                    if($scope.current_template!=null && $scope.current_template == index){
                        return;
                    }

                    $scope.current_template = index;

                    $scope.current_dashboard._applyLayoutTemplate(layout_templates[index], {
                      rowFactor: dashboardBase.configuration.minHeight
                    });
                    $scope.current_dashboard._change();
                }


            },
            link: function(scope, element, attrs){
                scope.use_template = false;
                scope.layout_templates = [
                    {
                        index: 0,
                       style:'style_1'
                    },
                    {
                        index: 1,
                        style:'style_2'
                    },
                    {
                        index:2,
                        style:'style_3'
                    },
                    {
                        index:3,
                        style:'style_4'
                    }


                ]
                },

        };
}])

angular.module( 'dashboard.components')
.directive('modal', [function(){
        return {
            restrict:'E',
            replace:true,
            template: '<div><modal-dialog-container [(dialog)]="dialog_name"><modal-dialog name="new_widget" [buttons]="[[\'ok\',\'Okey\']]"><div>sadfsadf</div></modal-dialog></modal-dialog-container></div>',
            controller: function($scope){

            },
            link: function(scope, element, attrs){
              
            }
        };
}])
angular.module( 'dashboard.components')
.directive('pieComponent', ['$rootScope','queryService','$timeout','configuration',function($rootScope,queryService, $timeout,configuration){
        var pie_chart;
        var verify_data = function(component){
            var chart_value = component.chart_value;
            var chart_label = component.chart_label;
            if(chart_value==null|| chart_label == null){
                return false;
            }
            if(chart_value.data==null || chart_label.data == null){
                return false;
            }

            if(chart_label.data.expr == null || chart_label.data.expr==''){
                return false;
            }

            if(chart_label.data.datasource == null || chart_label.data.datasource==''){
                return false;
            }

            return true;
        }

        var draw_chart = function(container, chart_data,_options){
            var options = configuration.chart_options();
            options.chart.type='pie';
            options.series = [{
                data: ((chart_data!=null&&chart_data.length>0)?chart_data:[["No Data",1]])
            }]
            options.legend.labelFormat = (function(opt){
                var formatter = "{name}";
                if(opt==null){
                    return formatter
                }
                if(opt.show_labels){
                    formatter +="({y:.2f})"
                }
                if(opt.show_percentage){
                    formatter+="[{percentage:.2f}%]"
                }
                return formatter
            })(_options)
            
            if(_options.legend_fcolor){
            	options.legend.itemStyle={
            			color:_options.legend_fcolor
            	}
            }
            
            if(_options.legend_bcolor){
            	options.legend.backgroundColor=_options.legend_bcolor
            }

            if(_options.background){
                options.chart.backgroundColor=_options.background;
            }
            switch(_options.legend_position){
                case 'left':
                    options.legend.align='left'
                    options.legend.verticalAlign='middle'
                    options.legend.layout= "vertical"
                    break;
                case 'right':
                    options.legend.align='right'
                    options.legend.verticalAlign='middle'
                    options.legend.layout= "vertical"
                    break;
                case 'bottom':
                    options.legend.align='right'
                    options.legend.verticalAlign='bottom'
                    options.legend.layout= "horizontal"
                    break;
                case 'top':
                    options.legend.align='right'
                    options.legend.verticalAlign='top'
                    options.legend.layout= "horizontal"
                    break;
                default:
                    break;
            }

            options.plotOptions.pie = {
                "allowPointSelect": true,
                "cursor": true,
                "showInLegend": _options?_options.show_legend:true,
                "innerSize": _options?_options.inner_size:"0%",
                borderWidth: 0,
                "dataLabels": {
                    enabled:_options?_options.data_labels:false,
                    format: (function(opt){
                        var formatter = "{point.name}";
                        if(opt==null){
                            return formatter
                        }
                        if(opt.show_labels){
                            formatter +="({point.y:.2f})"
                        }
                        if(opt.show_percentage){
                            formatter+="[{point.percentage:.2f}%]"
                        }
                        return formatter
                    })(_options),
                    style:{
                        fontWeight:"normal"
                    }
                },
                "tooltip": {
                    "headerFormat":"<strong>{point.key}</strong> : ",
                    "pointFormat": "<span style='text-decoration:italic'>{point.y}</span>"
                }
            }
            $timeout(function(){
                pie_chart = container.find(".chart-container").highcharts(options).highcharts();
                //pie_chart.reflow();
            },10)
            return;
        }
        return {
            restrict:'AE',
            replace:true,
            templateUrl:'/templates/view/components/pie.html',
            controller: function($scope,$element){
            	$scope.$emit('component-register', $scope.component);
                    if(!verify_data($scope.component)){
                        draw_chart($element, null,$scope.component.options);
                        setTimeout(function(){
                            $scope.$emit('component-ready', $scope.component);
                        },10)
                        return;
                    }
                var chart_data = []
                queryService.xhr_fetch_expr($scope.component.chart_label.data.expr, $scope.component.chart_label.data.datasource)
                    .success(function(_label_data){
                        var label_data = _label_data.data
                        if(label_data!=null && label_data.length>0){
                            if($scope.component.chart_value.data.datasource==null || $scope.component.chart_value.data.expr==null){
                                var value_data = []
                                var iteration_num = $scope.component.options.max_slice<label_data.length?$scope.component.options.max_slice:label_data.length;
                                for(var i = 0 ; i<iteration_num;i++){
                                    var push_data = [];
                                    push_data[0] = label_data[i];
                                    if(value_data[i]!= null && value_data[i] >=0 && _(value_data[i]).toNumber(2)!=NaN){
                                        push_data[1] = _(value_data[i]).toNumber(2);
                                    }
                                    else{
                                        push_data[1] = 1;
                                    }
                                    chart_data.push(push_data);
                                }
                                draw_chart($element, chart_data,$scope.component.options);
                                return;
                            }
                            queryService.xhr_fetch_expr($scope.component.chart_value.data.expr, $scope.component.chart_value.data.datasource)
                                .success(function(_value_data){
                                    var value_data = _value_data.data;
                                    var iteration_num = $scope.component.options.max_slice<label_data.length?$scope.component.options.max_slice:label_data.length;
                                    for(var i = 0 ; i<iteration_num;i++){
                                        var push_data = [];
                                        push_data[0] = label_data[i];
                                        if(value_data[i]!= null && value_data[i] >=0 && _(value_data[i]).toNumber(2)!=NaN){
                                            push_data[1] = _(value_data[i]).toNumber(2);
                                        }
                                        else{
                                            push_data[1] = 0;
                                        }
                                        chart_data.push(push_data);
                                    }

                                    if(label_data.length>$scope.component.options.max_slice){
                                        var push_data = [];
                                        push_data[0] = 'Others'
                                        var tmp_sum = 0;
                                        for(var i = $scope.component.options.max_slice;i<label_data.length;i++){
                                            if(value_data[i]!=null && value_data[i]>0 && _(value_data[i]).toNumber(2)!=NaN){
                                                tmp_sum+=_(value_data[i]).toNumber(2);
                                            }
                                        }
                                        push_data[1] = Math.round(tmp_sum*100)/100
                                        chart_data.push(push_data);
                                    }
                                    draw_chart($element, chart_data,$scope.component.options);
                                    setTimeout(function(){
                                        $scope.$emit('component-ready', $scope.component);
                                    },10)
                                })
                        }
                        else{
                        	setTimeout(function(){
                                $scope.$emit('component-ready', $scope.component);
                            },10)
                        }
                    })

                $scope.$on('component-reflow', function(){
                    pie_chart.reflow();
                })
            },
            link: function(scope, element, attrs){
                scope.component.chart_label.extra = scope.component.chart_label.extra||{}
                scope.component.chart_value.extra = scope.component.chart_value.extra||{}
                scope.component.chart_label.extra.chart = scope.component;
                scope.component.chart_value.extra.chart = scope.component;
                scope.component.components = [scope.component.chart_label, scope.component.chart_value]
            }
        };
}])

angular.module( 'dashboard.components')
.directive('separatorComponent', [function(){
        return {
            restrict:'E',
            replace:true,
            templateUrl: '/templates/view/components/separator.html',
            controller: function($scope){

            },
            link: function(scope, element, attrs){
            }
        };
}])
angular.module( 'dashboard.components')
.directive('serieschartComponent', ['$rootScope','configuration','queryService','$timeout',"$q",function($rootScope,configuration,queryService, $timeout, $q){

        var series_chart ; 
        var verify_data = function(component){
            var x_axis = component.extra.x_axis;
            var series_collection = component.extra.series_collection;
            if(x_axis.data == null) {
                return false;
            }
            return true;
        }


        return {
            restrict:'AE',
            templateUrl:'/templates/view/components/series-chart.html',
            controller: function($scope,$element){
            	$scope.$emit('component-register', $scope.component);
                $scope.component.extra = $scope.component.extra || {}



                var draw_chart = function(container, chart_data, _options){
                    if(chart_data && chart_data.stream){
                        Highcharts.setOptions({
                            global : {
                                useUTC : false
                            }
                        });
                    }
                    var options = configuration.chart_options();
                    options.legend = {
                        enabled: _options?_options.show_legend:true,
                        align: "right",
                        verticalAlign: "top",
                        layout: "horizontal",
                        itemStyle: {
                            fontWeight: "normal"
                        }
                    },
                        options.chart.type='line';

                    options.chart.inverted = _options?_options.inverted:false;
                    if(chart_data && chart_data.stream!=true) {

                        options.xAxis = {
                            categories: chart_data ? chart_data.xAxis.data : [1],
                            title: {
                                enabled: chart_data ? chart_data.xAxis.options.show_axis : false,
                                text: chart_data ? chart_data.xAxis.options.axis_title : null
                            },
                            labels: {
                                enabled: chart_data ? chart_data.xAxis.options.show_label : true,
                                style: {
                                    color: chart_data ? chart_data.xAxis.options.axis_color : "#000"
                                }
                            },
                            gridLineWidth: chart_data && chart_data.xAxis.options.show_grid ? 1 : 0
                        };

                        if (chart_data && chart_data.xAxis.options.show_tick == false) {
                            options.xAxis.tickLength = 0;
                        }
                        if (chart_data && chart_data.xAxis.options.rotation != 0) {
                            options.xAxis.labels.rotation = chart_data.xAxis.options.rotation;
                        }
                        if (chart_data && chart_data.xAxis.options.color) {
                            options.xAxis.labels.color = chart_data.xAxis.options.color;
                        }
                    }


                    if(chart_data && chart_data.stream){
                        options.rangeSelector = {
                            buttons: [{
                                count: 1,
                                type: 'minute',
                                text: '1M'
                            }, {
                                count: 5,
                                type: 'minute',
                                text: '5M'
                            }, {
                                type: 'all',
                                text: 'All'
                            }],
                            inputEnabled: false,
                            selected: 1
                        }
                    }
                    options.series = chart_data?chart_data.series:[
                        {
                            name:'empty',
                            data:[0]
                        }
                    ]

                    options.legend.labelFormat = (function(opt){
                        var formatter = "{name}";
                        if(opt==null){
                            return formatter
                        }
                        if(opt.show_labels){
                            formatter +="({y:.2f})"
                        }
                        if(opt.show_percentage){
                            formatter+="[{percentage:.2f}%]"
                        }
                        return formatter
                    })(_options)

                    if(_options.legend_fcolor){
                        options.legend.itemStyle={
                            color:_options.legend_fcolor
                        }
                    }

                    if(_options.legend_bcolor){
                        options.legend.backgroundColor=_options.legend_bcolor
                    }

                    if(_options.background){
                        options.chart.backgroundColor = _options.background;
                    }

                    if(chart_data && chart_data.loadCallback){
                        options.chart.events = {
                            load: function(){
                                var chart_obj = this
                                chart_data.loadCallback(chart_obj);
                            }
                        }
                    }
                    if($scope.intervals){
                        $scope.intervals.forEach(function(interval){
                            clearInterval(interval);
                        })
                    }
                    $timeout(function(){

                        if(chart_data && chart_data.stream){
                            if($scope.series_chart){
                                container.find(".chart-container").highcharts().destroy();
                            }
                            $scope.series_chart =null;
                            $scope.series_chart = container.find(".chart-container").highcharts('StockChart', options);
                        }
                        else {
                            $scope.series_chart = container.find(".chart-container").highcharts(options).highcharts();
                            $scope.series_chart.reflow();
                        }
                    },10)
                }






                angular.forEach($scope.component.components, function(component){
                    if(component.type=='series_collection'){
                        $scope.component.extra.series_collection = component.components;
                    }
                    else if(component.type == 'y_axis_collection'){
                        $scope.component.extra.y_axis_collection = component.components;
                    }
                    else{
                        $scope.component.extra.x_axis = component;
                    }
                })



                if(!verify_data($scope.component)){
                    draw_chart($element, null, $scope.component.options);
                    setTimeout(function(){
                        $scope.$emit('component-ready', $scope.component);
                    },10)
                    return;
                }


                $scope.query_update = function(){
                    var chart_data = {}
                    queryService.xhr_fetch_expr($scope.component.extra.x_axis.data.expr,$scope.component.extra.x_axis.data.datasource)
                        .success(function(_x_data){
                            var x_data = _x_data.data;
                            if(x_data.length>0){

                                chart_data.series = [];
                                var x_axis_data = x_data;
                                chart_data.xAxis = {
                                    data:x_axis_data,
                                    options: $scope.component.extra.x_axis.options
                                };
                                var promises = [];
                                angular.forEach($scope.component.extra.series_collection, function(series){
                                    if(series.data.expr!="" && series.data.datasource!=""){

                                        promises.push(queryService.xhr_fetch_expr(series.data.expr,series.data.datasource).success(function(_series_data){
                                            if($scope.component.extra.x_axis.data.type==='time'){
                                                if(_series_data.data.dataSource.type!=='NIFI'){
                                                    alert('time series chart should use stream data !');
                                                    return;
                                                }
                                            }
                                            var series_data = _series_data.data;
                                            var result_data = [];
                                            if(series_data.length!=x_data.length){
                                                for(var i = 0 ; i<x_data.length;i++){
                                                    if(series_data[i]!=null && _(series_data[i]).toNumber(2)!=NaN){
                                                        result_data[i] = _(series_data[i]).toNumber(2) ;
                                                    }
                                                    else{
                                                        result_data[i] = 0;
                                                    }
                                                }
                                            }
                                            else{
                                                result_data = series_data.map(function(data){
                                                    if(_(data).toNumber(2)!=NaN){
                                                        return _(data).toNumber(2);
                                                    }
                                                    else{
                                                        return 0;
                                                    }
                                                });
                                            }

                                            if($scope.component.extra.x_axis.options.sort!=0){
                                                var order= $scope.component.extra.x_axis.options.sort;
                                                var sort_obj = []
                                                for(var i = 0 ;i<x_data.length;i++){
                                                    sort_obj.push({key:x_data[i],value:result_data[i]})
                                                }
                                                sort_obj.sort(function(a,b){
                                                    return a.key>b.key?order:-order
                                                })

                                                result_data = []
                                                x_axis_data = []
                                                for(var i =0 ;i<x_data.length;i++){
                                                    x_axis_data.push(sort_obj[i].key)
                                                    result_data.push(sort_obj[i].value);
                                                }
                                            }



                                            var series_obj = {
                                                type:series.style,
                                                name:series.name,
                                                data: result_data,
                                                dataLabels: {
                                                    enabled:series.options.show_value,
                                                    style: {
                                                        color: "#717171",
                                                        fontSize: "8px",
                                                        fontWeight:"normal"
                                                    },
                                                }
                                            }

                                            if(series.options.use_custom_color){
                                                series_obj.color = series.options.custom_color;
                                            }
                                            chart_data.series.push(series_obj);
                                        }))
                                    }})

                                $q.all(promises).then(function(){
                                    draw_chart($element, chart_data, $scope.component.options);
                                    setTimeout(function(){
                                        $scope.$emit('component-ready', $scope.component);
                                    },10)
                                })
                            }
                            else{
                                draw_chart($element, null, $scope.component.options);
                                return;
                                setTimeout(function(){
                                    $scope.$emit('component-ready', $scope.component);
                                },10)
                            }

                        })


                }


                $scope.stream_update = function(){
                    var chart_data = {};
                    chart_data.stream = true;
                    chart_data.series = [];
                    angular.forEach($scope.component.extra.series_collection, function(series){


                        var series_obj= {
                            name:series.name,
                            style:series.style,
                            datasource:series.datasource,
                            dataLabels: {
                                enabled:series.options.show_value,
                                style: {
                                    color: "#717171",
                                    fontSize: "8px",
                                    fontWeight:"normal"
                                }
                            },
                            data: (function () {
                                // generate an array of random data
                                var data = [], time = (new Date()).getTime(), i;

                                for (i = -999; i <= 0; i += 1) {
                                    data.push([
                                        time + i * 1000,
                                        0
                                    ]);
                                }
                                return data;
                            }())
                        }

                        chart_data.series.push(series_obj);
                    });

                    chart_data.loadCallback = function(chart_obj){
                        chart_data.series.forEach(function(series_obj, index){
                            if(!$scope.interval){
                                $scope.intervals = [];
                            }
                            var current_series_component = $scope.component.extra.series_collection[index];
                            var interval = setInterval(function () {
                                var x = (new Date()).getTime(), // current time
                                    y = 0;
                                if(current_series_component.data.datasource) {
                                    queryService.xhr_fetch_data(current_series_component.data.datasource).success(function (data) {
                                        if (chart_obj.options) {
                                            y = data.data[0]
                                            if(angular.isArray(y)){
                                                y = y[0]

                                            }
                                            if(!isNaN(y)){
                                                y = parseInt(y);
                                            }
                                            chart_obj.series[index].addPoint([x, y], true, true);
                                        }
                                    })
                                }
                                else{
                                    if(chart_obj.options) {
                                        chart_obj.series[index].addPoint([x, y], true, true);
                                    }
                                }

                            }, 3000);

                            $scope.intervals.push(interval);
                        });

                    }
                    draw_chart($element, chart_data, $scope.component.options);
                    setTimeout(function(){
                        $scope.$emit('component-ready', $scope.component);
                    },10)

                }

                if($scope.component.extra.x_axis.data.type==='time'){
                    if($scope.init_stream_chart){

                    }
                    else {
                        $scope.init_stream_chart=  true;
                        $scope.stream_update();
                    }
                }
                else{
                    $scope.query_update();
                }


                $scope.$on('component-reflow', function(){
                    series_chart.reflow();
                })
            },
            link: function(scope, element, attrs){

            }
        };
}])
angular.module('dashboard.components')
.directive('tableComponent', [function(){
        return {
            restrict:'AE',
            replace:true,
            templateUrl: '/templates/view/components/table.html',
            controller: function($scope){
                 
            },
            link: function(scope, element, attrs){
                //scope.table = {}
                angular.forEach(scope.component.components, function(column){
                    column.extra = column.extra||{}
                    column.extra.table = scope.component;
                })   
            },

        };
}])


angular.module('dashboard.components')
.animation('.slide',function(){
   var NG_HIDE_CLASS = 'ng-hide';
    return {
        beforeAddClass: function(element, className, done) {
            if(className === NG_HIDE_CLASS) {
                element.slideUp('fast'); 
            }
        },
        removeClass: function(element, className, done) {
            if(className === NG_HIDE_CLASS) {
                element.hide().slideDown('fast');
            }
        }
    }
})
.animation(".fade",function(){
    var NG_HIDE_CLASS="ng-hide";
    return {
        beforeAddClass: function(element, className, done) {
            if(className === NG_HIDE_CLASS) {
                element.fadeOut(); 
            }
        },
        removeClass: function(element, className, done) {
            if(className === NG_HIDE_CLASS) {
                element.fadeIn();
            }
        }
    }

})
.directive('autofocus', ['$timeout', function($timeout) {
  return {
    restrict: 'A',
    link : function($scope, $element) {
      $timeout(function() {
        $element[0].focus();
      });
    }
  }
}]);
angular.module( 'dashboard.components')
.directive('valuepairComponent', [function(){
        return {
            restrict:'E',
            templateUrl: '/templates/view/components/valuepair.html',
            controller: function($scope){

                if($scope.component.components.length>1) {
                    $scope.key_component = $scope.component.components[0];
                    $scope.value_component = $scope.component.components[1];
                }
            },
            link: function(scope, element, attrs){

            }
        };
}])
angular.module( 'dashboard.components')
.directive('widget', [function(){

    return {
        restrict:'AE',
        replace:true,
        controller: function($scope){

        },
        template:"<div class=''><ul>"+
        	"<li ng-repeat='c in component.components'>"+
            "<x-inline-component component='c'></x-inline-component>"+
            "</li></ul></div>"+
        "</ul>",
        link: function(scope, element, attrs){
        },
        scope: {
            component:"=",
        }

    }
}])
.directive('canvas', ['$rootScope','$compile','$timeout', '$document',function($rootScope,$compile, $timeout, $document){
    return {
        restrict:'AE',
        replace:true,
        scope: {
            component:"=",
        },
        controller:function($scope, $element){
            var zoom_scale = 1.2
            var container = $element.find('.widget-canvas-container');
            var inner = $element.find('.widget-canvas-inner');
            if($scope.component.background!=null && $scope.component.background!=""){
                if($rootScope.is_snapshot) {
                    $scope.widget_bg = $scope.component.localBackground;
                }
                else{
                    $scope.widget_bg = $scope.component.background;
                }
            }
            $element.find('.widget-bg').load(function(){
                inner.height($(this).height())
            })

            $scope.zoomin = function(){
                //var width = inner.width()+zoom_scale;
                //var height = inner.height+zoom_scale;
                inner.width(inner.width()*zoom_scale);
                inner.height(inner.height()*zoom_scale);
                container.scrollTop(container.scrollTop()*zoom_scale);
                container.scrollLeft(container.scrollLeft()*zoom_scale);
            }

            $scope.zoomout = function(){
                inner.width(inner.width()/zoom_scale);
                inner.height(inner.height()/zoom_scale);
            }

        },
        template:"<div class='widget-canvas-content'>"+
                    "<div class='widget-canvas-container'>"+
                        "<div class='widget-canvas-inner'>"+
                            "<ul>"+
                                "<li ng-repeat='c in component.components' style='top:{{c.top}};left:{{c.left}};width:{{c.width}};height:{{c.height}}'>"+
                                    "<x-inline-component component='c'></x-inline-component>"+
                                "</li>"+
                            "</ul>"+
                            "<img class='widget-bg' ng-src='{{widget_bg?widget_bg:\"/img/transparent.png\"}}' width='100%'/>"+
                        "</div>"+
                    "</div>"+
        "<ul class='pan-navigator'>"+
        "<li><a href='#' ng-click='zoomin()'><span class='glyphicon glyphicon-plus'></span></a></li>"+
        "<li><a href='#' ng-click='zoomout()'><span class='glyphicon glyphicon-minus'></span></a></li>"+
        "</ul>"+
                "</div>",

        link:function(scope,element,attrs){


            scope.$watch('component.position', function(p, op){
                scope.$broadcast('component-reflow');
            })

            var container = element.find('.widget-canvas-container')
            var startx, starty;
            var start_scroll_x = container.scrollLeft();
            var start_scroll_y = container.scrollTop();
            var mousemove = function(event){
                var moving_x = event.pageX;
                var moving_y = event.pageY;
                var movex = moving_x - startx;
                var movey = moving_y - starty;

                console.log(start_scroll_y-movey);
                container.scrollTop(start_scroll_y-movey);
                container.scrollLeft(start_scroll_x-movex);
            }

            var mouseup = function(){
                $document.unbind('mousemove', mousemove);
                $document.unbind('mouseup', mouseup);
                container.unbind('mousemove', mousemove);
            }


            container.on('mousedown', function(event){
                event.stopPropagation();
                startx = event.pageX;
                starty = event.pageY;

                start_scroll_y = container.scrollTop();
                start_scroll_x = container.scrollLeft();
                container.on('mousemove', mousemove)
                $document.on('mouseup', mouseup);
            })

        }
    }
}])
.directive('inlineComponent',['$compile', function($compile){
    return {
        restrict:'E',
        replace:true,
        link: function(scope, element, attrs){
            scope.component.extra = scope.component.extra||{}
            var type = scope.component.type;
            var parent = element.parent();
            if(type!==null){
                var type_name = type.replace(/[_]+/,'')+"_component"
                parent.append($compile(angular.element("<"+type_name+"></"+type_name+">"))(scope))
                element.remove();
            }
        },
        scope: {
            component:"="
        }
    }
}])

angular.module('dashboard.components')
    .directive('widgetContainer', [function(){
        var components = [];
        function checkComponents(){
            for(var i = 0 ;i<components.length;i++){
                if(!components[i].ready){
                    return false;
                }
            }
            return true;
        }
        return {
            restrict:'AE',
            replace:true,
            controller: function($scope){
            },
            templateUrl: '/templates/view/components/widget_container.html',
            link: function(scope, element, attrs){
                scope.$on('component-ready', function(event, component){
                    component.ready = true;
                    if(checkComponents()){
                        //for phantomjs
                        element.attr('data-ready', true);
                    }
                });
                scope.$on('component-register', function(event,component){
                    component.ready = false;
                    components.push(component);
                });
                scope.$watch('component', function(nv, ov){
                	if(nv!= null){
                		if(scope.component.isCanvas==true && scope.component.background!=null){
                			var bg_component  = {ready: false}
                			components.push(bg_component)
                			element.find('.widget-bg').load(function(){
                				bg_component.ready = true
                				scope.$emit('component-ready', bg_component);
                			})
                		}
                		else{
                		if(scope.component.components == null){
                			element.attr('data-ready', true);
                		}
                		if(scope.component.components.length == 0){
                        	element.attr('data-ready', true);
                        }
                		}
                	}
                })
            },
            scope: {
                component:"="
            }

        }
    }])

angular.module( 'dashboard.components')
.directive('widgetCtrl', ["$compile","$uibModal", function($compile, $uibModal){
    return {
        restrict:'AE',
        replace:true,
        templateUrl:'/templates/view/components/widget_control.html',
        controller:function($scope){
        },
        link: function(scope, element, attrs){
            scope.open_new_widget_dialog = function(){
                $uibModal.open({
                  animation: true,
                  templateUrl: '/templates/view/components/modal_dialog.html',
                  size: 'lg'
                });
            }
        }
    };
}])

angular.module('dashboard.components')
    .directive('widgetSnapshot', [function(){

        return {
            restrict:'AE',
            replace:true,
            controller: function($scope){

            },
            templateUrl: '/templates/view/components/widget_snapshot.html',
            link: function(scope, element, attrs){
                
            },
            scope: {
                component:"="
            }

        }
    }])

angular.module('dashboard').controller(
		'controller',
		[
				'$rootScope',
				'$scope',
				'queryService',
				'constantsService',
				'initService',
				function($rootScope, $scope, queryService, constantsService,
						initService){
					initService.init_operation($scope);

					$scope.load = function() {

					};
					$scope.time = function(){
						return (new Date()).valueOf();
					}

				} ])

angular.module('dashboard')
    .controller('scrollController',
        [
            '$rootScope',
            '$scope',
            'queryService',
            function(
                $rootScope,
                $scope,
                queryService
            ){
                $scope.id = angular.element("#widget_id").val();

                getNextWidget();
                $scope.pause = false;
                function getNextWidget(){
                    return queryService.xhr_next_widget().success(function(data){
                        $scope.component = data;
                        if(data.components!=null){
                            $scope.component.components = JSON.parse(data.components);
                        }
                    })
                }

                function getPrevWidget(){
                    return queryService.xhr_prev_widget().success(function(data){
                        $scope.component = data;
                        if(data.components!=null){
                            $scope.component.components = JSON.parse(data.components);
                        }
                    })
                }

                function getFirstWidget(){
                    return queryService.xhr_first_widget().success(function(data){
                        $scope.component = data;
                        if(data.components!=null){
                            $scope.component.components = JSON.parse(data.components);
                        }
                    })
                }

                function getLastWidget(){
                    return queryService.xhr_first_widget().success(function(data){
                        $scope.component = data;
                        if(data.components!=null){
                            $scope.component.components = JSON.parse(data.components);
                        }
                    })
                }



                $scope.interval = setInterval(function(){
                    angular.element("#container").fadeOut();
                    getNextWidget().then(function(){
                        angular.element("#container").fadeIn();
                    });
                }, 5000);

                $scope.play_pause = function(){
                    if($scope.pause) {
                        $scope.interval = setInterval(function(){
                            angular.element("#container").fadeOut();
                            getWidget().then(function(){
                                angular.element("#container").fadeIn();
                            });
                        }, 5000);
                        $scope.pause = false;
                    }
                    else{
                        clearInterval($scope.interval);
                        $scope.pause = true;
                    }
                }


                $scope.next = function(){
                    clearInterval($scope.interval);
                    angular.element("#container").fadeOut();
                    getNextWidget().then(function(){
                        angular.element("#container").fadeIn();
                    });
                }

                $scope.last = function(){
                    clearInterval($scope.interval);
                    angular.element("#container").fadeOut();
                    getLastWidget().then(function(){
                        angular.element("#container").fadeIn();
                    });
                }

                $scope.first = function(){
                    clearInterval($scope.interval);
                    angular.element("#container").fadeOut();
                    getFirstWidget().then(function(){
                        angular.element("#container").fadeIn();
                    });
                }

                $scope.prev = function(){
                    clearInterval($scope.interval);
                    angular.element("#container").fadeOut();
                    getPrevWidget().then(function(){
                        angular.element("#container").fadeIn();
                    });
                }
                $scope.recover = function(){
                    $scope.interval = setInterval(function(){getNextWidget()}, 5000);
                }
            }])

angular.module('dashboard')
    .controller('singleController',
        [
            '$rootScope',
            '$scope',
            'queryService',
            function(
                $rootScope,
                $scope,
                queryService
            ){
                $scope.id = angular.element("#widget_id").val();
                $rootScope.widget_id = $scope.id;
                var is_snapshot = angular.element('#is_snapshot').val();

                if(is_snapshot) {
                    $rootScope.is_snapshot = true;
                }
                else{$rootScope.is_snapshot = false}

                queryService.xhr_widget($scope.id).success(function(data){
                    $scope.component = data;
                    if(data.components!=null){
                        $scope.component.components = JSON.parse(data.components);
                    }
                })
            }])

angular.module('dashboard.services')
    .factory("conditionService",[function(){



        var ifConditionMap = [' == ', ' > ', ' >= ', ' < ', ' <= ']

        return {

            parseCondition: function(condition){
                    var ifString = "if( (1==1) "
                    var ifs = condition._ifs;
                    if(!ifs) return;

                    angular.forEach(ifs, function(_if){
                        ifString+=" &&( ";

                        ifString+="<%=_if%>";
                        ifString+=ifConditionMap[_if._if];
                        ifString+=_if._value;
                        ifString+=" )";
                    })

                    ifString += " )";

                    var thenString = " { ";
                    thenString += " <%=_then%> ";
                    thenString += " } ";

                return ifString+thenString;
            }

        };
    }]);
angular.module("dashboard.services").factory(
  'configuration',
  [function () {
    var default_chart_options = function(){
            return {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    backgroundColor: 'rgba(0,0,0,0)',
                    "borderWidth": 1,
                    "borderColor": "rgba(0,0,0,0)",
                    "animation": false
                },
                title: {
                    text: ''
                },
                "legend": {
                    "align": "right",
                    "verticalAlign": "middle",
                    "layout": "vertical",
                    "itemStyle": {
                        "fontWeight": "normal"
                    }
                },

                "colors": [
                    "#497bac",
                    "#a63600",
                    "#fb7737",
                    "#8a6060",
                    "#2584ac",
                    "#29b4b8",
                    "#344a60",
                    "#00437a",
                    "#96b60d",
                    "#b6cbdf"
                ],

                credits: {
                    enabled: false
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false
                        },
                        shadow: false,
                        center: ['50%', '50%'],
                        borderWidth: 0 ,// < set this option
                        animation:false
                    },
                    series: {
                        states: {
                            hover: {
                                enabled: false,
                                halo: {
                                    size: 0
                                }
                            }
                        },
                        animation:false

                    },

                    "column": {
                        "borderWidth": 0,
                        animation:false
                    },
                    line:{
                        animation:false
                    }
                },
                series: [{
                    colorByPoint: true,

                    data: []
                }]

            }
        }

    return {
      chart_options: default_chart_options
    };
  }]
);

angular.module("dashboard.services").config(['$httpProvider',function(a){
    //a.defaults.headers.put['Content-Type'] = 'application/json'
    a.defaults.xsrfCookieName = 'csrftoken';
    a.defaults.xsrfHeaderName = 'X-CSRFToken';
    // /post_wrapper(a);
}])
.factory("queryService",["$rootScope","$http", function($rootScope, $http){
    var cleanUp = function (obj) {
      var ret = {};
      var keys = Object.keys(obj);
      keys.forEach(function (key) {
        if (!key.match(/^(?:_|\$)/)) {
          ret[key] = obj[key];
        }
      });
      return ret;
    };

    var dashboards = function(){
        return $http({
            url: '/api/dashboard/list'
        })
    }

    var widgets = function(){
        return $http({
            url: '/api/widgets/'
        })
    }
    var widget = function(id){
        return $http({
            url: '/api/widgets/'+id
        })
    }
    var active_dashboard = function(dashboard){
        return $http({
            url:'/api/account/mysession/',
            method:'post',
            data: {
                active_dashboard:dashboard.uid
            }
        })
    }

    var current_dashboard = function(){
        return $http({
            url:'/api/account/mysession/'
        })
    }
    var add_dashboard = function(dashboard){
        return $http({
            url:'/api/dashboard/create/',
            method:'post',
            data:{
                title: dashboard.title,
                grid_cols:dashboard.grid_cols
            }
        })
    }
    var update_dashboard = function(dashboard){
        return $http({
            url:'/api/dashboard/'+dashboard.uid+"/?detail=false",
            method:'post',
            data: cleanUp(dashboard)
        })
    }

    var remove_dashboard = function(dashboard){
        return $http({
            url:'/api/v1/dashboard/'+dashboard.id+"/",
            method:'delete',
            data: cleanUp(dashboard)
        })
    }
    var update_widget = function(widget){
        return $http({
            url:'/api/widget/'+widget.uid+"/?detail=false",
            method:'post',
            data:
                {
                    "width":widget.sizeX,
                    "height":widget.sizeY,
                    "coord_x":widget.row,
                    "coord_y":widget.col,
                    "title":widget.title,
                    "name":widget.name
                }

        })
    }
    var update_widget_verbatim = function(widget) {
        return $http({
            url:'/api/widget/'+widget.uid+"/?detail=false",
            method:'post',
            data: cleanUp(widget)
        })
    }

    var update_widgets = function(widgets){
        for(var i = 0 ; i<widgets.length; i++){
            update_widget(widgets[i]);
        }
    }
    var dashboard_pages = function(){
        return $http({
            url:'/api/v1/dashboard/?user='+$rootScope.current_user_id
        });
    };


    var add_widget = function(widget_data){
        return $http({
            url:'/api/widget/create/',
            method:'post',
            data: JSON.stringify(widget_data),
            headers: {
                'Content-Type': 'application/json'
            }
        })

    }


    var add_widget_to_dashboard = function(dashboard, widget){
        return $http({
            url:'/api/dashboard/'+dashboard.uid+"/add_widget/"+widget.uid+"/",
            method:'post'
        })
    }


    var fetch_data = function(uid){
        return $http({
            url:'/api/datasources/'+uid+'/data'
        })
    }



    var fetch_expr = function(expr, uid){
        var request_param="";
        if(expr.column_start_index!=null){
            request_param+=("&column_start_index="+expr.column_start_index);
        }
        if(expr.column_end_index!=null){
            request_param+=("&column_end_index="+expr.column_end_index);
        }
        if(expr.row_start_index!=null){
            request_param+=('&row_start_index='+expr.row_start_index);
        }
        if(expr.row_end_index!=null){
            request_param+=('&row_end_index='+expr.row_end_index);
        }
        return $http({
            url:'/api/datasources/'+uid+'/data/?flat=true'+request_param
        })//column_start_index=1&column_end_index=10&row_start_index=M&row_end_index=N
    }

    var fetch_expr_interval = function(expr, uid, interval, callback){
        var request_param="";
        if(expr.column_start_index!=null){
            request_param+=("&column_start_index="+expr.column_start_index);
        }
        if(expr.column_end_index!=null){
            request_param+=("&column_end_index="+expr.column_end_index);
        }
        if(expr.row_start_index!=null){
            request_param+=('&row_start_index='+expr.row_start_index);
        }
        if(expr.row_end_index!=null){
            request_param+=('&row_end_index='+expr.row_end_index);
        }


        $http({
            url:'/api/datasources/'+uid+'/data/?flat=true'+request_param
        }).success(function(data){
            callback(data)
        })

        setInterval(function(){
            $http({
                url:'/api/datasources/'+uid+'/data/?flat=true'+request_param
            }).success(function(data){
                callback(data)
            })
        }, interval)
    }

    var fetch_expr_both = function(expr, uid, interval, callback){
        if(interval){
            fetch_expr_interval(expr, uid, interval, function(data){
                callback(data)
            });
        }
        else {
            fetch_expr(expr, uid).success(function (data) {
                callback.call(null, data);
            })
        }
    }

    var next_widget = function(){
        return $http({
                url:'/api/widgets/next'
        })
    }

    var first_widget = function(){
        return $http({
            url:'/api/widgets/first'
        })
    }

    var last_widget = function(){
        return $http({
            url:'/api/widgets/last'
        })
    }

    var prev_widget = function(){
        return $http({
            url:'/api/widgets/prev'
        })
    }


    return {

        xhr_dashboards: function(){
            return dashboards();
        },

        xhr_widgets : function(){
            return widgets();
        },
        xhr_widget: function(id){
            return widget(id);
        },
        xhr_active_dashboard: function(dashboard){
            return active_dashboard(dashboard)
        },
        xhr_current_dashboard: function(){
            return current_dashboard()
        },
        xhr_add_dashboard: function(dashboard){
            return add_dashboard(dashboard);
        },
        xhr_add_widget: function(widget){
            return add_widget(widget)
        },
        xhr_update_widget: function(widget){
            return update_widget(widget);
        },
        xhr_update_widget_verbatim: function (widget) {
          return update_widget_verbatim(widget);
        },
        xhr_update_widgets: function(dashboard){
            return update_widgets(dashboard);
        },
        xhr_update_dashboard: function(dashboard){
            return update_dashboard(dashboard);
        },
        xhr_remove_dashboard: function(dashboard){
            return remove_dashboard(dashboard);
        },
        xhr_add_widget_to_dashboard: function(dashboard,widget){
            return add_widget_to_dashboard(dashboard,widget)
        },
        xhr_fetch_data : function(uid){
            return fetch_data(uid)
        },
        xhr_fetch_expr : function(expr,uid){
            return fetch_expr(expr, uid);
        },
        xhr_fetch_expr_interval: function(expr, uid, interval, callback){
            return fetch_expr_interval(expr, uid, interval, callback);
        },
        xhr_fetch_expr_both: function(expr, uid, interval, callback){
            return fetch_expr_both(expr, uid, interval, callback);
        },
        xhr_next_widget: function(){
            return next_widget();
        },
        xhr_prev_widget: function(){
            return prev_widget();
        },
        xhr_first_widget: function(){
            return first_widget();
        },
        xhr_last_widget: function(){
            return last_widget();
        }
    };
}]) ;

angular.module("dashboard.services").factory("constantsService",['queryService',function(queryService){
    return {
        init_constants: function($scope){
            $scope.dashboards = [];
            $scope.current_dashboard = null;
            $scope.widgets = [];
            $scope.hide_top_bar = false;
            $scope.use_custom = true;

            $scope.use_canvas = false;
            $scope.carouseling = null;
            $scope.seeding =false;

            $scope.status = {
                tab_adding : false,
                full_screen: false,
                layout_switch : false,
            }

            $scope.refresh_status = function(){
                $scope.status = {
                    tab_adding : false,
                    full_screen: false,
                    layout_switch : false,
                }
            }

            //overall dashboard options

            $scope.dashboard_opts = {
                columns:6,
                isMobile: true,
                minColumns: 1,
                pushing: true,
                margins: [15, 15],
                rowHeight: 150,
                swapping:true,
                row_factor:2,
                resizable: {
                    enabled: true,
                    handles: [ 'se'],
                    start: function(event, $element, widget) {
                        widget.current_resizing_x = widget.sizeX;
                        widget.current_resizing_y = widget.sizeY;
                    },
                    resize: function(event, $element, widget) {},
                    stop: function(event, $element, widget) {
                        if(widget.current_resizing_x == widget.sizeX && widget.current_resizing_y == widget.sizeY){
                            return ;
                        }
                        var widgets = $scope.widgets
                        angular.forEach(widgets, function(w){
                            w.height = w.sizeY;
                            w.width = w.sizeX;
                            queryService.xhr_update_widget(w);
                        })
                    }
                },
                draggable: {
                    enabled: true,
                    handle: '.widget-header',
                    start: function(event, $element, widget) {
                        widget.current_dragging_row = widget.row;
                        widget.current_dragging_col = widget.col
                    },
                    drag: function(event, $element, widget) {},
                    stop: function(event, $element, widget) {
                        if(widget.current_dragging_col == widget.col && widget.current_dragging_row == widget.row){
                            return;
                        }

                        var widgets = $scope.widgets
                        angular.forEach(widgets, function(w){
                            queryService.xhr_update_widget(w);
                        })
                    }
                }
            }

            //templates


        }
    };
}]);

angular.module("app.services.scope.fullscreen",[],angular.noop)
.factory("fullscreenOperationService",['$interval',function($interval){
    return {
       init_operation: function($scope){
            $scope.full_screen_mode = function(){
                if($scope.full_screen == false){
                    angular.element('body').addClass('fullscreen')
                }
                else{
                    angular.element('body').removeClass('fullscreen')
                }
                $scope.full_screen = !$scope.full_screen;
                $scope.layout_panel_visible = false;
                if($scope.carouseling!=null){
                    $interval.cancel($scope.carouseling)
                    $scope.carouseling = null;
                }
                 angular.element(window).resize();
            }

            $scope.toggle_carousel = function(){
                if($scope.full_screen){
                    if($scope.carouseling == null){
                        $scope.carouseling = $interval(function(){
                            $scope.tab_active_index  = ($scope.tab_active_index+1)%$scope.dashboards.length
                        }, 3000);
                    }
                    else{
                        $interval.cancel($scope.carouseling);
                        $scope.carouseling = null;
                    }
                }
            }
       }    
    };
}]);
angular.module("dashboard.services").factory("initService", [
  'queryService',
  function(
    queryService
  ) {
    return {
      init_operation: function($scope) {
        queryService.xhr_widgets().success(function(data){
          $scope.widgets = data;
        })
      }
    };
  }
]);

angular.module("app.services.scope.tab",[],angular.noop)
.factory("tabOperationService",['queryService', function(queryService){
    return {
       init_operation: function($scope){
            $scope.sw_tab = function(index){
                $scope.tab_active_index = index
            }

            $scope.tab_decration = function(){
               for(var i =0;i<$scope.tabs.length;i++) {
                    angular.extend($scope.tabs[i],$scope.default_tabs_ui)
               }
            }

            $scope.add_dashboard = function(){
                var default_title = "New Dashboard";
                queryService.xhr_add_dashboard(default_title).success(function(data){
                    var new_dashboard_tab = {
                        title: 'New Dashboard',
                        opts : angular.extend({columns: 6}, $scope.overall_dashboard_opts)
                    }
                    $scope.dashboards.push(new_dashboard_tab)
                    $scope.tab_active_index = $scope.dashboards.length-1
                    $scope.tab_adding = false;
                })
            }
            $scope.rename_dashboard = function(dashboard){
                dashboard.editing = false;
                if(dashboard.edit_title === dashboard.title){
                    return;
                }

                dashboard.title = dashboard.edit_title;
                //xhr update
                queryService.xhr_update_dashboard(dashboard);
            }

            $scope.remove_dashboard = function(dashboard){
                queryService.xhr_remove_dashboard(dashboard).success(function(data){
                    for(var i =0 ;i<$scope.dashboards.length;i++){
                        if($scope.dashboards[i].id == dashboard.id){
                            $scope.dashboards.splice(i, 1)
                        }
                    }
                    $scope.tab_active_index = 0;
                });
            }

       }    
    };
}]);