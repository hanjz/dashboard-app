package com.iota.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iota.app.data.DataSourceDao;
import com.iota.app.model.WidgetDataSource;
import com.iota.app.model.Widget;

@Service
public class WidgetDataSourceService {

	@Autowired
	private DataSourceDao dataSourceDao;
	
	@Autowired
	private WidgetService widgetService;
	
	public WidgetDataSource addDataSourceToWidget(String uuid, String widgetId){
		Widget widget = widgetService.findById(widgetId);
		return addDataSourceToWidget(uuid, widget);
	}
	public WidgetDataSource addDataSourceToWidget(String uuid, Widget widget){
		WidgetDataSource dataSource = new WidgetDataSource();
		dataSource.setUid(uuid);
		dataSource.setWidget(widget);
		dataSourceDao.save(dataSource);
		return dataSource;
	}

}
