angular.module('widget.components')
.directive('ctrlDraggable', [function(){
    return {
       scope:{
            data:"="
       },
       link:function(scope, element, attrs){
            if(attrs.ctrlDraggable == 'false'){
                return;
            }
            element.draggable({
                    helper:'clone',
                    appendTo:'.middle',
                    handle : attrs.handle=='no'?false:'.handle',
                    start:function(event, ui){
                        event.stopPropagation();
                        $(this).data("current-dragging-data", scope.data)
                        console.log(scope.data)
                    },
                    drag: function(event, ui) {
                        event.stopPropagation();
                        var $droppable = $(this).data("current-droppable");
                        if ($droppable){
                            var dragging_top = ui.offset.top;
                            var dragging_left = ui.offset.left;
                            var is_break = false;
                            var children_length = 0 ;
                            
                            $droppable.find(">ul").children("li").each(function(index, obj){
                                children_length +=1;
                                if(!is_break){
                                    var ctrls_top = $(obj).offset().top;
                                    var ctrls_left = $(obj).offset().left;
                                    if(dragging_top < ctrls_top){
                                        is_break = true;
                                        if($droppable.find(".placeholder").length==0 || $droppable.find(".placeholder").index()!=index){
                                            $($droppable).find('.placeholder').remove()
                                            $(obj).before($("<li class='placeholder'></li>"))
                                        }
                                    }
                                    
                                }
                            })

                            if(is_break == false ){
                                if($droppable.find(".placeholder").length ==0 || $droppable.find(".placeholder").index() != $droppable.children().length-1){
                                    $droppable.find(".placeholder").remove();
                                    $droppable.find(">ul").append("<li class='placeholder'></li>");
                                }
                            }
                        }
                    }
                })
            element.disableSelection();
       }
    }
}])
.directive('canvasDraggable',[function(){
     return {
       scope:{
            data:"="
       },
       link:function(scope, element, attrs){
            if(attrs.ctrlDraggable == 'false'){
                return;
            }
            element.draggable({
                    helper:attrs.clone?'clone':'original',
                    appendTo:'.middle',
                    handle : attrs.handle=='no'?false:'.handle',
                    start:function(event, ui){
                       
                        event.stopPropagation();
                        $(this).data("current-dragging-data", scope.data)
                        $(this).data('is-new-data', attrs.clone)
                    },
                    drag: function(event, ui) {
                        event.stopPropagation();
                    },
                    stop: function(event, ui){
                        var parent_width = $(this).parent().width();
                        var parent_height = $(this).parent().height();

                        var current_top = $(this).position().top;
                        var current_left = $(this).position().left;

                        var current_top_percentage = current_top*100/parent_height + "%";
                        var current_left_percentage = current_left*100/parent_width + "%";
                        $(this).css('left',current_left_percentage);
                        $(this).css('top',current_top_percentage);
                        scope.data.left = current_left_percentage;
                        scope.data.top = current_top_percentage;
                    }
            })
            element.disableSelection();
       }
    }
}])