angular.module( 'app.widget.components.chart',['services'], angular.noop )
.directive('chartseriesComponent', [function(){
        return {
            restrict:'E',
            templateUrl: '/templates/view/components/chart.html',
            controller: function($scope){

            },
            link: function(scope, element, attrs){
                var chart_data = scope.component;
                var axes_data =  chart_data.axes;
                var series_data = chart_data.series;

                var type_map = {
                    'bar':'column',
                    'line':'line',
                    'area':'area'
                }
                var x_axis_data = axes_data.filter(function(a){return a.axis_type==0})
                var y_axis_data = axes_data.filter(function(a){return a.axis_type==1})
                if(x_axis_data.length == 0 || y_axis_data.length==0) {
                    return ;
                }

                var x_axis_labels = null;
                try{
                    x_axis_labels = eval(x_axis_data[0].data);
                }   
                catch(error){
                    console.error('x axis data format error : '+ x_axis_data[0])
                    return;
                }   

                var chart_elem = element.find('.chart-container');
                var option = {
                    chart: {
                        renderTo: chart_elem[0],
                         events: {
                            load: function() {
                                var c = this;
                                console.log('load')
                                setTimeout(function() {
                                    c.reflow();
                                }, 123)
                            }
                        }
                    },
                    title: {
                        text: chart_data.name
                    },
                    legend: {
                        enabled: chart_data.show_legend||false
                    }, 
                    xAxis: {
                        categories: eval(x_axis_data[0].data)
                    },
                    yAxis: {
                        title: {
                            text: y_axis_data[0].title
                        },
                        lineWidth: 1,
                        lineColor: "#d4d4d4"
                    },
                    credits: {
                        "enabled": false
                    },
                    colors: [
                            "#518a86",
                            "#3f6973",
                            "#2e8780",
                            "#44989f",
                            
                        ],
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: series_data.map(function(obj){
                        var y_data = null;
                        try{
                            y_data = eval(obj.data)
                        }
                        catch(error){
                            console.error('y data error: '+ obj.data)
                        }
                        return {
                            name: obj.series_label,
                            data: y_data,
                            type: type_map[obj.chart_type] == null? obj.chart_type: type_map[obj.chart_type]
                        }
                    })
                    
                }

                var chart = new Highcharts.Chart(option)
                if(window.charts ==null){
                    window.charts = []
                }
                window.charts.push(chart)
                angular.element(window).resize(function(){
                    chart.reflow()
                })
                scope.$watch('component.widget.sizeX', function(x){
                    console.log('sizex')
                    setTimeout(function() {
                        chart.reflow();
                        }, 200)
                })

                scope.$watch('component.dashboard.opts.columns', function(x){
                    console.log('watch')


                    setTimeout(function() {
                        chart.reflow();
                        }, 200)

                }) 

            }
        };
}])