angular.module("widget.services").config(['$httpProvider',function(a){
    a.defaults.headers.put['Content-Type'] = 'application/json'
    a.defaults.xsrfCookieName = 'csrftoken';
    a.defaults.xsrfHeaderName = 'X-CSRFToken';
    //post_wrapper(a);
}])
.factory("queryService",["$rootScope","$http", function($rootScope, $http){
   
    var widget = function(widget_id){
        return $http({
            url:'/api/widgets/'+widget_id+"/",
        })
    }
    var new_widget = function(widget){
        return $http({
            url:'/api/widgets/',
            method:'post',
            data: widget
        })
    }
    var update_widget = function(widget){
        /*widget.coord_x = widget.sizeX;
        widget.coord_y = widget.sizeY;
        widget.height = widget.row;
        widget.width = widget.col*/
    	widget.isValid = true
        return $http({
            url:'/api/widgets/'+widget.id+"/",
            method:'post',
            data: widget
        })
    }

   
    var fetch_expr = function(expr, uid){
        var request_param="";
    	if(expr.column_start_index!=null){
    		request_param+=("&column_start_index="+expr.column_start_index);
    	}
    	if(expr.column_end_index!=null){
    		request_param+=("&column_end_index="+expr.column_end_index);
    	}
    	if(expr.row_start_index!=null){
    		request_param+=('&row_start_index='+expr.row_start_index);
    	}
    	if(expr.row_end_index!=null){
    		request_param+=('&row_end_index='+expr.row_end_index);
    	}
        return $http({
            url:'/api/datasources/'+uid+'/data/?flat=true'+request_param
        })//column_start_index=1&column_end_index=10&row_start_index=M&row_end_index=N
    }

    var datasources = function(){
        return $http({
            url:'/api/datasources/'
        })
    }

    var widget_datasources = function(widget_id){
    	return $http({
    		url:'/api/widgets/'+widget_id+"/datasources"
    	})
    }
    
    var datasources_detail = function(datasource_id_list){
    	var requestParam = "";
    	datasource_id_list.forEach(function(datasource_id){
    		requestParam+=("uids="+datasource_id+"&");
    	})
    	return $http({
    		url:'/api/datasources/list/?'+requestParam
    	})
    }
    var sample_data = function(id){
    	return $http({
    		url:'/api/datasources/'+id+'/data'
    	})
    }
    var fetch_data = function(uid){
        //
        return $http({
            url:'/api/datasources/'+uid+'/'
        })
    }

    var attach_datasource = function(widget_id, datasource_uid){
    	///api/widgets/{widget_id}/datasources/{datasource_id}
        return $http({
            url:'/api/widgets/'+widget_id+"/datasources/"+datasource_uid+"/",
            method:'post',
        })
    }

    return {
        xhr_widget: function(id){
            return widget(id);
        },
        xhr_get_dashboard_by_widget: function(uid) {
            return get_dashboard_by_widget(uid);
        },
        xhr_new_widget: function(widget){
            return new_widget(widget);
        },
        xhr_update_widget: function(widget){
            return update_widget(widget);
        },
        xhr_fetch_expr : function(expr,uid){
            return fetch_expr(expr, uid);
        },
        xhr_datasources: function(uid){
            return datasources(uid);
        },
        xhr_fetch_data : function(uid){
            return sample_data(uid)
        },
        xhr_attach_datasource: function(widget_uid, datasource_uid){
            return attach_datasource(widget_uid, datasource_uid);
        },
        xhr_sample_data: function(id){
        	return sample_data(id)
        },
        xhr_widget_datasources: function(widget_id){
        	return widget_datasources(widget_id)
        },
        xhr_datasources_detail: function(datasources_id_list){
        	return datasources_detail(datasources_id_list);
        }
    };
}]) ;