angular.module( 'dashboard.components')
.directive('gridComponent', [function(){
        return {
            restrict:'E',
            templateUrl: '/templates/view/components/grid.html',
            controller: function($scope){
                $scope.getTimes = function(n){
                    return new Array(n)
                }
            },
            link: function(scope, element, attrs){
                /*
                    to implement the colspan/rowspan,  need to hide the overflow td.
                    the hidden td can only be right or down position of the *span td.
                    so each cell must hold the ptr of right and down position cell so as to 
                    make them hidden when be set as *span.

                */
                var grid_component = scope.component;
                scope.rows = grid_component.rows;
                scope.cols = grid_component.cols;
                scope.widget = scope.component.widget;
                scope.dashboard = scope.component.dashboard;
                scope.layout_cells = new Array(scope.rows);

                for(var i = 0;i<scope.rows;i++){
                    scope.layout_cells[i] = new Array(scope.cols);
                }
                var grid_cells = grid_component.cells;
                angular.forEach(grid_cells, function(grid_cell){
                    grid_cell.rows = grid_component.rows;
                    grid_cell.cols = grid_component.cols;
                    grid_cell.visible = true;
                    if(grid_cell.coord_x<scope.rows&&grid_cell.coord_y<scope.cols){
                        scope.layout_cells[grid_cell.coord_x][grid_cell.coord_y] = grid_cell;
                    }
                })
                for(var i = 0 ;i<scope.rows;i++){
                    for(var j = 0;j<scope.cols;j++){
                       /* if(scope.layout_cells[i][j]!=null){
                            //to find the first not null right cell.
                           var k = j;
                           while(k<scope.cols&&scope.layout_cells[i][++k]==null);
                           scope.layout_cells[i][j].right_cell = scope.layout_cells[i][k];

                           //to find the first not null down cell.
                           var x = i;
                           while(x<scope.rows&&scope.layout_cells[++x][j]==null);
                           scope.layout_cells[i][j].down_cell = scope.layout_cells[x][j];
                        }*/

                        var current_cell = scope.layout_cells[i][j];
                        if(current_cell==null){
                            throw new Error('null cell in grid.');
                        }
                        if(j<scope.cols-1){
                            scope.layout_cells[i][j].right_cell = scope.layout_cells[i][j+1];
                        }
                        if(j>0) {
                            scope.layout_cells[i][j].left_cell = scope.layout_cells[i][j-1]
                        }
                        if(i<scope.rows-1){
                            scope.layout_cells[i][j].down_cell = scope.layout_cells[i+1][j];
                        }
                        if(i>0){
                            scope.layout_cells[i][j].up_cell = scope.layout_cells[i-1][j];
                        }

                        for(var k = 1 ;k<scope.layout_cells[i][j].width;k++){
                            scope.layout_cells[i][j+k].visible = false;
                        }

                        for(var x = 1;x<scope.layout_cells[i][j].height;x++){
                            scope.layout_cells[i+x][j].visible = false;
                        }
                    }
                }
                
            }
        };
}])