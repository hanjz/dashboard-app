package com.iota.app.service;

import java.util.ArrayList;
import java.util.List;

import com.iota.app.vo.WidgetVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.iota.app.model.Widget;
import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public class ValueService {
	@Value("${img.server}")
	private String imageServerUrl;

	@Value("${bg.server}")
	private String bgServerUrl;

	@Value("${bg.local}")
	private String bgServerLocal;

	@Value("${db.server}")
	private String dbServerUrl;

	@Value("${server.port}")
	private Integer serverPort;

	public WidgetVo fromWidget(Widget widget) {
		if (widget == null) {
			return null;
		}
		WidgetVo vo = new WidgetVo();
		BeanUtils.copyProperties(widget, vo);
		if(widget.getThumbnail()!=null){
			vo.setThumbnail(this.imageServerUrl+widget.getThumbnail());
		}
		if(widget.getBackground()!=null){
			vo.setBackground(this.bgServerUrl+widget.getBackground());
			vo.setLocalBackground(this.bgServerUrl+widget.getBackground());
			if(StringUtils.isNotEmpty(this.bgServerLocal)) {
				vo.setLocalBackground(this.bgServerLocal + widget.getBackground());
			}
		}


		return vo;
	}

	public Widget toWidget(WidgetVo vo){
		Widget widget = new Widget();
		if(vo.getThumbnail()!=null && vo.getThumbnail().startsWith(imageServerUrl)){
			vo.setThumbnail(vo.getThumbnail().substring(imageServerUrl.length()));
		}

		if(vo.getBackground()!=null && vo.getBackground().startsWith(bgServerUrl)){
			vo.setBackground(vo.getBackground().substring(bgServerUrl.length()));
		}
		BeanUtils.copyProperties(vo, widget);
		return widget;
	}
	public Widget copyWidget(Widget src, Widget dest){

		String id = dest.getId();
		BeanUtils.copyProperties(src, dest);
		dest.setId(id);
		return dest;
	}
	public List<WidgetVo> fromWidgets(List<Widget> widgets) {
		List<WidgetVo> vos = new ArrayList<WidgetVo>();
		if (widgets!= null) {
			for (Widget widget : widgets) {
				vos.add(fromWidget(widget));
			}
		}
		return vos;
	}
}
