angular.module('dashboard')
    .controller('scrollController',
        [
            '$rootScope',
            '$scope',
            'queryService',
            function(
                $rootScope,
                $scope,
                queryService
            ){
                $scope.id = angular.element("#widget_id").val();

                getNextWidget();
                $scope.pause = false;
                function getNextWidget(){
                    return queryService.xhr_next_widget().success(function(data){
                        $scope.component = data;
                        if(data.components!=null){
                            $scope.component.components = JSON.parse(data.components);
                        }
                    })
                }

                function getPrevWidget(){
                    return queryService.xhr_prev_widget().success(function(data){
                        $scope.component = data;
                        if(data.components!=null){
                            $scope.component.components = JSON.parse(data.components);
                        }
                    })
                }

                function getFirstWidget(){
                    return queryService.xhr_first_widget().success(function(data){
                        $scope.component = data;
                        if(data.components!=null){
                            $scope.component.components = JSON.parse(data.components);
                        }
                    })
                }

                function getLastWidget(){
                    return queryService.xhr_first_widget().success(function(data){
                        $scope.component = data;
                        if(data.components!=null){
                            $scope.component.components = JSON.parse(data.components);
                        }
                    })
                }



                $scope.interval = setInterval(function(){
                    angular.element("#container").fadeOut();
                    getNextWidget().then(function(){
                        angular.element("#container").fadeIn();
                    });
                }, 5000);

                $scope.play_pause = function(){
                    if($scope.pause) {
                        $scope.interval = setInterval(function(){
                            angular.element("#container").fadeOut();
                            getWidget().then(function(){
                                angular.element("#container").fadeIn();
                            });
                        }, 5000);
                        $scope.pause = false;
                    }
                    else{
                        clearInterval($scope.interval);
                        $scope.pause = true;
                    }
                }


                $scope.next = function(){
                    clearInterval($scope.interval);
                    angular.element("#container").fadeOut();
                    getNextWidget().then(function(){
                        angular.element("#container").fadeIn();
                    });
                }

                $scope.last = function(){
                    clearInterval($scope.interval);
                    angular.element("#container").fadeOut();
                    getLastWidget().then(function(){
                        angular.element("#container").fadeIn();
                    });
                }

                $scope.first = function(){
                    clearInterval($scope.interval);
                    angular.element("#container").fadeOut();
                    getFirstWidget().then(function(){
                        angular.element("#container").fadeIn();
                    });
                }

                $scope.prev = function(){
                    clearInterval($scope.interval);
                    angular.element("#container").fadeOut();
                    getPrevWidget().then(function(){
                        angular.element("#container").fadeIn();
                    });
                }
                $scope.recover = function(){
                    $scope.interval = setInterval(function(){getNextWidget()}, 5000);
                }
            }])
