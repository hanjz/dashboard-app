angular.module("dashboard.services").factory(
  'configuration',
  [function () {
    var default_chart_options = function(){
            return {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    backgroundColor: 'rgba(0,0,0,0)',
                    "borderWidth": 1,
                    "borderColor": "rgba(0,0,0,0)",
                    "animation": false
                },
                title: {
                    text: ''
                },
                "legend": {
                    "align": "right",
                    "verticalAlign": "middle",
                    "layout": "vertical",
                    "itemStyle": {
                        "fontWeight": "normal"
                    }
                },

                "colors": [
                    "#497bac",
                    "#a63600",
                    "#fb7737",
                    "#8a6060",
                    "#2584ac",
                    "#29b4b8",
                    "#344a60",
                    "#00437a",
                    "#96b60d",
                    "#b6cbdf"
                ],

                credits: {
                    enabled: false
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false
                        },
                        shadow: false,
                        center: ['50%', '50%'],
                        borderWidth: 0 ,// < set this option
                        animation:false
                    },
                    series: {
                        states: {
                            hover: {
                                enabled: false,
                                halo: {
                                    size: 0
                                }
                            }
                        },
                        animation:false

                    },

                    "column": {
                        "borderWidth": 0,
                        animation:false
                    },
                    line:{
                        animation:false
                    }
                },
                series: [{
                    colorByPoint: true,

                    data: []
                }]

            }
        }

    return {
      chart_options: default_chart_options
    };
  }]
);
