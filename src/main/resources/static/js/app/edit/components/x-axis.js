angular.module( 'widget.components')
.directive('xaxisComponent',[function(){
    return {
        restrict:'AE',
        controller: function($scope,$element){

        },
        link: function(scope, element, attrs){
            scope.component.extra.update = function(){
                scope.component.extra.chart_component.extra.update();
            }
                        
        }
    };
}])
.directive('xaxisProperties', [function(){
    return {
        restrict:'AE',
        templateUrl:'/templates/edit/properties/x-axis.html',
        controller: function($scope,$element){
            $scope.sort_order = [
                {
                    id:0,
                    name:"Don't Sort"
                },
                {
                    id:1,
                    name:'Ascending'
                },
                {
                    id:-1,
                    name:'Descending'
                }
                

            ]

            $scope.use_time_series = function(){
                if($scope.component.data.type =='data'){
                    $scope.component.data.type = 'time';
                }
                else{
                    $scope.component.data.type = 'data'
                }
                $scope.component.extra.update();
            }
            $scope.change_show_label = function(){
                $scope.component.options.show_label = !$scope.component.options.show_label;
                $scope.component.extra.update();
            }

            $scope.change_show_axis = function(){
                $scope.component.options.show_axis = !$scope.component.options.show_axis;
                $scope.component.extra.update();
            }

            $scope.$on('colorpicker-selected', function(event, colorObj){
            	$scope.component.options.axis_color = colorObj.value;
            	$scope.component.extra.update();
            })
            $scope.change_rotate = function(){
                $scope.component.options.rotation = ($scope.component.options.rotation+45)%360;
                $scope.component.extra.update();
            }

            $scope.change_show_tick = function(){
                $scope.component.options.show_tick = !$scope.component.options.show_tick;
                $scope.component.extra.update();
            }
            $scope.change_show_grid = function(){
                $scope.component.options.show_grid = !$scope.component.options.show_grid;
                $scope.component.extra.update();
            }

        },
        link: function(scope, element, attrs){
            
        }
    };
}])