package com.iota.app.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DataVo implements Serializable{
	private Object meta;
	private Object data;

	public static DataVo newInstace(Object meta, Object data){
		return new DataVo(meta, data);
	}
}
