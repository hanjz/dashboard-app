package com.iota.app.vo;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class WidgetVo {
	private String id;
	private String background;
	private String localBackground;
	private Boolean isCanvas;
	private Boolean isValid;
	private String name;
	private Integer index;
	private String components;
	private String type;
	private String thumbnail;
	private Boolean heightPercentage;
	private Boolean widthPercentage;
	private Integer width;
	private Integer height;

	public String toString(){
		return "ID: "+ this.id + " , name : "+this.name +" , isCanvas: "+this.isCanvas+ " , components : "+this.components;
	}
}
