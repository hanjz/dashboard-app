angular.module( 'dashboard.components')
.directive('serieschartComponent', ['$rootScope','configuration','queryService','$timeout',"$q",function($rootScope,configuration,queryService, $timeout, $q){

        var series_chart ; 
        var verify_data = function(component){
            var x_axis = component.extra.x_axis;
            var series_collection = component.extra.series_collection;
            if(x_axis.data == null) {
                return false;
            }
            return true;
        }


        return {
            restrict:'AE',
            templateUrl:'/templates/view/components/series-chart.html',
            controller: function($scope,$element){
            	$scope.$emit('component-register', $scope.component);
                $scope.component.extra = $scope.component.extra || {}



                var draw_chart = function(container, chart_data, _options){
                    if(chart_data && chart_data.stream){
                        Highcharts.setOptions({
                            global : {
                                useUTC : false
                            }
                        });
                    }
                    var options = configuration.chart_options();
                    options.legend = {
                        enabled: _options?_options.show_legend:true,
                        align: "right",
                        verticalAlign: "top",
                        layout: "horizontal",
                        itemStyle: {
                            fontWeight: "normal"
                        }
                    },
                        options.chart.type='line';

                    options.chart.inverted = _options?_options.inverted:false;
                    if(chart_data && chart_data.stream!=true) {

                        options.xAxis = {
                            categories: chart_data ? chart_data.xAxis.data : [1],
                            title: {
                                enabled: chart_data ? chart_data.xAxis.options.show_axis : false,
                                text: chart_data ? chart_data.xAxis.options.axis_title : null
                            },
                            labels: {
                                enabled: chart_data ? chart_data.xAxis.options.show_label : true,
                                style: {
                                    color: chart_data ? chart_data.xAxis.options.axis_color : "#000"
                                }
                            },
                            gridLineWidth: chart_data && chart_data.xAxis.options.show_grid ? 1 : 0
                        };

                        if (chart_data && chart_data.xAxis.options.show_tick == false) {
                            options.xAxis.tickLength = 0;
                        }
                        if (chart_data && chart_data.xAxis.options.rotation != 0) {
                            options.xAxis.labels.rotation = chart_data.xAxis.options.rotation;
                        }
                        if (chart_data && chart_data.xAxis.options.color) {
                            options.xAxis.labels.color = chart_data.xAxis.options.color;
                        }
                    }


                    if(chart_data && chart_data.stream){
                        options.rangeSelector = {
                            buttons: [{
                                count: 1,
                                type: 'minute',
                                text: '1M'
                            }, {
                                count: 5,
                                type: 'minute',
                                text: '5M'
                            }, {
                                type: 'all',
                                text: 'All'
                            }],
                            inputEnabled: false,
                            selected: 1
                        }
                    }
                    options.series = chart_data?chart_data.series:[
                        {
                            name:'empty',
                            data:[0]
                        }
                    ]

                    options.legend.labelFormat = (function(opt){
                        var formatter = "{name}";
                        if(opt==null){
                            return formatter
                        }
                        if(opt.show_labels){
                            formatter +="({y:.2f})"
                        }
                        if(opt.show_percentage){
                            formatter+="[{percentage:.2f}%]"
                        }
                        return formatter
                    })(_options)

                    if(_options.legend_fcolor){
                        options.legend.itemStyle={
                            color:_options.legend_fcolor
                        }
                    }

                    if(_options.legend_bcolor){
                        options.legend.backgroundColor=_options.legend_bcolor
                    }

                    if(_options.background){
                        options.chart.backgroundColor = _options.background;
                    }

                    if(chart_data && chart_data.loadCallback){
                        options.chart.events = {
                            load: function(){
                                var chart_obj = this
                                chart_data.loadCallback(chart_obj);
                            }
                        }
                    }
                    if($scope.intervals){
                        $scope.intervals.forEach(function(interval){
                            clearInterval(interval);
                        })
                    }
                    $timeout(function(){

                        if(chart_data && chart_data.stream){
                            if($scope.series_chart){
                                container.find(".chart-container").highcharts().destroy();
                            }
                            $scope.series_chart =null;
                            $scope.series_chart = container.find(".chart-container").highcharts('StockChart', options);
                        }
                        else {
                            $scope.series_chart = container.find(".chart-container").highcharts(options).highcharts();
                            $scope.series_chart.reflow();
                        }
                    },10)
                }






                angular.forEach($scope.component.components, function(component){
                    if(component.type=='series_collection'){
                        $scope.component.extra.series_collection = component.components;
                    }
                    else if(component.type == 'y_axis_collection'){
                        $scope.component.extra.y_axis_collection = component.components;
                    }
                    else{
                        $scope.component.extra.x_axis = component;
                    }
                })



                if(!verify_data($scope.component)){
                    draw_chart($element, null, $scope.component.options);
                    setTimeout(function(){
                        $scope.$emit('component-ready', $scope.component);
                    },10)
                    return;
                }


                $scope.query_update = function(){
                    var chart_data = {}
                    queryService.xhr_fetch_expr($scope.component.extra.x_axis.data.expr,$scope.component.extra.x_axis.data.datasource)
                        .success(function(_x_data){
                            var x_data = _x_data.data;
                            if(x_data.length>0){

                                chart_data.series = [];
                                var x_axis_data = x_data;
                                chart_data.xAxis = {
                                    data:x_axis_data,
                                    options: $scope.component.extra.x_axis.options
                                };
                                var promises = [];
                                angular.forEach($scope.component.extra.series_collection, function(series){
                                    if(series.data.expr!="" && series.data.datasource!=""){

                                        promises.push(queryService.xhr_fetch_expr(series.data.expr,series.data.datasource).success(function(_series_data){
                                            if($scope.component.extra.x_axis.data.type==='time'){
                                                if(_series_data.data.dataSource.type!=='NIFI'){
                                                    alert('time series chart should use stream data !');
                                                    return;
                                                }
                                            }
                                            var series_data = _series_data.data;
                                            var result_data = [];
                                            if(series_data.length!=x_data.length){
                                                for(var i = 0 ; i<x_data.length;i++){
                                                    if(series_data[i]!=null && _(series_data[i]).toNumber(2)!=NaN){
                                                        result_data[i] = _(series_data[i]).toNumber(2) ;
                                                    }
                                                    else{
                                                        result_data[i] = 0;
                                                    }
                                                }
                                            }
                                            else{
                                                result_data = series_data.map(function(data){
                                                    if(_(data).toNumber(2)!=NaN){
                                                        return _(data).toNumber(2);
                                                    }
                                                    else{
                                                        return 0;
                                                    }
                                                });
                                            }

                                            if($scope.component.extra.x_axis.options.sort!=0){
                                                var order= $scope.component.extra.x_axis.options.sort;
                                                var sort_obj = []
                                                for(var i = 0 ;i<x_data.length;i++){
                                                    sort_obj.push({key:x_data[i],value:result_data[i]})
                                                }
                                                sort_obj.sort(function(a,b){
                                                    return a.key>b.key?order:-order
                                                })

                                                result_data = []
                                                x_axis_data = []
                                                for(var i =0 ;i<x_data.length;i++){
                                                    x_axis_data.push(sort_obj[i].key)
                                                    result_data.push(sort_obj[i].value);
                                                }
                                            }



                                            var series_obj = {
                                                type:series.style,
                                                name:series.name,
                                                data: result_data,
                                                dataLabels: {
                                                    enabled:series.options.show_value,
                                                    style: {
                                                        color: "#717171",
                                                        fontSize: "8px",
                                                        fontWeight:"normal"
                                                    },
                                                }
                                            }

                                            if(series.options.use_custom_color){
                                                series_obj.color = series.options.custom_color;
                                            }
                                            chart_data.series.push(series_obj);
                                        }))
                                    }})

                                $q.all(promises).then(function(){
                                    draw_chart($element, chart_data, $scope.component.options);
                                    setTimeout(function(){
                                        $scope.$emit('component-ready', $scope.component);
                                    },10)
                                })
                            }
                            else{
                                draw_chart($element, null, $scope.component.options);
                                return;
                                setTimeout(function(){
                                    $scope.$emit('component-ready', $scope.component);
                                },10)
                            }

                        })


                }


                $scope.stream_update = function(){
                    var chart_data = {};
                    chart_data.stream = true;
                    chart_data.series = [];
                    angular.forEach($scope.component.extra.series_collection, function(series){


                        var series_obj= {
                            name:series.name,
                            style:series.style,
                            datasource:series.datasource,
                            dataLabels: {
                                enabled:series.options.show_value,
                                style: {
                                    color: "#717171",
                                    fontSize: "8px",
                                    fontWeight:"normal"
                                }
                            },
                            data: (function () {
                                // generate an array of random data
                                var data = [], time = (new Date()).getTime(), i;

                                for (i = -999; i <= 0; i += 1) {
                                    data.push([
                                        time + i * 1000,
                                        0
                                    ]);
                                }
                                return data;
                            }())
                        }

                        chart_data.series.push(series_obj);
                    });

                    chart_data.loadCallback = function(chart_obj){
                        chart_data.series.forEach(function(series_obj, index){
                            if(!$scope.interval){
                                $scope.intervals = [];
                            }
                            var current_series_component = $scope.component.extra.series_collection[index];
                            var interval = setInterval(function () {
                                var x = (new Date()).getTime(), // current time
                                    y = 0;
                                if(current_series_component.data.datasource) {
                                    queryService.xhr_fetch_data(current_series_component.data.datasource).success(function (data) {
                                        if (chart_obj.options) {
                                            y = data.data[0]
                                            if(angular.isArray(y)){
                                                y = y[0]

                                            }
                                            if(!isNaN(y)){
                                                y = parseInt(y);
                                            }
                                            chart_obj.series[index].addPoint([x, y], true, true);
                                        }
                                    })
                                }
                                else{
                                    if(chart_obj.options) {
                                        chart_obj.series[index].addPoint([x, y], true, true);
                                    }
                                }

                            }, 3000);

                            $scope.intervals.push(interval);
                        });

                    }
                    draw_chart($element, chart_data, $scope.component.options);
                    setTimeout(function(){
                        $scope.$emit('component-ready', $scope.component);
                    },10)

                }

                if($scope.component.extra.x_axis.data.type==='time'){
                    if($scope.init_stream_chart){

                    }
                    else {
                        $scope.init_stream_chart=  true;
                        $scope.stream_update();
                    }
                }
                else{
                    $scope.query_update();
                }


                $scope.$on('component-reflow', function(){
                    series_chart.reflow();
                })
            },
            link: function(scope, element, attrs){

            }
        };
}])