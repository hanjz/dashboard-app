package com.iota.app.dto.builder;

import com.iota.app.dto.DataObject;
import com.iota.app.dto.category.CSVDataObject;
import com.iota.app.dto.category.CSVMetadata;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by guoxu.wu on 16/8/6.
 */
public class CSVDataObjectBuilder extends DataObjectBuilder{
	/**
	 * 创建csv的数据对象
	 */
	
	@Override
    public DataObject buildDataObject(Map infoMap) throws Exception {
        CSVMetadata metadata = new CSVMetadata();
        buildMetadata(metadata, infoMap);
        CSVDataObject dataObject = new CSVDataObject();
        dataObject.setMeta(metadata);
        dataObject.setData(parseContent(infoMap));
        List<Object> records = dataObject.getData();
        if(records.size()>0){
            Object[] header = (Object[])records.get(0);
            for(int i = 0 ; i<header.length; i++){
                metadata.addColumn(header[i].toString(), "string");
            }
        }
        return dataObject;
    }

    public List<Object[]> parseContent(Map infoMap) throws Exception {
        List<Object[]> parseData = new ArrayList();
        InputStream is = (InputStream) infoMap.get("content");
        CSVParser parser = CSVFormat.DEFAULT.
                withIgnoreSurroundingSpaces().
                withTrim().
                withAllowMissingColumnNames(false).
                withIgnoreHeaderCase().
                withIgnoreEmptyLines().
                withTrim().parse(new InputStreamReader(is));
        Iterable<CSVRecord> records = parser.getRecords();
        for (CSVRecord record : records) {
            List<String> dataItem = new ArrayList();
            record.forEach(n -> dataItem.add(n));
            Boolean isNotEmpty = false;
            for (String data : dataItem) {
                if (StringUtils.isNotBlank(data)) {
                    isNotEmpty = true;
                }
            }
            if (isNotEmpty) {
                parseData.add(dataItem.toArray());
            }
        }
        return parseData;
    }
}
