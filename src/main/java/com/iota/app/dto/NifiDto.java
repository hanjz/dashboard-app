package com.iota.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by wgx on 16/8/31.
 */
@Getter
@Setter
public class NifiDto {
    private String type = "NIFI";
    private Map params = new HashMap();
    private String description ;
    private String name;

    public static NifiDto newInstance(String name, String description, String portName, String url){
        NifiDto dto = new NifiDto();
        dto.setDescription(description);
        dto.setName(name);
        dto.getParams().put("nifi.output.port.name", portName);
        dto.getParams().put("nifi.url", url);

        return dto;
    }
}
