package com.iota.app.data;

import com.iota.app.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by wgx on 16/9/26.
 */
public interface AccountDao  extends JpaRepository<Account, String> {
    Account findOneByEmail(String email);
}
