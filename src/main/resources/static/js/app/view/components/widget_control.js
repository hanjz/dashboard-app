angular.module( 'dashboard.components')
.directive('widgetCtrl', ["$compile","$uibModal", function($compile, $uibModal){
    return {
        restrict:'AE',
        replace:true,
        templateUrl:'/templates/view/components/widget_control.html',
        controller:function($scope){
        },
        link: function(scope, element, attrs){
            scope.open_new_widget_dialog = function(){
                $uibModal.open({
                  animation: true,
                  templateUrl: '/templates/view/components/modal_dialog.html',
                  size: 'lg'
                });
            }
        }
    };
}])
