angular.module("dashboard.services",[
    'ngSanitize'
]);
angular.module("dashboard.components",[
    'ngAnimate',
    'ui.bootstrap',
    'dashboard.services'
]);
angular.module('dashboard', [
  'dashboard.services',
  'dashboard.components'
]);