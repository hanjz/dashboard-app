angular.module('dashboard.components')
.directive('tableComponent', [function(){
        return {
            restrict:'AE',
            replace:true,
            templateUrl: '/templates/view/components/table.html',
            controller: function($scope){
                 
            },
            link: function(scope, element, attrs){
                //scope.table = {}
                angular.forEach(scope.component.components, function(column){
                    column.extra = column.extra||{}
                    column.extra.table = scope.component;
                })   
            },

        };
}])

