angular.module( 'widget.components')
.directive('gridComponent', ['renderService', '$rootScope','util',function(renderService, $rootScope, util){
        function render_grid(scope){
            var grid_component = scope.component;
            scope.rows = grid_component.rows;
            scope.cols = grid_component.cols;
            scope.layout_cells = new Array(scope.rows);

            for(var i = 0;i<scope.rows;i++){
                scope.layout_cells[i] = new Array(scope.cols);
            }
            var grid_cells = grid_component.cells;
            angular.forEach(grid_cells, function(grid_cell){
                grid_cell.ctrl_id = util.uuid();
                grid_cell.rows = grid_component.rows;
                grid_cell.cols = grid_component.cols;
                grid_cell.visible = true;
                if(grid_cell.coord_x<scope.rows&&grid_cell.coord_y<scope.cols){
                    scope.layout_cells[grid_cell.coord_x][grid_cell.coord_y] = grid_cell;
                }
            })
            for(var i = 0 ;i<scope.rows;i++){
                for(var j = 0;j<scope.cols;j++){
                   /* if(scope.layout_cells[i][j]!=null){
                        //to find the first not null right cell.
                       var k = j;
                       while(k<scope.cols&&scope.layout_cells[i][++k]==null);
                       scope.layout_cells[i][j].right_cell = scope.layout_cells[i][k];

                       //to find the first not null down cell.
                       var x = i;
                       while(x<scope.rows&&scope.layout_cells[++x][j]==null);
                       scope.layout_cells[i][j].down_cell = scope.layout_cells[x][j];
                    }*/

                    var current_cell = scope.layout_cells[i][j];
                    if(current_cell==null){
                        throw new Error('null cell in grid.');
                    }
                    if(j<scope.cols-1){
                        scope.layout_cells[i][j].right_cell = scope.layout_cells[i][j+1];
                    }
                    if(j>0) {
                        scope.layout_cells[i][j].left_cell = scope.layout_cells[i][j-1]
                    }
                    if(i<scope.rows-1){
                        scope.layout_cells[i][j].down_cell = scope.layout_cells[i+1][j];
                    }
                    if(i>0){
                        scope.layout_cells[i][j].up_cell = scope.layout_cells[i-1][j];
                    }

                    for(var k = 1 ;k<scope.layout_cells[i][j].width;k++){
                        scope.layout_cells[i][j+k].visible = false;
                    }

                    for(var x = 1;x<scope.layout_cells[i][j].height;x++){
                        scope.layout_cells[i+x][j].visible = false;
                    }
                }
            }
        }
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/components/grid.html',
            replace:true,
            controller: function($scope){
                $scope.getTimes = function(n){
                    return new Array(n)
                }

                $scope.toggle_cell_select = function(cell, $event){
                    $event.stopPropagation();
                    if($rootScope.selected_component != null && $rootScope.selected_component.ctrl_id == cell.ctrl_id){
                        $rootScope.selected_component = null;
                    }
                    else{
                        $rootScope.selected_component = cell;
                    }
                }

                $scope.$watch('component.cols', function(newv, oldv){
                    if(newv!=null && newv!=oldv){
                        render_grid($scope);
                    }
                })

                $scope.$watch('component.rows', function(newv, oldv){
                    if(newv!=null && newv!=oldv){
                        render_grid($scope);
                    }
                })
            },
            link: function(scope, element, attrs){
                /*
                    to implement the colspan/rowspan,  need to hide the overflow td.
                    the hidden td can only be right or down position of the *span td.
                    so each cell must hold the ptr of right and down position cell so as to 
                    make them hidden when be set as *span.

                */
                render_grid(scope);
            }
        };
}])
.directive('gridProperties', [function(){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/grid.html',
            replace:true,
            controller: function($scope){
                var grid =  $scope.component;
                $scope.increase_cols = function(){
                    for(var i = 0 ; i<grid.rows; i++){
                        var cell = {
                            "type": "cell",
                            "name": "new_cell",
                            "width": 1,
                            "height": 1,
                            "coord_x": i,
                            "coord_y": grid.cols,
                            "padding": 0,
                            "components": []
                        }
                        grid.cells.push(cell);
                    }
                    grid.cols +=1;
                }

                $scope.decrease_cols = function(){
                    if(grid.cols==1){
                        return;
                    }
                    for(var i = 0 ; i<grid.cells.length;i++){
                        var cell = grid.cells[i];
                        if(cell.coord_y == grid.cols-1){
                            // determine whether to remove, one fail, all fail. 
                            if(cell.visible == false){
                                return;
                            }
                            else{
                                if(cell.components!=null&&cell.components.length>0){
                                    return;
                                }
                            }
                        }
                    }

                    // all pass, remove(splice)
                    for(var i = 0;i<grid.cells.length;i++){
                        var cell = grid.cells[i];
                        if(cell.coord_y == grid.cols-1){
                            grid.cells.splice(i,1);
                        }
                    }
                    grid.cols-=1; 
                }


                $scope.increase_rows = function(){
                    for(var i = 0 ; i<grid.cols; i++){
                        var cell = {
                            "type": "cell",
                            "name": "new_cell",
                            "width": 1,
                            "height": 1,
                            "coord_x": grid.rows,
                            "coord_y": i,
                            "padding": 0,
                            "components": []
                        }
                        grid.cells.push(cell);
                    }

                    grid.rows+=1; 
                }

                $scope.decrease_rows = function(){
                    if(grid.rows==1){
                        return;
                    }
                    for(var i = 0;i<grid.cells.length;i++){
                        var cell = grid.cells[i];
                        if(cell.coord_x==grid.rows-1){
                            if(cell.visible == false){
                                return;
                            }
                            else{
                                if(cell.components!=null && cell.components.length>0){
                                    return;
                                }
                            }
                        }
                    }

                    for(var i = 0;i<grid.cells.length;i++){
                         var cell = grid.cells[i];
                        if(cell.coord_x==grid.rows-1){
                            grid.cells.splice(i, 1);
                        }
                    }

                    grid.rows-=1; 
                }
            },
            link: function(scope, element, attrs){
                
            },
        };
}])
.directive('cellProperties', [function(){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/cell.html',
            replace:true,
            controller: function($scope){

                $scope.increase_colspan = function(){
                    var cell = $scope.component;

                    if(cell.height>1){
                        return;
                    }
                    if(cell.right_cell == null || cell.width == cell.cols){
                        return;
                    }

                   
                    var tmp_cell = cell.right_cell;
                    while(tmp_cell != null && tmp_cell.visible!=true){
                        tmp_cell = tmp_cell.right_cell;
                    }

                    if(tmp_cell == null){
                        return;
                    }
                    else{
                        //tmp_cell.visible == true;
                        //tmp_cell.visible = false;
                        if(tmp_cell.width >1){
                            return;
                        }
                        else{
                            tmp_cell.visible = false
                            cell.width +=1;
                        }
                    }
                    
                }

                $scope.decrease_colspan = function(){
                    var cell = $scope.component;
                    if(cell.height>1){
                        return;
                    }
                    if(cell.right_cell == null || cell.width == 1){
                        return;
                    }

                    var tmp_cell = cell;
                    while(tmp_cell.right_cell != null && tmp_cell.right_cell.visible!=true){
                        tmp_cell = tmp_cell.right_cell;
                    }
                    tmp_cell.visible = true;
                    cell.width-=1;
                }


                $scope.increase_rowspan = function(){
                    var cell = $scope.component;

                    if(cell.width>1){
                        return;
                    }
                     if(cell.down_cell == null || cell.height == cell.rows){
                        return;
                    }

                   
                    var tmp_cell = cell.down_cell;
                    while(tmp_cell != null && tmp_cell.visible!=true){
                        tmp_cell = tmp_cell.down_cell;
                    }

                    if(tmp_cell == null){
                        return;
                    }
                    else{
                        //tmp_cell.visible == true;
                        //tmp_cell.visible = false;
                        if(tmp_cell.height >1){
                            return;
                        }
                        else{
                            tmp_cell.visible = false
                            cell.height +=1;
                        }
                    }
                }

                $scope.decrease_rowspan = function(){
                    var cell = $scope.component;
                    if(cell.width>1){
                        return;
                    }
                    if(cell.down_cell == null || cell.height == 1){
                        return;
                    }

                    var tmp_cell = cell;
                    while(tmp_cell.down_cell != null && tmp_cell.down_cell.visible!=true){
                        tmp_cell = tmp_cell.down_cell;
                    }
                    tmp_cell.visible = true;
                    cell.height-=1;
                }
            },
            link: function(scope, element, attrs){
                
            },
        };
}])