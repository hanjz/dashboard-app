package com.iota.app.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WidgetSize {
	/**/
	public static final Integer PERCENTAGE_TYPE=1;
	public static final Integer PIXEL_TYPE=2;
	
	private Integer sizeType;
	private Integer sizeValue;
	
	/**
	 * 实例化widgetSize
	 * @return
	 */
	public static WidgetSize newInstance(){
		WidgetSize size = new WidgetSize();
		size.setSizeType(WidgetSize.PIXEL_TYPE);
		size.setSizeValue(0);
		return size;
	}
	
	/**
	 * 新建widget的宽
	 * @return
	 */
	public static WidgetSize newWidth(){
		WidgetSize size = new WidgetSize();
		size.setSizeType(WidgetSize.PIXEL_TYPE);
		size.setSizeValue(800);
		return size;
	}
	
	
	/**
	 * 新建的widget的高
	 * @return
	 */
	public static WidgetSize newHeight(){
		WidgetSize size = new WidgetSize();
		size.setSizeType(WidgetSize.PIXEL_TYPE);
		size.setSizeValue(600);
		return size;
	}
}
