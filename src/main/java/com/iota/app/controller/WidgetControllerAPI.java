package com.iota.app.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.iota.app.service.WidgetDataSourceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.iota.app.data.AccountDao;
import com.iota.app.model.Account;
import com.iota.app.model.Widget;
import com.iota.app.model.WidgetDataSource;
import com.iota.app.service.ImageCaptureService;
import com.iota.app.service.ValueService;
import com.iota.app.service.WidgetService;
import com.iota.app.vo.WidgetVo;

@Controller
@RequestMapping(value="/api/widgets")
public class WidgetControllerAPI {
	@Resource
	private WidgetService widgetService;
	@Resource
	private ImageCaptureService imageCaptureService;
	@Resource
	private ValueService valueService;
	@Resource
	private WidgetDataSourceService widgetDataSourceService;
	
	 @Autowired
	 private AccountDao accountRepository;
	/**
	 * GET widget info by ID
	 * 根据当前登录人的id查找当前登录人保存的widget
	 * @param
	 * @return
	 */
	@RequestMapping(value="/",method=RequestMethod.GET)
	@ResponseBody
	public List<WidgetVo> findwidget(){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    UserDetails principal = (UserDetails) auth.getPrincipal();
		Account account = accountRepository.findOneByEmail(principal.getUsername());
		List<Widget> newFindAllNew= new ArrayList<Widget>();
		List<Widget> findAll = widgetService.findAll();
		for (Widget widget : findAll) {
			if (account.getId().equals(widget.getUserId())||account.getId()==widget.getUserId()) {
				newFindAllNew.add(widget);
			}
		}
		return valueService.fromWidgets(newFindAllNew);
	}

	
	@RequestMapping(value="/",method=RequestMethod.POST)
	@ResponseBody
	public WidgetVo add(HttpServletRequest request, HttpServletResponse response, @RequestBody WidgetVo widgetVo) throws IOException{
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();	
//		 UserDetails principal = (UserDetails) auth.getPrincipal();
//		 Account account = accountRepository.findOneByEmail(principal.getUsername());
//		String userId=account.getId();
//		System.out.println(userId+"2222222222222222222222222");
		
		Widget widget = widgetService.save(valueService.toWidget(widgetVo));
		String thumbnail = imageCaptureService.capture(request, widget.getId());
//	    widget.setUserId(userId);
		widget.setThumbnail(thumbnail);
		widgetService.save(widget);
		return valueService.fromWidget(widget);
	}
	/**
	 * 作者：张明育  
	  *  日期：2017年1月22日 
	  *  功能:根据id查找widget
	 */
	@RequestMapping(value="/{widget_id}", method=RequestMethod.GET)
	@ResponseBody
	public WidgetVo get(@PathVariable("widget_id") String id){
		return valueService.fromWidget(widgetService.findById(id));
	}

	/**
	 * UPDATE widget info by id
	 * 保存widget，根据widget的id修改
	 * @param
	 * @return
	 */
	@RequestMapping(value="/{widget_id}",method=RequestMethod.POST)
	@ResponseBody
	public WidgetVo update(HttpServletRequest request, HttpServletResponse response, @PathVariable("widget_id") String id, @RequestBody WidgetVo widgetVo) throws IOException {
		//根据当前登录用户获取用户id
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();	
		 UserDetails principal = (UserDetails) auth.getPrincipal();
		 Account account = accountRepository.findOneByEmail(principal.getUsername());
		String userId=account.getId();
		System.out.println("输出："+userId);
		
		//根据widget的id查找
		Widget widget = widgetService.findById(id);
		valueService.copyWidget(valueService.toWidget(widgetVo), widget);
		widget.setUserId(userId);
		widgetService.save(widget);
		int width = 1600;
		int height = 900;
		if(widget.getWidth()!=null && widget.getWidth()>0){
			width = widget.getWidth();
		}
		if(widget.getHeight()!=null && widget.getHeight()>0){
			height = widget.getHeight();
		}
		String thumbnail = imageCaptureService.capture(request, id, width, height);
		widget.setThumbnail(thumbnail);
		widgetService.save(widget);
		return widgetVo;
	}

	/**
	 * 作者：张明育  
	  *  日期：2017年1月22日 
	  *  功能:下一个widget
	 */
	@RequestMapping(value="/next",method= RequestMethod.GET)
	@ResponseBody
	public WidgetVo next(HttpServletRequest request){
		HttpSession session = request.getSession();
		Integer order = (Integer)session.getAttribute("order");
		if(null == order){
			order = 0;
			session.setAttribute("order", order);
		}
		Widget widget = widgetService.findByOrder(order);
		session.setAttribute("order", ++order);
		return valueService.fromWidget(widget);
	}

	/**
	 * 作者：张明育  
	  *  日期：2017年1月22日 
	  *  功能:上一个widget
	 */
	@RequestMapping(value="/prev",method= RequestMethod.GET)
	@ResponseBody
	public WidgetVo prev(HttpServletRequest request){
		HttpSession session = request.getSession();
		Integer order = (Integer)session.getAttribute("order");
		if(null == order){
			order = 0;
			session.setAttribute("order", order);
		}
		Widget widget = widgetService.findByOrder(--order);
		session.setAttribute("order", --order);
		return valueService.fromWidget(widget);
	}

	/**
	 * 作者：张明育  
	  *  日期：2017年1月22日 
	  *  功能: 第一个widget
	 */
	@RequestMapping(value="/first",method= RequestMethod.GET)
	@ResponseBody
	public WidgetVo first(HttpServletRequest request){
		HttpSession session = request.getSession();
		session.setAttribute("order", 0);
		Widget widget = widgetService.findByOrder(0);
		return valueService.fromWidget(widget);
	}


	/**
	 * 作者：张明育  
	  *  日期：2017年1月22日 
	  *  功能:最后一个widgt
	 */
	@RequestMapping(value="/last",method= RequestMethod.GET)
	@ResponseBody
	public WidgetVo last(HttpServletRequest request){
		
		HttpSession session = request.getSession();
		session.setAttribute("order", -1);
		Widget widget = widgetService.findByOrder(-1);
		return valueService.fromWidget(widget);
	}
	/**
	 * ADD new widget
	 * @return
	 */
	@RequestMapping(value="/", method=RequestMethod.PUT)
	@ResponseBody
	public WidgetVo _new(){
		return null;
	}

	/**
	 * DELETE widget by id
	 */
	@RequestMapping(value="/{widget_id}",method=RequestMethod.DELETE)
	@ResponseBody
	public WidgetVo delete(@PathVariable("widget_id") Long id){
		return null;
	}

	@RequestMapping(value="/{widget_id}/datasources")
	@ResponseBody
	public List<WidgetDataSource> getDatasourcesByWidget(@PathVariable("widget_id") String widgetId){
		Widget widget = widgetService.findById(widgetId);
		return widget.getDataSources();
	}

	/**
	 * 作者：张明育  
	  *  日期：2017年1月22日 
	  *  功能:根据widget  ID 和数据源列表的ID 添加数据到widget
	 */
	@RequestMapping(value="/{widget_id}/datasources/{datasource_id}", method=RequestMethod.POST)
	@ResponseBody
	public WidgetDataSource addDatasourceToWidget(@PathVariable("widget_id") String widgetId, @PathVariable("datasource_id") String dataSourceId){
		return widgetDataSourceService.addDataSourceToWidget(dataSourceId, widgetId);
	}
	
	/**
	 * 作者：张明育  
	  *  日期：2017年1月22日 
	  *  功能:上传背景图
	 */
	@RequestMapping(value="/upload", method=RequestMethod.POST)
	@ResponseBody
	public Map uploadBackground(@RequestParam(value="file", required=false) MultipartFile file,@RequestParam(value="widget",required=false) String widgetId) throws IOException{
		String filePath = widgetService.uploadBg(widgetId, file.getOriginalFilename(),file.getInputStream());
		Widget widget= widgetService.findById(widgetId);
		widget.setBackground(filePath);

		widgetService.save(widget);
		Map map = new HashMap();
		map.put("url", filePath);
		map.put("remote", valueService.getBgServerUrl());
		map.put("local", valueService.getBgServerLocal());
		return map;
	}
}
