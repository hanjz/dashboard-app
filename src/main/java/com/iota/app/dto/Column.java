package com.iota.app.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by guoxu.wu on 16/8/5.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Column {
    private String type;
    private String name;
    private Integer index;
}
