package com.iota.app.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guoxu.wu on 16/8/5.
 */
@Getter
@Setter
public class Data {
    private List<Object[]> data = new ArrayList();

    public static Data newInstance() {
       return new Data();
    }
    public void addData(Object[] d){
        this.data.add(d);
    }
}
