angular.module('dashboard.services')
    .factory("conditionService",[function(){



        var ifConditionMap = [' == ', ' > ', ' >= ', ' < ', ' <= ']

        return {

            parseCondition: function(condition){
                    var ifString = "if( (1==1) "
                    var ifs = condition._ifs;
                    if(!ifs) return;

                    angular.forEach(ifs, function(_if){
                        ifString+=" &&( ";

                        ifString+="<%=_if%>";
                        ifString+=ifConditionMap[_if._if];
                        ifString+=_if._value;
                        ifString+=" )";
                    })

                    ifString += " )";

                    var thenString = " { ";
                    thenString += " <%=_then%> ";
                    thenString += " } ";

                return ifString+thenString;
            }

        };
    }]);