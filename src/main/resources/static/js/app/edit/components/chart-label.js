angular.module('widget.components')
.directive('chartlabelComponent', ['queryService',function(queryService){
        return {
            restrict:'E',
            controller: function($scope){
                /*$scope.component.extra.update = function(){
                     $scope.component.extra.chart.extra.update()
                } */
            },
            link: function(scope, element, attrs){
                scope.component.extra.update = function(){
                     //scope.component.extra.chart.extra.update()
                     scope.component.extra.chart.extra.update();
                } 
            }
        };
}])
.directive('chartlabelProperties', ['queryService',function(queryService){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/chart-label.html',
            replace:true,
            
            controller: function($scope){
                 
            },
            link: function(scope, element, attrs){

            },
        };
}])