angular.module('widget.components')
.directive('dataProperties', ['queryService','util','$uibModal',function(queryService, util, $uibModal){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/data-properties.html',
            replace:true,
            scope:{
                component:"=",
                defaultOption:"@"
            },
            controller: function($scope,$element){
                var visualizers_map = {};
               this.disable_input = function(){
                    $scope.value_expr = ""
               } 

               
               this.set_pointer = function(uid,expr){
                    for(var key in visualizers_map){
                        if(key==uid){
                            visualizers_map[key].setPointer(expr)
                        }
                    }
                }

               this.add_visualizer = function(uid,visualizer){
                    visualizers_map[uid] = visualizer;

                    visualizer.setSelectionCallback(function(obj){
                        var data = obj.data;
                        var current_component = $scope.component;
                        current_component.data = {
                            type:'data',
                            expr:data,
                            datasource : uid
                        }
                        current_component.extra.update();

                        for(var key in visualizers_map){
                            if(key!=uid){
                                visualizers_map[key].setPointer(false);
                            }
                        }

                        $scope.value_expr = "";
                    })
               }

                $scope.apply_value= function(){
                    $scope.component.data = $scope.component.data||{}
                    $scope.component.data.type = 'value' ;
                    $scope.component.data.expr = $scope.value_expr;
                    $scope.component.extra.update();
                }
            },
            link: function(scope, element, attrs){
                scope.datasources = util.cache.get('datasources');
                if(scope.component.data == null){
                    scope.option = scope.defaultOption;
                }
                else{
                    if(scope.component.data.type == 'data'){
                        scope.option = 'data';
                    }
                    else{
                        scope.option = 'value';
                        scope.value_expr = scope.component.display_value;
                    }
                }
                scope.toggle_option = function(){
                    if(scope.option == 'data'){
                        scope.option = 'value';
                    }
                    else{
                        scope.option = 'data';
                    }
                }
            }
        };
}])