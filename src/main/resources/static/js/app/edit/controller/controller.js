
angular.module('widget', ['widget.services', 'widget.components'], angular.noop)
.run(["$rootScope",function($rootScope){
    //$rootScope.context_url = angular.element("#context_url").val();
    //$rootScope.current_user_id = angular.element("#usr_id").val();
}])
.controller('controller', 
    [
    '$rootScope',
    '$scope',
    'queryService',
    'initService',
    'util',
    '$element',
    function(
        $rootScope,
        $scope, 
        queryService,
        initService,
        util, $element){
        $scope.widget_id = angular.element("#widget_id").val();
        initService.init_operation($scope);
        $scope.load = function(){
        }

        this.switch_selected_component = function(component){
            $scope.selected_component = component;
        }

        $scope.save= function(){
            //!important : must send heartbeat ping before wash.
        	$scope.save = function(){}
        	$scope.loading = true;
            var widget = $scope.widget;
            var _widget = angular.copy(widget);
            var new_height = Math.ceil(angular.element('.editing-widget').height()/200);
            util.wash(_widget);
            _widget.components = JSON.stringify(_widget.components);
            if($scope.mode === 'new'){
                queryService.xhr_new_widget(_widget).success(function(data){
                	 window.location.href="/widget/home/"
                })
            }
            else {
                queryService.xhr_update_widget(_widget).success(function (data) {
                    window.location.href="/widget/home/"
                }).error(function (data) {
                    alert('Save Error, see the console for detail.');
                })
            }
            
            
        }


        $scope.hide_scrollbar = function(a, b){
            angular.element('.nicescroll-cursors').hide();
        }

        $scope.show_scrollbar = function(a, b){
            angular.element('.nicescroll-cursors').show();
        }

        $scope.reflow = function(){
            $scope.$broadcast('component-reflow')
        }
}])
