angular.module('widget.components')
.directive('legendProperties',[function(){
  return {
    restrict:'E',
      replace:true,
      templateUrl:'/templates/edit/properties/legend.html',
      scope:{
        component:"=",
      },
      controller: function($scope){

      },
      link: function(scope,element,attributes){
          scope.change_align = function(align_str){
              scope.component.options.legend_position = align_str;
              scope.component.extra.update();
          }
      }
  }
}])
.directive('alignProperties',[function(){
    return {
      restrict:'E',
      replace:true,
      templateUrl:'/templates/edit/properties/align.html',
      scope:{
        component:"=",
        title:"@"
      },
      controller: function($scope){

      },
      link: function(scope,element,attributes){
          scope.align_manager = new css_manager(scope.component.css_style);
          scope.change_align = function(obj){
              scope.align_manager = new css_manager(scope.component.css_style);
              scope.align_manager.apply_css(obj)
              scope.component.css_style = scope.align_manager.tostring();
          }
      }
    }
}])
    .directive('autoWidthProperties', [function(){
        return {
            restrict:'E',
            replace:true,
            templateUrl:'/templates/edit/properties/autowidth.html',
            scope:{
                component:'=',
                title: "@"
            },
            link: function(scope, element, attributes){
                scope.width_manager = new css_manager(scope.component.css_style);
                if(!scope.width_manager.style['display']){
                    scope.width_manager.style['display']  = 'block';
                }
                scope.change_width_auto = function(){
                    scope.width_manager = new css_manager(scope.component.css_style);
                    if(scope.width_manager.style['display']==='block') {
                        scope.width_manager.apply_css({'display':'inline-block'});
                    }
                    else{
                        scope.width_manager.apply_css({'display':'block'})
                    }
                    scope.component.css_style = scope.width_manager.tostring();
                }
            }
        }
    }])
    .directive('radiusProperties', [function(){
        return {
            restrict:'E',
            replace:true,
            templateUrl:'/templates/edit/properties/radius.html',
            scope:{
                component:'=',
                title: "@"
            },
            link: function(scope, element, attributes){
                scope.radius_manager = new css_manager(scope.component.css_style);
                if(!scope.radius_manager.style['border-radius']){
                   scope.radius_manager.style['border-radius'] = 'initial'
                }
                scope.change_radius = function(obj){
                    scope.radius_manager = new css_manager(scope.component.css_style);
                    scope.radius_manager.apply_css(obj);
                    scope.component.css_style = scope.radius_manager.tostring();
                }
            }
        }
    }])
.directive('fontStyleProperties',[function(){
    return {
      restrict:'E',
      replace:true,
      templateUrl:'/templates/edit/properties/fontstyle.html',
      scope:{
        component:"=",
        title:"@"
      },
      controller: function($scope){

      },
      link: function(scope,element,attributes){
          scope.font_manager = new css_manager(scope.component.css_style);
          scope.change_font_style = function(obj){
              scope.font_manager = new css_manager(scope.component.css_style);
              scope.font_manager.apply_css(obj)
              scope.component.css_style = scope.font_manager.tostring();
          }
      }
    }
}])
.directive('fontSizeProperties',[function(){
   return {
      restrict:'E',
      replace:true,
      templateUrl:'/templates/edit/properties/fontsize.html',
      scope:{
        component:"=",
        title:"@"
      },
      controller: function($scope){
      },
      link: function(scope,element,attributes){
          scope.font_manager = new css_manager(scope.component.css_style);
          scope.change_font_size = function(obj){
              scope.font_manager = new css_manager(scope.component.css_style);
              scope.font_manager.apply_css(obj)
              scope.component.css_style = scope.font_manager.tostring();
          }
      }
   }
}])
.directive('colorProperties',[function(){
   return {
      restrict:'E',
      replace:true,
      templateUrl:'/templates/edit/properties/color.html',
      scope:{
          option:"@",  //0,  前景， 1 背景,  2 both
          component:"=",
          title:"@",
          cssData:"@",
          style:"@",
          pureCss:"@",
          type:"@"
      },
      controller: function($scope){
      },
      link: function(scope,element,attributes){

          if(!scope.style){
              scope.style = "hex"
          }

          if(scope.cssData==null) {
              scope.color_manager = new css_manager(scope.component.css_style);
          }
          else{
              scope.color_manager =  new css_manager(scope.component[scope.cssData]||"");
          }

          scope.fcolor=scope.color_manager.style['color']||'#000'
          scope.bcolor=scope.color_manager.style['background']||'#fff'

          scope.$watch('fcolor', function(nv,ov){
                if(nv!=null && nv!=ov){
                  if(scope.cssData==null) {
                    scope.color_manager = new css_manager(scope.component.css_style);
                }
                else{
                    scope.color_manager =  new css_manager(scope.component[scope.cssData]||"");
                }
                scope.color_manager.apply_css({color:nv})
                if(scope.cssData==null) {
                  scope.component.css_style = scope.color_manager.tostring();
                }
                else{
                  scope.component[scope.cssData] = scope.color_manager.tostring();
                }
              }
          })
          scope.$watch('bcolor', function(nv,ov){
              if(nv!=null && nv!=ov){
                if(scope.cssData==null) {
                    scope.color_manager = new css_manager(scope.component.css_style);
                }
                else{
                    scope.color_manager =  new css_manager(scope.component[scope.cssData]||"");
                }
                scope.color_manager.apply_css({background:nv})
                if(scope.cssData==null) {
                  scope.component.css_style = scope.color_manager.tostring();
                }
                else{
                  scope.component[scope.cssData] = scope.color_manager.tostring();
                }
              }
          })
      }
   }
}])
    .directive('streamData', [function(){
        return {
            restrict:'E',
            template:'<div class="stream-data-control"><div class="stream-indicator"></div><div class="stream-data">{{}}</div></div>',
            scope:{
                data:'@'
            },
            controller: function($scope){
                    
            }
        }
    }])
.directive('borderProperties', [function() {
  return {
    restrict: 'E',
    templateUrl:'/templates/edit/properties/border.html',
    controller: function($scope){
      var refresh_style = function(key, value){
          if($scope.component.css_style == null){
            $scope.component.css_style = key+":"+value+";";
            return;
          }
          var style_array = $scope.component.css_style.split(";");
          var css = {}
          for(var i = 0 ;i<style_array.length;i++){
              var css_clip = style_array[i];
              if(css_clip!=null && css_clip!=""){
                  if(css_clip.split(":").length==2){
                      var _key = css_clip.split(":")[0];
                      var _value = css_clip.split(":")[1];
                      if(_key == key){
                        css[key] = value;
                      }
                      else{
                        css[_key] = _value;
                      }
                  }
              }
          }
          if(css[key] == null){
            css[key] = value;
          }
          var component_css = ""
          for(var k in css){
              var css_clip = ""+k+":"+css[k]+";";
              component_css+=css_clip
          }
         $scope.component.css_style = component_css;

      }
      $scope.$watch('css["border-width"]', function(newv, oldv){
          if(newv!=null && newv!=oldv){
            if(!isNaN(newv.replace("px",''))){

              if(newv>10){
                newv = 10;
              }
              refresh_style('border-width', newv)
            }
          }
      })

      $scope.$watch('css["border-style"]', function(newv, oldv){
        if(newv!=null&&newv!=oldv){
          refresh_style('border-style', newv)
        }
      })

      $scope.$watch('css["border-color"]', function(newv, oldv){
        if(newv!=null && newv!=oldv){
          refresh_style('border-color', newv);
        }
      })
    },
    link: function(scope, element, attributes) {
        scope.css = {}
            if(scope.component.css_style!=null){
                var style_array = scope.component.css_style.split(";");
                for(var i = 0 ;i<style_array.length;i++){
                    var css_clip = style_array[i];
                    if(css_clip!=null && css_clip!=""){
                        if(css_clip.split(":").length==2){
                            scope.css[css_clip.split(":")[0]] = css_clip.split(":")[1];
                        }
                    }
                }
        }
    },
    scope: {
        component:"="
    }
  }
}]);
