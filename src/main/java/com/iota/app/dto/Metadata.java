package com.iota.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by guoxu.wu on 16/8/5.
 */
@Getter
@Setter
public class Metadata  {
	/**
	 * 元数据
	 * 数据源列表的属性
	 */
	
	private String dataSourceName;
    private String dataSourceUid ;
    private String dataSourceDescription;
    private Date createTime ;
    private Date updateTime ;
    private List<Column> columns = new ArrayList();

    public Column addColumn(String name, String type){
        Column column = new Column();
        column.setName(name);
        column.setType(type);
        column.setIndex(this.columns.size());
        columns.add(column);
        return column;
    }
}
