angular.module('widget.services')
.factory("renderService",['$rootScope','$compile',function($rootScope, $compile){
    function remove_by_id(widget, ctrl_id){
        for(var i = 0 ;i<widget.components.length; i++){
            var component = widget.components[i];
            if(component.ctrl_id == ctrl_id){
                // remove
                widget.components.splice(i, 1);
            }

            if(component.cells!=null){
                for(var j = 0;j<component.cells.length; j++){
                    var cell = component.cells[j];
                    remove_by_id(cell,ctrl_id);
                }
            }
        }
       
    }
    return {
        render:function(scope){
            //return $dom
            
        },

        render_properties:function(component){
            //return $dom
            if(component == null){
                return;
            }
            var type = component.type;
            if(type!==null){
                var type_name = type.replace(/[_]+/,'')+"_properties"
                var new_scope = $rootScope.$new(true)
                new_scope.component = component
                return $compile(angular.element("<"+type_name+"></"+type_name+">"))(new_scope)
            }
        },

        remove: function(ctrl_id){
            if($rootScope.widget){
                remove_by_id($rootScope.widget, ctrl_id)
            }
        }

    };
}]);