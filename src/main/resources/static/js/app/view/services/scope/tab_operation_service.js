angular.module("app.services.scope.tab",[],angular.noop)
.factory("tabOperationService",['queryService', function(queryService){
    return {
       init_operation: function($scope){
            $scope.sw_tab = function(index){
                $scope.tab_active_index = index
            }

            $scope.tab_decration = function(){
               for(var i =0;i<$scope.tabs.length;i++) {
                    angular.extend($scope.tabs[i],$scope.default_tabs_ui)
               }
            }

            $scope.add_dashboard = function(){
                var default_title = "New Dashboard";
                queryService.xhr_add_dashboard(default_title).success(function(data){
                    var new_dashboard_tab = {
                        title: 'New Dashboard',
                        opts : angular.extend({columns: 6}, $scope.overall_dashboard_opts)
                    }
                    $scope.dashboards.push(new_dashboard_tab)
                    $scope.tab_active_index = $scope.dashboards.length-1
                    $scope.tab_adding = false;
                })
            }
            $scope.rename_dashboard = function(dashboard){
                dashboard.editing = false;
                if(dashboard.edit_title === dashboard.title){
                    return;
                }

                dashboard.title = dashboard.edit_title;
                //xhr update
                queryService.xhr_update_dashboard(dashboard);
            }

            $scope.remove_dashboard = function(dashboard){
                queryService.xhr_remove_dashboard(dashboard).success(function(data){
                    for(var i =0 ;i<$scope.dashboards.length;i++){
                        if($scope.dashboards[i].id == dashboard.id){
                            $scope.dashboards.splice(i, 1)
                        }
                    }
                    $scope.tab_active_index = 0;
                });
            }

       }    
    };
}]);