iota安装配置文档

系统要求Linux. 本文以Debian为例

各个发行版安装前确保以下命令可以正常执行.

apt-get
wget
tar
vim
sudo(如果不是管理员帐号)

如果不能正常执行， 需用各自包管理器安装

注意:

1. 安装过程需要全程联网.

2. 确保是新安装的干净的机器!

3. vm确保已经保存了出厂镜像, 用作恢复重装!

安装步骤：
首先下载安装文件:

https://pan.baidu.com/s/1pLIramZ


1. 在第一台机器(ip例如: aaa.aaa.aaa.aaa)安装app
	a. 解压iota-app.tar.gz
	b. 运行install.sh(运行之前可能要chmod 777), 中间会出现git输入用户名密码的交互， 可能需要加组
	c. 运行start.sh测试网页是否能打开

2. 在第二台机器(ip例如: bbb.bbb.bbb.bbb)安装image server
	a. 解压iota-ftp.tar.gz
	b. 运行install.sh
	c. 运行start.sh开启图片服务器

3. 在第三台机器(ip例如:ccc.ccc.ccc.ccc)安装data service
	a. 解压iota-service.tar.gz
	b. 运行install.sh(同样可能要设置chmod 777), 中间会弹出mysql设置root密码的界面， 记住设置的密码, 记住设置的密码,记住设置的密码,用于下一步配置.

4. 在第4台机器(ip例如: ddd.ddd.ddd.ddd)安装配置nifi
	需手动或qiu写脚本


5. 配置app
	a. 打开第三台机器(ccc.ccc.ccc.ccc)的iota-service.tar.gz的解压目录， 进入dashboard-service/src/main/resources, 打开application.properties

		修改spring.datasource.url=本地mysql连接字符串(可直接将其中的ip改为localhost)
		修改spring.datasource.password配置项为刚刚设置的mysql密码
		修改iota.dataservice.nifiurl为第四台机器 :   http://ddd.ddd.ddd.ddd/xxx/xxx(nifi的url)
		启动iota-service: 进入第三台机器的iota-service解压目录， 运行start.sh
		浏览器中输入http://localhost:9990/api/datasources/看是否有输出

	b. 打开第一台机器(aaa.aaa.aaa.aaa)的iota-app.tar.gz的解压目录， 进入dashboard-app/src/main/resources, 打开application.properties
		修改img.server为第二台机器 http://bbb.bbb.bbb.bbb/
		修改bg.server为第二台机器 http://bbb.bbb.bbb.bbb/
		修改ftp.server为第二台机器 bbb.bbb.bbb.bbb  (没有http://)
		修改db.server为第三台机器 http://ccc.ccc.ccc.ccc:9990/api/datasources/
		重启iota-app: 进入第一胎机器iota-app的解压目录， 运行start.sh
		浏览器输入http://localhost:9999/看是否有输出




说明: 第三台机器将mysql和数据服务捆绑在一台机器上， 在实际运行中也可以将mysql单独放在一台server上(安装mysql后需要导入import.sql文件)， 并修改相应数据服务iota-service的数据库连接配置(dashboard-service/src/main/resources/application.properties), 事实上安装脚本比较简单， 可通过阅读安装脚本完全实现各种环境的手动安装。
