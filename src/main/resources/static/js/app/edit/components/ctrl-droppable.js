angular.module('widget.components')
.directive('ctrlDroppable', ['util', 'renderService',function(util,renderService){
    return {
        restrict:'A',
       link:function(scope, element, attrs){
            element.disableSelection();
            element.droppable({
                greedy:true,
                hoverClass:'drop-hover',
                drop: function(event, ui){
                    $('.replaceholder').removeClass("replaceholder");
                    var data = ui.draggable.data('current-dragging-data');
                    var cloned_data = angular.copy(data);
                    cloned_data.ctrl_id = util.uuid()
                    if(scope.widget.type == 'cell'){
                        scope.widget.components = [cloned_data]                 
                    }
                    else{
                        var placeholder = $(this).find(".placeholder");
                        var index = placeholder.index();
                         $(".placeholder").remove();
                        //data.ctrl_id = uuid.gen();
                        scope.widget.components.splice(index, 0, cloned_data); 

                    }
                   

                    //deep iteration for finding the dragging data 
                    renderService.remove(data.ctrl_id);

                    scope.$apply();
                },
                over: function(event, ui){
                    $(".replaceholder").removeClass('replaceholder')
                    if($(this).attr("root") == 'true'){
                        ui.draggable.data("current-droppable", $(this));
                    }
                    else{
                        $(this).addClass('replaceholder');
                    }
                },
                out: function(event, ui){
                    $('.replaceholder').removeClass("replaceholder")
                    $(".placeholder").remove();
                    ui.draggable.removeData("current-droppable");  
                }
            }) 
            
       }
    }
}])
.directive('canvasDroppable', ['util','renderService',function(util, renderService){
    return {
        restrict:'A',
        link:function(scope, element, attrs){
            element.droppable({
                greedy:true,
                drop: function(event, ui){
                    var data = ui.draggable.data('current-dragging-data');
                    var new_data = ui.draggable.data('is-new-data');
                    if(!new_data){
                        return;
                    }
                    var offset_left = ui.position.left;
                    var offset_top = ui.position.top;
                    var scroll_left = element.parent().scrollLeft();
                    var scroll_top = element.parent().scrollTop();
                   
                    var absolute_top = offset_top+scroll_top;
                    var absolute_left = offset_left + scroll_left;

                    var absolute_top_percentage = absolute_top*100/element.height()+"%";
                    var absolute_left_percentage = absolute_left*100/element.width()+"%";

                    var cloned_data = angular.copy(data);
                    cloned_data.ctrl_id = util.uuid()
                    cloned_data.top = absolute_top_percentage;
                    cloned_data.left = absolute_left_percentage

                    scope.widget.components.push(cloned_data);
                    scope.$apply();
                }
            });
        }
    }
}])