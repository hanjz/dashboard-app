angular.module('widget.components')
.directive('dataComponent', ['queryService','util','$rootScope','$uibModal',function(queryService, util, $rootScope,$uibModal){
        return {

            restrict:'AE',
            templateUrl: '/templates/edit/properties/data.html',
            replace:true,
            controller: function($scope,$element){
            },
            scope:{
                component:"=",
                defaultOption:"="
            },
            link: function(scope, element, attrs){
                scope.datasources = util.cache.get('datasources');
                var datasources_map = {};
                angular.forEach(scope.datasources, function(datasource){
                    datasources_map[datasource.uid] = datasource;
                })

                scope.toggle_datasource = function(uid){
                    scope.current_datasource_uid = uid;
                    if(datasources_map[scope.current_datasource_uid].data==null){
                        queryService.xhr_fetch_data(scope.current_datasource_uid).success(function(data){
                            datasources_map[scope.current_datasource_uid].data = data;

                            if(data.dataSource.type==='NIFI' && (!!data.dataSource.intervaled)===false ){
                                if(!scope.component.data){
                                   alert('component does not support Nifi!') ;
                                    return;
                                }
                                data.dataSource.intervaled = true;
                                scope.component.data.interval = 5000;
                                setInterval(function(){
                                    queryService.xhr_fetch_data(scope.current_datasource_uid).success(function(data){
                                        datasources_map[scope.current_datasource_uid].data = data;
                                    })
                                }, 5000);
                            }
                        })
                    }
                }

                scope.add_datasource = function(type){
                     scope.modal_instance = $uibModal.open({
                        animation: true,
                        template: "<div class='add-datasource-popup'>"+
                        "<h4>Select A Datasource : </h4>"+
                        "<div class='add-datasource-list'><table>"+
                        	"<thead><tr><th>Name</th><th>Type</th><th>Create Time</th><th>Description</th></tr></thead>"+
                        	"<tbody><tr ng-repeat='datasource in datasources' ng-click='choose(datasource)'>"+
                        	"<td>{{datasource.name}}</td>"+
                        	"<td>{{datasource.type}}</td>"+
                        	"<td>{{datasource.createdTime}}</td>"+
                        	"<td>{{datasource.description}}</td>"+
                        	"</tr></tbody>"+
                        "</table></div></div>",
                        size: 'lg',
                        controller:function($scope){
                        	queryService.xhr_datasources().success(function(datasources){
                                if(type){
                                    $scope.datasources = datasources.filter(function(obj){
                                        return obj.type==type
                                    })
                                }
                                else {
                                    $scope.datasources = datasources;
                                }
                        	});
                            $scope.choose= function(datasource){
                            	scope.choose(datasource);
                            	scope.modal_instance.close();
                            }
                        }
                    });
                }
                scope.choose = function(datasource) {
                	var datasources = util.cache.get('datasources');
                	if(datasources && datasources.length>0){
                		if(datasources.filter(function(obj){
                			return obj.uid==datasource.uid
                		}).length>0){return;}
                	}
                    //
                    //var datasources = util.cache.get('datasources')
                    /*angular.forEach(datasources, function(datasource){
                        if(datasource.uid == uid){
                            return;
                        }
                    })*/
                    //var widget_uid = $rootScope.widget.uid;
                    //scope.datasources.push(datasource);
                    //datasources_map[datasource.uid]= datasource;
                	/*
                	 *
                	 *  var widget_uid = $rootScope.widget.uid;
                    queryService.xhr_attach_datasource(widget_uid, uid).success(function(datasource){
                        scope.datasources.push(datasource);
                        datasources_map[datasource.uid]= datasource;
                        scope.toggle_datasource(datasource.uid)
                    })
                	 *
                	 * */
                	queryService.xhr_attach_datasource($rootScope.widget.id, datasource.uid).success(function(){
                		scope.datasources.push(datasource);
                        datasources_map[datasource.uid]= datasource;
                        scope.toggle_datasource(datasource.uid)
                	})
                };

                if(scope.datasources.length>0){
                    var datasource_uid;
                    if(scope.component.data!=null){
                        if(scope.component.data.datasource==null){
                            datasource_uid = scope.datasources[0].uid;
                        }
                        else{
                            if(datasources_map[scope.component.data.datasource]!=null){
                                datasource_uid = scope.component.data.datasource;
                            }
                            else{
                                datasource_uid = scope.datasources[0].uid;
                            }
                        }
                        scope.toggle_datasource(datasource_uid)
                    }
                    else{
                        if(scope.defaultOption=='data'){
                            datasource_uid = scope.datasources[0].uid;
                            scope.toggle_datasource(datasource_uid)
                        }
                    }
                }


            }
        };
}])
.directive('dataElement', ['queryService','util','$rootScope',function(queryService, util, $rootScope){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/components/data-element.html',
            replace:true,
            require:"^dataProperties",
            scope:{
                datasource:"=",
                component:"="
            },
            controller: function($scope,$element){

            	$scope.select_column = function(index){
            		var expr = {
            			column_start_index: index,
            			column_end_index: index
            		}

            		var current_component = $scope.component;
                    current_component.data = {
                        type:'data',
                        expr:expr,
                        datasource : $scope.datasource.uid
                    }
                    if(current_component.extra){
                    	current_component.extra.update();
                    }
                    $scope.select_start = {column:index, row:0}
                    $scope.select_end = {column:index, row: $scope.datasource.data.data.length}
            	}

            	$scope.selecting = false;
            	$scope.select_item_start = function(row, column){
            		$scope.selecting = true;
            		$scope.select_end = null;
            		$scope.select_start = {
            				column:column,
            				row:row
            		}
            	}

            	$scope.select_item_move = function(row, column){
            		if($scope.selecting){
            			$scope.select_end = {
            					column: column,
            					row:row
            			};
            		}
            	}
            	$scope.select_item_end = function(row,column){
            		if($scope.selecting){

            			$scope.select_end = {
            					column: column,
            					row:row
            			};
            			var expr = {
            				column_start_index: $scope.select_start.column,
                    		column_end_index: $scope.select_end.column,
                    		row_start_index : $scope.select_start.row,
                    		row_end_index: $scope.select_end.row
            			}
            			var current_component = $scope.component;
            			current_component.data = {
                                type:'data',
                                expr:expr,
                                datasource : $scope.datasource.uid
                        }
            			if(current_component.extra){
                            if(current_component.extra.interval){
                                clearInterval(current_component.extra.interval);
                            }
                            current_component.extra.update();
                        }
            		}
            		$scope.selecting =false;
            	}
                $scope.select_stream = function(){
                    var current_component = $scope.component;
                    current_component.data = {
                        type:'data',
                        expr:'',
                        datasource: $scope.datasource.uid,
                        interval: 5000
                    }
                    if(current_component.extra)    {
                        current_component.extra.update();
                        if(current_component.extra.interval){
                            clearInterval(current_component.extra.interval);
                         }
                        current_component.extra.interval = setInterval(function(){current_component.extra.update()},5000);

                    }
                }
                if($scope.datasource.type==='NIFI') {
                    $scope.$watch('datasource.data', function (nv, ov) {
                        var si = $element.find('.stream-indicator').html('')[0];


                        if (nv && nv != ov) {
                            var circle = new ProgressBar.Circle(si, {
                                strokeWidth: 10,
                                duration: 5000,
                                easing: 'easeInOut',
                                trailColor: '#aaa',
                                color: '#FCB03C'
                            });
                            circle.animate(1);
                        }
                    })
                }

            },
            link: function(scope, element, attrs, dataPropertiesCtrl){

                var datasource = scope.datasource;


                if(datasource.data!=null){
                    if(scope.component.data!=null) {
                        if (scope.component.data.type == 'data' && scope.component.data.datasource == datasource.uid) {
                            var data_expr = scope.component.data.expr;
                            scope.select_start = {};
                            scope.select_end = {};
                            scope.select_start.column = data_expr.column_start_index;
                            scope.select_end.column =data_expr.column_end_index;
                            scope.select_start.row =data_expr.row_start_index||0;
                            scope.select_end.row =data_expr.row_end_index!=null?data_expr.row_end_index:datasource.data.data.length;
                        }
                    }
                }
            }

        };
}])
