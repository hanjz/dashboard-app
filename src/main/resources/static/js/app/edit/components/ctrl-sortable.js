angular.module('widget.components')
.directive('ctrlSortable', [function(){
    return {
       link:function(scope, element, attrs){
            element.sortable({
               revert:true
            })

            element.disableSelection();
       }
    }
}])
