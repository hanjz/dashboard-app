package com.iota.app;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
/**
 * 
 * @author admin
 *  运行之后跳转到AuthController里的 value="signin"的方法中
 */

@SpringBootApplication
@Configuration
public class DashboardApplication {
	public static void main(String[] args) {
		SpringApplication.run(DashboardApplication.class, args);
	}
}
