angular.module('widget.components')
.directive('inlineComponent',['renderService', 'util','$compile', function(renderService,util, $compile){
    return {
        restrict:'E',
        replace:true,
        link: function(scope, element, attrs){
            scope.component.ctrl_id = util.uuid();
            scope.component.extra = scope.component.extra||{}
            var type = scope.component.type;
            if(type!==null){
                var type_name = type.replace(/[_]+/,'')+"_component"
                element.parent().append($compile(angular.element("<"+type_name+"></"+type_name+">"))(scope))
                element.remove();
            }
        },
        scope: {
            component:"="
        }
    }
}])
.directive('propertiesComponent',['queryService','$rootScope','$compile','util',function(queryService, $rootScope, $compile,util){
    return {
        restrict:"AE",
        replace:true,
        controller: function($scope,$element){
            $rootScope.$watch("selected_component", function(data){
                if(data!=null){
                    var type = data.type;
                    if(type){
                        var type_name = type.replace(/[_]+/,'')+"_properties"
                        $scope.component= data
                        var _html = $compile(angular.element("<"+type_name+"></"+type_name+">"))($scope)
                        $element.html(_html)
                    }
                }
            })


        },
        link: function(scope, element, attrs){
            scope.properties_init_height = 180;
        },
        scope:true
    }
}])
.directive('modelSuffix', [function() {
  return {
    restrict: 'AE',
    require: '^ngModel',
    link: function(scope, element, attributes, ngModelController) {
          var suffix = attributes.modelSuffix;
          // Pipeline of functions called to read value from DOM 
          ngModelController.$parsers.push(function(value) {
            return value==null?value:value + suffix;
          });

          // Pipeline of functions called to display on DOM
          ngModelController.$formatters.push(function(value) {
            return value==null?value:value.replace(suffix, '');
          });
        }
  }
}])
.directive('integer', function(){
    return {
        require: 'ngModel',
        link: function(scope, ele, attr, ctrl){
            ctrl.$parsers.unshift(function(viewValue){
                var clean = viewValue.replace( /[^0-9]+/g, '');
                if (viewValue !== clean) {
                  ctrl.$setViewValue(clean);
                  ctrl.$render();
                }
                else{
                    return parseInt(viewValue, 10);
                }
            });

        }
    };
});