angular.module( 'widget.components' )
.directive('imageComponent', ["$rootScope","$sce",function($rootScope, $sce){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/components/image.html',
            replace:true,
            controller: function($scope, $element){
                
                $scope.imgload = function($event){
                    var origin_width = $element.find('img')[0].naturalWidth;
                    var origin_height = $element.find('img')[0].naturalHeight
                    $scope.component.aspect_ratio = origin_width/origin_height;
                }                
            },
            link: function(scope, element, attrs){
                scope.component.tmp_url = $sce.trustAsResourceUrl(scope.component.url)
                
            }
        };
}])
.directive('imgLoad', ['$parse', function ($parse) {
return {
  restrict: 'A',
  link: function (scope, elem, attrs) {
    var fn = $parse(attrs.imgLoad);
    elem.on('load', function (event) {
      scope.$apply(function() {
        fn(scope, { $event: event });
      });
    });
  }
};
}])
.directive('imageProperties', ['$sce', function($sce){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/image.html',
            replace:true,
            controller: function($scope, $element){
                $scope.change_url = function(){
                   var current_url = $scope.component.url;
                   $scope.component.tmp_url = $sce.trustAsResourceUrl(current_url);
                }
                $scope.change_overflow = function(){
                    
                }
                $scope.change_ratio = function(){
                    $scope.component.keep_ratio = !$scope.component.keep_ratio
                    if($scope.component.keep_ratio == true){
                        var custom_width = $scope.component.custom_resize_width;
                        if(custom_width!=null && $scope.component.aspect_ratio!=null){
                            $scope.component.custom_resize_height = Math.round(custom_width/$scope.component.aspect_ratio);
                        }
                    }
                }

                $scope.change_width = function(){
                        if($scope.component.keep_ratio == true){
                           $scope.component.custom_resize_height = Math.round($scope.component.custom_resize_width/$scope.component.aspect_ratio);
                        }
                }
                $scope.change_height = function(){
                        if($scope.component.keep_ratio == true){
                            $scope.component.custom_resize_width = Math.round($scope.component.custom_resize_height*$scope.component.aspect_ratio)
                        }
                }

                $scope.resize_options = [
                   {
                        id:0,
                        name:'Do not Resize'
                   } ,
                   {
                        id:1,
                        name:'Full'
                   },
                   {
                        id:2,
                        name:'Custom'
                   }

                ]

                $scope.height_options = [
                    {
                        id:'Auto',
                        name:'Auto'
                    },
                    {
                        id:'Small',
                        name:'Small'
                    },
                    {
                        id:'Medium',
                        name:'Medium'
                    },
                    {
                        id:'Large',
                        name:'Large'
                    },
                    {
                        id:'Custom',
                        name:'Custom'
                    }

                ]
            },
            link: function(scope, element, attrs){
               
            },
        };
}])
