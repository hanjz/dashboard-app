angular.module("widget.services")
.factory("util",['$cacheFactory',function($cacheFactory){
    function wash_cell(cell){
        if(cell.type!='cell'){
            return;
        }
        delete cell.right_cell;
        delete cell.left_cell;
        delete cell.up_cell;
        delete cell.down_cell;
    }

    function wash_component(component){
        if(component.index ==null || isNaN(component.index)){
          component.index = 0
        }
        delete component.ctrl_id;
        delete component.last_modified;
        delete component.$$hashkey;
        delete component.version;
        delete component.extra;
        delete component.display_value
        if(component.type=='pie'){
            delete component.components;
        }
    }
    function wash_other(widget){
        return;
    }

    function _wash(_widget){
        if(_widget==null){
          return;
        }

        if(_widget.components!=null){
          for(var i =0 ;i<_widget.components.length;i++){
              _wash(_widget.components[i]);
          }
        }

        if(_widget.cells!=null){
          for(var  i =0 ;i<_widget.cells.length;i++){
              _wash(_widget.cells[i]);
          }
        }

        if(_widget.type =='value_pair'){
            _wash(_widget.primary);
            _wash(_widget.secondary);
        }

        wash_component(_widget);
        wash_cell(_widget);
        wash_other(_widget);
        return _widget;
    }

    function getRandom(max) {
        return Math.random() * max;
    }

    function __uuid(){
        var id = '', i;

        for(i = 0; i < 36; i++)
        {
          if (i === 14) {
            id += '4';
          }
          else if (i === 19) {
            id += '89ab'.charAt(getRandom(4));
          }
          else if(i === 8 || i === 13 || i === 18 || i === 23) {
            id += '-';
          }
          else
          {
            id += '0123456789abcdef'.charAt(getRandom(16));
          }
        }
        return id;
    }

    var cache = $cacheFactory('cacheId');
    return {
        wash: function(widget){
            return _wash(widget);
        },
        uuid: function(){
            return __uuid();
        },
        cache: {
            get: function(key){
                return cache.get(key)
            },
            put: function(key , value){
                cache.put(key, value)
            }
        },
        credits: {
            "enabled": false
        },
        charts_options : function(){
            return {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    backgroundColor: 'rgba(0,0,0,0)',
                    animation:false,
                    "borderWidth": 1,
                    "borderColor": "rgba(0,0,0,0)"
                },
                title: {
                    text: ''
                },
                "legend": {
                    "align": "right",
                    "verticalAlign": "middle",
                    "layout": "vertical",
                    "itemStyle": {
                        "fontWeight": "normal"
                    }
                },
                "colors": [
                    "#497bac",
                    "#a63600",
                    "#fb7737",
                    "#8a6060",
                    "#2584ac",
                    "#29b4b8",
                    "#344a60",
                    "#00437a",
                    "#96b60d",
                    "#b6cbdf"
                ],
                credits: {
                    enabled: false
                },
                plotOptions: {
                    dataLabels: {
                        style: {
                            color: "#717171",
                            fontSize: "10px",
                            fontWeight:"normal"
                        },
                        enabled:true
                    },

                    pie: {
                        dataLabels: {
                            enabled: false
                        },
                        shadow: false,
                        center: ['50%', '50%'],
                        borderWidth: 0 // < set this option
                    },
                    series: {
                        states: {
                            hover: {
                                enabled: false,
                                halo: {
                                    size: 0
                                }
                            }
                        }
                    },
                    "column": {
                        "borderWidth": 0
                    }

                },
                series: [{
                    colorByPoint: true,

                    data: []
                }]

            }
        }

    };
}]);