angular.module( 'dashboard.components')
.directive('htmltemplateComponent', [function(){
        return {
            restrict:'E',
            replace:true,
            templateUrl: '/templates/view/components/html.html',
            controller: function($scope){
            	$scope.$emit('component-register', $scope.component);
            },
            link: function(scope, element, attrs){
            	setTimeout(function(){
            		scope.$emit('component-ready', scope.component);
            	},0)
            }
        };
}])
.filter('to_trusted', ['$sce', function($sce){
    return function(text){
        return $sce.trustAsHtml(text);
    }
}])