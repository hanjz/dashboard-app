angular.module('dashboard.components')
    .directive('widgetContainer', [function(){
        var components = [];
        function checkComponents(){
            for(var i = 0 ;i<components.length;i++){
                if(!components[i].ready){
                    return false;
                }
            }
            return true;
        }
        return {
            restrict:'AE',
            replace:true,
            controller: function($scope){
            },
            templateUrl: '/templates/view/components/widget_container.html',
            link: function(scope, element, attrs){
                scope.$on('component-ready', function(event, component){
                    component.ready = true;
                    if(checkComponents()){
                        //for phantomjs
                        element.attr('data-ready', true);
                    }
                });
                scope.$on('component-register', function(event,component){
                    component.ready = false;
                    components.push(component);
                });
                scope.$watch('component', function(nv, ov){
                	if(nv!= null){
                		if(scope.component.isCanvas==true && scope.component.background!=null){
                			var bg_component  = {ready: false}
                			components.push(bg_component)
                			element.find('.widget-bg').load(function(){
                				bg_component.ready = true
                				scope.$emit('component-ready', bg_component);
                			})
                		}
                		else{
                		if(scope.component.components == null){
                			element.attr('data-ready', true);
                		}
                		if(scope.component.components.length == 0){
                        	element.attr('data-ready', true);
                        }
                		}
                	}
                })
            },
            scope: {
                component:"="
            }

        }
    }])
