angular.module('dashboard.components')
.directive('layoutSelector', ['queryService', 'layoutAlgorithms', 'dashboardBase', function(queryService, layoutAlgorithms, dashboardBase){

    var layout_templates = [];
    var layout_template_columns = 3;

    layout_templates.push(new layoutAlgorithms.LayoutTemplate(2, [2, 1, 1]));
    layout_templates.push(new layoutAlgorithms.LayoutTemplate(3, [1, 1, 1]));
    layout_templates.push(new layoutAlgorithms.LayoutTemplate(3, [1, 1, 1, 3]));

//for test.
    layout_templates.push(new layoutAlgorithms.LayoutTemplate(6, [1, 1, 2, 2, 3, 3, 2, 2, 2]));


        return {
            restrict:'E',
            replace:true,
            templateUrl: '/templates/view/components/layout_selector.html',
            controller: function($scope){

                 $scope.disable_layout_mode = function(){
                    $scope.use_custom = false;
                    $scope.use_template = false;
                    $scope.use_canvas = false;
                }

                $scope.toggle_custom_layout = function(){
                    $scope.disable_layout_mode()
                    $scope.use_custom = true;
                }

                $scope.get_default_layout_column_number = function(num){
                    return new Array(num);
                }

                $scope.decrease_layout_column = function(){
                    --$scope.current_dashboard.grid_cols;
                    $scope.current_dashboard._reflow();
                    $scope.current_dashboard._change();
                }

                $scope.increase_layout_column = function(){
                    $scope.current_dashboard.grid_cols+=1;
                    $scope.current_dashboard._change();
                }

                $scope.switch_layout = function(index){
                    var columns =  $scope.current_dashboard.grid_cols;

                    $scope.disable_layout_mode();
                    $scope.use_template =true;

                    if($scope.current_template!=null && $scope.current_template == index){
                        return;
                    }

                    $scope.current_template = index;

                    $scope.current_dashboard._applyLayoutTemplate(layout_templates[index], {
                      rowFactor: dashboardBase.configuration.minHeight
                    });
                    $scope.current_dashboard._change();
                }


            },
            link: function(scope, element, attrs){
                scope.use_template = false;
                scope.layout_templates = [
                    {
                        index: 0,
                       style:'style_1'
                    },
                    {
                        index: 1,
                        style:'style_2'
                    },
                    {
                        index:2,
                        style:'style_3'
                    },
                    {
                        index:3,
                        style:'style_4'
                    }


                ]
                },

        };
}])
