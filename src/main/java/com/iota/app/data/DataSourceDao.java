package com.iota.app.data;

import com.iota.app.model.WidgetDataSource;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DataSourceDao extends JpaRepository<WidgetDataSource, String>{

}
