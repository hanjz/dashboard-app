angular.module('widget.components')
.directive('componentsTree', ["$compile","$rootScope","renderService",function($compile, $rootScope, renderService){
    return {
        replace:true,
        templateUrl:'/templates/edit/components/tree.html',
        controller: function($scope){
            $scope.icon_selector = {
                'widget':'fa fa-object-group',
                'grid':'fa fa-th-large',
                'cell':'fa fa-hashtag',
                'label':'fa fa-font',
                'value_pair':'fa fa-cc',
                'pie':'fa fa-pie-chart',
                'separator':'fa fa-scissors',
                'html_template':'fa fa-code',
                'image':'fa fa-picture-o',
                'table':'fa fa-table',
                'chart_label':'fa fa-cog',
                'chart_value':'fa fa-cog',
                'series_chart':'fa fa-bar-chart',
                'series_collection':'fa fa-list',
                'y_axis_collection':'fa fa-list',
                'series':'fa fa-cog',
                'x_axis':'fa fa-cog',
                'y_axis':'fa fa-cog'
            }

            $scope.collapsed = false;
            $scope.highlight = false;
            $scope.toggle_select = function(component){
                 if($rootScope.selected_component != null && $rootScope.selected_component.ctrl_id == component.ctrl_id){
                        //$rootScope.selected_component = null;
                    }
                    else{
                        $rootScope.selected_component = component;
                    }
            }
            $scope.remove_component = function(widget){
                if(confirm('Are You Sure? ')){
                    renderService.remove(widget.ctrl_id);
                }   
            }
            $scope.over_close = function(){
            	$scope.highlight = true;
            }
            $scope.out_close= function(){
            	$scope.highlight = false;
            }
            
        },
        link: function(scope,element,attrs){
            //if(scope.root==='true'){
                //$rootScope.treescope = scope;
        },

        compile: function(tElement, tAttr) {
            var contents = tElement.contents().remove();
            var compiledContents;
            return function(scope, iElement, iAttr) {
                if(scope.root === 'true'){
                    $rootScope.treescope = scope;
                }
                if(!compiledContents) {
                    compiledContents = $compile(contents);
                }
                compiledContents(scope, function(clone, scope) {
                    iElement.append(clone); 
                });
            };
        },

        scope:{
            widget:'=',
            root:'@'
        }
    }
}])
