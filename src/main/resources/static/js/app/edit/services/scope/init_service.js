angular.module("widget.services")
.factory("initService",["$rootScope","queryService",'util','$compile',function($rootScope,queryService, util, $compile){
    function init_widget($scope, data){
        var widget = {}
        if(data) {
            widget = data;
            widget.components = JSON.parse(data.components);
        }
        else{
            widget.type = 'widget';
            widget.isCanvas = false;
            widget.isValid = true;
            widget.name="new widget";
            widget.components = [];
        }
        $scope.widget= widget;
        widget.ctrl_id =  util.uuid();
        $rootScope.selected_component = $scope.widget = $rootScope.widget=widget;
        $scope.datasources = [];
        $scope.widget.datasources = [];
        $scope.loading = false;
    }
    return {
       init_operation: function($scope){

          $scope.widget_style={width:'120px;'}
          $scope.selected_component = null;
          $scope.loading = true;
          
          $rootScope.$watch('selected_component', function(component){
              if(component!=null){
               var ctrl_scope = angular.element("#"+component.ctrl_id).scope()
               if(ctrl_scope == null){
                  return;
               }
               var parent_scope = ctrl_scope.$parent;
               while(parent_scope!=null){
                  if(parent_scope.hasOwnProperty('collapsed')){
                      parent_scope.collapsed = true;
                  }

                  if(parent_scope.hasOwnProperty('root')&&parent_scope.root=='true'){
                      break;
                  }
                  parent_scope = parent_scope.$parent;
               }
             }
           })
           if($scope.widget_id===""){
               $scope.mode = 'new';
               init_widget($scope);
           }
           else {
               $scope.mode = 'edit';
               queryService.xhr_widget($scope.widget_id).success(function (data) {
                   init_widget($scope, data);
                   queryService.xhr_widget_datasources($scope.widget_id).success(function(datasources){
                	   if(datasources.length==0){
                		   $scope.datasources = [];
                		   util.cache.put('datasources', datasources);
                		   return;
                	   }
                	   var uids = datasources.map(function(obj){return obj.uid});
                	   queryService.xhr_datasources_detail(uids).success(function(datasources_details){
                		  $scope.datasources= datasources_details; 
                		  util.cache.put('datasources', datasources_details);
                	   })
                    //angular.element('body').append(visualizer)
              })
               })
           }
          
       }    
    };
}]);