package com.iota.app.controller;

import java.util.Map;

import com.iota.app.model.Widget;
import com.iota.app.service.WidgetService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class HomeController{

    @Resource
    private WidgetService widgetService;

    /**
     * 
     * 作者：张明育  
     * 日期：2017年1月22日 
     * 功能：登陆成功后重定向到/widget/home
     */
    @RequestMapping(value="/")
    public String home(Map model){
        return "redirect:/widget/home"; //重定向到widget下的home
    }
    
    /**
     * 
     * 作者：张明育  
     * 日期：2017年1月22日 
     * 功能：跳转到login页面
     */
    @RequestMapping(value="/login")
    public String login(Map model){
        return "login";
    }

    /**
     * 
       * 作者：张明育  
       * 日期：2017年1月22日 
       * 功能：
     */
    @RequestMapping(value="/image/{id}")
    public void img(@PathVariable String id, HttpServletRequest request, HttpServletResponse response){
        Widget widget = widgetService.findById(id);

    }
}
