package com.iota.app.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by guoxu.wu on 16/8/5.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MetaItem {
    private String type;
    private Integer index;
    private String name;

    public static MetaItem newInstance(String type,Integer index, String name){
        return new MetaItem(type, index, name);
    }
}
