angular.module( 'dashboard.components')
.directive('valuepairComponent', [function(){
        return {
            restrict:'E',
            templateUrl: '/templates/view/components/valuepair.html',
            controller: function($scope){

                if($scope.component.components.length>1) {
                    $scope.key_component = $scope.component.components[0];
                    $scope.value_component = $scope.component.components[1];
                }
            },
            link: function(scope, element, attrs){

            }
        };
}])