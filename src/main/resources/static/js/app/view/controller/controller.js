angular.module('dashboard').controller(
		'controller',
		[
				'$rootScope',
				'$scope',
				'queryService',
				'constantsService',
				'initService',
				function($rootScope, $scope, queryService, constantsService,
						initService){
					initService.init_operation($scope);

					$scope.load = function() {

					};
					$scope.time = function(){
						return (new Date()).valueOf();
					}

				} ])
