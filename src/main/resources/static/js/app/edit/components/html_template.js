angular.module( 'widget.components' )
.directive('htmltemplateComponent', ["$rootScope",function($rootScope){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/components/html.html',
            replace:true,
            controller: function($scope){
                $scope.toggle_select = function($event){
                    $event.stopPropagation() 
                    if($rootScope.selected_component != null && $rootScope.selected_component.ctrl_id == $scope.component.ctrl_id){
                        $rootScope.selected_component = null;
                    }
                    else{
                        $rootScope.selected_component = $scope.component;
                    }
                }

                
            },
            link: function(scope, element, attrs){
                //console.log(scope.component)
                //element.find('.html-container').html(scope.component.html_string);
            }
        };
}])
.filter('to_trusted', ['$sce', function($sce){
    return function(text){
        return $sce.trustAsHtml(text);
    }
}])
.directive('htmltemplateProperties', [function(){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/html.html',
            replace:true,
            controller: function($scope){
                $scope.change_overflow = function(){
                    if($scope.component.overflow == 0){
                        $scope.component.overflow = 1;
                    }
                    else{
                        $scope.component.overflow = 0;
                    }
                }
            },
            link: function(scope, element, attrs){
                
            },
        };
}])
