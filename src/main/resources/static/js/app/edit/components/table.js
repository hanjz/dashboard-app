angular.module('widget.components')
.directive('tableComponent', ['util', function(util){
        return {
            restrict:'AE',
            replace:true,
            templateUrl: '/templates/edit/components/table.html',
            controller: function($scope){
                 
            },
            link: function(scope, element, attrs){
                //scope.table = {}
                angular.forEach(scope.component.components, function(column){
                    column.extra = column.extra||{}
                    column.extra.table = scope.component;
                })

            },

        };
}])
.directive('tableProperties', ['util',function(util){
        return {
            restrict:'AE',
            replace:true,
            templateUrl: '/templates/edit/properties/table.html',
            controller: function($scope){
                $scope.rows_options = [
                   {
                        id:0,
                        name:'Show At Most : '
                   } ,
                   {
                        id:1,
                        name:'Show Exactly : '
                   },
                   {
                        id:2,
                        name:'Show All '
                   }

                ]
            },
            link: function(scope, element, attrs){
                //scope.table = {}

                             

                scope.increase_cols = function(){
                    scope.component.total_cols+=1;
                    var column = {
                        title:'untitled',
                        name:'column',
                        type:'column',
                        ctrl_id:util.uuid(),
                        data:[],
                        extra:{},
                    }
                    column.extra.table = scope.component;
                    scope.component.components.push(column);
                }

                scope.decrease_cols = function(){
                    scope.component.total_cols-=1;
                    scope.component.components.pop();
                }

                scope.increase_row_num = function(){
                    scope.component.row_num +=1;
                }

                scope.decrease_row_num = function(){
                    if(scope.component.row_num>1){
                        scope.component.row_num-=1;
                    }
                    else{
                        scope.component.row_num = 1;
                    }
                }
            },

        };
}])
