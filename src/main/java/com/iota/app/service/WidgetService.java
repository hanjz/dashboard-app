package com.iota.app.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.iota.app.data.WidgetDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.iota.app.model.Widget;

@Service
public class WidgetService {
	public final static String BG_PATH_PREFIX="bg/";
	@Resource
	private WidgetDao widgetDao;

	@Resource
	private FtpService ftpService;

	@Resource
	private ValueService valueService;

	
	/***
	 * 设置时间
	 * @param widget
	 * @return
	 */
	public Widget save(Widget widget){
		widget.setUpdateDate(new Date());
		return widgetDao.save(widget);
	}
	
	/**
	 * 根据id删除widget
	 * @param id
	 */
	public void delete(String id){
		widgetDao.delete(id);
	}
	
	
	public void delete(Widget widget){
		widgetDao.delete(widget);
	}
	
	/**
	 * 查找widget
	 * @return
	 */
	public List<Widget> findAll(){
		return widgetDao.findValidWidgets();
	}
	public Widget findById(String id){
		return widgetDao.findOne(id);
	}

	
	/**
	 * 创建新的widget
	 * @return
	 */
	public Widget newWidget(){
		Widget widget = new Widget();
		return save(widget);
	}

	
	/**
	 * 上传背景图
	 * @param widgetId
	 * @param fileName
	 * @param is
	 * @return
	 * @throws IOException
	 */
	public String uploadBg(String widgetId,String fileName, InputStream is) throws IOException{
		int imgSuffixIndex = fileName.lastIndexOf(".");
		String imgSuffix = fileName.substring(imgSuffixIndex);
		if(!imgSuffix.startsWith(".")){
			imgSuffix = "."+imgSuffix;
		}
		String filePath = BG_PATH_PREFIX+widgetId+imgSuffix;
		ftpService.sendFile(filePath, is);


		return filePath;
	}

	
	/**
	 * 查找widget
	 * @param order
	 * @return
	 */
	public Widget findByOrder(Integer order){
		int count = widgetDao.countByIsValid(true).intValue();
		if(order>=count){
			order = order % count;
		}
		if(order<0){
			order+=count;
		}
		final PageRequest page = new PageRequest(
				order, 1
		);
		Page<Widget> queryWidgets = widgetDao.findByIsValid(true, page);
		return (Widget)queryWidgets.getContent().get(0);
	}
}
