angular.module( 'dashboard.components')
.directive('imageComponent', [function(){
        return {
            restrict:'E',
            replace:true,
            templateUrl: '/templates/view/components/image.html',
            controller: function($scope){
            	$scope.$emit('component-register', $scope.component);
            },
            link: function(scope, element, attrs){
            	setTimeout(function(){
            		scope.$emit('component-ready', scope.component);
            	},0)
            }
        };
}])