angular.module('widget.components')
.directive('widget', ['$rootScope',function($rootScope){
    return {
        restrict:'E',
        replace:true,
        template: "<div class='editing-component' ctrl-droppable class='droppable' ng-class=\"{true:'root-content', false:''}[root=='true']\">"+
                        "<ul>"+
                            "<li ng-repeat='component in widget.components' "+
                            "ctrl-draggable='{{!component.atom}}' data='component' "+
                            "ng-click='toggle_select(component,$event)' "+
                            "ng-class=\"{'ctrl-selected':component.ctrl_id == $root.selected_component.ctrl_id, "+
                            "'height-small':component.height=='Small','height-medium':component.height=='Medium', 'height-large':component.height=='Large'}\""+
                            "ng-style=\"component.height=='Custom'?{height:component.custom_height+'px'}:{}\">"+
                                "<x-inline-component component='component'></x-inline-component>"+
                            "</li>"+
                        "</ul>"+
                  "</div>",

                  /*
                        ng-class="{'height-small':component.options.height=='Small', 'height-medium':component.options.height=='Medium','height-large':component.options.height=='Large'}" ng-style="component.options.height=='Custom'?{height:component.options.custom_height+'px'}:{}"
                  */
        controller:function($scope){
            $scope.active =function(event){
            }

            $scope.toggle_select = function(component, $event){
                    $event.stopPropagation()
                    if($rootScope.selected_component != null && $rootScope.selected_component.ctrl_id == component.ctrl_id){
                        $rootScope.selected_component = null;
                    }
                    else{
                        $rootScope.selected_component = component;
                    }
            }
        },
        link: function(scope, element, attrs){
            element.attr('root', scope.root)
        },
        scope: {
            widget:"=",
            root:"@"
        }
    }
}])
.directive('canvas', ['$document','Upload','$rootScope',function($document, Upload,$rootScope){
    return {
        restrict:'E',
        replace:true,
        template: "<div class='editing-canvas-content' free='true' class='droppable' ng-class=\"{true:'root-content', false:''}[root=='true']\">"+
                        "<div class='editing-canvas-container'>"+
                            "<div class='editing-canvas-inner' canvas-droppable ng-style='container_style'>"+ // big canvas width:1000px height:1000px
                                "<ul class='editing-canvas-target'>"+
                                    "<li ng-repeat='component in widget.components' canvas-draggable='{{!component.atom}}' resizable data='component' ng-click='toggle_select(component,$event)' ng-class=\"{true:'ctrl-selected', false:''}[component.ctrl_id == $root.selected_component.ctrl_id]\" style='top:{{component.top}};left:{{component.left}};width:{{component.width}};height:{{component.height}}' >"+
                                        "<x-inline-component component='component'></x-inline-component>"+
                                    "</li>"+
                                "</ul>"+
                                "<img class='widget-bg' draggable=\"false\"  ng-src='{{background}}' width='100%'/>"+
                            "</div>"+
                        "</div>"+
                        "<ul class='pan-navigator'>"+
                            "<li><a href='#' ng-click='zoomin()'><i class='fa fa-plus'></i></a></li>"+
                            "<li><a href='#' ng-click='zoomout()'><i class='fa fa-minus'></i></a></li>"+
                            "<li><a href='#' class='fa fa-picture-o upload-image'>"+
                                "<input type='file' ng-model='upload_image' name='upload_image' ngf-max-size=\"20MB\"  ngf-select='upload_file($file)'/>"+
                            "</a></li>"+
                        "</ul>"+
                  "</div>",
        controller:function($scope, $element){
            var zoom_scale = 1.2
            var container = $element.find('.editing-canvas-container');
            var init_width = container.width();
            if($scope.widget.width < init_width){
                $scope.widget.width = init_width;
                $scope.widget.height= null;
            }
            var inner = $element.find('.editing-canvas-inner');
            if($scope.widget.background==""|| $scope.widget.background==null){
                $scope.background = "/img/transparent.png";
            }
            else{
                $scope.background = $scope.widget.background;
            }
             $element.find('.widget-bg').load(function(){
                 if(!$scope.widget.height) {
                     inner.height($(this).height());
                     $scope.widget.height = $(this).height();
                 }
                 if(!$scope.widget.width){
                     inner.width($(this).width());
                     $scope.widget.width = $(this).width();
                 }
             })
             $scope.$broadcast('component-reflow');
             $scope.toggle_select = function(component, $event){
                    $event.stopPropagation()
                    if($rootScope.selected_component != null && $rootScope.selected_component.ctrl_id == component.ctrl_id){
                        $rootScope.selected_component = null;
                    }
                    else{
                        $rootScope.selected_component = component;
                    }
            }
            $scope.refresh_size = function(){
                $scope.container_style = {
                    width: 'auto',
                    height: 'auto'
                }

                if($scope.widget.width && $scope.widget.height) {
                    $scope.container_style =  {
                        width: $scope.widget.width+"px",
                        height: $scope.widget.height+"px"
                    }
                }
            }

            $scope.refresh_size();

            $scope.zoomin = function(){
                //var width = inner.width()+zoom_scale;
                //var height = inner.height+zoom_scale;

                var widget_width =  Math.round(inner.width()*zoom_scale);
                var widget_height = Math.round(inner.height()*zoom_scale) ;
                inner.width(widget_width);
                inner.height(widget_height);
                container.scrollTop(container.scrollTop()*zoom_scale);
                container.scrollLeft(container.scrollLeft()*zoom_scale);
                $scope.widget.width = widget_width;
                $scope.widget.height = widget_height;
                $scope.refresh_size();
            }

            $scope.zoomout = function(){
                var widget_width = Math.round(inner.width()/zoom_scale);
                if(widget_width<init_width){return;};
                var widget_height = Math.round(inner.height()/zoom_scale);
                inner.width(widget_width);
                inner.height(widget_height);
                $scope.widget.width = widget_width;
                $scope.widget.height = widget_height;
                $scope.refresh_size();
            }

            $scope.upload_file = function(file){
                if(file){
                    Upload.upload({
                        url:'/api/widgets/upload/',
                        data:{
                            widget:$rootScope.widget.id,
                            file:file
                        }
                    }).then(function(ret){
                        $scope.widget_bg = ret.data.url;
                        $scope.background= ret.data.remote+ret.data.url;
                        $rootScope.widget.background = ret.data.url;
                    })
                }
            }
        },
        link: function(scope, element, attrs){
            element.attr('root', scope.root)

            var container = element.find('.editing-canvas-container')
            var startx, starty;
            var start_scroll_x = container.scrollLeft();
            var start_scroll_y = container.scrollTop();
            var mousemove = function(event){
                var moving_x = event.pageX;
                var moving_y = event.pageY;
                var movex = moving_x - startx;
                var movey = moving_y - starty;

                container.scrollTop(start_scroll_y-movey);
                container.scrollLeft(start_scroll_x-movex);
            }

            var mouseup = function(){
                $document.unbind('mousemove', mousemove);
                $document.unbind('mouseup', mouseup);
                container.unbind('mousemove', mousemove);
            }


            container.on('mousedown', function(event){
                if(!$(event.target).hasClass('editing-canvas-target')){
                    return;
                }
                event.stopPropagation();
                startx = event.pageX;
                starty = event.pageY;

                start_scroll_y = container.scrollTop();
                start_scroll_x = container.scrollLeft();
                container.on('mousemove', mousemove)
                $document.on('mouseup', mouseup);
            })

        },
        scope: {
            widget:"=",
            root:"@"
        }
    }
}])
.directive('widgetProperties', [function(){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/widget.html',
            replace:true,
            controller: function($scope){
            	$scope.change_width_percentage = function(){
            		$scope.component.widthPercentage = !$scope.component.widthPercentage;
            		if($scope.component.widthPercentage === true){
            			$scope.component.width = 100;
            		}
            		else{
            			$scope.component.width = 800;
            		}
            	}
            	$scope.change_height_percentage = function(){
            		$scope.component.heightPercentage = !$scope.component.heightPercentage;
            		if($scope.component.heightPercentage === true){
            			$scope.component.height=100;
            		}
            		else{
            			$scope.component.height = 600;
            		}
            	}
            },
            link: function(scope, element, attrs){

            },
        };
}])
.directive('resizable',[function(){
    return {
            restrict:'AE',
            controller: function($scope){
            },
            link: function(scope, element, attrs){
                   element.resizable({
                        stop: function(event, ui){
                            scope.component.width=ui.size.width+"px";
                            scope.component.height = ui.size.height+"px";
                            scope.$broadcast('component-reflow')
                        }
                   });
            },
        };
}])
