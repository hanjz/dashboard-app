angular.module('widget.components')
.directive('separatorComponent', ["$rootScope",function($rootScope){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/components/separator.html',
            replace:true,
            controller: function($scope){
               
            },
            link: function(scope, element, attrs){
            }
        };
}])
.directive('separatorProperties', [function(){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/separator.html',
            replace:true,
            controller: function($scope){

            },
            link: function(scope, element, attrs){
            },
        };
}])