package com.iota.app.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guoxu.wu on 16/8/5.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Meta {
    private String dataSourceUid;
    private String dataSourceName;
    private String dataSourceDescription;
    private String type;
    private List<MetaItem> columns = new ArrayList();

    public void addMetaItem(MetaItem item){
        columns.add(item);
    }
    public static Meta newInstance(String uid, String name){
        Meta meta = new Meta();
        meta.setDataSourceUid(uid);
        meta.setDataSourceName(name);
        return meta;
    }
}
