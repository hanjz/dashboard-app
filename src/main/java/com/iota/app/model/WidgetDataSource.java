package com.iota.app.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class WidgetDataSource extends BaseModel{

	@Column
	private String uid;
	
	@ManyToOne
	@JsonIgnore
	private Widget widget;
}
