angular.module('widget.components')
.directive('valuepairComponent', ["$rootScope",function($rootScope){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/components/valuepair.html',
            controller: function($scope){
                if($scope.component.components.length>1) {
                    $scope.key_component = $scope.component.components[0];
                    $scope.value_component = $scope.component.components[1];
                }
            },
            link: function(scope, element, attrs){

            }
        };
}])
.directive('valuepairProperties', [function(){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/valuepair.html',
            replace:true,
            controller: function($scope){

            },
            link: function(scope, element, attrs){
            }
        };
}])