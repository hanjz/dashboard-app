angular.module( 'widget.components')
.directive('seriesComponent',[function(){
    return {
        restrict:'AE',
        controller: function($scope,$element){

        },
        link: function(scope, element, attrs){

        }
    };
}])
.directive('seriesProperties',[function(){
    return {
        restrict:'AE',
        templateUrl:'/templates/edit/properties/series.html',
        controller: function($scope,$element){

        },
        link: function(scope, element, attrs){
            scope.component.extra.update = function(){
                scope.component.extra.chart_component.extra.update();
            }
            scope.toggle_line_chart = function(){
                scope.component.style='line';
                scope.component.extra.update()
            }

            scope.toggle_bar_chart = function(){
                scope.component.style='column';
                scope.component.extra.update()
            }

            scope.change_showvalue = function(){
                scope.component.options.show_value = !scope.component.options.show_value;
                scope.component.extra.update();
            }
            scope.update_label = function(){
                scope.component.extra.update();
            }
            scope.use_custom_color = function(){
                scope.component.options.use_custom_color = !scope.component.options.use_custom_color;
                scope.component.extra.update();
            }
            /*scope.$on("colorpicker-selected", function(evt, colorObj){
                console.log(colorObj)
                scope.component.options.custom_color=colorObj.value;
                scope.component.extra.update();
            })*/

            scope.change_color = function(){
                scope.component.extra.update();
            }

        }
    };
}])
.directive('seriescollectionComponent', [function(){
    return {
        restrict:'AE',
        template:'<div><ul><li ng-repeat="item in component.components"><x-inline-component component="item"></x-inline-component></li></ul></div>',
        controller: function($scope,$element){
        },
        link: function(scope, element, attrs){
            var chart_component = scope.component.extra.parent;
            angular.forEach(scope.component.components, function(component){
                component.extra=component.extra||{}
                component.extra.chart_component = chart_component;
            })
        }
    };
}])
