package com.iota.app.data;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.iota.app.model.Widget;

public interface WidgetDao extends JpaRepository<Widget, String> {
	@Query("select w from Widget w where w.isValid=true")
	public List<Widget> findValidWidgets();

	public Page<Widget> findByIsValid(Boolean isValid, Pageable page);

	public List<Widget> findByIsValid(Boolean isValid);

	public Long countByIsValid(Boolean isValid);
}
