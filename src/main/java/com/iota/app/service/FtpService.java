package com.iota.app.service;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.ftp.session.DefaultFtpSessionFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by guoxu.wu on 16/7/8.
 */
@Service
public class FtpService {

    @Value("${ftp.server}")
    private String ftpServer;
    @Value("${ftp.port}")
    private int ftpPort;
    @Value("${ftp.user}")
    private String ftpUser;
    @Value("${ftp.pass}")
    private String ftpPass;
    public void sendFile(String filePath, InputStream is) throws IOException {
    	FTPClient ftpClient = new FTPClient();
    	ftpClient.connect(ftpServer,ftpPort);
    	ftpClient.login(ftpUser, ftpPass);
    	ftpClient.enterLocalPassiveMode();
        ftpClient.setKeepAlive(true);
        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
        ftpClient.storeFile(filePath, is);
        ftpClient.disconnect();
    }

    public void getFile(String id, OutputStream os) throws IOException {
    	FTPClient ftpClient = new FTPClient();
    	ftpClient.connect(ftpServer,ftpPort);
    	ftpClient.login(ftpUser, ftpPass);
    	ftpClient.enterLocalPassiveMode();
        ftpClient.setKeepAlive(true);
    	ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
        ftpClient.retrieveFile(String.valueOf(id)+".png", os);
        ftpClient.disconnect();
    }
}
