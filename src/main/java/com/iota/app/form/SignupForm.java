package com.iota.app.form;

import com.iota.app.annotations.FieldMatch;
import com.iota.app.model.Account;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
/**
 * 注册信息所用属性
 * @author admin
 *
 */
@FieldMatch.List({
        @FieldMatch(first = "password", second = "confirmPassword", message = "The password fields must match"),
})
@Getter
@Setter
public class SignupForm {

    private static final String NOT_BLANK_MESSAGE = "Value should not be blank";//值不能为空
    private static final String EMAIL_MESSAGE = "You must input correct email format";//必须输入正确的email格式

    @NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)//非空验证
    @Email(message = SignupForm.EMAIL_MESSAGE)      //输入正确的邮箱格式
    private String email;

    @NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
    private String password;

    @NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
    private String confirmPassword;

    public Account createAccount() {
        return new Account(getEmail(), getPassword(), "ROLE_USER");
    }

}
