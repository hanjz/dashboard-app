angular
		.module('widget.components')
		.directive(
				'ctrlList',
				[
						'queryService',
						function(queryService) {
							var ctrls_list = function(widget_type) { /*
																		 * widget
																		 * or
																		 * canvas
																		 */
								return [
										{
											name : 'Layout',
											icon : 'fa-th',
											canvas_exclude : true,
											data : {
												"type" : "grid",    //表格
												"name" : "new_grid",
												"rows" : 2,
												"cols" : 3,
												"cells" : [
														{
															"id" : 5,
															"parent" : 24,
															"type" : "cell",
															"name" : "new_cell",
															"last_modified" : "2015-12-23T09:44:52.027423Z",
															"width" : 1,
															"height" : 1,
															"coord_x" : 0,
															"coord_y" : 0,
															"padding" : 0,
															"components" : []
														},
														{
															"type" : "cell",
															"name" : "new_cell",
															"width" : 1,
															"height" : 1,
															"coord_x" : 0,
															"coord_y" : 1,
															"padding" : 0,
															"components" : []
														},
														{
															"type" : "cell",
															"name" : "new_cell",
															"width" : 1,
															"height" : 1,
															"coord_x" : 0,
															"coord_y" : 2,
															"padding" : 0,
															"components" : []
														},
														{
															"type" : "cell",
															"name" : "new_cell",
															"width" : 1,
															"height" : 1,
															"coord_x" : 1,
															"coord_y" : 0,
															"padding" : 0,
															"components" : []
														},
														{
															"type" : "cell",
															"name" : "new_cell",
															"width" : 1,
															"height" : 1,
															"coord_x" : 1,
															"coord_y" : 1,
															"padding" : 0,
															"components" : []
														},
														{
															"type" : "cell",
															"name" : "new_cell",
															"width" : 1,
															"height" : 1,
															"coord_x" : 1,
															"coord_y" : 2,
															"padding" : 0,
															"components" : []
														}, ]
											}
										},

										{
											name : 'Label',    //label标签
											icon : 'fa-font',
											data : {
												css_style : "line-height:1.42857143;font-size:1.4em;",
												ctrl_id : "4d062f49-47d4-4556-af42-d362afcca92f",
												auto_width: false,
												name : "label",
												format : "text",
												data : {
													type : 'value',
													expr : 'Label'
												},
												display_value : "Label",
												type : "label",
												wrap : true
											}
										},
										{
											name : 'Value Pair',   //cc
											icon : 'fa-cc',
											data : {
												ctrl_id : "166f3432-edec-4f9b-86e5-32f970cf2d73",
												name : "valuepair",
												components:[
													{
														css_style : "line-height:1.42857143;font-size:1.4em;font-style:italic",
														ctrl_id : "4d062f49-47d4-4556-af423232-d362afcca92f",
														name : "key",
														format : "text",
														data : {
															type : 'value',
															expr : 'Key'
														},
														display_value : "Label",
														type : "label",
														wrap : true
													},
													{
														css_style : "line-height:1.42857143;font-size:1.8em;font-weight:bold",
														ctrl_id : "4d062f49-47d4-4556-af4223232-d362afcca92f",
														name : "value",
														format : "text",
														data : {
															type : 'value',
															expr : 'Value'
														},
														display_value : "Label",
														type : "label",
														wrap : true
													}
												],
												type : "value_pair"
											}
										},
										{
											name : 'Separator',    //剪刀
											icon : 'fa-scissors',
											canvas_exclude : true,
											data : {
												ctrl_id : "166f3432-edec-4f9b-86e5-32f97dcf2d73",
												name : "separator",
												line_color : "#aaa",
												line_weight : 1,
												orientation : 0,
												type : "separator"
											}
										},
										{
											name : 'HTMLTemplate',  //网页模板 </>
											icon : 'fa-code',
											data : {
												ctrl_id : '166f34ad-edec-4f9b-86e5-32f97dcf2d73',
												html_string : '<h4>HTML Template</h4><p>Please edit the html code in the properties panel</p>',
												html_script : ' ',
												name : 'html_template',
												height : widget_type == 'canvas' ? '200px'
														: 'Auto',
												custom_height : 200,
												overflow : 0,
												type : 'html_template'
											}
										},
										{
											name : 'Inline Frame',   //@
											icon : 'fa-at',
											data : {
												type : 'inline_frame',
												ctrl_id : '166f34ad-edec-4f9b-86e5-32f97dcf2dbp',
												name : 'inline_frame',
												url : '/widget/baidu_map',
												tmp_url : '/widget/sample/frame',
												height : widget_type == 'canvas' ? '200px'
														: 'Medium',
												custom_height : 200,
												overflow : 0,
												refresh : -1
											}
										},
										{
											name : 'Image',   //图片
											icon : 'fa-picture-o',
											data : {
												type : "image",
												name : "image_1",
												ctrl_id : "166f34ad-32ec-4f9b-86e5-32f97dcf2dbp",
												url : '',
												height : widget_type == 'canvas' ? '200px'
														: 'Auto',
												custom_height : 200,
												keep_ratio : false,
												resize : 0,
												custom_resize_width : 100,
												custom_resize_height : 100,
												alignment_horizontal : 0
											}
										},
										{
											name : 'Table',  //表格
											icon : 'fa-table',
											data : {
												type : "table",
												name : "table_1",
												ctrl_id : "166f34ad-32ec-4bcd-86e5-32f97dcf2dbp",
												show_header : true,
												first_row_header : false,
												total_cols : 4,
												row_option : 0,// 0 at most, 1
																// exactly, 2
																// all rows
												row_num : 8,
												components : [
														{
															title : 'untitled',
															name : 'column',
															type : 'column',
															ctrl_id : "166f34ad-32ec-4bcd-86e5-32f97dcf2db1",
														},
														{
															title : 'untitled',
															name : 'column',
															type : 'column',
															ctrl_id : "166f34ad-32ec-4bcd-86e5-32f97dcf2db2",
														},
														{
															title : 'untitled',
															name : 'column',
															type : 'column',
															ctrl_id : "166f34ad-32ec-4bcd-86e5-32f97dcf2db3",
														},
														{
															title : 'untitled',
															name : 'column',
															type : 'column',
															ctrl_id : "166f34ad-32ec-4bcd-86e5-32f97dcf2db4",
														} ]
											}
										},
										{
											name : 'Pie',    //饼图
											icon : 'fa-pie-chart',
											data : {
												type : 'pie',
												name : "pie",
												ctrl_id : "166f34ad-32ec-4bcd-86e5-32c3d2df2dbp",
												height : widget_type == 'canvas' ? '200px'
														: 'Medium',
												custom_height : 200,
												options : {
													max_slice : 6,
													label : 2,
													show_legend : true,
													show_labels : true,
													show_percentage : false,
													legend_position : "t,r",
													legend_fcolor : '#000',
													legend_bcolor : '#fff',
													background:'#fff'
												},
												chart_value : {
													name : 'values',
													type : 'chart_value',
													ctrl_id : "166f34ad-32ec-4e322-86e5-32f97dcf2db4",
													data : {
														type : 'data',
														expr : ''
													}
												},
												chart_label : {
													name : 'labels',
													type : 'chart_label',
													ctrl_id : "165564ad-32ec-4bc9-86e5-32f97dcf2db4",
													data : {
														type : 'data',
														expr : ''
													}
												},
											}
										},
										{
											name : 'Gauge',   //仪表盘
											icon : 'fa-tachometer',
											data : {
												type : 'gauge',
												name : "Gauge",
												ctrl_id : "166f34ad-32ec-4bcd-86e5-32c3d2df2djp",
												height : widget_type == 'canvas' ? '200px'
														: 'Medium',
												custom_height : 200,
												options : {
													title:'value',
													min:0,
													max:100
												},
												data : {
													type : 'value',
													expr : '50',
													datasource : ''
												}
											}
										},
										{
											name : 'Line/Bar',  //柱状图或者折线图
											icon : 'fa-bar-chart',
											data : {
												type : 'series_chart',
												name : "line/bar",
												ctrl_id : "166f34ad-32qc-4bcd-86e5-32a392df2dbp",
												height : widget_type == 'canvas' ? '200px'
														: 'Medium',
												custom_height : 200,
												options : {
													show_legend : true,
													stack_bar : false,
													stack_line : false,
													stack_area : false,
													inverted : false
												},

												components : [
														{
															// x-axis
															type : 'x_axis',
															name : 'X Axis',
															ctrl_id : "166334ad-32qc-4bcd-86e5-32a392df2dbp",
															data : {
																type : 'data',
																expr : '',
																datasource : ''
															},
															time: {
																frequency:'S',
																duration:0
															},
															options : {
																show_axis : false,
																axis_title : '',
																show_label : true,
																rotation : 0,
																show_tick : true,
																sort : 0, // -1,desc
																			// 0,no
																			// sort
																			// 1
																			// ase
																show_grid : false,
																axis_color:'#000'
															}
														},
														
														{
															// series
															type : 'series_collection',
															ctrl_id : "166f34aa-32qc-4bcd-86e5-32a392df2dbp",
															name : "Series Set",
															components : [
																	{
																		type : 'series',
																		style:'line',
																		axis : 'untitled',
																		ctrl_id : "166134ad-32qc-4bcd-86e5-32a392df2dbp",
																		name : 'series 1',
																		options : {
																			show_value : false,
																			use_custom_color : false,
																			custom_color : '#f00',
																			line : {},
																			bar : {},
																		},
																		data : {
																			type : 'data',
																			expr : '',
																			datasource : ''
																		}
																	},

															]
														}, ]

											}
										}

								]

							}

							return {
								scope : {
									widget : '='
								},
								restrict : 'E',
								template : "<div class='ctrls-list'>"
										+ "<ul>"
										+ "<li ng-repeat='ctrl in ctrls'>"
										+ "<div ng-if='widget.isCanvas==false' class='ctrl-item' ctrl-draggable data='ctrl.data' handle='no' clone='true'>"
										+ "<i class='fa' ng-class='ctrl.icon'></i>"
										+ "</div>"
										+ "<div ng-if='widget.isCanvas==true' class='ctrl-item' canvas-draggable data='ctrl.data' handle='no' clone='true'>"
										+ "<i class='fa' ng-class='ctrl.icon'></i>"
										+ "</div>" + "</li>"
										+ "</ul>",
								controller : function($scope) {
									$scope.ctrls = [];
									$scope
											.$watch(
													'widget',
													function(nv, ov) {
														if (nv != null) {
															if (!$scope.widget.isCanvas) {
																$scope.ctrls = ctrls_list('widget');
															} else {
																angular
																		.forEach(
																				ctrls_list('canvas'),
																				function(
																						ctrl) {
																					if (!ctrl.canvas_exclude) {
																						$scope.ctrls
																								.push(ctrl)
																					}
																				})
															}
														}
													})

								},
								link : function(scope, element, attrs) {

								}
							};
						} ])