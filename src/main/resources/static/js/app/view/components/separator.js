angular.module( 'dashboard.components')
.directive('separatorComponent', [function(){
        return {
            restrict:'E',
            replace:true,
            templateUrl: '/templates/view/components/separator.html',
            controller: function($scope){

            },
            link: function(scope, element, attrs){
            }
        };
}])