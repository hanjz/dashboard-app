angular.module('dashboard.components')
.directive('columnComponent', ["$rootScope",'queryService',function($rootScope,queryService){
        return {
            restrict:'AE',
            replace:true,
            templateUrl: '/templates/view/components/column.html',
            controller: function($scope){
            	$scope.$emit('component-register', $scope.component);
                $scope.component.display_value = "";
                if($scope.component.data == null){
                	  setTimeout(function(){
                          $scope.$emit('component-ready', $scope.component);
                      },10)
                    return;
                }
                if($scope.component.data.type=='data'){
                    if($scope.component.data.expr!=null && $scope.component.data.datasource!=null){
                        queryService.xhr_fetch_expr($scope.component.data.expr, $scope.component.data.datasource).success(function(data){
                            $scope.component.display_value = data.data;
                            setTimeout(function(){
                                $scope.$emit('component-ready', $scope.component);
                            },10)
                        })
                    }
                    else{
                    	setTimeout(function(){
                            $scope.$emit('component-ready', $scope.component);
                        },10)
                    }
                }
                else{
                    $scope.component.display_value = $scope.component.data.expr;
                    setTimeout(function(){
                        $scope.$emit('component-ready', $scope.component);
                    },10)
                }
            },
            link: function(scope, element, attrs){
                //scope.table = {}
                scope.data = [];
                scope.component.extra.change_data = function(data){

                    var row_option = scope.component.extra.table.row_option;
                    var row_num = scope.component.extra.table.row_num;


                    if(angular.isArray(data)){
                        if(row_option == 2){
                            scope.data = data;
                        }
                        else{
                            if(row_option == 0){
                                //show at most;
                                if(data.length>row_num){
                                    scope.data = data.slice(0, row_num);
                                }
                                else{
                                    scope.data = data;
                                }
                            }
                            else{
                                if(data.length>row_num){
                                    scope.data = data.slice(0, row_num);
                                }
                                else{
                                    scope.data = [];
                                    for(var i = 0 ;i < row_num ; i++) {
                                        if(i<data.length){
                                            scope.data[i] = data[i]
                                        }
                                        else{
                                            scope.data[i] = "";
                                        }

                                    }
                                }
                            }
                        }
                    }
                    else{
                        scope.data = [data];
                        if(row_option == 1) {
                            for(var i = 1 ;i<row_num ;i ++){
                                scope.data[i] = ""
                            }
                        }
                    }
                }

            }
        };
}])

.filter('chop', function(){
    return function(data, row_option, row_num){
        if(data==null){
            return data;
        }

       if(angular.isArray(data)){
            if(row_option == 2){
                return data;
            }
            else{
                if(row_option == 0){
                    //show at most;
                    if(data.length>row_num){
                        return data.slice(0, row_num);
                    }
                    else{
                        return data
                    }
                }
                else{
                    if(data.length>row_num){
                        return data.slice(0, row_num);
                    }
                    else{
                        for(var i = 0 ;i < row_num ; i++) {
                            if(i>=data.length){
                                data[i] = "";
                            }
                        }
                        return data;
                    }
                }
            }
        }
        else{
            data = [data];
            if(row_option == 1) {
                for(var i = 1 ;i<row_num ;i ++){
                    data[i] = ""
                }
            }
            return data;
        }
    }
})
