angular.module('widget.components')
.directive('chartvalueComponent', ['queryService',function(queryService){
        return {
            restrict:'E',
            controller: function($scope){
            },
            link: function(scope, element, attrs){
               scope.component.extra.update = function(){
                     //scope.component.extra.chart.extra.update()
                     scope.component.extra.chart.extra.update();
                }  
            }
        };
}])
.directive('chartvalueProperties', ['queryService',function(queryService){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/chart-value.html',
            replace:true,
            controller: function($scope){
                
            },
            link: function(scope, element, attrs){

            },
        };
}])