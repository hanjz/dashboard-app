angular.module("dashboard.services").factory("constantsService",['queryService',function(queryService){
    return {
        init_constants: function($scope){
            $scope.dashboards = [];
            $scope.current_dashboard = null;
            $scope.widgets = [];
            $scope.hide_top_bar = false;
            $scope.use_custom = true;

            $scope.use_canvas = false;
            $scope.carouseling = null;
            $scope.seeding =false;

            $scope.status = {
                tab_adding : false,
                full_screen: false,
                layout_switch : false,
            }

            $scope.refresh_status = function(){
                $scope.status = {
                    tab_adding : false,
                    full_screen: false,
                    layout_switch : false,
                }
            }

            //overall dashboard options

            $scope.dashboard_opts = {
                columns:6,
                isMobile: true,
                minColumns: 1,
                pushing: true,
                margins: [15, 15],
                rowHeight: 150,
                swapping:true,
                row_factor:2,
                resizable: {
                    enabled: true,
                    handles: [ 'se'],
                    start: function(event, $element, widget) {
                        widget.current_resizing_x = widget.sizeX;
                        widget.current_resizing_y = widget.sizeY;
                    },
                    resize: function(event, $element, widget) {},
                    stop: function(event, $element, widget) {
                        if(widget.current_resizing_x == widget.sizeX && widget.current_resizing_y == widget.sizeY){
                            return ;
                        }
                        var widgets = $scope.widgets
                        angular.forEach(widgets, function(w){
                            w.height = w.sizeY;
                            w.width = w.sizeX;
                            queryService.xhr_update_widget(w);
                        })
                    }
                },
                draggable: {
                    enabled: true,
                    handle: '.widget-header',
                    start: function(event, $element, widget) {
                        widget.current_dragging_row = widget.row;
                        widget.current_dragging_col = widget.col
                    },
                    drag: function(event, $element, widget) {},
                    stop: function(event, $element, widget) {
                        if(widget.current_dragging_col == widget.col && widget.current_dragging_row == widget.row){
                            return;
                        }

                        var widgets = $scope.widgets
                        angular.forEach(widgets, function(w){
                            queryService.xhr_update_widget(w);
                        })
                    }
                }
            }

            //templates


        }
    };
}]);
