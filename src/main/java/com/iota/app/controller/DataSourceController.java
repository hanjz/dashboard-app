package com.iota.app.controller;

import java.util.*;

import com.iota.app.service.DataSourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
/**
 * 数据源
 * @author admin
 *
 */
@Controller
@RequestMapping(value="datasources")
public class DataSourceController {
	@Autowired
	private DataSourceService dataSourceService;
	
	/**
	 * 
	 * 作者：张明育  
	 * 日期：2017年1月22日 
	 * 功能：数据源列表
	 */
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String list(ModelMap map){
		Object dataSources = dataSourceService.list();
		map.put("dataSources", dataSources);
		return "datasource/datasource";
	}
	
	@RequestMapping(value="/testData", method=RequestMethod.GET)
	public String testData(){

		return "";
	}
	
	/**
	 * 
	 * 作者：张明育  
	 * 日期：2017年1月22日 
	 * 功能：创建数据源（添加）来源：csv或nifi
	 */
	@RequestMapping(value="/new")
	public String newDataSource(){

		return "datasource/new";
	}

	/**
	 * 
	 * 作者：张明育  
	 * 日期：2017年1月22日 
	 * 功能：跳转到数据源为：csv的页面
	 */
	@RequestMapping(value="/new/csv")
	public String newCSV(){
		return "datasource/csv";
	}

	/**
	 * 
	   * 作者：张明育  
	   * 日期：2017年1月22日 
	   * 功能：跳转到数据源为：nifi的页面
	 */
	@RequestMapping(value="/new/nifi", method=RequestMethod.GET)
	public String newNifi(){
		return "datasource/nifi";
	}


	/**
	 * 
	 * 作者：张明育  
	 * 日期：2017年1月22日 
	 * 功能：添加nifi数据源，添加成功后重定向到数据源列表页面
	 */
	@RequestMapping(value="/new/nifi", method=RequestMethod.POST)
	public String newNifiPost(@RequestParam("name") String name,
							  @RequestParam("url") String url,
							  @RequestParam("description")String description,
							  @RequestParam("portName") String portName){

		Map infoMap = new HashMap();
		infoMap.put("name", name);
		infoMap.put("url", url);
		infoMap.put("description", description);
		infoMap.put("portName", portName);
		dataSourceService.newNifi(infoMap);
		return "redirect:/datasources/"	;
	}
	
	/**
	 * 
	 * 作者：张明育  
	 * 日期：2017年1月22日 
	 * 功能：（添加）上传csv文件,提交成功后重定向到数据源列表页面
	 */
	@RequestMapping(value="/upload")
	public String upload(@RequestParam("content") MultipartFile file,
						 @RequestParam("name") String name,
						 @RequestParam("description") String description,
						 @RequestParam("type") String dataType
						 ) throws Exception {

		Map map = new HashMap();
		map.put("name", name);
		map.put("description", description);
		map.put("content", file.getInputStream());
		map.put("type", dataType);
		dataSourceService.newDataSource(map);
		return "redirect:/datasources/";
	}
	
	/**
	 * 
	 * 作者：张明育  
	 * 日期：2017年1月22日 
	 * 功能：根据uid删除数据，删除成功后重定向到数据源列表
	 */
	@RequestMapping(value="/delete")
	public String delete(@RequestParam("uid") String uid){

		dataSourceService.deleteByUid(uid);
		return "redirect:/datasources/";
	}
	
}
