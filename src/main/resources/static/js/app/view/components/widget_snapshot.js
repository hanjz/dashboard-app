angular.module('dashboard.components')
    .directive('widgetSnapshot', [function(){

        return {
            restrict:'AE',
            replace:true,
            controller: function($scope){

            },
            templateUrl: '/templates/view/components/widget_snapshot.html',
            link: function(scope, element, attrs){
                
            },
            scope: {
                component:"="
            }

        }
    }])
