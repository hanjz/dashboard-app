angular.module( 'widget.components')
.directive('inlineframeComponent', ["$rootScope","$sce",function($rootScope, $sce){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/components/frame.html',
            replace:true,
            controller: function($scope){
                
            },
            link: function(scope, element, attrs){
                //console.log(scope.component)
                //element.find('.html-container').html(scope.component.html_string);
                scope.component.tmp_url = $sce.trustAsResourceUrl(scope.component.url)
            }
        };
}])
.directive('inlineframeProperties', ['util', '$sce', function(util, $sce){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/frame.html',
            replace:true,
            controller: function($scope, $element){
                $scope.change_url = function(){
                   var current_url = $scope.component.url;
                   $scope.component.tmp_url = $sce.trustAsResourceUrl(current_url);
                }
                $scope.change_overflow = function(){
                    if($scope.component.overflow == 0){
                        $scope.component.overflow = 1;
                    }
                    else{
                        $scope.component.overflow = 0;
                    }
                    var url = $scope.component.url;
                    //$scope.component.url = url;
                    //console.log($element.html())
                    //$element.find('iframe').attr('src', url)

                    if(url!=null){
                        if(typeof url == 'object'){
                            url = url.toString();
                        }

                        var seed_index = url.indexOf('&refresh_seed');
                        if(seed_index>0){
                            url = url.substring(0,seed_index);
                        }
                        seed_index = url.indexOf('?refresh_seed')
                        if(seed_index>0){
                            url = url.substring(0,seed_index);
                        }
                        if(url.indexOf("?")>0){
                            $scope.component.tmp_url = $sce.trustAsResourceUrl(url+"&refresh_seed="+util.uuid());
                        }
                        else{
                            $scope.component.tmp_url = $sce.trustAsResourceUrl(url+"?refresh_seed="+util.uuid());
                        }
                    }
                }

                $scope.refresh_intervals = [
                    {
                        id:-1,
                        name:'Never'
                    },
                    {
                        id:15,
                        name:'15 min.'
                    },
                    {
                        id:30,
                        name:'30 min.'
                    },
                    {
                        id:60,
                        name:'60 min.'
                    }
                ]
            },
            link: function(scope, element, attrs){
                
            },
        };
}])
