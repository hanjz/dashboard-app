angular.module( 'widget.components')
.directive('pieComponent', ['$rootScope','util','queryService','$timeout',function($rootScope,util,queryService, $timeout){
        
        var verify_data = function(component){
            var chart_value = component.chart_value;
            var chart_label = component.chart_label;
            if(chart_value==null|| chart_label == null){
                return false;
            }
            if(chart_value.data==null || chart_label.data == null){
                return false;
            }
           
            if(chart_label.data.expr == null || chart_label.data.expr==''){
                return false;
            }

            if(chart_label.data.datasource == null || chart_label.data.datasource==''){
                return false;
            }

            return true;
        }

        
        return {
            restrict:'AE',
            templateUrl:'/templates/edit/components/pie.html',
            replace:true,
            controller: function($scope,$element){


            var draw_chart = function(container, chart_data,_options){
                var options = util.charts_options();
                options.chart.type='pie';
                
                //options.chart.height=container.height();

                options.series = [{
                    data: ((chart_data!=null&&chart_data.length>0)?chart_data:[["No Data",1]])
                }]
                options.legend.labelFormat = (function(opt){
                    var formatter = "{name}";
                    if(opt==null){
                        return formatter
                    }
                    if(opt.show_labels){
                        formatter +="({y:.2f})"
                    }
                    if(opt.show_percentage){
                        formatter+="[{percentage:.2f}%]"
                    }
                    return formatter
                })(_options)
                if(_options.legend_fcolor){
                	options.legend.itemStyle={
                			color:_options.legend_fcolor
                	}
                }
                if(_options.legend_bcolor){
                	options.legend.backgroundColor=_options.legend_bcolor
                }
                if(_options.background){
                    options.chart.backgroundColor=_options.background;
                }
                switch(_options.legend_position){
                    case 'left':
                        options.legend.align='left'
                        options.legend.verticalAlign='middle'
                        options.legend.layout= "vertical"
                        break;
                    case 'right':
                        options.legend.align='right'
                        options.legend.verticalAlign='middle'
                        options.legend.layout= "vertical"
                        break;
                    case 'bottom':
                        options.legend.align='right'
                        options.legend.verticalAlign='bottom'
                        options.legend.layout= "horizontal"
                        break;
                    case 'top':
                        options.legend.align='right'
                        options.legend.verticalAlign='top'
                        options.legend.layout= "horizontal"
                        break;
                    default:
                        break;
                }

                options.plotOptions.pie = {
                    "allowPointSelect": true,
                    "cursor": true,
                    "showInLegend": _options?_options.show_legend:true,
                    "innerSize": _options?_options.inner_size:"0%",
                    borderWidth: 0,
                    "dataLabels": {
                        enabled:_options?_options.data_labels:false,
                        format: (function(opt){
                            var formatter = "{point.name}";
                            if(opt==null){
                                return formatter
                            }
                            if(opt.show_labels){
                                formatter +="({point.y:.2f})"
                            }
                            if(opt.show_percentage){
                                formatter+="[{point.percentage:.2f}%]"
                            }
                            return formatter
                        })(_options),
                        style:{
                            fontWeight:"normal"
                        }
                    },
                    "tooltip": {
                        "headerFormat":"<strong>{point.key}</strong> : ",
                        "pointFormat": "<span style='text-decoration:italic'>{point.y}</span>"
                    }
                }
                $timeout(function(){
                     $scope.pie_chart=container.find(".chart-container").highcharts(options).highcharts();
                     $scope.pie_chart.reflow()
                },10)
                return 

            }


                $scope.component.extra.update = function(_data){
                    if(!verify_data($scope.component)){
                        draw_chart($element, null,$scope.component.options);
                        return;
                    }
                    var chart_data = []
                    queryService.xhr_fetch_expr($scope.component.chart_label.data.expr, $scope.component.chart_label.data.datasource)
                    .success(function(_label_data){
                        var label_data = _label_data.data
                        if(label_data!=null && label_data.length>0){
                            if($scope.component.chart_value.data.datasource==null || $scope.component.chart_value.data.expr==null){
                                var value_data = []
                                var iteration_num = $scope.component.options.max_slice<label_data.length?$scope.component.options.max_slice:label_data.length;
                                for(var i = 0 ; i<iteration_num;i++){
                                    var push_data = [];
                                    push_data[0] = label_data[i];
                                    if(value_data[i]!= null && value_data[i] >=0 && _(value_data[i]).toNumber(2)!=NaN){
                                        push_data[1] = _(value_data[i]).toNumber(2);
                                    }
                                    else{
                                        push_data[1] = 1;
                                    }
                                    chart_data.push(push_data);
                                }
                                draw_chart($element, chart_data,$scope.component.options);
                                return;
                            }
                             queryService.xhr_fetch_expr($scope.component.chart_value.data.expr, $scope.component.chart_value.data.datasource)
                             .success(function(_value_data){
                                 var value_data = _value_data.data;
                                var iteration_num = $scope.component.options.max_slice<label_data.length?$scope.component.options.max_slice:label_data.length;
                                for(var i = 0 ; i<iteration_num;i++){
                                    var push_data = [];
                                    push_data[0] = label_data[i];
                                    if(value_data[i]!= null && value_data[i] >=0 && _(value_data[i]).toNumber(2)!=NaN){
                                        push_data[1] = _(value_data[i]).toNumber(2);
                                    }
                                    else{
                                        push_data[1] = 0;
                                    }
                                    chart_data.push(push_data);
                                }

                                if(label_data.length>$scope.component.options.max_slice){
                                    var push_data = [];
                                    push_data[0] = 'Others'
                                    var tmp_sum = 0;
                                    for(var i = $scope.component.options.max_slice;i<label_data.length;i++){
                                        if(value_data[i]!=null && value_data[i]>0 && _(value_data[i]).toNumber(2)!=NaN){
                                            tmp_sum+=_(value_data[i]).toNumber(2);
                                        }
                                    }
                                    push_data[1] = Math.round(tmp_sum*100)/100
                                    chart_data.push(push_data);
                                }
                                draw_chart($element, chart_data,$scope.component.options);
                             })
                        }
                    })

                    
                    
                }
                
                $scope.component.extra.update();

                $scope.$on('component-reflow',function(){
                    $scope.pie_chart.reflow();
                })

            },
            link: function(scope, element, attrs){
                scope.component.chart_label.extra = scope.component.chart_label.extra||{}
                scope.component.chart_value.extra = scope.component.chart_value.extra||{}
                scope.component.chart_label.extra.chart = scope.component;
                scope.component.chart_value.extra.chart = scope.component;
                scope.component.components = [scope.component.chart_label, scope.component.chart_value] 
            }
        };
}])
.directive('pieProperties', [function(){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/pie.html',
            replace:true,
            controller: function($scope){
                $scope.backgroundCss = {background:$scope.component.options.background};
                $scope.legendCss = {color:$scope.component.options.legend_fcolor, background :$scope.component.options.legend_bcolor};

                $scope.toggle_pie_style = function(_options){
                    $scope.component.options = $scope.component.options||{};
                    if(_options == 0){
                        $scope.component.options.inner_size="0%"
                    }
                    else{
                        $scope.component.options.inner_size="60%"
                    }
                    $scope.component.extra.update()

                } 

                $scope.label_options = [
                    {
                        id:0,
                        name:'Show Labels Around Chart'
                    },
                    {
                        id:1,
                        name:"Don't Show Labels"
                    },
                    {
                        id:2,
                        name:'Show Labels in Legend'
                    }
                ]
                if(!$scope.bindColorEvent) {
                    $scope.bindColorEvent = true;
                    var offCall = $scope.$on('colorpicker-selected', function (event, colorObj) {
                            var type = event.targetScope.type;
                            if (type === 'legend') {
                                if (colorObj.name == 'fcolor') {
                                    $scope.component.options.legend_fcolor = colorObj.value;
                                }
                                else if (colorObj.name == 'bcolor') {
                                    $scope.component.options.legend_bcolor = colorObj.value;
                                }
                            }
                            else if (type === 'background') {
                                $scope.component.options.background = colorObj.value;
                            }
                            $scope.component.extra.update();
                    })
                    $scope.$on("$destroy", function(){
                        offCall();
                    })
                }


                $scope.change_label = function(){
                    $scope.component.options = $scope.component.options||{};
                    switch($scope.component.options.label){
                        case 1:
                            $scope.component.options.show_legend = false
                            $scope.component.options.data_labels=false
                            break;
                        case 0:
                            $scope.component.options.data_labels=true
                            $scope.component.options.show_legend = false 
                            break;
                        case 2:
                            $scope.component.options.show_legend = true; 
                            $scope.component.options.data_labels=false
                            break;
                        default:
                            break;
                    }
                        
$scope.component.extra.update();
                }

                $scope.change_show_labels = function(){
                    $scope.component.options = $scope.component.options||{}
                    $scope.component.options.show_labels = !$scope.component.options.show_labels;
                    $scope.component.extra.update();
                }

                $scope.change_show_percentage = function(){
                    $scope.component.options = $scope.component.options||{}
                    $scope.component.options.show_percentage = !$scope.component.options.show_percentage;
                    $scope.component.extra.update();
                }

            },
            link: function(scope, element, attrs){
               
            },
        };
}])
