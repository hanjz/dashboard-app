package com.iota.app.rest;

/**
 * Created by cqiu on 16/08/02.
 */

public class RestResponseCode {
	public final static int OK                     = 0;
	public final static int DATASOURCE_NOTFOUND    = 1001;
	public final static int FORMAT_ERROR           = 1002;
	public final static int DATAMODEL_NOTMODIFIED  = 1003;
	public final static int DATAMODEL_INVALID      = 1004;
	public final static int NIFI_NOTSTARTED        = 1005;
	public final static int PARAMS_INVALID         = 1006;
	public final static int ERROR_UNKNOWN          = 9999;
}
