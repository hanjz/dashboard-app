package com.iota.app.controller;

import com.iota.app.form.SignupForm;
import com.iota.app.model.Account;
import com.iota.app.service.AccountService;
import org.hibernate.pretty.MessageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

/**
 * Created by wgx on 16/9/26.
 */
@Controller
public class AuthController {

    @Autowired
    private AccountService accountService;

    /**
     * 
     * 作者：张明育  
     * 日期：2017年1月22日 
     * 功能：跳转到登陆的页面（resources/templates/signin.html）
     */
    @RequestMapping(value = "signin")
    public String signin() {
        return "signin";
    }
    
    /**
     * 
     * 作者：张明育  
     * 日期：2017年1月22日 
     * 功能：点击登录页面的（申请自己的用户名和密码）按钮,跳转到signup.html页面
     */
    @RequestMapping(value="signup")
    public String signup(Model model){
        model.addAttribute(new SignupForm());
        return "signup";
    }
     
    /**
     * 
     * 作者：张明育  
     * 日期：2017年1月22日 
     * 功能：新用户注册，注册成功重定向到HomeController下的“/”
     */
    @RequestMapping(value = "signup", method = RequestMethod.POST)
    public String signup(@Valid @ModelAttribute SignupForm signupForm, Errors errors, RedirectAttributes ra) {
        if (errors.hasErrors()) {
            return "signup"; //注册不成功返回到注册页面
        }
        Account account = accountService.save(signupForm.createAccount());  //创建用户
        accountService.signin(account);  //根据创建的新用户登入
        return "redirect:/";   //重定向到HomeController下的“/”
    }

}
