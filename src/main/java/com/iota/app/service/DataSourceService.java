package com.iota.app.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iota.app.dto.DataObject;
import com.iota.app.dto.NifiDto;
import com.iota.app.dto.builder.DataObjectBuilder;
import com.iota.app.rest.RestResponse;
import com.iota.app.vo.DataSourceVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class DataSourceService {

	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private ValueService valueService;

	
	/**
	 * 数据源列表
	 * @return
	 */
	public Object list(){
		String url = valueService.getDbServerUrl();
		RestResponse dataSources = restTemplate.getForObject(url, RestResponse.class, new String[]{});
		return dataSources.getResult();
	}

	
	/**
	 * 根据id删除数据源
	 * @param id
	 */
	public void deleteByUid(String id){
		String url = valueService.getDbServerUrl();
		restTemplate.delete(url+"/"+id);
	}
	
	/**
	 * 根据行和列的下标取数据
	 * @param id
	 * @param cs
	 * @param ce
	 * @param rs
	 * @param re
	 * @param flat
	 * @return
	 */
	public Object data(String id, Long cs, Long ce, Long rs, Long re, boolean flat){

		String url = valueService.getDbServerUrl()+"/"+id+"/data/?"+(flat?"flat=true":"flat=false");
		String requestParam="";
		if(cs!=null){
			requestParam+=("&column_start_index="+cs);
		}
		if(ce!=null){
			requestParam+=("&column_end_index="+ce);
		}
		if(rs!=null){
			requestParam+=("&row_start_index="+rs);
		}
		if(re!=null){
			requestParam+=("&row_end_index="+re);
		}
		RestResponse data = restTemplate.getForObject(url+requestParam, RestResponse.class, new String[]{});
		return data.getResult();
	}
	
	/**
	 * 选中数据源之后，显示详细的数据列表
	 * @param uuids
	 * @return
	 */
	public Object datasources(List<String> uuids){
		StringBuffer param = new StringBuffer();
		for(String uuid : uuids){
			param.append("uids=");
			param.append(uuid);
			param.append("&");
		}
		String url = valueService.getDbServerUrl()+"/list/?"+param.toString();


		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY);
		mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
		MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
		messageConverter.setObjectMapper(mapper);

		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(messageConverter);

		RestResponse data = new RestResponse();
		restTemplate.setMessageConverters(messageConverters);
		 data = restTemplate.getForObject(url, data.getClass(), new String[]{});
		return data.getResult();
	}

	
	/**
	 * 执行添加csv文件的功能
	 * @param infoMap
	 * @throws Exception
	 */
	public void newDataSource(Map infoMap) throws Exception{
		DataObject dataObject = DataObjectBuilder.build(infoMap);


		/**convert to api data (wtf!)**/

		/**name
		 * type
		 * params(shit)
		 * owner
		 * roles
		 * datamodel {index, name,type}
		 * description
		 * */
		Map map = new HashMap();
		map.put("name",dataObject.getMeta().getDataSourceName());
		map.put("description", dataObject.getMeta().getDataSourceDescription());
		map.put("type", "CSV");
		map.put("datamodel", dataObject.getMeta().getColumns());
		RestResponse response = restTemplate.postForObject(valueService.getDbServerUrl()+"/new", map, RestResponse.class,new String[]{});
		Object vo = response.getResult();

		//shit ...
		//String uid = vo.getUid();
		//PUT value="/{uid}/data",

		/**&
		*
		 * @RequestMapping(value="/{uid}/data", method=RequestMethod.PUT)
		 @ResponseBody
		 public RestResponse addDataSourceContents(@PathVariable("uid") String uid, @RequestBody String dataVect) {
		 return restResponseService.addDataSourceContents(uid, dataVect);
		 }


		 WHAT THE FUCK 'DATAVECT'
		* */

		String uid = ((Map)vo).get("uid").toString();
		restTemplate.put(valueService.getDbServerUrl()+"/"+uid+"/data", dataObject.getData());

	}

	
	/**
	 * 执行添加nifi数据的功能
	 * @param infoMap
	 */
	public void newNifi(Map infoMap){
		NifiDto dto = NifiDto.newInstance(infoMap.get("name").toString(), infoMap.get("description").toString(), infoMap.get("portName").toString(), infoMap.get("url").toString());
		restTemplate.postForObject(valueService.getDbServerUrl()+"/new", dto, Object.class,new String[]{});
	}


}
