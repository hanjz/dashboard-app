package com.iota.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iota.app.service.DataSourceService;

@Controller
@RequestMapping(value="/api/datasources")
public class DataSourceControllerAPI {

	@Autowired
	private DataSourceService dataSourceService;
	
	/**
	 * 
	 * 作者：张明育  
	 * 日期：2017年1月22日 
	 * 功能：画布中添加了控件之后，点击添加数据源按钮，查找数据源列表
	 */
	@RequestMapping(value="/")
	@ResponseBody
	public Object list(){
		return dataSourceService.list();
	}
	
	/**
	 * 
	 * 作者：张明育  
	 * 日期：2017年1月22日 
	 * 功能：选中数据源列表后，根据选中的当前的id查找数据，然后根据航和列的下标取数据
	 */
	@RequestMapping(value={"/{uid}","/{uid}/data"})
	@ResponseBody
	public Object data(@PathVariable("uid") String uid,
					   @RequestParam(value="flat", required = false, defaultValue = "false") boolean flat,
			@RequestParam(value="column_start_index",required=false) Long columnStartIndex, 
			@RequestParam(value="column_end_index", required=false) Long columnEndIndex, 
			@RequestParam(value="row_start_index",required=false) Long rowStartIndex, 
			@RequestParam(value="row_end_index", required=false) Long rowEndIndex){
		return dataSourceService.data(uid, columnStartIndex, columnEndIndex, rowStartIndex, rowEndIndex,flat);
	}
	
	/**
	 * 
	 * 作者：张明育  
	 * 日期：2017年1月22日 
	 * 功能：双击数据源列表之后查询详细数据列表
	 */
	@RequestMapping(value="/list/")
	@ResponseBody
	public Object datasources(@RequestParam(value="uids") List<String> uuids){
		return dataSourceService.datasources(uuids);
	}
}
