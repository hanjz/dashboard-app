angular.module( 'widget.components')
.directive('gaugeComponent', ['$rootScope','util','queryService','$timeout','$q',function($rootScope,util,queryService, $timeout, $q){
	
	var verify_data = function(component){
       
        return true;
    }

	
        return {
            restrict:'AE',
            templateUrl:'/templates/edit/components/gauge.html',
            replace:true,
            controller: function($scope,$element){
                $scope.gauge_chart = null; 
                var gaugeOptions = {

        chart: {
            type: 'solidgauge',
            backgroundColor: "rgba(255, 255, 255, 0)",
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false
        },

        pane: {
    		"center": [
    			"50%",
    			"85%"
    		],
    		"size": "140%",
    		"startAngle": "-90",
    		"endAngle": "90",
    		"background": {
    			"backgroundColor": "#EEE",
    			"innerRadius": "60%",
    			"outerRadius": "100%",
    			"shape": "arc"
    		}
    	},
    	"tooltip": {
    		"enabled": true
    	},
    	credits: {
            enabled: false
        },
        // the value axis
    	"yAxis": {
    		"stops": [
    			[
    				0.1,
    				"#73d423"
    			],
                [
                    0.25,
                    "#b5dc31"
                ],
    			[
    				0.5,
    				"#EFCE2B"
    			],
                [
                    0.75,
                    "#d4823b"
                ],
    			[
    				0.9,
    				"#A00808"
    			]
    		],
            tickAmount:2,
            tickPositioner: function() {
                return [this.min, this.max];
            },
    		"min": 0,
    		"max": 100,
    		"lineWidth": 0,
    		"minorTickInterval": null,
    		"tickPixelInterval": 400,
    		"tickWidth": 0,
    		"title": {
    			"y": -70
    		},
    		"labels": {
    			"y": 16
    		}
    	},
        series: [{
            name: 'Speed',
            data: [50]
        }]

    }
                var draw_chart = function(container, chart_data){
                    gaugeOptions.title={text:chart_data.key};
                    if(chart_data.background){
                        gaugeOptions.chart.backgroundColor=chart_data.background
                    }
                	gaugeOptions.series[0].name = chart_data.key;
                	if(isNaN(chart_data.value)){
                		chart_data.value = 0;
                	}
                	gaugeOptions.series[0].data = [parseFloat(chart_data.value)];
                	gaugeOptions.yAxis.min = isNaN(chart_data.min)?0:parseFloat(chart_data.min);
                	gaugeOptions.yAxis.max = isNaN(chart_data.max)?100:parseFloat(chart_data.max);
                    $timeout(function(){
                         $scope.gauge_chart=container.find(".chart-container").highcharts(gaugeOptions).highcharts();
                         $scope.gauge_chart.reflow()
                    },10)
                    return ;

                }



                $scope.component.extra.update = function(){
                    var chart_data = {
                        key:$scope.component.options.title,
                        min:$scope.component.options.min,
                        max:$scope.component.options.max,
                        value:$scope.component.data.expr,
                        background: $scope.component.options.background
                    };
                	if(!verify_data($scope.component)){
                		draw_chart($element, chart_data);
                        return;
                    }	
                	if($scope.component.data.type=='data'){
                        if($scope.component.data.expr!=null && $scope.component.data.datasource!=null){
                            queryService.xhr_fetch_expr($scope.component.data.expr, $scope.component.data.datasource).success(function(data){
                                chart_data.value = data.data.join();
                                draw_chart($element, chart_data);
                            })
                        }
                    }
                    else{
                    	chart_data.value = $scope.component.data.expr;
                    	draw_chart($element, chart_data);
                    }
                	
                	
                } 
                $scope.component.extra.update();

                $scope.$on('component-reflow',function(){
                    $scope.gauge_chart.reflow();
                })

            },
            link: function(scope, element, attrs){
            
            }
        };
}])
.directive('gaugeProperties', [function(){
        return {
            restrict:'AE',
            templateUrl: '/templates/edit/properties/gauge.html',
            replace:true,
            controller: function($scope){
                $scope.toggle_pie_style = function(_options){
                    $scope.component.options = $scope.component.options||{};
                    if(_options == 0){
                        $scope.component.options.inner_size="0%"
                    }
                    else{
                        $scope.component.options.inner_size="60%"
                    }
                    $scope.component.extra.update()

                } 

                $scope.label_options = [
                    {
                        id:0,
                        name:'Show Labels Around Chart'
                    },
                    {
                        id:1,
                        name:"Don't Show Labels"
                    },
                    {
                        id:2,
                        name:'Show Labels in Legend'
                    }
                ]

                $scope.apply_value = function(){
                    $scope.component.extra.update();
                }

                $scope.change_label = function(){
                    $scope.component.options = $scope.component.options||{};
                    switch($scope.component.options.label){
                        case 1:
                            $scope.component.options.show_legend = false
                            $scope.component.options.data_labels=false
                            break;
                        case 0:
                            $scope.component.options.data_labels=true
                            $scope.component.options.show_legend = false 
                            break;
                        case 2:
                            $scope.component.options.show_legend = true; 
                            $scope.component.options.data_labels=false
                            break;
                        default:
                            break;
                    }
                        
                    $scope.component.extra.update();
                }


                $scope.$on('colorpicker-selected', function (event, colorObj) {
                    $scope.component.options.background = colorObj.value;
                    $scope.component.extra.update();
                })


                $scope.change_show_labels = function(){
                    $scope.component.options = $scope.component.options||{}
                    $scope.component.options.show_labels = !$scope.component.options.show_labels;
                    $scope.component.extra.update();
                }

                $scope.change_show_percentage = function(){
                    $scope.component.options = $scope.component.options||{}
                    $scope.component.options.show_percentage = !$scope.component.options.show_percentage;
                    $scope.component.extra.update();
                }

            },
            link: function(scope, element, attrs){
               scope.component.display_value=scope.component.data.expr;
            },
        };
}])