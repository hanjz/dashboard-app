angular.module( 'dashboard.components')
.directive('inlineframeComponent', ["$sce",function($sce){
        return {
            restrict:'E',
            replace:true,
            templateUrl: '/templates/view/components/frame.html',
            controller: function($scope){
            	$scope.$emit('component-register', $scope.component);
            },
            link: function(scope, element, attrs){
                scope.tmp_url = $sce.trustAsResourceUrl(scope.component.url);
                setTimeout(function(){
            		scope.$emit('component-ready', scope.component);
            	},0)
            }
        };
}])