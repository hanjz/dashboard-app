angular.module( 'widget.components')
.directive('yaxisComponent',[function(){
    return {
        restrict:'AE',
        controller: function($scope,$element){
            
        },
        link: function(scope, element, attrs){
            
        }
    };
}])
.directive('yaxiscollectionComponent', [function(){
        return {
            restrict:'AE',
            templateUrl:'<div><ul><li ng-repeat="item in component.components"><x-inline-component component="item"></x-inline-component></li></ul></div>',
            controller: function($scope,$element){
            },
            link: function(scope, element, attrs){
            }
        };
}])
