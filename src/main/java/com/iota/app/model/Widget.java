package com.iota.app.model;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;
import lombok.Setter;
/**
 * Created by guoxu.wu on 16/6/27.
 */
@Getter
@Setter
@Entity
public class Widget extends BaseModel{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column
	private String type="widget";

	@Column
    private String name="new widget";

    @Column
    private Integer index;

    @Column
    private Boolean widthPercentage = false; 
    
    @Column
    private Integer width = 1200;

    @Column
    private Boolean heightPercentage = false;
    
    @Column
    private Integer height = 900;

    @Lob
    @Column
    private String components="[]";
    
    @Column
    private Boolean isValid=false;

    @Column
    private Boolean isCanvas=false;

    @Column
    private String background;

    @Column 
    private String thumbnail;
    
    @Column 
    private String userId;
    
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
    @JoinColumn(name="widget_id")
    private List<WidgetDataSource> dataSources;
    
    public String toJson() throws JsonGenerationException, JsonMappingException, IOException{
    	ObjectMapper om = new ObjectMapper();
    	StringWriter sw = new StringWriter();
    	om.writeValue(sw, this);
    	return sw.toString();
    }

	@Override
	public String toString() {
		return "Widget [type=" + type + ", name=" + name + ", index=" + index + ", widthPercentage=" + widthPercentage
				+ ", width=" + width + ", heightPercentage=" + heightPercentage + ", height=" + height + ", components="
				+ components + ", isValid=" + isValid + ", isCanvas=" + isCanvas + ", background=" + background
				+ ", thumbnail=" + thumbnail + ", userId=" + userId + ", dataSources=" + dataSources + "]";
	}
}
