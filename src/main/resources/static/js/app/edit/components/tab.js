angular.module('widget.components'). 
directive('tabs', function() { 
    return { 
      restrict: 'E', 
      transclude: true, 
      link:function(scope, element, attrs){
          scope.pane_style = {
              height: "280px"
          }
      },
      controller: [ "$scope", function($scope) { 
        var panes = $scope.panes = []; 
  
        $scope.select = function(pane) { 
          angular.forEach(panes, function(pane) { 
            pane.selected = false; 
          }); 
          pane.selected = true; 
        } 
  
        this.addPane = function(pane) { 
          if (panes.length == 0) $scope.select(pane); 
          panes.push(pane); 
          if(pane.active=='true'){
            $scope.select(pane)
          }
        } 

        $scope.resize_properties_panel_moving = function(offset_x, offset_y){

            var properties_panel = angular.element('.tab-content');
            var edit_panel = angular.element('.edit-panel')
            edit_panel.css('bottom', properties_panel.height())
            //$scope.properties_init_height = (parseInt($scope.properties_init_height)-offset_y)+"px";
            //console.log($scope.pro)
        }

        $scope.resize_properties_panel_over = function(offset_x, offset_y){
            $scope.properties_init_height = $scope.properties_init_height-offset_y;
        }

      }], 
      template: 
        '<div class="tabbable">' + 
          '<ul class="nav nav-tabs">' + 
            '<li ng-repeat="pane in panes" ng-class="{active:pane.selected}">'+ 
              '<a href="" ng-click="select(pane)">{{pane.title}}</a>' + 
            '</li>' + 
          '</ul>' + 
          '<div class="tab-content" ng-style="pane_style">'+
          '<x-resizer position="top" direction="vertical" maxoffset="200" resizeclass="transparent" callbackmoving="resize_properties_panel_moving(offset_x, offset_y)" callbackover="resize_properties_panel_over(offset_x, offset_y)"></x-resizer>'+
          '<div style="height:100%;" ng-transclude></div>'+
          '</div>' + 
        '</div>', 
      replace: true 
    }; 
  }). 
  directive('pane', function() { 
    return { 
      require: '^tabs', 
      restrict: 'E', 
      replace:true,
      transclude: true, 
      scope: { title: '@' , active:'@'}, 
      link: function(scope, element, attrs, tabsCtrl) { 
        tabsCtrl.addPane(scope); 
      }, 
      template: 
        '<div class="tab-pane" ng-class="{active: selected}" ng-transclude>' + 
        '</div>', 
      replace: true 
    }; 
  })