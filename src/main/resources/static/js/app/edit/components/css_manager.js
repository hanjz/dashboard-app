function css_manager(css_string){
    function parse_css_string(css_string){
        
        var style_obj = {};
        if(css_string==null){
            return style_obj;
        }
        var style_array = css_string.split(";")
        for(var i = 0 ;i<style_array.length;i++){
            var css_clip = style_array[i];
            if(css_clip!=null && css_clip!=""){
                var css_kv = css_clip.split(":");
                if(css_kv.length==2){
                    var _key = _.trim(css_kv[0]);
                    var _value = _.trim(css_kv[1]);
                    style_obj[_key] = _value;
                }
            }
        }

        return style_obj;
    }

    this.append_css = function(css_obj){
        if(typeof css_obj == 'string'){
            var append_obj = parse_css_string(css_obj)
            this.append_css(append_obj);
        }
        else if(typeof css_obj == 'object'){
            for(var key in css_obj){
                this.style[key] = css_obj[key];
            }
        }
    }
    this.apply_css = function(css_obj){
        if(typeof css_obj == 'string'){
            var append_obj = parse_css_string(css_obj)
            this.apply_css(append_obj);
        }
        else if(typeof css_obj == 'object'){
            for(var key in css_obj){
                //this.style[key] = css_obj[key];
                if(this.style[key]!=null){
                    if(this.style[key] == css_obj[key]){
                        delete this.style[key];
                    }
                    else{
                        this.style[key] = css_obj[key]
                    }
                }
                else{
                    this.style[key] = css_obj[key]
                }
            }
        }
    }
    this.remove_css = function(css_key){
        delete style_obj[css_key]
    }

    this.tostring = function(){
        var ret = "";
        for(var key in this.style){
            ret = ret+key+":"+this.style[key]+";"
        }

        return ret;
    }

    this.style = parse_css_string(css_string)
}