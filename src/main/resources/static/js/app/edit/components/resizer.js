angular.module('widget.services')
.directive('resizer', ['$document',function($document){
    return {
        restrict:'E',
        replace:true,
        scope:{
            callbackstart:"&",
            callbackmoving:"&",
            callbackover:"&"
        },
        template: '<div class="resize-bar"></div>',
        controller: function(){

        },
        link: function(scope, element, attrs){

            var position = attrs['position'];
            var h_or_v = attrs['direction'];
            var max_string = attrs['maxoffset'];
            var resize_class = attrs['resizeclass'];
            var is_double = attrs['double']

            var resize_node_selector = attrs["resizenode"];
            var resize_node = element.parent();
            if(resize_node_selector!=null){
                resize_node = angular.element(resize_node_selector)
            }
            var max_offset = -1;
            var init_width = 0;
            var init_height = 0;
            scope.offset_x = 0;
            scope.offset_y = 0;
            if(max_string){
                if(!isNaN(max_string)){
                    max_offset = parseFloat(max_string);
                }
            }
            element.addClass(_.startsWith(h_or_v,'h')?'horizontal':'vertical')

            element.addClass( {'left': 'pl', "right":'pr',"top":"pt", "bottom":"pb"}[position]);
            element.addClass(resize_class)
            if(resize_node.css("position")!=('relative'||'absolute')){
                resize_node.css('position','relative')
            }
            var window_width = angular.element(window).width();

            var start_x, start_y ;
            
            element.on('mousedown', function(event) {
                init_width = resize_node.width();
                init_height = resize_node.height();
                event.preventDefault();
                start_x = event.pageX;
                start_y = event.pageY;
                $document.on('mousemove', mousemove);
                $document.on('mouseup', mouseup);
                scope.callbackstart()
            });

            function mousemove(event) {
                $document.find('body').addClass(_.startsWith(h_or_v,'h')?'e-resize':'s-resize');
                if (_.startsWith(h_or_v,'h')) {
                    // Handle h resizer
                    scope.offset_x = event.pageX - start_x;
                    if(_.startsWith(position,'r')){
                        if(max_offset<0 || scope.offset_x<max_offset){
                            if(is_double){
                                scope.offset_x*=2;
                            }
                            resize_node.css({
                                width:(init_width+scope.offset_x)+'px'
                            })
                        }
                    }
                    else{
                        if(max_offset<0||-scope.offset_x<max_offset){
                            if(is_double){
                                scope.offset_x*=2;
                            }
                            resize_node.css({
                                width: (init_width-scope.offset_x) +'px'
                            })
                        }
                    }

                }
                else{
                    scope.offset_y = event.pageY - start_y;
                    if(_.startsWith(position,"t")){
                        if(max_offset<0 || -scope.offset_y<max_offset){
                            if(is_double){
                                scope.offset_y*=2;
                            }
                            resize_node.css({
                                height: (init_height - scope.offset_y)+'px'
                            })
                        }
                    }
                }

                scope.callbackmoving({
                    offset_x : scope.offset_x,
                    offset_y : scope.offset_y
                });

            }

            function mouseup(event) {
                $document.find('body').removeClass('e-resize');
                $document.find('body').removeClass('s-resize');
                $document.unbind('mousemove', mousemove);
                $document.unbind('mouseup', mouseup);
                scope.callbackover({
                    offset_x : scope.offset_x,
                    offset_y : scope.offset_y
                });
            }

        }
    }
}])