#!/bin/sh

JAVA_HOME=../jdk1.8.0_111
MAVEN_HOME=../apache-maven-3.3.9
PATH=$JAVA_HOME/bin:$MAVEN_HOME/bin:$PATH
curl -X POST localhost:9999/shutdown
mvn clean package -f ./pom.xml
nohup java -jar target/dashboard-app-0.0.1-SNAPSHOT.jar
#> /dev/null 2>&1 &
