angular.module("dashboard.services").factory("initService", [
  'queryService',
  function(
    queryService
  ) {
    return {
      init_operation: function($scope) {
        queryService.xhr_widgets().success(function(data){
          $scope.widgets = data;
        })
      }
    };
  }
]);
