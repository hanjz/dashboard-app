angular.module("dashboard.services").config(['$httpProvider',function(a){
    //a.defaults.headers.put['Content-Type'] = 'application/json'
    a.defaults.xsrfCookieName = 'csrftoken';
    a.defaults.xsrfHeaderName = 'X-CSRFToken';
    // /post_wrapper(a);
}])
.factory("queryService",["$rootScope","$http", function($rootScope, $http){
    var cleanUp = function (obj) {
      var ret = {};
      var keys = Object.keys(obj);
      keys.forEach(function (key) {
        if (!key.match(/^(?:_|\$)/)) {
          ret[key] = obj[key];
        }
      });
      return ret;
    };

    var dashboards = function(){
        return $http({
            url: '/api/dashboard/list'
        })
    }

    var widgets = function(){
        return $http({
            url: '/api/widgets/'
        })
    }
    var widget = function(id){
        return $http({
            url: '/api/widgets/'+id
        })
    }
    var active_dashboard = function(dashboard){
        return $http({
            url:'/api/account/mysession/',
            method:'post',
            data: {
                active_dashboard:dashboard.uid
            }
        })
    }

    var current_dashboard = function(){
        return $http({
            url:'/api/account/mysession/'
        })
    }
    var add_dashboard = function(dashboard){
        return $http({
            url:'/api/dashboard/create/',
            method:'post',
            data:{
                title: dashboard.title,
                grid_cols:dashboard.grid_cols
            }
        })
    }
    var update_dashboard = function(dashboard){
        return $http({
            url:'/api/dashboard/'+dashboard.uid+"/?detail=false",
            method:'post',
            data: cleanUp(dashboard)
        })
    }

    var remove_dashboard = function(dashboard){
        return $http({
            url:'/api/v1/dashboard/'+dashboard.id+"/",
            method:'delete',
            data: cleanUp(dashboard)
        })
    }
    var update_widget = function(widget){
        return $http({
            url:'/api/widget/'+widget.uid+"/?detail=false",
            method:'post',
            data:
                {
                    "width":widget.sizeX,
                    "height":widget.sizeY,
                    "coord_x":widget.row,
                    "coord_y":widget.col,
                    "title":widget.title,
                    "name":widget.name
                }

        })
    }
    var update_widget_verbatim = function(widget) {
        return $http({
            url:'/api/widget/'+widget.uid+"/?detail=false",
            method:'post',
            data: cleanUp(widget)
        })
    }

    var update_widgets = function(widgets){
        for(var i = 0 ; i<widgets.length; i++){
            update_widget(widgets[i]);
        }
    }
    var dashboard_pages = function(){
        return $http({
            url:'/api/v1/dashboard/?user='+$rootScope.current_user_id
        });
    };


    var add_widget = function(widget_data){
        return $http({
            url:'/api/widget/create/',
            method:'post',
            data: JSON.stringify(widget_data),
            headers: {
                'Content-Type': 'application/json'
            }
        })

    }


    var add_widget_to_dashboard = function(dashboard, widget){
        return $http({
            url:'/api/dashboard/'+dashboard.uid+"/add_widget/"+widget.uid+"/",
            method:'post'
        })
    }


    var fetch_data = function(uid){
        return $http({
            url:'/api/datasources/'+uid+'/data'
        })
    }



    var fetch_expr = function(expr, uid){
        var request_param="";
        if(expr.column_start_index!=null){
            request_param+=("&column_start_index="+expr.column_start_index);
        }
        if(expr.column_end_index!=null){
            request_param+=("&column_end_index="+expr.column_end_index);
        }
        if(expr.row_start_index!=null){
            request_param+=('&row_start_index='+expr.row_start_index);
        }
        if(expr.row_end_index!=null){
            request_param+=('&row_end_index='+expr.row_end_index);
        }
        return $http({
            url:'/api/datasources/'+uid+'/data/?flat=true'+request_param
        })//column_start_index=1&column_end_index=10&row_start_index=M&row_end_index=N
    }

    var fetch_expr_interval = function(expr, uid, interval, callback){
        var request_param="";
        if(expr.column_start_index!=null){
            request_param+=("&column_start_index="+expr.column_start_index);
        }
        if(expr.column_end_index!=null){
            request_param+=("&column_end_index="+expr.column_end_index);
        }
        if(expr.row_start_index!=null){
            request_param+=('&row_start_index='+expr.row_start_index);
        }
        if(expr.row_end_index!=null){
            request_param+=('&row_end_index='+expr.row_end_index);
        }


        $http({
            url:'/api/datasources/'+uid+'/data/?flat=true'+request_param
        }).success(function(data){
            callback(data)
        })

        setInterval(function(){
            $http({
                url:'/api/datasources/'+uid+'/data/?flat=true'+request_param
            }).success(function(data){
                callback(data)
            })
        }, interval)
    }

    var fetch_expr_both = function(expr, uid, interval, callback){
        if(interval){
            fetch_expr_interval(expr, uid, interval, function(data){
                callback(data)
            });
        }
        else {
            fetch_expr(expr, uid).success(function (data) {
                callback.call(null, data);
            })
        }
    }

    var next_widget = function(){
        return $http({
                url:'/api/widgets/next'
        })
    }

    var first_widget = function(){
        return $http({
            url:'/api/widgets/first'
        })
    }

    var last_widget = function(){
        return $http({
            url:'/api/widgets/last'
        })
    }

    var prev_widget = function(){
        return $http({
            url:'/api/widgets/prev'
        })
    }


    return {

        xhr_dashboards: function(){
            return dashboards();
        },

        xhr_widgets : function(){
            return widgets();
        },
        xhr_widget: function(id){
            return widget(id);
        },
        xhr_active_dashboard: function(dashboard){
            return active_dashboard(dashboard)
        },
        xhr_current_dashboard: function(){
            return current_dashboard()
        },
        xhr_add_dashboard: function(dashboard){
            return add_dashboard(dashboard);
        },
        xhr_add_widget: function(widget){
            return add_widget(widget)
        },
        xhr_update_widget: function(widget){
            return update_widget(widget);
        },
        xhr_update_widget_verbatim: function (widget) {
          return update_widget_verbatim(widget);
        },
        xhr_update_widgets: function(dashboard){
            return update_widgets(dashboard);
        },
        xhr_update_dashboard: function(dashboard){
            return update_dashboard(dashboard);
        },
        xhr_remove_dashboard: function(dashboard){
            return remove_dashboard(dashboard);
        },
        xhr_add_widget_to_dashboard: function(dashboard,widget){
            return add_widget_to_dashboard(dashboard,widget)
        },
        xhr_fetch_data : function(uid){
            return fetch_data(uid)
        },
        xhr_fetch_expr : function(expr,uid){
            return fetch_expr(expr, uid);
        },
        xhr_fetch_expr_interval: function(expr, uid, interval, callback){
            return fetch_expr_interval(expr, uid, interval, callback);
        },
        xhr_fetch_expr_both: function(expr, uid, interval, callback){
            return fetch_expr_both(expr, uid, interval, callback);
        },
        xhr_next_widget: function(){
            return next_widget();
        },
        xhr_prev_widget: function(){
            return prev_widget();
        },
        xhr_first_widget: function(){
            return first_widget();
        },
        xhr_last_widget: function(){
            return last_widget();
        }
    };
}]) ;
