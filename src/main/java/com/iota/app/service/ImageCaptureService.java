package com.iota.app.service;

import org.openqa.selenium.*;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;

import javax.annotation.Resource;
import javax.servlet.http.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by guoxu.wu on 16/7/8.
 */

/**
 * 上传图片
 * @author admin
 *
 */
@Service
public class ImageCaptureService {
	public static final String IMGS_PATH_PREFIX="imgs/";
	public static final int canvasHeight = 270;
	public static final int canvasWidth = 480;
    @Resource
    private FtpService ftpService;
    @Resource
    private ValueService valueService;
    public byte[] genImage(String servername, int serverPort, String id){
    	String localPath = "http://localhost:"+serverPort+"/widget/html/"+id;
        WebDriver driver = new PhantomJSDriver();
        driver.manage().window().setSize(new Dimension(480, 270));
        driver.get(localPath);
       try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        new WebDriverWait(driver, 500).until(new ExpectedCondition<Boolean>(){
            @Override
            public Boolean apply(WebDriver driver) {
                // TODO Auto-generated method stub
                JavascriptExecutor js = (JavascriptExecutor) driver;
                return (Boolean)js.executeScript("return $('.widget').data('ready')");
            }
        });


        byte[] imageData = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        driver.quit();
        return imageData;
    }
    public String capture(HttpServletRequest request, String id) throws IOException{
    	return capture(request, id, canvasWidth, canvasHeight);
    }
    
    
    
    /**
     * 根据WIDGET的id获取图像
     * @param request
     * @param id
     * @param width
     * @param height
     * @return
     * @throws IOException
     */
    public String capture(HttpServletRequest request, String id, int width, int height) throws IOException {
        Integer serverPort;

        if(valueService.getServerPort()!=null){
            serverPort = valueService.getServerPort();
        }
        else{
            serverPort = 9999;
        }
        String localPath = "http://localhost:"+serverPort+"/widget/html/"+id;
        System.out.println("localPath"+localPath);
        WebDriver driver = new PhantomJSDriver();
        driver.manage().window().setSize(new Dimension(width, height));
        driver.get(localPath);
        String filePath ="";
        try {
            new WebDriverWait(driver, 500).until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver driver) {
                    // TODO Auto-generated method stub
                    JavascriptExecutor js = (JavascriptExecutor) driver;
                    return (Boolean) js.executeScript("return $('.widget').data('ready')");
                }
            });

            byte[] imageData = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            driver.quit();
            InputStream fis = new ByteArrayInputStream(imageData);
            filePath = IMGS_PATH_PREFIX + id + ".png";
            System.out.println("filePath==="+filePath);
            try {
				ftpService.sendFile(filePath, fis);
			} catch (Exception e) {
				 System.out.println("filePathERROR......................");
				e.printStackTrace();
			}
            return filePath;
        }catch(Exception e){
            e.printStackTrace();
            driver.quit();
            return "";
        }
    }

    public void getImage(String id, OutputStream os) throws IOException {
       ftpService.getFile(id, os);
    }

}
