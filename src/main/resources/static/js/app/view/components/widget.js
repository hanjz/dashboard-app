angular.module( 'dashboard.components')
.directive('widget', [function(){

    return {
        restrict:'AE',
        replace:true,
        controller: function($scope){

        },
        template:"<div class=''><ul>"+
        	"<li ng-repeat='c in component.components'>"+
            "<x-inline-component component='c'></x-inline-component>"+
            "</li></ul></div>"+
        "</ul>",
        link: function(scope, element, attrs){
        },
        scope: {
            component:"=",
        }

    }
}])
.directive('canvas', ['$rootScope','$compile','$timeout', '$document',function($rootScope,$compile, $timeout, $document){
    return {
        restrict:'AE',
        replace:true,
        scope: {
            component:"=",
        },
        controller:function($scope, $element){
            var zoom_scale = 1.2
            var container = $element.find('.widget-canvas-container');
            var inner = $element.find('.widget-canvas-inner');
            if($scope.component.background!=null && $scope.component.background!=""){
                if($rootScope.is_snapshot) {
                    $scope.widget_bg = $scope.component.localBackground;
                }
                else{
                    $scope.widget_bg = $scope.component.background;
                }
            }
            $element.find('.widget-bg').load(function(){
                inner.height($(this).height())
            })

            $scope.zoomin = function(){
                //var width = inner.width()+zoom_scale;
                //var height = inner.height+zoom_scale;
                inner.width(inner.width()*zoom_scale);
                inner.height(inner.height()*zoom_scale);
                container.scrollTop(container.scrollTop()*zoom_scale);
                container.scrollLeft(container.scrollLeft()*zoom_scale);
            }

            $scope.zoomout = function(){
                inner.width(inner.width()/zoom_scale);
                inner.height(inner.height()/zoom_scale);
            }

        },
        template:"<div class='widget-canvas-content'>"+
                    "<div class='widget-canvas-container'>"+
                        "<div class='widget-canvas-inner'>"+
                            "<ul>"+
                                "<li ng-repeat='c in component.components' style='top:{{c.top}};left:{{c.left}};width:{{c.width}};height:{{c.height}}'>"+
                                    "<x-inline-component component='c'></x-inline-component>"+
                                "</li>"+
                            "</ul>"+
                            "<img class='widget-bg' ng-src='{{widget_bg?widget_bg:\"/img/transparent.png\"}}' width='100%'/>"+
                        "</div>"+
                    "</div>"+
        "<ul class='pan-navigator'>"+
        "<li><a href='#' ng-click='zoomin()'><span class='glyphicon glyphicon-plus'></span></a></li>"+
        "<li><a href='#' ng-click='zoomout()'><span class='glyphicon glyphicon-minus'></span></a></li>"+
        "</ul>"+
                "</div>",

        link:function(scope,element,attrs){


            scope.$watch('component.position', function(p, op){
                scope.$broadcast('component-reflow');
            })

            var container = element.find('.widget-canvas-container')
            var startx, starty;
            var start_scroll_x = container.scrollLeft();
            var start_scroll_y = container.scrollTop();
            var mousemove = function(event){
                var moving_x = event.pageX;
                var moving_y = event.pageY;
                var movex = moving_x - startx;
                var movey = moving_y - starty;

                console.log(start_scroll_y-movey);
                container.scrollTop(start_scroll_y-movey);
                container.scrollLeft(start_scroll_x-movex);
            }

            var mouseup = function(){
                $document.unbind('mousemove', mousemove);
                $document.unbind('mouseup', mouseup);
                container.unbind('mousemove', mousemove);
            }


            container.on('mousedown', function(event){
                event.stopPropagation();
                startx = event.pageX;
                starty = event.pageY;

                start_scroll_y = container.scrollTop();
                start_scroll_x = container.scrollLeft();
                container.on('mousemove', mousemove)
                $document.on('mouseup', mouseup);
            })

        }
    }
}])
.directive('inlineComponent',['$compile', function($compile){
    return {
        restrict:'E',
        replace:true,
        link: function(scope, element, attrs){
            scope.component.extra = scope.component.extra||{}
            var type = scope.component.type;
            var parent = element.parent();
            if(type!==null){
                var type_name = type.replace(/[_]+/,'')+"_component"
                parent.append($compile(angular.element("<"+type_name+"></"+type_name+">"))(scope))
                element.remove();
            }
        },
        scope: {
            component:"="
        }
    }
}])
